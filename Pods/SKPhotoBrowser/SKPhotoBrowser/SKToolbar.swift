//
//  SKToolbar.swift
//  SKPhotoBrowser
//
//  Created by keishi_suzuki on 2017/12/20.
//  Copyright © 2017年 suzuki_keishi. All rights reserved.
//

import Foundation

// helpers which often used
private let bundle = Bundle(for: SKPhotoBrowser.self)

class SKToolbar: UIToolbar {
    var toolActionButton: UIBarButtonItem!
    func createLikeButton()->UIBarButtonItem{
        let upButton = UIButton(type: .custom)
        var image: UIImage = UIImage(named: "icon_heart_unliked")!
        if (SKToolbarOptions.is_i_liked == 1){
            image = UIImage(named: "icon_heart_liked")!
        }
        upButton.setImage(image, for: .normal)
        upButton.setTitleColor(UIColor.lightText, for: .normal)
        upButton.setTitle("\(SKToolbarOptions.like_count)", for: .normal)
        upButton.titleLabel?.font =  UIFont.systemFont(ofSize: 14)
        upButton.frame = CGRect(x: 0, y: 0, width: 70, height: 30)
        upButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 14.0, bottom: 0.0, right: 0.0)
        upButton.addTarget(browser, action: #selector((SKPhotoBrowser.actionLikePressed)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: upButton)
        return item
    }
    func createCommentButton()->UIBarButtonItem{
        let upButton = UIButton(type: .custom)
        let image: UIImage = UIImage(named: "icon_g_comments")!
        upButton.setImage(image, for: .normal)
        upButton.setTitleColor(UIColor.lightText, for: .normal)
        upButton.setTitle("\(SKToolbarOptions.comment_count)", for: .normal)
        upButton.titleLabel?.font =  UIFont.systemFont(ofSize: 14)
        upButton.frame = CGRect(x: 0, y: 0, width: 70, height: 30)
        upButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 14.0, bottom: 0.0, right: 0.0)
        upButton.addTarget(browser, action: #selector((SKPhotoBrowser.actionCommentPressed)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: upButton)
        return item
    }
    func createResendButton()->UIBarButtonItem{
        let upButton = UIButton(type: .custom)
        let image: UIImage = UIImage(named: "icon_g_resend")!
        upButton.setImage(image, for: .normal)
        upButton.setTitleColor(UIColor.lightText, for: .normal)
        upButton.setTitle("\(SKToolbarOptions.resend_count)", for: .normal)
        upButton.titleLabel?.font =  UIFont.systemFont(ofSize: 14)
        upButton.frame = CGRect(x: 0, y: 0, width: 70, height: 30)
        upButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 14.0, bottom: 0.0, right: 0.0)
        upButton.addTarget(browser, action: #selector((SKPhotoBrowser.actionResendPressed)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: upButton)
        return item
    }
    func createViewButton()->UIBarButtonItem{
        let upButton = UIButton(type: .custom)
        let image: UIImage = UIImage(named: "icon_g_views")!
        upButton.setImage(image, for: .normal)
        upButton.setTitleColor(UIColor.lightText, for: .normal)
        upButton.setTitle("\(SKToolbarOptions.view_count)", for: .normal)
        upButton.titleLabel?.font =  UIFont.systemFont(ofSize: 14)
        upButton.frame = CGRect(x: 0, y: 0, width: 70, height: 30)
        upButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 14.0, bottom: 0.0, right: 0.0)
        upButton.addTarget(browser, action: #selector((SKPhotoBrowser.actionViewPressed)), for: .touchUpInside)
        let item = UIBarButtonItem(customView: upButton)
        return item
    }
    fileprivate weak var browser: SKPhotoBrowser?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(frame: CGRect, browser: SKPhotoBrowser) {
        self.init(frame: frame)
        self.browser = browser
        
        setupApperance()
        setupToolbar()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if SKPhotoBrowserOptions.displayGallery{
            if let view = super.hitTest(point, with: event) {
                return view
            }
            return nil
        }
        else {
            if let view = super.hitTest(point, with: event) {
                if SKMesurement.screenWidth - point.x < 50 { // FIXME: not good idea
                    return view
                }
            }
            return nil
            
        }
    }
}

private extension SKToolbar {
    func setupApperance() {
        backgroundColor = .clear
        clipsToBounds = true
        isTranslucent = true
        setBackgroundImage(UIImage(), forToolbarPosition: .any, barMetrics: .default)
    }
    
        func setupToolbar() {
        toolActionButton = UIBarButtonItem(barButtonSystemItem: .action, target: browser, action: #selector(SKPhotoBrowser.actionButtonPressed))
        toolActionButton.tintColor = UIColor.white
        
        var items = [UIBarButtonItem]()
        if SKPhotoBrowserOptions.displayGallery{
            
            items.append(createLikeButton())
            items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil))
            items.append(createCommentButton())
            items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil))
            items.append(createResendButton())
            items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil))
            items.append(createViewButton())
        }
        if SKPhotoBrowserOptions.displayAction {
            items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil))
            items.append(toolActionButton)
        }
        setItems(items, animated: false)
        
    }
    
    func setupActionButton() {
    }
}

