//
//  Languageontroller.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/13/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit

class Languageontroller: NSObject {

    // MARK: - Properties

    // MARK: -

    // Initialization

    // MARK: - Accessors

    class func languageType() -> Int {
        return UserDefaults.standard.integer(forKey: "lang")
    }

}
