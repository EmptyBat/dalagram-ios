//
//  AppManager.swift
//  Dalagram
//
//  Created by Toremurat on 11.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation

class AppManager {
    
    static let baseUrl = "https://dalagram.com"
    static let loadDialogsNotification = NSNotification.Name("loadDialogsNotification")
    static let diloagHistoryNotification = NSNotification.Name("diloagHistoryNotification")
    static let dialogDetailsNotification = NSNotification.Name("dialogDetailsNotification")
    static let selectNotification = NSNotification.Name("SELECTING")
    static let selectionCanceled = NSNotification.Name("selectionCanceled")
    static let is_readNotification = NSNotification.Name("is_readNotification")
    
}
