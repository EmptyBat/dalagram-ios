//
//  APITarget.swift
//  Dalagram
//
//  Created by Toremurat on 07.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import Moya

public enum Dalagram {
    
    // MARK: - Auth
    case signUp(phone: String)
    case signIn(phone: String,password:String)
    case confirmAccount(phone:String, code: String)
    case resetPassword(email: String)
    
    // MARK: - Profile
    case getProfile(user_id: Int?)
    case updateProfile([String: String])
    case editProfile([String: String])
    case uploadAvatar(Data)
    case uploadBackground(Data)
    
    // MARK: - Contacts
    case addContacts([[String: String]])
    case getContacts()
    case getFriends([String: Any])
    case getFollowers()
    case removeContact(user_id: Int)
    
    // MARK: - Chat
    case getPageByChatId([String: Any])
    case sendMessage([String: Any])
    case sendFileMessage(params: [String: Any], files: [[String: String]])
    case getDialogs([String: Any])
    case getDialogDetails([String: Any])
    case removeChat([String: Any])
    case uploadChatFile(Any, format: String, fileExtension: String, fileName: String)
    case blockUser([String: Any])
    case getMediaFiles([String: Any])
    case addBookmark([String: Any])
    case muteDialog([String: Any])
    case resendMessage(chat_id: [Int],user_id:String)
    // MARK: - Group
    case createGroup(name: String, users: [[String: Int]], image: Data?)
    case uploadGroupPhoto(group_id: Int, image: Data)
    case editGroup(group_id: Int, group_name: String)
    case getGroups([String: String])
    case getGroupDetails(group_id: Int)
    case getGroupMembers([String: String])
    case addGroupMember(group_id: Int, user_id: Int)
    case removeGroupMember(group_id: Int, user_id: Int)
    case setGroupAdmin(group_id: Int, user_id: Int)
    case declineGroupAdmin(group_id: Int, user_id: Int)
    
    // MARK: - Channel
    case createChannel(name: String, login: String, users: [[String: Int]], image: Data?)
    case uploadChannelPhoto(channel_id: Int, image: Data)
    case editChannel(channel_id: Int, name: String, login: String)
    case getChannels([String: String])
    case getChannelDetails(channel_id: Int)
    case getChannelMembers(channel_id: Int, page: Int, count: Int)
    case addChannelMember(channel_id: Int, user_id: Int)
    case removeChannelMember(channel_id: Int, user_id: Int)
    case setChannelAdmin(channel_id: Int, user_id: Int)
    case declineChannelAdmin(channel_id: Int, user_id: Int)
    
    // MARK: - News
    case getPublicationDetail(publication_id: Int)
    case removeNews(news_id: Int)
    case getNews([String: Any])
    case addComment([String: Any])
    case uploadCommentFile(Any, format: String, fileExtension: String, fileName: String)
    case sendFileComment(params: [String: Any], files: [[String: String]])
    case getComments([String: Any])
    case getGallery([String: Any])
    case likeNews([String: Any])
    case addNews([String: Any])
    case addFileNews(params: [String: Any], files: [[String: String]])
    case editNews(news_id: Int, [String: String])
    case uploadPhoto(image: Data)
    case removePhoto(imageUrl: String)
    
    // MARK: Friends Request
    case sendFriendRequest([String: Any])
    case acceptFriendRequest([String: Any])
    case rejectFriendRequest([String: Any])
    case inComingFriends()
    case outComingFriends()
    case blockedUsers()
    
    // MARK: Push
    case addPushToken([String: String])
    case removePushToken([String: String])
    case sendPushNotification([String: String])
    
    // MARK: Users
    case getUser([String: Any])
    case getSpeciality()
    
    // MARK: getNotifications
    case getNotification([String: Any])
    
    //MARK: report
    case sendReport([String: Any])
    
    //MARK:Banner
    case getBanner([String: Any])
    
    //MARK:Projects
    case getProjects([String: Any])
    
    //MARK:cities
    case getCity()
}

extension Dalagram: TargetType {
    
    public var baseURL: URL { return URL(string: "https://api.dalagram.com/api")! }
    
    public var path: String {
        
        switch self {
        
        // MARK: - Auth Path
        case .signUp:
            return "/auth/login"
        case .signIn:
            return "/auth/login/password"
        case .confirmAccount:
            return "/auth/confirm"
        case .resetPassword:
            return "/auth/reset"
            
        // MARK: - Profile Path
        case .getProfile:
            return "/profile"
        case .updateProfile:
            return "/profile"
        case .editProfile:
            return "/profile"
        case .uploadAvatar:
            return "/profile/avatar"
        case .uploadBackground:
            return "/profile/background"
        // MARK: - Contacts Path
        case .getContacts, .addContacts, .removeContact:
            return "/contact"
        case .getFollowers:
            return "/follower"
        case .getFriends:
            return "/follower/friend"
        // MARK: - Chat
        case .getPageByChatId:
            return "/chat/page"
        case .sendMessage, .getDialogs:
            return "/chat"
        case .sendFileMessage:
            return "/chat"
        case .getDialogDetails:
            return "/chat/detail"
        case .removeChat:
            return "/chat"
        case .uploadChatFile:
            return "/chat/file"
        case .blockUser:
            return "/chat/block"
        case .getMediaFiles:
            return "/chat/media"
        case .addBookmark:
            return "/chat/bookmark"
        case .muteDialog:
            return "/chat/mute"
        case .resendMessage:
            return "/chat/resend"
        // MARK: - Group
        case .createGroup, .getGroups:
            return "/group"
        case .uploadGroupPhoto:
            return  "/group/avatar"
        case .editGroup(let group_id, _):
            return "/group/\(group_id)"
        case .getGroupDetails(let group_id):
            return "/group/\(group_id)"
        case .getGroupMembers(let group_id):
            return "/group/user/\(group_id)"
        case .addGroupMember(let group_id, _):
            return "/group/user/\(group_id)"
        case .removeGroupMember(let group_id, _):
            return "/group/user/\(group_id)"
        case .setGroupAdmin(let group_id, _):
            return "/group/admin/\(group_id)"
        case .declineGroupAdmin(let group_id, _):
            return "/group/admin/\(group_id)"
            
        // MARK: - Channel
        case .createChannel, .getChannels:
            return "/channel"
        case .uploadChannelPhoto:
            return "/channel/avatar"
        case .editChannel(let channel_id, _, _):
            return "/channel/\(channel_id)"
        case .getChannelDetails(let channel_id):
            return "/channel/\(channel_id)"
        case .getChannelMembers(let channel_id, _, _):
            return "/channel/user/\(channel_id)"
        case .addChannelMember(let channel_id, _):
            return "/channel/user/\(channel_id)"
        case .removeChannelMember(let channel_id, _):
            return "/channel/user/\(channel_id)"
        case .setChannelAdmin(let channel_id, _):
            return "/channel/admin/\(channel_id)"
        case .declineChannelAdmin(let channel_id, _):
            return "/channel/admin/\(channel_id)"
            
        // MARK: - News
        case .getPublicationDetail(let publication_id):
            return "/publication/\(publication_id)"
        case .getNews, .addNews,.addFileNews:
            return "/publication"
        case .likeNews:
            return "/like"
        case .addComment,.getComments,.sendFileComment:
             return "/comment"
        case .editNews(let news_id, _):
            return "/news/\(news_id)"
        case .uploadPhoto, .removePhoto:
            return "/image"
        case .uploadCommentFile:
            return "/comment/file"
        case .getGallery:
            return "/publication/gallery"
        case .removeNews:
            return "/publication"
        // MARK: Friends Request
        case .sendFriendRequest:
            return "/follower/send"
        case .acceptFriendRequest:
            return "/follower/accept"
        case .rejectFriendRequest:
            return "/follower/reject?"
        case .inComingFriends:
            return "/follower/in"
        case .outComingFriends:
            return "/follower/out"
        case .blockedUsers:
            return "/block"
        // MARK: Push
        case .addPushToken, .removePushToken:
            return "/push"
        case .sendPushNotification:
            return "/push/send"
        // MARK: Users
        case .getUser:
            return "/users"
        case .getSpeciality:
            return "/speciality"
        // MARK: Notifications
        case .getNotification:
            return "/notification"
        case.sendReport:
            return "/complaint"
        case.getBanner:
            return "/banner"
        case.getProjects:
            return "/project"
        case.getCity:
            return "/city"
            
        }
        
    }
    public var method: Moya.Method {
        switch self {
        case .getProfile, .getContacts,.getFriends,.getFollowers, .getDialogs, .getDialogDetails, .getGroups, .getGroupDetails,
             .getGroupMembers, .getMediaFiles, .getChannels, .getChannelDetails, .getChannelMembers, .getNews,.getComments,.getPageByChatId,.getNotification,.getGallery,.inComingFriends,.outComingFriends,.blockedUsers,.getUser,.getPublicationDetail,.getSpeciality,.getBanner,.getProjects,.getCity:
            return .get
        case .removePushToken, .removeGroupMember, .declineGroupAdmin, .removeChannelMember,
             .declineChannelAdmin, .removePhoto, .removeChat,.removeNews:
            return .delete
        default:
            return .post
        }
    }
    
    public var headers: [String : String]? {
        return nil
    }
    
    public var task: Task {
        switch self {
        
        // MARK: - Auth Params
            
        case .signIn(let phone , let password):
            return .requestParameters(parameters: ["phone" : phone, "password":password], encoding: URLEncoding.default)
            
        case .signUp(let phone):
            return .requestParameters(parameters: ["phone" : phone], encoding: URLEncoding.default)
            
        case .confirmAccount(let phone, let code):
            return .requestParameters(parameters: ["phone" : phone, "sms_code": code], encoding: URLEncoding.default)
            
        case .resetPassword(let email):
            return .requestParameters(parameters: ["email" : email], encoding: URLEncoding.default)
        //push
        case .addPushToken(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,
                                               urlParameters: ["token" : User.getToken()])
        // MARK: - Profile Params
            
        case .updateProfile(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,
                                               urlParameters: ["token" : User.getToken()])
            
        case .getProfile(let user_id):
            var params = ["token" : User.getToken()]
            if let id = user_id {
                params["user_id"] = "\(id)"
            }
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            
        case .editProfile(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,
                                               urlParameters: ["token" : User.getToken()])
            
        case .uploadAvatar(let data):
            let imgData = MultipartFormData(provider: .data(data), name: "image", fileName: "photo.jpg", mimeType: "image/jpeg")
            return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
        case .uploadBackground(let data):
            let imgData = MultipartFormData(provider: .data(data), name: "image", fileName: "photo.jpg", mimeType: "image/jpeg")
            return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
        // MARK: - Contacts Params
            
        case .addContacts(let jsonParams):
            return .requestCompositeParameters(bodyParameters: ["contact_users" : jsonParams], bodyEncoding: JSONEncoding.default, urlParameters: ["token" : User.getToken()])
            
        case .getContacts():
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
        case .getFollowers():
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
        case .getFriends(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .inComingFriends():
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
        case .outComingFriends():
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
        case .blockedUsers():
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
        // MARK: Chat
        case .getPageByChatId(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .sendMessage(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,
                                               urlParameters: ["token" : User.getToken()])
        case .resendMessage(let chat_id,let user_id):
            return .requestCompositeParameters(bodyParameters: ["recipient_user_id":user_id,"chat_ids":chat_id], bodyEncoding: URLEncoding.httpBody,
                                               urlParameters: ["token" : User.getToken()])
        
        case .sendFileMessage(let params, let files):
            var parameters = params
            parameters["file_list"] = files
            return .requestCompositeParameters(bodyParameters: parameters, bodyEncoding: JSONEncoding.default, urlParameters: ["token" : User.getToken()])
            
        case .getDialogs(let params), .getDialogDetails(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        
        case .getMediaFiles(let params):
            var parameters = params
            parameters["token"] = User.getToken()
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)

        case .blockUser(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,
                                               urlParameters: ["token" : User.getToken()])
            
        case .removeChat(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody, urlParameters: ["token" : User.getToken()])
        case .removeNews(let params):
            return .requestParameters(parameters:["publication_id":params,"token" : User.getToken()], encoding: URLEncoding.default)
            
        case .addBookmark(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
            
        case .muteDialog(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
            
        case .uploadChatFile(let file, let format, let fileExtension, let fileName):
            switch format {
            case "image":
                if let image = file as? Data {
                    let imgData = MultipartFormData(provider: .data(image), name: "file", fileName: "photo.jpg", mimeType: "jpeg")
                    return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
                }
            case "document":
                if let file = file as? Data {
                    let imgData = MultipartFormData(provider: .data(file), name: "file", fileName: fileName, mimeType: fileExtension)
                    return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
                }
            case "video":
                if let video = file as? Data {
                    let imgData = MultipartFormData(provider: .data(video), name: "file", fileName: "video.mp4", mimeType: "mp4")
                    return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
                }
            case "audio":
                if let audio = file as? Data {
                    let imgData = MultipartFormData(provider: .data(audio), name: "file", fileName: "audio.mp3", mimeType: "mp3")
                    return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
                }
            default:
                return .requestPlain
            }
            return .requestPlain
            
        // MARK: Group
        case .createGroup(let groupName, let usersParams, let imageData):
            if let image = imageData {
                let imgData = MultipartFormData(provider: .data(image), name: "image", fileName: "photo.jpg", mimeType: "image/jpeg")
                return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken(),"group_name": groupName, "group_users": usersParams])
            } else {
                return .requestCompositeParameters(bodyParameters: ["group_name": groupName, "group_users": usersParams], bodyEncoding: JSONEncoding.default, urlParameters: ["token" : User.getToken()])
            }
            
        case .uploadGroupPhoto(let group_id, let imageData):
            let imgData = MultipartFormData(provider: .data(imageData), name: "image", fileName: "group_photo.jpg", mimeType: "image/jpeg")
            return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken(), "group_id": group_id])
            
        case .editGroup(_, let group_name):
            return .requestCompositeParameters(bodyParameters: ["group_name": group_name], bodyEncoding: URLEncoding.httpBody, urlParameters: ["token" : User.getToken()])
            
        case .getGroupDetails(_):
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
            
        case .setGroupAdmin(_, let user_id), .addGroupMember(_, let user_id):
            return .requestCompositeParameters(bodyParameters: ["user_id": user_id], bodyEncoding: URLEncoding.httpBody, urlParameters: ["token" : User.getToken()])
            
        case .removeGroupMember(let group_id, let user_id), .declineGroupAdmin(let group_id, let user_id):
            return .requestCompositeParameters(bodyParameters: ["group_id": group_id, "user_id": user_id], bodyEncoding: URLEncoding.httpBody, urlParameters: ["token" : User.getToken()])
            
        // MARK: Channel
            
        case .createChannel(let name, let login, let usersParams, let imageData):
            if let image = imageData {
                let imgData = MultipartFormData(provider: .data(image), name: "image", fileName: "photo.jpg", mimeType: "image/jpeg")
                return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken(),"channel_name": name, "channel_login": login, "channel_users": usersParams,"is_public":1])
            } else {
                return .requestCompositeParameters(bodyParameters: ["channel_name": name, "channel_login": login, "is_public": 1, "channel_users": usersParams], bodyEncoding: JSONEncoding.default, urlParameters: ["token" : User.getToken()])
            }
            
        case .uploadChannelPhoto(let channel_id, let imageData):
            let imgData = MultipartFormData(provider: .data(imageData), name: "image", fileName: "group_photo.jpg", mimeType: "image/jpeg")
            return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken(), "channel_id": channel_id])
        
        case .editChannel(_, let name, let login):
            return .requestCompositeParameters(bodyParameters: ["is_public": "0", "channel_name": name, "channel_login": login], bodyEncoding: URLEncoding.httpBody, urlParameters: ["token" : User.getToken()])
        
        case .getChannelDetails(_):
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
            
        case .getChannelMembers(_, let page, let count):
            return .requestParameters(parameters: ["token": User.getToken(), "page": page, "per_page": count], encoding: URLEncoding.default)
            
        case .addChannelMember(_, let user_id), .setChannelAdmin(_, let user_id):
            return .requestCompositeParameters(bodyParameters: ["user_id": user_id], bodyEncoding: URLEncoding.httpBody, urlParameters: ["token" : User.getToken()])
            
        case .removeChannelMember(_, let user_id), .declineChannelAdmin(_, let user_id):
            return .requestCompositeParameters(bodyParameters: ["user_id": user_id], bodyEncoding: URLEncoding.httpBody, urlParameters: ["token" : User.getToken()])
            // user notifications
        case .getNotification(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
            // get users
        case .getUser(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getSpeciality():
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
        case .getPublicationDetail(let params):
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
         // Mark news
        case .getGallery(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getNews(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .sendReport(let params):
            return  .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
        case .sendFriendRequest(let params):
            return  .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
        case .acceptFriendRequest(let params):
            return  .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
        case .rejectFriendRequest(let params):
            return  .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
        case .addComment(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
        case .sendFileComment(let params, let files):
            var parameters = params
            parameters["file_list"] = files
            return .requestCompositeParameters(bodyParameters: parameters, bodyEncoding: JSONEncoding.default, urlParameters: ["token" : User.getToken()])
        case .addNews(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
        case .addFileNews(let params, let files):
            var parameters = params
            parameters["file_list"] = files
            return .requestCompositeParameters(bodyParameters: parameters, bodyEncoding: JSONEncoding.default, urlParameters: ["token" : User.getToken()])
        case .getComments(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .likeNews(let params):
            return .requestCompositeParameters(bodyParameters: params, bodyEncoding: URLEncoding.httpBody,urlParameters: ["token" : User.getToken()])
            //another end points
        case .getProjects(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getBanner(let params):
            return .requestParameters(parameters: params, encoding: URLEncoding.default)
        case .getCity():
            return .requestParameters(parameters: ["token": User.getToken()], encoding: URLEncoding.default)
        case .uploadCommentFile(let file, let format, let fileExtension, let fileName):
            switch format {
            case "image":
                if let image = file as? Data {
                    let imgData = MultipartFormData(provider: .data(image), name: "file", fileName: "photo.jpg", mimeType: "jpeg")
                    return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
                }
            case "document":
                if let file = file as? Data {
                    let imgData = MultipartFormData(provider: .data(file), name: "file", fileName: fileName, mimeType: fileExtension)
                    return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
                }
            case "video":
                if let video = file as? Data {
                    let imgData = MultipartFormData(provider: .data(video), name: "file", fileName: "video.mp4", mimeType: "mp4")
                    return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
                }
            case "audio":
                if let audio = file as? Data {
                    let imgData = MultipartFormData(provider: .data(audio), name: "file", fileName: "audio.mp3", mimeType: "mp3")
                    return .uploadCompositeMultipart([imgData], urlParameters: ["token" : User.getToken()])
                }
            default:
                return .requestPlain
            }
            return .requestPlain
        default:
            return .requestPlain
        }
    }
        
    public var sampleData: Data {
        return "Default sample data".data(using: String.Encoding.utf8)!
    }
    
}

