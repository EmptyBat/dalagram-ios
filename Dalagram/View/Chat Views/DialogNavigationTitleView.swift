//
//  DialogNavigationTitleView.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/15/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import UIKit

class DialogNavigationTitleView: UIView {
    
    var userAvatarView: UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "placeholder"))
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()
    var dalagramLogoView: UIImageView = {
        let image = UIImageView(image: #imageLiteral(resourceName: "dalagram_logo"))
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        return image
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        userAvatarView.layer.cornerRadius = userAvatarView.frame.height/2
    }
    func setupViews() {
        
        addSubview(userAvatarView)
        addSubview(dalagramLogoView)
        
        userAvatarView.snp.makeConstraints { (make) in
            make.left.equalTo(0.0)
            make.top.equalTo(2.0)
            make.bottom.equalTo(-3.0)
            make.width.equalTo(userAvatarView.snp.height).multipliedBy(1/1)
        }
        
        dalagramLogoView.snp.makeConstraints { (make) in
            make.left.equalTo(userAvatarView.snp.right).offset(8.0)
            make.top.equalTo(8.0)
            make.right.equalTo(-10.0)
            make.bottom.equalTo(-6.0)
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
