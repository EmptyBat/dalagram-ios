
//
//  File.swift
//  Dalagram
//
//  Created by Toremurat on 29.08.2018.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import UIKit
import AsyncDisplayKit
import AVFoundation
import Alamofire
import RealmSwift
import RxSwift
import Kingfisher
enum PlayIconState {
    case play
    case pause
    case download
}

class AudioContentNode: ContentNode,AVAudioPlayerDelegate,ASTextNodeDelegate,ASNetworkImageNodeDelegate {
    //answer content
    open var incomingTextFont = UIFont.n1B1Font() {
        didSet {
            //    self.updateAttributedText()
        }
    }
    /** UIFont for outgoinf text messages*/
    open var outgoingTextFont = UIFont.n1B1Font() {
        didSet {
            //  self.updateAttributedText()
        }
    }
    /** UIColor for incoming text messages*/
    open var incomingTextColor = UIColor.n1DarkestGreyColor() {
        didSet {
            //    self.updateAttributedText()
        }
    }
    /** UIColor for outgoinf text messages*/
    open var outgoingTextColor = UIColor.black {
        didSet {
            //    self.updateAttributedText()
        }
    }
    open var insets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8) {
        didSet {
            setNeedsLayout()
        }
    }
    var ouputAnswerMessage: AnswerMessage?
    open fileprivate(set) var answerTextMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var nameMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var backgroundAnswerNode: ASDisplayNode = ASDisplayNode()
    fileprivate var is_has_image: Bool  = false
    fileprivate var AnswerMessage_id: Int = 0
    fileprivate var is_answer: Bool  = false
    fileprivate var AnswertextMessageString: String = ""
    fileprivate var senderTextMessageString: String = ""
    fileprivate var answerUrl: String = ""
    open fileprivate(set) var networAnswerkImageMessageNode:ASNetworkImageNode = ASNetworkImageNode()
    // MARK: Private Variables
    open var message_id: Int? {
        didSet {
            //      self.updateAttributedText()
        }
    }
    var RemoveOuput: RemoveMessageOutput?
    var ouput: TextContentNodeOutput?
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    var ouputfirstReply: TextContentFirstReply?
    var audioPlayer : AVAudioPlayer!
    var info: DialogInfo?
    var sliderNode : UISlider!
    var timer: Timer?
    var fileTime = 0.0
    var chat_id = ""
    var replaced_chat_id = ""
    var incommingMessage = false
    var viewModelProfile = ProfileViewModel()
    private var is_read = 0
    open fileprivate(set) var loaderNode = ASDisplayNode()
    open fileprivate(set) var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    fileprivate(set) var avatarIconNode : ASNetworkImageNode = ASNetworkImageNode()
    fileprivate(set) var playIconNode : ASImageNode = ASImageNode()
    fileprivate(set) var audioSlider: ASDisplayNode = ASDisplayNode()
    fileprivate(set) var audioTimeNode: ASTextNode = ASTextNode()
    fileprivate(set) var dateNode: ASTextNode = ASTextNode()
    open fileprivate(set) var resendBackground: ASButtonNode = ASButtonNode()
    fileprivate var is_selecting: Bool = false
    fileprivate var senderName: String = ""
    fileprivate var is_resend: Int = 0
    let disposeBag = DisposeBag()
    private var audioUrl: URL?
    private var avatarUrl = ""
    private var playIconState: PlayIconState = PlayIconState.play {
        didSet {
            switch playIconState {
            case .play:
                guard chat_id != "" else {
                    if (self.audioPlayer != nil){
                        if (self.audioPlayer.isPlaying){
                            self.audioPlayer.stop()
                        }
                    }
                    self.timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
                    self.audioPlayer = try! AVAudioPlayer(contentsOf:audioUrl!.absoluteURL)
                    self.audioPlayer.currentTime = TimeInterval(self.sliderNode.value)
                    self.audioPlayer.volume = 1
                    self.audioPlayer.prepareToPlay()
                    self.audioPlayer.delegate = self
                    self.audioPlayer.play()
                    self.playIconNode.image = UIImage(named: "icon_mini_pause")
                    return
                    
                }
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let existingItem = AudioContentNode.getCurrentObject(chat_id: Int(replaced_chat_id)!) {
                if let dirPath          = paths.first
                {
                    let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(existingItem.file_list[0].file_url).csv")
                    self.timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
                    if (self.audioPlayer != nil){
                        if (self.audioPlayer.isPlaying){
                             self.audioPlayer.stop()
                        }
                    }
                    if Int(self.replaced_chat_id) != 0{
                        
                    self.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                    }
                    else {
                        let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(UserDefaults.standard.integer(forKey: "music_count")).csv")
                        self.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                    }
                    self.audioPlayer.currentTime = TimeInterval(self.sliderNode.value)
                    self.audioPlayer.prepareToPlay()
                    self.audioPlayer.delegate = self
                    self.audioPlayer.play()
                    self.playIconNode.image = UIImage(named: "icon_mini_pause")
                }
                }
            case .pause:
                timer?.invalidate()
                self.audioPlayer.pause()
                playIconNode.image = UIImage(named: "icon_mini_play")
            case .download:
                self.playIconNode.image = UIImage(named: "icon_mini_play")
                self.playIconNode.isHidden = true
                downloadFileFromURL()
                activityIndicator.startAnimating()
                self.is_read  = 1
                self.sliderNode.setThumbImage(UIImage(named: "icon_slider_thumb"), for: .normal)
                self.sliderNode.tintColor = UIColor.darkBlueNavColor
            }
        }
    }
    static func getCurrentObject(chat_id: Int) -> DialogHistory? {
        let realm = try! Realm()
        let obj = realm.objects(DialogHistory.self).filter("chat_id = \(chat_id)")
        return obj.first
    }
    func downloadFileFromURL(){
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            var nextName = 1
            if  UserDefaults.standard.integer(forKey: "music_count") != 0 {
                nextName = UserDefaults.standard.integer(forKey: "music_count") + 1
                UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "music_count") + 1, forKey: "music_count")
            }
            else {
                UserDefaults.standard.set(UserDefaults.standard.integer(forKey: "music_count") + 1, forKey: "music_count")
            }
            documentsURL.appendPathComponent("\(nextName).csv")
            return (documentsURL, [.removePreviousFile])
        }
        AF.download(audioUrl!, to: destination).responseData { response in
            let realm = try! Realm()
            if let existingItem = AudioContentNode.getCurrentObject(chat_id: Int(self.replaced_chat_id)!) {

                if (existingItem.file_list[0].file_url.contains("https")){
                    self.audioUrl = NSURL.fileURL(withPath: "\(UserDefaults.standard.integer(forKey: "music_count"))")
                    try! realm.write {
                        let file = ChatFile()
                        file.file_url = "\(UserDefaults.standard.integer(forKey: "music_count"))"
                        file.file_data = existingItem.file_list[0].file_data
                        file.file_name = existingItem.file_list[0].file_name
                        file.file_format = existingItem.file_list[0].file_format
                        file.file_time = existingItem.file_list[0].file_time
                        existingItem.file_list[0] = file
                    }
                }
                else {
                    self.audioUrl = NSURL.fileURL(withPath: "\(UserDefaults.standard.integer(forKey: "music_count"))")
                }
            }
            self.activityIndicator.stopAnimating()
            self.playIconNode.isHidden = false
            self.playIconNode.image = UIImage(named: "icon_mini_play")
        }
        }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
      playIconNode.image = UIImage(named: "icon_mini_play")
    }
    // MARK: Initialisers
    @objc func updateSlider(){
        
        sliderNode.value = Float(audioPlayer.currentTime)
    }
    public init(id: Int,AnswerMessage_id:Int = 0,duration: String, date: String, currentVC: UIViewController,audioUrl: URL,chat_id :String, fileTime : String,info: DialogInfo? = nil,avatarUrl:String = "",incommingMessage:Bool = false,senderName:String,is_resend:Int = 0,is_read:Int = 0,textMessageString: String = "",senderTextMessageString: String = "",url:String = "",is_answer:Bool = false, bubbleConfiguration: BubbleConfigurationProtocol? = nil) {
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.audioUrl = audioUrl
        self.fileTime = Double(fileTime)!
        self.chat_id = chat_id
        self.avatarUrl = avatarUrl
        self.info = info
        self.incommingMessage = incommingMessage
        self.is_read = is_read
        self.currentViewController = currentVC
        if (is_answer == true){
            self.is_answer = is_answer
            self.AnswertextMessageString = textMessageString
            self.senderTextMessageString = senderTextMessageString
            self.answerUrl = url
            if let vc = currentViewController as? AnswerMessage {
                self.ouputAnswerMessage = vc
            }
            if (url != ""){
                is_has_image = true
            }
            self.AnswerMessage_id = AnswerMessage_id
        }
        self.setupAudioNode(duration: duration, date: date)
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.is_resend = is_resend
        self.message_id = id
        self.senderName = senderName
    }
    @objc func makeIs_read() {
        print("makeIs_read")
        self.is_read = 1
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    // MARK: Initialiser helper method
    fileprivate func setupAudioNode(duration: String, date: String) {
        if let url = audioUrl {
            if url.absoluteString.contains("https") {
            
                if (incommingMessage == true){
                    let origImage = UIImage(named: "icon_mini_download_green")
                    
                    playIconNode.image = origImage
                }
                else {
                    playIconNode.image = UIImage(named: "icon_mini_download")
                }
            }
            else {
                    playIconNode.image = UIImage(named: "icon_mini_play")
            }
        }
        //let gradientImage = info!.group_id != 0 ? #imageLiteral(resourceName: "bg_gradient_0") : info!.channel_id != 0 ? #imageLiteral(resourceName: "bg_gradient_3")  : #imageLiteral(resourceName: "bg_gradient_2")
       // avatarIconNode.defaultImage = gradientImage
        avatarIconNode = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
        avatarIconNode.shouldCacheImage = true
        avatarIconNode.shouldRenderProgressImages = true
        if (incommingMessage == false){
            let url = URL(string:avatarUrl.encodeUrl()!)
            avatarIconNode.url = url
            MakeCornerImage()
        }
        else {
            let url = URL(string:(info?.avatar.encodeUrl()!)!)
            avatarIconNode.url = url
            MakeCornerImage()
        }
        avatarIconNode.contentMode = .scaleAspectFill
        avatarIconNode.clipsToBounds = true
     //   avatarIconNode.image = UIImage(named: "icon_mini_play")
        replaceChatId()
        playIconNode.contentMode = .scaleAspectFit
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.n1Black50Color()]
        dateNode.attributedText = NSAttributedString(string: date, attributes: dateTextAttr)
        dateNode.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        let temp:Int = Int(fileTime)
            let mins:Int = temp / 60
            let secs:Int = temp % 60
        if (mins >= 10){
            if (secs >= 10){
                audioTimeNode.attributedText = NSAttributedString(string: "\(mins):\(secs)", attributes: dateTextAttr)
            }
            else {
                audioTimeNode.attributedText = NSAttributedString(string: "\(mins):0\(secs)", attributes: dateTextAttr)
            }
        }
        else {
            if (secs >= 10){
                audioTimeNode.attributedText = NSAttributedString(string: "0\(mins):\(secs)", attributes: dateTextAttr)
            }
            else {
                audioTimeNode.attributedText = NSAttributedString(string: "0\(mins):0\(secs)", attributes: dateTextAttr)
            }
        }
        audioTimeNode.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        
        DispatchQueue.main.async {
            self.sliderNode = UISlider(frame: CGRect.init(x: 5, y: 4, width: UIScreen.main.bounds.width/1.9, height: 20))
            if let url = self.audioUrl {
                if url.absoluteString.contains("https") && (self.incommingMessage == true){
                let origImage = UIImage(named: "icon_slider_thumb")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                self.sliderNode.setThumbImage(tintedImage, for: .normal)
                self.sliderNode.tintColor = UIColor(red: 70/255, green: 191/255, blue: 39/255, alpha: 1.0)
                }
                else {
                    self.sliderNode.setThumbImage(UIImage(named: "icon_slider_thumb"), for: .normal)
                    self.sliderNode.tintColor = UIColor.darkBlueNavColor
                }
                
            }
            let asset = AVURLAsset(url: self.audioUrl!, options: nil)
            let audioDuration = asset.duration
            _ = CMTimeGetSeconds(audioDuration)

            self.sliderNode.maximumValue = Float(self.fileTime)
            self.sliderNode.minimumValue = 0
            self.sliderNode.value = 0
            self.audioSlider.view.addSubview(self.sliderNode)
        }
        playIconNode.addTarget(self, action: #selector(playButtonPressed), forControlEvents: .touchUpInside)
        
        self.resendBackground.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        self.resendBackground.setImage(UIImage(named:"icon_resended"), for: .normal)
        self.resendBackground.setTitle("  Пересланное", with: UIFont.systemFont(ofSize: 13), with: UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0), for: .normal)
        self.resendBackground.contentHorizontalAlignment = .left
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = CGPoint(x: 12, y: 12)
        loaderNode.view.addSubview(activityIndicator)
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        
        if (is_answer){
            //answer Content Node
            backgroundAnswerNode.backgroundColor =  UIColor(red: 89/255.0, green: 198/255.0, blue: 75/255.0, alpha: 1.0)
            self.addSubnode(backgroundAnswerNode)
            let nameString = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(red: 54/255, green: 143/255, blue: 28/255, alpha: 1.0)]
            let fontAndSizeAndTextColor = [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
            let TextString = NSMutableAttributedString(string: AnswertextMessageString, attributes: fontAndSizeAndTextColor )
            self.nameMessageNode.delegate = self
            self.nameMessageNode.isUserInteractionEnabled = true
            self.nameMessageNode.attributedText = NSMutableAttributedString(string:  senderTextMessageString, attributes: nameString)
            self.nameMessageNode.accessibilityIdentifier = "labelNameAnswerMessage"
            self.nameMessageNode.isAccessibilityElement = true
            self.nameMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            self.answerTextMessageNode.truncationMode = .byWordWrapping
            self.answerTextMessageNode.maximumNumberOfLines = 3
            
            self.answerTextMessageNode.delegate = self
            self.answerTextMessageNode.isUserInteractionEnabled = true
            self.answerTextMessageNode.attributedText = TextString
            if senderTextMessageString.contains("http") {
                self.answerTextMessageNode.attributedText = senderTextMessageString.attributedHtmlString
            }
            self.answerTextMessageNode.accessibilityIdentifier = "labelanswerMessage"
            self.answerTextMessageNode.isAccessibilityElement = true
            self.answerTextMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            if !isIncomingMessage {
                self.answerTextMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
                self.nameMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
            } else {
                self.answerTextMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
                self.nameMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
            }
            print("afsasffas",answerUrl,AnswertextMessageString,senderTextMessageString,senderName)
            self.nameMessageNode.maximumNumberOfLines = 1
            self.addSubnode(nameMessageNode)
            self.addSubnode(answerTextMessageNode)
            networAnswerkImageMessageNode = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
            networAnswerkImageMessageNode.setURL(URL(string: answerUrl), resetToDefault: false)
            networAnswerkImageMessageNode.shouldCacheImage = true
            networAnswerkImageMessageNode.delegate = self
            self.addSubnode(networAnswerkImageMessageNode)
        }
        //
        self.addSubnode(sendIconNode)
        self.addSubnode(loaderNode)
        self.addSubnode(resendBackground)
        
        self.addSubnode(playIconNode)
        self.addSubnode(avatarIconNode)
        self.addSubnode(audioSlider)
        self.addSubnode(audioTimeNode)
        self.addSubnode(dateNode)

        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: AppManager.is_readNotification, object: nil)
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
            let image: UIImage = UIImage(named: "icon_create")!
            self.ouputfirstReply?.messagefirstReplyAction(text: "Аудио", messageId: self.message_id!, title: self.senderName,image:image)
            
                }}
            
        }
        
        is_selecting.toggle()
    }
    @objc func answerMessageClicked() {
        self.ouputAnswerMessage!.answerMessageClicked(chatId:AnswerMessage_id)
    }
    func replaceChatId(){
         replaced_chat_id = self.chat_id.replacingOccurrences(of: "U", with: "", options: .regularExpression, range: nil)
        replaced_chat_id = replaced_chat_id.replacingOccurrences(of: "C", with: "", options: .regularExpression, range: nil)
        replaced_chat_id = replaced_chat_id.replacingOccurrences(of: "G", with: "",    options: .regularExpression, range: nil)
    }
    open override func updateBubbleConfig(_ newValue: BubbleConfigurationProtocol) {
        var maskedBubbleConfig = newValue
        maskedBubbleConfig.isMasked = true
        super.updateBubbleConfig(maskedBubbleConfig)
    }
    fileprivate func updateAttributedText() {
        setNeedsLayout()
    }
    @objc fileprivate func playButtonPressed() {
        if (self.audioPlayer != nil){
            if (self.audioPlayer.isPlaying){
            playIconState = .pause
            }
            else {
                if let url = audioUrl {
                    if url.absoluteString.contains("https") {
                        playIconState = .download
                    }
                    else {
                        playIconState = .play
                    }
                }
            }
        }
        else {
        if let url = audioUrl {
            if url.absoluteString.contains("https") {
                 playIconState = .download
            }
            else {
                  playIconState = .play
            }
        }
        }
       
    }
    // MARK: Override AsycDisaplyKit Methods
    
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
    
        playIconNode.style.width = ASDimension(unit: .points, value: 25)
        playIconNode.style.height = ASDimension(unit: .points, value: 25)
        
        loaderNode.style.width = ASDimension(unit: .points, value: 25)
        loaderNode.style.height = ASDimension(unit: .points, value: 25)

        let play = ASOverlayLayoutSpec(child: playIconNode, overlay: loaderNode)
        let playLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 0), child: play)
        avatarIconNode.style.width = ASDimension(unit: .points, value: 50)
        avatarIconNode.style.height = ASDimension(unit: .points, value: 50)
        var avatarLayout : ASInsetLayoutSpec?
        if (incommingMessage == false){
            avatarLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 1, left:4, bottom: 1, right: -6), child: avatarIconNode)
        }
        else {
            avatarLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 1, left: -6, bottom: 1, right: 4), child: avatarIconNode)
        }
        dateNode.style.alignSelf = .end
        audioTimeNode.style.alignSelf = .start
        let dateNodeLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 65, bottom: 0, right: 0), child: dateNode)
        var dateLayout = ASStackLayoutSpec(direction: .horizontal, spacing:30.0, justifyContent: .spaceBetween, alignItems: .stretch, children: [audioTimeNode, dateNodeLayout])
        
        if !isIncomingMessage {
             let dateCombiner = ASStackLayoutSpec(direction: .horizontal, spacing: 1, justifyContent: .spaceBetween, alignItems: .center, children: [dateNodeLayout,sendIconNode])
             dateLayout = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .spaceBetween, alignItems: .center, children: [audioTimeNode,dateCombiner])
        }
        audioSlider.style.height = ASDimension(unit: .points, value: 20)
        audioSlider.style.width = ASDimension(unit: .points, value: constrainedSize.max.width/1.6)
         let audio = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0), child: audioSlider)
        dateLayout.style.width = ASDimension(unit: .points, value: 160)
       
        let verticalLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0.0, justifyContent: .start, alignItems: .center, children: [audio, dateLayout])
        
        let insetVert = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0), child: verticalLayout)
        var stackLayout : ASStackLayoutSpec?
        if (incommingMessage == false){
            stackLayout = ASStackLayoutSpec(direction: .horizontal, spacing: 8.0, justifyContent: .start, alignItems: .center, children: [avatarLayout!,playLayout, insetVert])
        }
        else {
            stackLayout = ASStackLayoutSpec(direction: .horizontal, spacing: 8.0, justifyContent: .start, alignItems: .center, children: [playLayout, insetVert,avatarLayout!])
        }
        if (!is_answer){
        if (is_resend == 1){
            let inserLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0), child: stackLayout!)
          let stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,inserLayout])
            return stackLayout
        }
        else {
        let inserLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0), child: stackLayout!)
        return inserLayout
    }
        }
        else {
            let width = (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.1
                self.nameMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
                self.answerTextMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
            let answerkLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceAround, alignItems: .stretch, children: [nameMessageNode,answerTextMessageNode])
            answerkLayout.style.flexShrink = 1
            var answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout])
            var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
            if (is_has_image){
                self.networAnswerkImageMessageNode.style.width = ASDimension(unit: .points, value: 40)
                self.networAnswerkImageMessageNode.style.height = ASDimension(unit: .points, value: 40)
                answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout,networAnswerkImageMessageNode])
            }
            let localinsets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
            let insertLayout = ASInsetLayoutSpec(insets: localinsets, child: answerStack)
            backgroundAnswerNode.style.width = ASDimension(unit: .points, value: 2)
            backgroundAnswerNode.style.height = ASDimension(unit: .points, value: 40)
            var stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [insertLayout,ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0), child: stackLayout!)])
            if (is_resend == 1){
                let inserLay = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0), child: stackLayout)
                let stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,insertLayout,inserLay])
            }
            // answerTextMessageNode.style.width = ASDimension(unit: .points, value: self.view.bounds.width)
            //   answerTextMessageNode.frame = CGRect(0, 0, textMessageNode.frame.width, 30)
            stackLayout.style.maxWidth = ASDimension(unit: .points, value: width)
            stackLayout.style.maxHeight = ASDimension(unit: .fraction, value: 1)
            return stackLayout
        }
    }
    
    // MARK: UIGestureRecognizer Selector Methods
    
    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        if (is_selecting == false){
            let touchLocation = recognizer.location(in: view)
            if (!self.playIconNode.frame.contains(touchLocation)){
            let alert = UIAlertController(title:"choise_action".localized(), message: nil, preferredStyle: .actionSheet)
                
                let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                    let image: UIImage = UIImage(named: "icon_create")!
                    self.ouput?.messageReplyAction(text: "audio".localized(), messageId: self.message_id!, title: self.senderName,image:image)
                })
                let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    let vc = ReplyController(chatType: .single,chatIds:messagesList)
                    vc.title = "resend".localized()
                    self.currentViewController!.show(vc, sender: nil)
                })
                let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
                })
                let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
                
                alert.addAction(replyAction)
                alert.addAction(resendAction)
                alert.addAction(cancelAction)
                alert.view.tintColor = UIColor.navBlueColor
                self.currentViewController?.present(alert, animated: true, completion: nil)
            }
        }
    }
    func MakeCornerImage(){
        avatarIconNode.imageModificationBlock = { [weak avatarIconNode] image in
            if image == nil {
                return image
            }
            var modifiedImage: UIImage?
            var rect = CGRect(origin: CGPoint(x: 0,y :0), size: image.size)
            
            UIGraphicsBeginImageContextWithOptions(image.size, false, UIScreen.main.scale)
            let maskPath = UIBezierPath(roundedRect: rect, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: 100, height: 100))
            maskPath.addClip()
            image.draw(in: rect)
            modifiedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return modifiedImage
        }
    }
}
