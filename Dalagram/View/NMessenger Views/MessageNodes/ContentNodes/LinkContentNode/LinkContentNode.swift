//
//  LinkContentNode.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 2/19/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import AsyncDisplayKit

protocol RemoveMessageOutput {
    func RemoveMessageAction(messageId: Int)
}
class LinkContentNode: ContentNode, ASTextNodeDelegate,ASNetworkImageNodeDelegate {
    
    var ouput: TextContentNodeOutput?
    
    var RemoveOuput: RemoveMessageOutput?
    
    var ouputfirstReply: TextContentFirstReply?
    // MARK: Public Variables
    /** Insets for the node */
    open var insets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8) {
        didSet {
            setNeedsLayout()
        }
    }
    
    /** UIFont for incoming text messages*/
    open var incomingTextFont = UIFont.n1B1Font() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIFont for outgoinf text messages*/
    open var outgoingTextFont = UIFont.n1B1Font() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for incoming text messages*/
    open var incomingTextColor = UIColor.n1DarkestGreyColor() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for outgoinf text messages*/
    open var outgoingTextColor = UIColor.black {
        didSet {
            self.updateAttributedText()
        }
    }
    /** String to present as the content of the cell*/
    open var textMessageString: NSAttributedString? {
        get {
            return self.textMessageNode.attributedText
        } set {
            self.textMessageNode.attributedText = newValue
        }
    }
    /** Overriding from super class
     Set backgroundBubble.bubbleColor and the text color when valus is set
     */
    open override var isIncomingMessage: Bool
        {
        didSet {
            if isIncomingMessage {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getOutgoingColor()
                self.updateAttributedText()
            } else {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getOutgoingColor()
                self.updateAttributedText()
            }
        }
    }
    
    // MARK: Private Variables
    /** ASTextNode as the content of the cell*/
    open fileprivate(set) var textMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var dateMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var viewsMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var viewsIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var networkImageMessageNode:ASNetworkImageNode = ASNetworkImageNode()
    
    //answer content
    open fileprivate(set) var answerTextMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var nameMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var backgroundAnswerNode: ASDisplayNode = ASDisplayNode()
    open fileprivate(set) var resendBackground: ASButtonNode = ASButtonNode()
    
    /** Bool as mutex for handling attributed link long presses*/
    fileprivate var lockKey: Bool = false
    fileprivate var message_id: Int = 0
    fileprivate var is_read: Int = 0
    fileprivate var is_has_image: Bool  = false
    fileprivate var AnswertextMessageString: String = ""
    fileprivate var is_selecting: Bool = false
    fileprivate var senderName: String = ""
    fileprivate var urlGlobal: String = ""
    fileprivate var is_resend: Int = 0
    // MARK: Initialisers
    
    /**
     Initialiser for the cell.
     - parameter textMessageString: Must be String. Sets text for cell.
     Calls helper method to setup cell
     */
    public init(id: Int,title:String, textMessageString: String, dateString: String,is_read:Int = 0,senderTextMessageString: String,senderName: String,url:String = "",link:String = "",is_resend:Int = 0, bubbleConfiguration: BubbleConfigurationProtocol? = nil, viewsCount: Int = 0) {
        
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.is_read = is_read
        self.AnswertextMessageString = textMessageString
        if (url != ""){
            is_has_image = true
        }
        self.is_resend = is_resend
        self.setupTextNode(title,textMessageString, dateString: dateString,senderName: senderName,url:url,link:link, viewsCount: viewsCount)
        self.message_id = id
        self.urlGlobal = link
    }
    /**
     Initialiser for the cell.
     - parameter textMessageString: Must be String. Sets text for cell.
     - parameter currentViewController: Must be an UIViewController. Set current view controller holding the cell.
     Calls helper method to setup cell
     */
    public init(id: Int,title:String, textMessageString: String, dateString: String,is_read:Int = 0,senderName: String,url:String = "",link:String = "", currentViewController: UIViewController,is_resend:Int = 0, bubbleConfiguration: BubbleConfigurationProtocol? = nil, viewsCount: Int = 0)
    {
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.currentViewController = currentViewController
        self.is_read = is_read
        self.AnswertextMessageString = textMessageString
        if (url != ""){
            is_has_image = true
        }
        self.senderName = senderName
        self.setupTextNode(title,textMessageString, dateString: dateString,senderName: senderName,url:url,link:link, viewsCount: viewsCount)
        self.message_id = id
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.urlGlobal = link
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
    }
    @objc func makeIs_read() {
        print("makeIs_read")
        self.is_read = 1
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    
    // MARK: Initialiser helper method
    
    /**
     Creates the text to be display in the cell. Finds links and phone number in the string and creates atrributed string.
     - parameter textMessageString: Must be String. Sets text for cell.
     */
    fileprivate func setupTextNode(_ title: String,_ textMessageString: String, dateString: String, senderName: String,url:String = "",link:String = "", viewsCount: Int)
    {
        self.backgroundBubble = self.bubbleConfiguration.getBubble()
        textMessageNode.delegate = self
        textMessageNode.isUserInteractionEnabled = true
        textMessageNode.linkAttributeNames = ["LinkAttribute","PhoneNumberAttribute"]
        
        let fontAndSizeAndTextColor = [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
        let textAttributts = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(red: 54/255, green: 143/255, blue: 28/255, alpha: 1.0)]
        
        let outputString = NSMutableAttributedString(string: textMessageString, attributes: fontAndSizeAndTextColor )
        let types: NSTextCheckingResult.CheckingType = [.link, .phoneNumber]
        let detector = try! NSDataDetector(types: types.rawValue)
        let matches = detector.matches(in: textMessageString, options: [], range: NSMakeRange(0, textMessageString.count))
        for match in matches {
            if let url = match.url {
                outputString.addAttribute(NSAttributedStringKey(rawValue: "LinkAttribute"), value: url, range: match.range)
                outputString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: match.range)
                outputString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: match.range)
            }
            if let phoneNumber = match.phoneNumber  {
                outputString.addAttribute(NSAttributedStringKey(rawValue: "PhoneNumberAttribute"), value: phoneNumber, range: match.range)
                outputString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: match.range)
                outputString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: match.range)
            }
        }
        
        self.textMessageNode.attributedText = outputString
        self.textMessageNode.accessibilityIdentifier = "labelMessage"
        self.textMessageNode.isAccessibilityElement = true
        self.addSubnode(textMessageNode)
        
        backgroundAnswerNode.backgroundColor =  UIColor(red: 89/255.0, green: 198/255.0, blue: 75/255.0, alpha: 1.0)
        self.addSubnode(backgroundAnswerNode)
        let nameString = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(red: 54/255, green: 143/255, blue: 28/255, alpha: 1.0)]
        
        let TextString = NSMutableAttributedString(string: title, attributes: textAttributts )
        self.nameMessageNode.delegate = self
        self.nameMessageNode.isUserInteractionEnabled = true
        self.nameMessageNode.attributedText = link.attributedHtmlString
        self.nameMessageNode.accessibilityIdentifier = "labelNameAnswerMessage"
        self.nameMessageNode.isAccessibilityElement = true
        self.nameMessageNode.addTarget(self, action:#selector(link_clickEvent), forControlEvents: .touchUpInside)
        
        self.answerTextMessageNode.truncationMode = .byWordWrapping
        
        self.answerTextMessageNode.delegate = self
        self.answerTextMessageNode.isUserInteractionEnabled = true
        self.answerTextMessageNode.attributedText = TextString
        self.answerTextMessageNode.accessibilityIdentifier = "labelanswerMessage"
        self.answerTextMessageNode.isAccessibilityElement = true
        
        if !isIncomingMessage {
            self.answerTextMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
            self.nameMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
        } else {
            self.answerTextMessageNode.backgroundColor =  self.bubbleConfiguration.getOutgoingColor()
            self.nameMessageNode.backgroundColor =  self.bubbleConfiguration.getOutgoingColor()
        }
        self.addSubnode(nameMessageNode)
        self.addSubnode(answerTextMessageNode)
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.n1Black50Color()]
        self.dateMessageNode.attributedText = NSMutableAttributedString(string: dateString, attributes: dateTextAttr)
        self.dateMessageNode.accessibilityIdentifier = "dateMessage"
        self.addSubnode(dateMessageNode)
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        self.addSubnode(sendIconNode)
        
        self.viewsMessageNode.attributedText = NSMutableAttributedString(string:  "\(viewsCount)", attributes: dateTextAttr)
        self.viewsMessageNode.accessibilityIdentifier = "viewsMessage"
        self.addSubnode(viewsMessageNode)
        
        self.viewsIconNode.image = UIImage(named: "icon_views")
        self.addSubnode(viewsIconNode)
        
        // Show view icon only for channel chat
        
        if viewsCount == 0 {
            self.viewsIconNode.isHidden = true
            self.viewsMessageNode.isHidden = true
        }
        if (is_has_image){
            networkImageMessageNode = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
            networkImageMessageNode.setURL(URL(string: url), resetToDefault: false)
            networkImageMessageNode.shouldCacheImage = true
            networkImageMessageNode.delegate = self
            self.addSubnode(networkImageMessageNode)
        }
        
        self.resendBackground.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        self.resendBackground.setImage(UIImage(named:"icon_resended"), for: .normal)
        self.resendBackground.setTitle("  Пересланное", with: UIFont.systemFont(ofSize: 13), with: UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0), for: .normal)
        self.resendBackground.contentHorizontalAlignment = .left
        self.addSubnode(resendBackground)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: AppManager.is_readNotification, object: nil)
    }
    @objc func link_clickEvent() {
        let input = urlGlobal
                print("asffsfas call",urlGlobal)
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
        
        for match in matches {
            guard let range = Range(match.range, in: input) else { continue }
            let url = input[range]
            print(url)
            let url_link = URL(string: String(url))
            UIApplication.shared.openURL(url_link!)
        }
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
                    if let stringText = self.textMessageNode.attributedText?.string {
                        let image: UIImage = UIImage(named: "icon_create")!
                        self.ouputfirstReply?.messagefirstReplyAction(text: stringText, messageId: self.message_id, title: self.senderName,image:image)
                    }
            }
        }
            is_selecting.toggle()
        }
    }
    
    //MARK: Helper Methods
    /** Updates the attributed string to the correct incoming/outgoing settings and lays out the component again*/
    
    fileprivate func updateAttributedText() {
        let tmpString = NSMutableAttributedString(attributedString: self.textMessageNode.attributedText!)
        tmpString.addAttributes([NSAttributedStringKey.foregroundColor: isIncomingMessage ? incomingTextColor : outgoingTextColor, NSAttributedStringKey.font: isIncomingMessage ? incomingTextFont : outgoingTextFont], range: NSMakeRange(0, tmpString.length))
        self.textMessageNode.attributedText = tmpString
        //  self.answerTextMessageNode.attributedText = tmpString
        self.sendIconNode.isHidden = isIncomingMessage
        setNeedsLayout()
    }
    
    // MARK: Override AsycDisaplyKit Methods
    
    /**
     Overriding layoutSpecThatFits to specifiy relatiohsips between elements in the cell
     */
    
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let width = (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.2
        var dateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .end, alignItems: .center, children: [dateMessageNode, sendIconNode])
        
        // Show views icon for channel
        if !viewsIconNode.isHidden {
            dateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 6, justifyContent: .end, alignItems: .center, children: [viewsIconNode,viewsMessageNode, dateMessageNode, sendIconNode])
        }
        
        if isIncomingMessage {
            dateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .end, alignItems: .center, children: [dateMessageNode])
        }
        //    answerTextMessageNode.style.height = ASDimension(unit: .points, value: textMessageNode.style.he.value - 2)
        let answerkLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceAround, alignItems: .stretch, children: [nameMessageNode,answerTextMessageNode])
        answerkLayout.style.flexShrink = 1
        var answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [answerkLayout])

            //    if (textWidth(text:AnswertextMessageString,font:UIFont.n1B1Font()) > width){
            //        answerTextMessageNode.style.width = ASDimension(unit: .points, value: width)
            //        nameMessageNode.style.width = ASDimension(unit: .points, value: width)
            //     }
            //   else {
            //      answerTextMessageNode.style.width = ASDimension(unit: .points, value: textWidth(text:AnswertextMessageString,font:UIFont.n1B1Font()) + 20)
            //       nameMessageNode.style.width = ASDimension(unit: .points, value: textWidth(text:AnswertextMessageString,font:UIFont.n1B1Font()) + 20)
            //       }
        
        backgroundAnswerNode.style.width = ASDimension(unit: .points, value: 2)
        backgroundAnswerNode.style.height = ASDimension(unit: .points, value: 40)
        
        
        var stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [answerStack,textMessageNode, dateStack])
        if (is_has_image){
            let width = constrainedSize.max.width / 1.2
            self.networkImageMessageNode.style.width = ASDimension(unit: .points, value: width)
            self.networkImageMessageNode.style.height = ASDimension(unit: .points, value: width/4*3)
                
             stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [answerStack,textMessageNode,networkImageMessageNode, dateStack])
            if (is_resend == 1){
                stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,answerStack,textMessageNode,networkImageMessageNode, dateStack])
            }
        }
        if (is_resend == 1){
          stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,answerStack,textMessageNode, dateStack])
        }
        // answerTextMessageNode.style.width = ASDimension(unit: .points, value: self.view.bounds.width)
        //   answerTextMessageNode.frame = CGRect(0, 0, textMessageNode.frame.width, 30)
        stackLayout.style.maxWidth = ASDimension(unit: .points, value: width)
        stackLayout.style.maxHeight = ASDimension(unit: .fraction, value: 1)
        print("afssfa",textMessageNode.frame.width)
        return ASInsetLayoutSpec(insets: insets, child: stackLayout)
        
    }
    func textWidth(text: String, font: UIFont?) -> CGFloat {
        let attributes = font != nil ? [NSAttributedStringKey.font: font] : [:]
        return text.size(withAttributes: attributes as [NSAttributedStringKey : Any]).width
    }
    // MARK: ASTextNodeDelegate
    
    /**
     Implementing shouldHighlightLinkAttribute - returning true for both link and phone numbers
     */
    
    public func textNode(_ textNode: ASTextNode, shouldHighlightLinkAttribute attribute: String, value: Any, at point: CGPoint) -> Bool {
        if attribute == "LinkAttribute"
        {
            return true
        }
        else if attribute == "PhoneNumberAttribute"
        {
            return true
        }
        return false
        
    }
    
    /*open func textNode(_ textNode: ASTextNode, shouldHighlightLinkAttribute attribute: String, value: AnyObject, at point: CGPoint) -> Bool {
     if attribute == "LinkAttribute"
     {
     return true
     }
     else if attribute == "PhoneNumberAttribute"
     {
     return true
     }
     return false
     }*/
    
    /**
     Implementing tappedLinkAttribute - handle tap event on links and phone numbers
     */
    
    //open func textNode(_ textNode: ASTextNode, tappedLinkAttribute attribute: String, value: AnyObject, at point: CGPoint, textRange: NSRange) {
    public func textNode(_ textNode: ASTextNode, tappedLinkAttribute attribute: String, value: Any, at point: CGPoint, textRange: NSRange) {
        if attribute == "LinkAttribute"
        {
            if !self.lockKey
            {
                if let tmpString = self.textMessageNode.attributedText
                {
                    let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                    attributedString.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.lightGray, range: textRange)
                    self.textMessageNode.attributedText = attributedString
                    UIApplication.shared.openURL(value as! URL)
                    delay(0.4) {
                        if let tmpString = self.textMessageNode.attributedText
                        {
                            let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                            attributedString.removeAttribute(NSAttributedStringKey.backgroundColor, range: textRange)
                            self.textMessageNode.attributedText = attributedString
                        }
                    }
                }
            }
        }
        else if attribute == "PhoneNumberAttribute"
        {
            let phoneNumber = value as! String
            UIApplication.shared.openURL(URL(string: "tel://\(phoneNumber)")!)
        }
    }
    
    /**
     Implementing shouldLongPressLinkAttribute - returning true for both link and phone numbers
     */
    //open func textNode(_ textNode: ASTextNode, shouldLongPressLinkAttribute attribute: String, value: AnyObject, at point: CGPoint) -> Bool {
    public func textNode(_ textNode: ASTextNode, shouldLongPressLinkAttribute attribute: String, value: Any, at point: CGPoint) -> Bool {
        if attribute == "LinkAttribute"
        {
            return true
        }
        else if attribute == "PhoneNumberAttribute"
        {
            return true
        }
        return false
    }
    
    /**
     Implementing longPressedLinkAttribute - handles long tap event on links and phone numbers
     */
    //open func textNode(_ textNode: ASTextNode, longPressedLinkAttribute attribute: String, value: AnyObject, at point: CGPoint, textRange: NSRange) {
    public func textNode(_ textNode: ASTextNode, longPressedLinkAttribute attribute: String, value: Any, at point: CGPoint, textRange: NSRange) {
        if attribute == "LinkAttribute"
        {
            self.lockKey = true
            if let tmpString = self.textMessageNode.attributedText
            {
                let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                attributedString.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.lightGray, range: textRange)
                self.textMessageNode.attributedText = attributedString
                
                let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                let openAction = UIAlertAction(title: "Open", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                let addToReadingListAction = UIAlertAction(title: "Add to Reading List", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let copyAction = UIAlertAction(title: "Copy", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                optionMenu.addAction(openAction)
                optionMenu.addAction(addToReadingListAction)
                optionMenu.addAction(copyAction)
                optionMenu.addAction(cancelAction)
                
                if let tmpCurrentViewController = self.currentViewController
                {
                    DispatchQueue.main.async(execute: { () -> Void in
                        tmpCurrentViewController.present(optionMenu, animated: true, completion: nil)
                    })
                    
                }
                
            }
            delay(0.4) {
                if let tmpString = self.textMessageNode.attributedText
                {
                    let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                    attributedString.removeAttribute(NSAttributedStringKey.backgroundColor, range: textRange)
                    self.textMessageNode.attributedText = attributedString
                }
            }
        }
        else if attribute == "PhoneNumberAttribute"
        {
            let phoneNumber = value as! String
            self.lockKey = true
            if let tmpString = self.textMessageNode.attributedText
            {
                let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                attributedString.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.lightGray, range: textRange)
                self.textMessageNode.attributedText = attributedString
                let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                let callPhoneNumberAction = UIAlertAction(title: "Call \(phoneNumber)", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                let facetimeAudioAction = UIAlertAction(title: "Facetime Audio", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let sendMessageAction = UIAlertAction(title: "Send Message", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let addToContactsAction = UIAlertAction(title: "Add to Contacts", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let copyAction = UIAlertAction(title: "Copy", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (alert: UIAlertAction) -> Void in
                    //print("Cancelled")
                    self.lockKey = false
                })
                
                optionMenu.addAction(callPhoneNumberAction)
                optionMenu.addAction(facetimeAudioAction)
                optionMenu.addAction(sendMessageAction)
                optionMenu.addAction(addToContactsAction)
                optionMenu.addAction(copyAction)
                optionMenu.addAction(cancelAction)
                
                if let tmpCurrentViewController = self.currentViewController
                {
                    DispatchQueue.main.async(execute: { () -> Void in
                        tmpCurrentViewController.present(optionMenu, animated: true, completion: nil)
                    })
                    
                }
                
            }
            delay(0.4) {
                if let tmpString = self.textMessageNode.attributedText
                {
                    let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                    attributedString.removeAttribute(NSAttributedStringKey.backgroundColor, range: textRange)
                    self.textMessageNode.attributedText = attributedString
                }
            }
        }
    }
    
    // MARK: UILongPressGestureRecognizer Selector Methods
    
    
    /**
     Overriding canBecomeFirstResponder to make cell first responder
     */
    override open func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    /**
     Overriding resignFirstResponder to resign responder
     */
    override open func resignFirstResponder() -> Bool {
        return view.resignFirstResponder()
    }
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any) -> Bool {
        return true
    }
    
    /**
     Override method from superclass
     */
    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        if (is_selecting == false){
            let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
            
            let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                if let stringText = self.textMessageNode.attributedText?.string {
                    let image: UIImage = UIImage(named: "icon_create")!
                    self.ouput?.messageReplyAction(text: stringText, messageId: self.message_id, title: self.senderName,image:image)
                }
            })
            let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                UIPasteboard.general.string = self.textMessageNode.attributedText?.string
            })
            let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id)
                let vc = ReplyController(chatType: .single,chatIds:messagesList)
                vc.title = "resend".localized()
                self.currentViewController!.show(vc, sender: nil)
            })
            let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id)
                self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id)
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            
            alert.addAction(replyAction)
            alert.addAction(copyAction)
            alert.addAction(resendAction)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            alert.view.tintColor = UIColor.navBlueColor
            self.currentViewController?.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    /**
     Copy Selector for UIMenuController
     */
    @objc open func copySelector() {
        UIPasteboard.general.string = self.textMessageNode.attributedText!.string
    }
    
}
//    let menuController = UIMenuController.shared
//    menuController.menuItems = [UIMenuItem(title: "Скопировать", action: #selector(TextContentNode.copySelector)), UIMenuItem(title: "Еще", action: #selector(TextContentNode.copySelector))]
//    print(self.textMessageNode.frame)
//    menuController.setTargetRect(self.textMessageNode.frame, in: self.view)
//    menuController.setMenuVisible(true, animated: true)
