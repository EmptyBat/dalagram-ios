//
//  File.swift
//  Dalagram
//
//  Created by Toremurat on 13.08.2018.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import SafariServices

open class DocumentContentNode: ContentNode,ASTextNodeDelegate,ASNetworkImageNodeDelegate {
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    open var insets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8) {
        didSet {
            setNeedsLayout()
        }
    }
    var ouput: TextContentNodeOutput?
    var RemoveOuput: RemoveMessageOutput?
    var ouputfirstReply: TextContentFirstReply?
    open override var isIncomingMessage: Bool
        {
        didSet {
            if isIncomingMessage {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getIncomingColor()
                self.updateAttributedText()
            } else {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getOutgoingColor()
                self.updateAttributedText()
            }
        }
    }
    
    /** UIFont for incoming text messages*/
    open var incomingTextFont = UIFont.n1B1Font() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIFont for outgoinf text messages*/
    open var outgoingTextFont = UIFont.n1B1Font() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for incoming text messages*/
    open var incomingTextColor = UIColor.n1DarkestGreyColor() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for outgoinf text messages*/
    open var outgoingTextColor = UIColor.black {
        didSet {
            self.updateAttributedText()
        }
    }
    open var message_id: Int? {
        didSet {
            self.updateAttributedText()
        }
    }
    fileprivate var is_selecting: Bool = false
    var documentUrl: URL?
    fileprivate var senderName: String = ""
    open fileprivate(set) var loaderNode: ASDisplayNode = ASDisplayNode()
    open fileprivate(set) var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    open fileprivate(set) var documentText: ASTextNode = ASTextNode()
    open fileprivate(set) var documentIcon: ASImageNode = ASImageNode()
    open fileprivate(set) var documentDate: ASTextNode = ASTextNode()
    open fileprivate(set) var fileExtension: ASTextNode = ASTextNode()
    open fileprivate(set) var resendBackground: ASButtonNode = ASButtonNode()
    fileprivate var is_resend: Int = 0
    fileprivate var is_read: Int = 0
    
    //answer content
    var ouputAnswerMessage: AnswerMessage?
    open fileprivate(set) var answerTextMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var nameMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var backgroundAnswerNode: ASDisplayNode = ASDisplayNode()
    fileprivate var is_has_image: Bool  = false
    fileprivate var AnswerMessage_id: Int = 0
    fileprivate var is_answer: Bool  = false
    fileprivate var AnswertextMessageString: String = ""
    fileprivate var senderTextMessageString: String = ""
    fileprivate var answerUrl: String = ""
    open fileprivate(set) var networAnswerkImageMessageNode:ASNetworkImageNode = ASNetworkImageNode()
    public init(id: Int,AnswerMessage_id:Int = 0,text: String, date: String, documentUrl: URL? = nil, currentVC: UIViewController,senderName:String,is_read:Int = 0,is_resend:Int = 0,textMessageString: String = "",senderTextMessageString: String = "",url:String = "",is_answer:Bool = false, bubbleConfiguration: BubbleConfigurationProtocol? = nil) {
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.is_read = is_read
        self.currentViewController = currentVC
        if (is_answer == true){
            self.is_answer = is_answer
            self.AnswertextMessageString = textMessageString
            self.senderTextMessageString = senderTextMessageString
            self.answerUrl = url
            if let vc = currentViewController as? AnswerMessage {
                self.ouputAnswerMessage = vc
            }
            if (url != ""){
                is_has_image = true
            }
            self.AnswerMessage_id = AnswerMessage_id
        }
        self.setupDocumentNode(text, date: date)
        self.documentUrl = documentUrl
        self.message_id = id
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.is_resend = is_resend
        self.senderName = senderName
        self.is_read = is_read
    }
    
    fileprivate func setupDocumentNode(_ text: String, date: String)
    {
        self.backgroundBubble = self.bubbleConfiguration.getBubble()
        self.backgroundColor = UIColor.lightGreenColor
        
        let origImage = UIImage(named: "icon_file_default")
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        
        documentIcon.image = tintedImage
        documentIcon.imageModificationBlock = ASImageNodeTintColorModificationBlock(.random)
        documentIcon.contentMode = UIViewContentMode.scaleAspectFit
        
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.n1Black50Color()]
        documentDate.attributedText = NSMutableAttributedString(string: date, attributes: dateTextAttr)
        
        let attributes =  [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor
        ]
        
        documentText.maximumNumberOfLines = 3
        documentText.truncationMode = .byTruncatingTail
        documentText.attributedText = NSAttributedString(string: text, attributes: attributes)
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        let extensionFileFont = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.white]
            fileExtension.attributedText = NSMutableAttributedString(string: text.fileExtension(), attributes: extensionFileFont)
        
        if (is_answer){
            //answer Content Node
            backgroundAnswerNode.backgroundColor =  UIColor(red: 89/255.0, green: 198/255.0, blue: 75/255.0, alpha: 1.0)
            self.addSubnode(backgroundAnswerNode)
            let nameString = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(red: 54/255, green: 143/255, blue: 28/255, alpha: 1.0)]
            let fontAndSizeAndTextColor = [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
            let TextString = NSMutableAttributedString(string: AnswertextMessageString, attributes: fontAndSizeAndTextColor )
            self.nameMessageNode.delegate = self
            self.nameMessageNode.isUserInteractionEnabled = true
            self.nameMessageNode.attributedText = NSMutableAttributedString(string:  senderTextMessageString, attributes: nameString)
            self.nameMessageNode.accessibilityIdentifier = "labelNameAnswerMessage"
            self.nameMessageNode.isAccessibilityElement = true
            self.nameMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            self.answerTextMessageNode.truncationMode = .byWordWrapping
            self.answerTextMessageNode.maximumNumberOfLines = 3
            
            self.answerTextMessageNode.delegate = self
            self.answerTextMessageNode.isUserInteractionEnabled = true
            self.answerTextMessageNode.attributedText = TextString
            if senderTextMessageString.contains("http") {
                self.answerTextMessageNode.attributedText = senderTextMessageString.attributedHtmlString
            }
            self.answerTextMessageNode.accessibilityIdentifier = "labelanswerMessage"
            self.answerTextMessageNode.isAccessibilityElement = true
            self.answerTextMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            if !isIncomingMessage {
                self.answerTextMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
                self.nameMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
            } else {
                self.answerTextMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
                self.nameMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
            }
            print("afsasffas",answerUrl,AnswertextMessageString,senderTextMessageString,senderName)
            self.nameMessageNode.maximumNumberOfLines = 1
            self.addSubnode(nameMessageNode)
            self.addSubnode(answerTextMessageNode)
            networAnswerkImageMessageNode = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
            networAnswerkImageMessageNode.setURL(URL(string: answerUrl), resetToDefault: false)
            networAnswerkImageMessageNode.shouldCacheImage = true
            networAnswerkImageMessageNode.delegate = self
            self.addSubnode(networAnswerkImageMessageNode)
        }
        //
        self.addSubnode(documentIcon)
        self.addSubnode(documentText)
        self.addSubnode(documentDate)
        self.addSubnode(sendIconNode)
        self.addSubnode(fileExtension)
        self.resendBackground.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        self.resendBackground.setImage(UIImage(named:"icon_resended"), for: .normal)
        self.resendBackground.setTitle("  Пересланное", with: UIFont.systemFont(ofSize: 13), with: UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0), for: .normal)
        self.resendBackground.contentHorizontalAlignment = .left
        self.addSubnode(resendBackground)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = CGPoint(x: 28, y: 28)
        loaderNode.view.addSubview(activityIndicator)
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        self.addSubnode(sendIconNode)
        
        self.addSubnode(loaderNode)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: AppManager.is_readNotification, object: nil)
    }
    @objc func answerMessageClicked() {
        self.ouputAnswerMessage!.answerMessageClicked(chatId:AnswerMessage_id)
    }
    @objc func makeIs_read() {
        print("makeIs_read")
        self.is_read = 1
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
            let image: UIImage = UIImage(named: "icon_create")!
            self.ouputfirstReply?.messagefirstReplyAction(text: "Документ", messageId: self.message_id!, title: self.senderName,image:image)
        }
            }
            
        }
        is_selecting.toggle()
    }
    
    //MARK: Helper Methods
    /** Updates the attributed string to the correct incoming/outgoing settings and lays out the component again*/
    fileprivate func updateAttributedText() {
        let tmpString = NSMutableAttributedString(attributedString: documentText.attributedText!)
        tmpString.addAttributes([NSAttributedStringKey.foregroundColor: isIncomingMessage ? incomingTextColor : outgoingTextColor, NSAttributedStringKey.font: isIncomingMessage ? incomingTextFont : outgoingTextFont], range: NSMakeRange(0, tmpString.length))
        self.documentText.attributedText = tmpString
       // self.sendIconNode.isHidden = isIncomingMessage
        setNeedsLayout()
    }
    
    // MARK: Upload Status Delegates
    
    func uploadBegin() {
        print("start")
        activityIndicator.startAnimating()
    }
    
    func uploadEnd() {
        print("ended")
        activityIndicator.stopAnimating()
    }
    
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
         let width = (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.2
        let centerSpec = ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: [], child: fileExtension)
        let overlay = ASOverlayLayoutSpec(child: documentIcon, overlay: centerSpec)
        self.documentIcon.style.width = ASDimension(unit: .points, value: 40)
        self.documentIcon.style.height = ASDimension(unit: .points, value: 40)
        
       self.documentText.style.maxWidth = ASDimension(unit: .points, value: (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.10)
        var dateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .end, alignItems: .end, children: [self.documentDate,sendIconNode])
        if (isIncomingMessage){
         dateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .end, alignItems: .end, children: [self.documentDate])
        }
        let VerticalStack = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .end, alignItems: .end, children: [overlay,dateStack])
        
        let textStack = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [documentText])
      //  textStack.style.width = ASDimension(unit: .points, value: width - 40 - self.insets.left - self.insets.right - 20)
        
        let stackLayout = ASStackLayoutSpec(direction: .horizontal, spacing: 8, justifyContent: .start, alignItems: .center, children: [textStack, VerticalStack])
        
        //let stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceAround, alignItems: .stretch, children: [documentText])
        if (!is_answer){
        if (is_resend == 1){
            let stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,ASInsetLayoutSpec(insets: insets, child: stackLayout)])
            return stackLayout
        }
        else {
        return ASInsetLayoutSpec(insets: insets, child: stackLayout)
        }
        }
        else {
            let width = (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.1
                self.nameMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
                self.answerTextMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
            let answerkLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceAround, alignItems: .stretch, children: [nameMessageNode,answerTextMessageNode])
            answerkLayout.style.flexShrink = 1
            var answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout])
            if (is_has_image){
                self.networAnswerkImageMessageNode.style.width = ASDimension(unit: .points, value: 40)
                self.networAnswerkImageMessageNode.style.height = ASDimension(unit: .points, value: 40)
                answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout,networAnswerkImageMessageNode])
            }
            let localinsets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
            let insertLayout = ASInsetLayoutSpec(insets: localinsets, child: answerStack)
            backgroundAnswerNode.style.width = ASDimension(unit: .points, value: 2)
            backgroundAnswerNode.style.height = ASDimension(unit: .points, value: 40)
            var stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [insertLayout,ASInsetLayoutSpec(insets: insets, child: stackLayout)])
            if (is_resend == 1){
                stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,insertLayout,ASInsetLayoutSpec(insets: insets, child: stackLayout)])
            }
            // answerTextMessageNode.style.width = ASDimension(unit: .points, value: self.view.bounds.width)
            //   answerTextMessageNode.frame = CGRect(0, 0, textMessageNode.frame.width, 30)
            stackLayout.style.maxWidth = ASDimension(unit: .points, value: width)
            stackLayout.style.maxHeight = ASDimension(unit: .fraction, value: 1)
            return stackLayout
        }
        
    }
    
    /**
     Override messageTapSelector Method
     */
    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        if (is_selecting == false){
            let touchLocation = recognizer.location(in: view)
            if (!self.view.frame.contains(touchLocation)){
            let alert = UIAlertController(title:"choise_action".localized(), message: nil, preferredStyle: .actionSheet)
            
            let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                let image: UIImage = UIImage(named: "icon_create")!
                self.ouput?.messageReplyAction(text: "document".localized(), messageId: self.message_id!, title: self.senderName,image:image)
            })
            let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                //Show activity indicator
                DispatchQueue.global(qos: .background).async {
                    if let urlData = NSData(contentsOf: self.documentUrl!){
                        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                        let filePath="\(documentsPath)/tempFile"
                        DispatchQueue.main.async {
                            urlData.write(toFile: filePath, atomically: true)
                            //Hide activity indicator
                            let activityVC = UIActivityViewController(activityItems: [NSURL(fileURLWithPath: filePath)], applicationActivities: nil)
                            activityVC.excludedActivityTypes = [.addToReadingList, .assignToContact]
                            self.currentViewController!.present(activityVC, animated: true, completion: nil)
                        }
                    }
                }
            })
            let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id!)
                let vc = ReplyController(chatType: .single,chatIds:messagesList)
                vc.title = "resend".localized()
                self.currentViewController!.show(vc, sender: nil)
            })
                let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
                })
                let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
                
                alert.addAction(replyAction)
                alert.addAction(copyAction)
                alert.addAction(resendAction)
                alert.addAction(deleteAction)
                alert.addAction(cancelAction)
            alert.view.tintColor = UIColor.navBlueColor
            self.currentViewController?.present(alert, animated: true, completion: nil)
        }
            else {
                if let documentUrl = documentUrl {
                    let vc = SFSafariViewController(url: documentUrl)
                    currentViewController?.present(vc, animated: true, completion: nil)
                }
        }
        }
    }
    
}
extension String
{
    func encodeUrl() -> String?
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
    }
    func decodeUrl() -> String?
    {
        return self.removingPercentEncoding
    }
}
