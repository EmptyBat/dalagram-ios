//
//  FourImagesNode.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/6/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import SKPhotoBrowser
//MARK: NetworkImageContentNode
/**
 NetworkImageContentNode class for N Messenger. Extends MessageNode.
 Defines content that is a network image (An image that is provided via url).
 */
open class FourImagesNode: ContentNode, ASNetworkImageNodeDelegate,ASTextNodeDelegate {
    
    // MARK: Public Variables
    /** NSURL for the image*/
    open var url1: URL? {
        get {
            return networkImageMessageNode1.url
        } set {
            networkImageMessageNode1.url = newValue
        }
    }
    open var url2: URL? {
        get {
            return networkImageMessageNode2.url
        } set {
            networkImageMessageNode2.url = newValue
        }
    }
    open var url3: URL? {
        get {
            return networkImageMessageNode3.url
        } set {
            networkImageMessageNode3.url = newValue
        }
    }
    open var url4: URL? {
        get {
            return networkImageMessageNode4.url
        } set {
            networkImageMessageNode4.url = newValue
        }
    }
    open var message_id: Int? {
        didSet {
       //     self.updateAttributedText()
        }
    }
    
    // MARK: Private Variables
    /** ASNetworkImageNode as the content of the cell*/
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var networkImageMessageNode1:ASNetworkImageNode = ASNetworkImageNode()
    open fileprivate(set) var networkImageMessageNode2:ASNetworkImageNode = ASNetworkImageNode()
    open fileprivate(set) var networkImageMessageNode3:ASNetworkImageNode = ASNetworkImageNode()
    open fileprivate(set) var networkImageMessageNode4:ASNetworkImageNode = ASNetworkImageNode()
    open fileprivate(set) var resendBackground: ASButtonNode = ASButtonNode()
    open fileprivate(set) var dateNode: ASTextNode = ASTextNode()
    fileprivate var is_selecting: Bool = false
    var ouput: TextContentNodeOutput?
    fileprivate var is_resend: Int = 0
    var ouputfirstReply: TextContentFirstReply?
    var RemoveOuput: RemoveMessageOutput?
    var images = [SKPhoto]()
    fileprivate var senderName: String = ""
    fileprivate var is_read: Int = 0
    // MARK: Initialisers
    //answer content
    open var incomingTextFont = UIFont.n1B1Font() {
        didSet {
            //    self.updateAttributedText()
        }
    }
    /** UIFont for outgoinf text messages*/
    open var outgoingTextFont = UIFont.n1B1Font() {
        didSet {
            //  self.updateAttributedText()
        }
    }
    /** UIColor for incoming text messages*/
    open var incomingTextColor = UIColor.n1DarkestGreyColor() {
        didSet {
            //    self.updateAttributedText()
        }
    }
    /** UIColor for outgoinf text messages*/
    open var outgoingTextColor = UIColor.black {
        didSet {
            //    self.updateAttributedText()
        }
    }
    open var insets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8) {
        didSet {
            setNeedsLayout()
        }
    }
    var ouputAnswerMessage: AnswerMessage?
    open fileprivate(set) var answerTextMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var nameMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var backgroundAnswerNode: ASDisplayNode = ASDisplayNode()
    fileprivate var is_has_image: Bool  = false
    fileprivate var AnswerMessage_id: Int = 0
    fileprivate var is_answer: Bool  = false
    fileprivate var AnswertextMessageString: String = ""
    fileprivate var senderTextMessageString: String = ""
    fileprivate var answerUrl: String = ""
    open fileprivate(set) var networAnswerkImageMessageNode:ASNetworkImageNode = ASNetworkImageNode()
    /**
     Initialiser for the cell.
     - parameter imageURL: Must be String. Sets url for the image in the cell.
     Calls helper method to setup cell
     */
    public init(id: Int,AnswerMessage_id:Int = 0,urls: [String] = [], date: String, currentVC: UIViewController,senderName:String,is_read:Int = 0,is_resend:Int = 0,textMessageString: String = "",senderTextMessageString: String = "",url:String = "",is_answer:Bool = false, bubbleConfiguration: BubbleConfigurationProtocol? = nil) {
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.currentViewController = currentVC
        if (is_answer == true){
            self.is_answer = is_answer
            self.AnswertextMessageString = textMessageString
            self.senderTextMessageString = senderTextMessageString
            self.answerUrl = url
            if let vc = currentViewController as? AnswerMessage {
                self.ouputAnswerMessage = vc
            }
            if (url != ""){
                is_has_image = true
            }
            self.AnswerMessage_id = AnswerMessage_id
        }
        self.setupNetworkImageNode(urls:urls, date: date)
        for url in urls {
            let photo = SKPhoto.photoWithImageURL(url)
            photo.shouldCachePhotoURLImage = true
            images.append(photo)
        }
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.is_read = is_read
        self.message_id = id
        self.senderName = senderName
        self.is_resend = is_resend
    }
    
    // MARK: Initialiser helper method
    /** Override updateBubbleConfig to set bubble mask */
    open override func updateBubbleConfig(_ newValue: BubbleConfigurationProtocol) {
        var maskedBubbleConfig = newValue
        maskedBubbleConfig.isMasked = true
        super.updateBubbleConfig(maskedBubbleConfig)
    }
    
    /**
     Sets the URL to be display in the image. Clips and rounds the corners.
     - parameter imageURL: Must be String. Sets url for the image in the cell.
     */
    fileprivate func setupNetworkImageNode(urls: [String] = [], date: String)
    {
        //network image 1
        networkImageMessageNode1 = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
        networkImageMessageNode1.setURL(URL(string: urls[0].encodeUrl()!), resetToDefault: false)
        networkImageMessageNode1.shouldCacheImage = true
        networkImageMessageNode1.shouldRenderProgressImages = true
        networkImageMessageNode1.delegate = self
        //network image 2
        networkImageMessageNode2 = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
        networkImageMessageNode2.setURL(URL(string: urls[1].encodeUrl()!), resetToDefault: false)
        networkImageMessageNode2.shouldCacheImage = true
        networkImageMessageNode2.shouldRenderProgressImages = true
        networkImageMessageNode2.delegate = self
        //network image 3
        networkImageMessageNode3 = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
        networkImageMessageNode3.setURL(URL(string: urls[2].encodeUrl()!), resetToDefault: false)
        networkImageMessageNode3.shouldCacheImage = true
        networkImageMessageNode3.shouldRenderProgressImages = true
        networkImageMessageNode3.delegate = self
        //network image 4
        networkImageMessageNode4 = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
        networkImageMessageNode4.setURL(URL(string: urls[3].encodeUrl()!), resetToDefault: false)
        networkImageMessageNode4.shouldCacheImage = true
        networkImageMessageNode4.shouldRenderProgressImages = true
        networkImageMessageNode4.delegate = self
        
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.white]
        dateNode.attributedText = NSAttributedString(string: date, attributes: dateTextAttr)
        dateNode.backgroundColor = UIColor.darkGray
        dateNode.cornerRadius = 5.0
        dateNode.clipsToBounds = true
        dateNode.textContainerInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        
        self.resendBackground.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        self.resendBackground.setImage(UIImage(named:"icon_resended"), for: .normal)
        self.resendBackground.setTitle("  Пересланное", with: UIFont.systemFont(ofSize: 13), with: UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0), for: .normal)
        self.resendBackground.contentHorizontalAlignment = .left
        
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        if (is_answer){
            //answer Content Node
            backgroundAnswerNode.backgroundColor =  UIColor(red: 89/255.0, green: 198/255.0, blue: 75/255.0, alpha: 1.0)
            self.addSubnode(backgroundAnswerNode)
            let nameString = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(red: 54/255, green: 143/255, blue: 28/255, alpha: 1.0)]
            let fontAndSizeAndTextColor = [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
            let TextString = NSMutableAttributedString(string: AnswertextMessageString, attributes: fontAndSizeAndTextColor )
            self.nameMessageNode.delegate = self
            self.nameMessageNode.isUserInteractionEnabled = true
            self.nameMessageNode.attributedText = NSMutableAttributedString(string:  senderTextMessageString, attributes: nameString)
            self.nameMessageNode.accessibilityIdentifier = "labelNameAnswerMessage"
            self.nameMessageNode.isAccessibilityElement = true
            self.nameMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            self.answerTextMessageNode.truncationMode = .byWordWrapping
            self.answerTextMessageNode.maximumNumberOfLines = 3
            
            self.answerTextMessageNode.delegate = self
            self.answerTextMessageNode.isUserInteractionEnabled = true
            self.answerTextMessageNode.attributedText = TextString
            if senderTextMessageString.contains("http") {
                self.answerTextMessageNode.attributedText = senderTextMessageString.attributedHtmlString
            }
            self.answerTextMessageNode.accessibilityIdentifier = "labelanswerMessage"
            self.answerTextMessageNode.isAccessibilityElement = true
            self.answerTextMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            if !isIncomingMessage {
                self.answerTextMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
                self.nameMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
            } else {
                self.answerTextMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
                self.nameMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
            }
            print("afsasffas",answerUrl,AnswertextMessageString,senderTextMessageString,senderName)
            self.nameMessageNode.maximumNumberOfLines = 1
            self.addSubnode(nameMessageNode)
            self.addSubnode(answerTextMessageNode)
            networAnswerkImageMessageNode = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
            networAnswerkImageMessageNode.setURL(URL(string: answerUrl), resetToDefault: false)
            networAnswerkImageMessageNode.shouldCacheImage = true
            networAnswerkImageMessageNode.delegate = self
            self.addSubnode(networAnswerkImageMessageNode)
        }
        //
        self.addSubnode(sendIconNode)
        
        self.addSubnode(resendBackground)
        
        self.addSubnode(networkImageMessageNode1)
        self.addSubnode(networkImageMessageNode2)
        self.addSubnode(networkImageMessageNode3)
        self.addSubnode(networkImageMessageNode4)
        self.addSubnode(dateNode)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: AppManager.is_readNotification, object: nil)
    }
    @objc func answerMessageClicked() {
        self.ouputAnswerMessage!.answerMessageClicked(chatId:AnswerMessage_id)
    }
    @objc func makeIs_read() {
        print("makeIs_read")
        self.is_read = 1
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
            self.ouputfirstReply?.messagefirstReplyAction(text: "Фотография", messageId: self.message_id!, title: self.senderName,image:self.networkImageMessageNode1.image!)
        }
            }
            
        }
        is_selecting.toggle()
    }
    
    
    // MARK: Override AsycDisaplyKit Methods
    
    /**
     Overriding layoutSpecThatFits to specifiy relatiohsips between elements in the cell
     */
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let width = (constrainedSize.max.width / 2) / 1.2
        self.networkImageMessageNode1.style.width = ASDimension(unit: .points, value: width)
        self.networkImageMessageNode1.style.height = ASDimension(unit: .points, value: width/4*3)
        
        self.networkImageMessageNode2.style.width = ASDimension(unit: .points, value: width)
        self.networkImageMessageNode2.style.height = ASDimension(unit: .points, value: width/4*3)
        
        self.networkImageMessageNode3.style.width = ASDimension(unit: .points, value: width)
        self.networkImageMessageNode3.style.height = ASDimension(unit: .points, value: width/4*3)
        
        self.networkImageMessageNode4.style.width = ASDimension(unit: .points, value: width)
        self.networkImageMessageNode4.style.height = ASDimension(unit: .points, value: width/4*3)
        
        
        let insets = UIEdgeInsets(top: CGFloat.infinity, left: CGFloat.infinity, bottom: 8, right: 12)
        var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
        if !isIncomingMessage {
            let dateCombiner = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .spaceBetween, alignItems: .center, children: [dateNode,sendIconNode])
            
            dateLayout = ASInsetLayoutSpec(insets: insets, child: dateCombiner)
        }
        let date_layout = ASOverlayLayoutSpec(child: self.networkImageMessageNode4, overlay: dateLayout)
        
        
        let verticalLayout1 = ASStackLayoutSpec(direction: .vertical, spacing: 2.0, justifyContent: .start, alignItems: .center, children: [self.networkImageMessageNode1, self.networkImageMessageNode2])
        
        let verticalLayout2 = ASStackLayoutSpec(direction: .vertical, spacing: 2.0, justifyContent: .start, alignItems: .center, children: [self.networkImageMessageNode3, date_layout])
        if (!is_answer){
        if (is_resend == 1){
            let imageLayout = ASStackLayoutSpec(direction: .horizontal, spacing: 2.0, justifyContent: .start, alignItems: .center, children: [verticalLayout1,verticalLayout2])
            let verticalStack = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,imageLayout])
            return verticalStack
        }
        else {
        return ASStackLayoutSpec(direction: .horizontal, spacing: 2.0, justifyContent: .start, alignItems: .center, children: [verticalLayout1,verticalLayout2])
        }
        }
        else {
            let width = (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.1
            self.nameMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
            self.answerTextMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
            
            let answerkLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceAround, alignItems: .stretch, children: [nameMessageNode,answerTextMessageNode])
            answerkLayout.style.flexShrink = 1
            var answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout])
            var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
            if (is_has_image){
                self.networAnswerkImageMessageNode.style.width = ASDimension(unit: .points, value: 40)
                self.networAnswerkImageMessageNode.style.height = ASDimension(unit: .points, value: 40)
                answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout,networAnswerkImageMessageNode])
            }
            let localinsets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
            let insertLayout = ASInsetLayoutSpec(insets: localinsets, child: answerStack)
            backgroundAnswerNode.style.width = ASDimension(unit: .points, value: 2)
            backgroundAnswerNode.style.height = ASDimension(unit: .points, value: 40)
            var stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [insertLayout,ASStackLayoutSpec(direction: .horizontal, spacing: 2.0, justifyContent: .start, alignItems: .center, children: [verticalLayout1,verticalLayout2])])
            if (is_resend == 1){
                stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,insertLayout,ASStackLayoutSpec(direction: .horizontal, spacing: 2.0, justifyContent: .start, alignItems: .center, children: [verticalLayout1,verticalLayout2])])
            }
            // answerTextMessageNode.style.width = ASDimension(unit: .points, value: self.view.bounds.width)
            //   answerTextMessageNode.frame = CGRect(0, 0, textMessageNode.frame.width, 30)
            stackLayout.style.maxWidth = ASDimension(unit: .points, value: width)
            stackLayout.style.maxHeight = ASDimension(unit: .fraction, value: 1)
            return stackLayout
        }
    }
    
    // MARK: ASNetworkImageNodeDelegate
    /**
     Overriding didLoadImage to layout the node once the image is loaded
     */
    open func imageNode(_ imageNode: ASNetworkImageNode, didLoad image: UIImage) {
        self.setNeedsLayout()
    }
    
    // MARK: UILongPressGestureRecognizer Selector Methods
    
    /**
     Overriding canBecomeFirstResponder to make cell first responder
     */
    override open func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    /**
     Override method from superclass
     */
    open override func messageNodeLongPressSelector(_ recognizer: UITapGestureRecognizer) {
        if recognizer.state == UIGestureRecognizerState.began {
            
            let touchLocation = recognizer.location(in: view)
            if self.networkImageMessageNode1.frame.contains(touchLocation) {
                
                view.becomeFirstResponder()
                
                delay(0.1, closure: {
                    let menuController = UIMenuController.shared
                    menuController.menuItems = [UIMenuItem(title: "Copy", action: #selector(NetworkImageContentNode.copySelector))]
                    menuController.setTargetRect(self.networkImageMessageNode1.frame, in: self.view)
                    menuController.setMenuVisible(true, animated:true)
                })
            }
        }
    }
    
    /**
     Copy Selector for UIMenuController
     Puts the node's image on UIPasteboard
     */
    @objc open func copySelector() {
        if let image = self.networkImageMessageNode1.image {
            UIPasteboard.general.image = image
        }
    }
    
    /**
     Override messageTapSelector Method
     */
    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        if (is_selecting == false){
              let touchLocation = recognizer.location(in: view)
    if (self.networkImageMessageNode1.frame.contains(touchLocation))||(self.networkImageMessageNode2.frame.contains(touchLocation))||(self.networkImageMessageNode3.frame.contains(touchLocation))||(self.networkImageMessageNode4.frame.contains(touchLocation)){
        var index = 0
        if self.networkImageMessageNode1.frame.contains(touchLocation) {
            index = 0
        }
        else if self.networkImageMessageNode2.frame.contains(touchLocation) {
            index = 1
        }
        else if self.networkImageMessageNode3.frame.contains(touchLocation) {
            index = 2
        }
        else if self.networkImageMessageNode4.frame.contains(touchLocation) {
            index = 3
        }
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(index)
        self.currentViewController?.present(browser, animated: true, completion: nil)
    }
        else {
        let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
        
        let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
            self.ouput?.messageReplyAction(text: "photo".localized(), messageId: self.message_id!, title: self.senderName,image:self.networkImageMessageNode1.image!)
        })
        let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
            
            let dataToShare = [self.networkImageMessageNode1.image!, self.networkImageMessageNode2.image!]
            
            let activityController = UIActivityViewController(activityItems: dataToShare, applicationActivities: nil)
            self.currentViewController!.present(activityController, animated: true, completion: nil)
        })
        let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
            var messagesList = [Int]()
            messagesList.append(self.message_id!)
            let vc = ReplyController(chatType: .single,chatIds:messagesList)
            vc.title = "resend".localized()
            self.currentViewController!.show(vc, sender: nil)
        })
        let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
            var messagesList = [Int]()
            messagesList.append(self.message_id!)
            self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
        })
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        
        alert.addAction(replyAction)
        alert.addAction(copyAction)
        alert.addAction(resendAction)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.currentViewController?.present(alert, animated: true, completion: nil)
            }
    }
    }
}

