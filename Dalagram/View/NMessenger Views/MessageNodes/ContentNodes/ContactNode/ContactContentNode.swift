//
//  ContactNode.swift
//  Dalagram
//
//  Created by Toremurat on 29.08.2018.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import AsyncDisplayKit
import ContactsUI
import RealmSwift
open class ContactContentNode: ContentNode,CNContactViewControllerDelegate,ASTextNodeDelegate,ASNetworkImageNodeDelegate {
    var ouput: TextContentNodeOutput?
    
    var ouputfirstReply: TextContentFirstReply?
    var RemoveOuput: RemoveMessageOutput?
    /** UIFont for incoming text messages*/
    open var incomingTextFont = UIFont.n1B1Font() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIFont for outgoinf text messages*/
    open var outgoingTextFont = UIFont.n1B1Font() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for incoming text messages*/
    open var incomingTextColor = UIColor.n1DarkestGreyColor() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for outgoinf text messages*/
    open var outgoingTextColor = UIColor.black {
        didSet {
            self.updateAttributedText()
        }
    }
    open var message_id: Int? {
        didSet {
            self.updateAttributedText()
        }
    }
    open var is_exist: Int? {
        didSet {
            self.updateAttributedText()
        }
    }
    open var contact_user_id: String? {
        didSet {
            self.updateAttributedText()
        }
    }
    open var insets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8) {
        didSet {
            setNeedsLayout()
        }
    }
    // MARK: Private Variables
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var contactImageNode : ASNetworkImageNode = ASNetworkImageNode()
    open fileprivate(set) var contactName: ASTextNode = ASTextNode()
    open fileprivate(set) var contactPhone: ASTextNode = ASTextNode()
    open fileprivate(set) var UserCreds: ASTextNode = ASTextNode()
    open fileprivate(set) var dateNode: ASTextNode = ASTextNode()
    open fileprivate(set) var saveContact: ASButtonNode = ASButtonNode()
    open fileprivate(set) var messageContact: ASButtonNode = ASButtonNode()
    fileprivate var is_selecting: Bool = false
    fileprivate var contactPhoneString: String = ""
    fileprivate var contactNameString: String = ""
    fileprivate var senderName: String = ""
    open fileprivate(set) var spaceVertical: ASDisplayNode = ASDisplayNode()
    open fileprivate(set) var spaceHorizontal: ASDisplayNode = ASDisplayNode()
    open fileprivate(set) var resendBackground: ASButtonNode = ASButtonNode()
    fileprivate var is_resend: Int = 0
    fileprivate var is_read: Int = 0
    // MARK: Initialisers
    //answer content
    var ouputAnswerMessage: AnswerMessage?
    open fileprivate(set) var answerTextMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var nameMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var backgroundAnswerNode: ASDisplayNode = ASDisplayNode()
    fileprivate var is_has_image: Bool  = false
    fileprivate var AnswerMessage_id: Int = 0
    fileprivate var is_answer: Bool  = false
    fileprivate var AnswertextMessageString: String = ""
    fileprivate var senderTextMessageString: String = ""
    fileprivate var answerUrl: String = ""
    open fileprivate(set) var networAnswerkImageMessageNode:ASNetworkImageNode = ASNetworkImageNode()
    public init(id: Int,AnswerMessage_id:Int = 0,imageUrl: String = "", name: String, phone: String, date: String, currentVC: UIViewController,senderName:String,is_resend:Int = 0,is_exist:Int = 0,contact_user_id:String = "",is_read:Int = 0, bubbleConfiguration: BubbleConfigurationProtocol? = nil, userId: Int = 0,textMessageString: String = "",senderTextMessageString: String = "",url:String = "",is_answer:Bool = false) {
        contactPhoneString = phone
        contactNameString = name
        self.message_id = id
        self.is_exist = is_exist
        self.contact_user_id = contact_user_id
        self.is_resend = is_resend
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.currentViewController = currentVC
        if (is_answer == true){
            self.is_answer = is_answer
            self.AnswertextMessageString = textMessageString
            self.senderTextMessageString = senderTextMessageString
            self.answerUrl = url
            if let vc = currentViewController as? AnswerMessage {
                self.ouputAnswerMessage = vc
            }
            if (url != ""){
                is_has_image = true
            }
            self.AnswerMessage_id = AnswerMessage_id
        }
        self.setupImageNode(imageUrl, name: name, phone: phone, date: date)
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.is_read = is_read
        self.senderName = senderName
    }
    
    // MARK: Initialiser helper method
   
    open override func updateBubbleConfig(_ newValue: BubbleConfigurationProtocol) {
        var maskedBubbleConfig = newValue
        maskedBubbleConfig.isMasked = true
        super.updateBubbleConfig(maskedBubbleConfig)
    }
    
    fileprivate func setupImageNode(_ imageUrl: String, name: String, phone: String, date: String) {
        
        contactImageNode.clipsToBounds = true
        contactImageNode.contentMode = UIViewContentMode.scaleAspectFill
        contactImageNode.url = URL(string: imageUrl)
        contactImageNode.shouldCacheImage = false
        let blueColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1.0)
        //saveContact button
        saveContact.setTitle("Сохранить", with: UIFont.boldSystemFont(ofSize: 14), with: blueColor, for: .normal)
        saveContact.addTarget(self, action: #selector(addPhoneNumber), forControlEvents: .touchUpInside)
        //message button
        if (is_exist == 0){
        messageContact.setTitle("Пригласить", with: UIFont.boldSystemFont(ofSize: 14), with: blueColor, for: .normal)
        messageContact.addTarget(self, action: #selector(inviteContact), forControlEvents: .touchUpInside)
        }
        else {
            messageContact.setTitle("Написать", with: UIFont.boldSystemFont(ofSize: 14), with: blueColor, for: .normal)
            messageContact.addTarget(self, action: #selector(startChatWithContact), forControlEvents: .touchUpInside)
        }
        
        if imageUrl.isEmpty {
            contactImageNode.image = #imageLiteral(resourceName: "bg_gradient_2")
        }
        
        let nameTextAttr = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
        
        let phoneTextAttr = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor:  self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
        
        
        self.contactName.attributedText = NSMutableAttributedString(string: name, attributes: nameTextAttr)
        
        self.contactPhone.attributedText = NSMutableAttributedString(string: phone, attributes: phoneTextAttr)
        
        let userCredsAttr = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor.white]
        if !imageUrl.isEmpty {
            UserCreds.isHidden = true
        }
        else {
            UserCreds.isHidden = false
            self.UserCreds.attributedText = NSMutableAttributedString(string: "\(name.first!)", attributes: userCredsAttr)
        }
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.n1Black50Color()]
        dateNode.attributedText = NSAttributedString(string: date, attributes: dateTextAttr)
        dateNode.textContainerInset = UIEdgeInsets(top: 0, left: 2, bottom: 2, right: 2)
        self.resendBackground.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        self.resendBackground.setImage(UIImage(named:"icon_resended"), for: .normal)
        self.resendBackground.setTitle("  Пересланное", with: UIFont.systemFont(ofSize: 13), with: UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0), for: .normal)
        self.resendBackground.contentHorizontalAlignment = .left
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        
        if (is_answer){
            //answer Content Node
            backgroundAnswerNode.backgroundColor =  UIColor(red: 89/255.0, green: 198/255.0, blue: 75/255.0, alpha: 1.0)
            self.addSubnode(backgroundAnswerNode)
            let nameString = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(red: 54/255, green: 143/255, blue: 28/255, alpha: 1.0)]
            let fontAndSizeAndTextColor = [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
            let TextString = NSMutableAttributedString(string: AnswertextMessageString, attributes: fontAndSizeAndTextColor )
            self.nameMessageNode.delegate = self
            self.nameMessageNode.isUserInteractionEnabled = true
            self.nameMessageNode.attributedText = NSMutableAttributedString(string:  senderTextMessageString, attributes: nameString)
            self.nameMessageNode.accessibilityIdentifier = "labelNameAnswerMessage"
            self.nameMessageNode.isAccessibilityElement = true
            self.nameMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            self.answerTextMessageNode.truncationMode = .byWordWrapping
            self.answerTextMessageNode.maximumNumberOfLines = 3
            
            self.answerTextMessageNode.delegate = self
            self.answerTextMessageNode.isUserInteractionEnabled = true
            self.answerTextMessageNode.attributedText = TextString
            if senderTextMessageString.contains("http") {
                self.answerTextMessageNode.attributedText = senderTextMessageString.attributedHtmlString
            }
            self.answerTextMessageNode.accessibilityIdentifier = "labelanswerMessage"
            self.answerTextMessageNode.isAccessibilityElement = true
            self.answerTextMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            if !isIncomingMessage {
                self.answerTextMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
                self.nameMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
            } else {
                self.answerTextMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
                self.nameMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
            }
            print("afsasffas",answerUrl,AnswertextMessageString,senderTextMessageString,senderName)
            self.nameMessageNode.maximumNumberOfLines = 1
            self.addSubnode(nameMessageNode)
            self.addSubnode(answerTextMessageNode)
            networAnswerkImageMessageNode = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
            networAnswerkImageMessageNode.setURL(URL(string: answerUrl), resetToDefault: false)
            networAnswerkImageMessageNode.shouldCacheImage = true
            networAnswerkImageMessageNode.delegate = self
            self.addSubnode(networAnswerkImageMessageNode)
        }
        //
        self.addSubnode(sendIconNode)
        self.addSubnode(resendBackground)
        self.addSubnode(contactImageNode)
        self.addSubnode(UserCreds)
        self.addSubnode(dateNode)
        self.addSubnode(saveContact)
        self.addSubnode(messageContact)
        self.addSubnode(contactName)
        self.addSubnode(contactPhone)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
        spaceHorizontal.backgroundColor = UIColor(red: 108/255, green: 108/255, blue: 111/255, alpha: 1.0)
        spaceVertical.backgroundColor = UIColor(red: 108/255, green: 108/255, blue: 111/255, alpha: 1.0)
        self.addSubnode(spaceHorizontal)
        self.addSubnode(spaceVertical)
    }
    @objc func answerMessageClicked() {
        self.ouputAnswerMessage!.answerMessageClicked(chatId:AnswerMessage_id)
    }
    @objc func makeIs_read() {
        print("makeIs_read")
        self.is_read = 1
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
            self.ouputfirstReply?.messagefirstReplyAction(text: self.contactNameString, messageId: self.message_id!, title: self.senderName,image:self.contactImageNode.image!)
        }
            }}
        is_selecting.toggle()
    }
    
    fileprivate func updateAttributedText() {
        setNeedsLayout()
    }

    // MARK: Override AsycDisaplyKit Methods
    
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let width = constrainedSize.max.width
        let imageWidth: CGFloat = 50.0
        
        self.contactImageNode.style.width = ASDimension(unit: .points, value: imageWidth)
        self.contactImageNode.style.height = ASDimension(unit: .points, value: imageWidth)
        
        let image =  ASOverlayLayoutSpec(child: contactImageNode, overlay: ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: [], child: UserCreds))
        //for buttons
        self.saveContact.style.width = ASDimension(unit: .points, value: 90)
        self.saveContact.style.height = ASDimension(unit: .points, value: 30)
        
        self.messageContact.style.width = ASDimension(unit: .points, value: 85)
        self.messageContact.style.height = ASDimension(unit: .points, value: 30)
        
        DispatchQueue.main.async {
            self.contactImageNode.view.layer.cornerRadius = imageWidth/2
        }
        spaceHorizontal.style.width = ASDimension(unit: .points, value: 0.5)
        spaceHorizontal.style.height = ASDimension(unit: .points, value: 40)
        
        spaceVertical.style.width = ASDimension(unit: .points, value: 180)
        spaceVertical.style.height = ASDimension(unit: .points, value: 0.5)
        self.dateNode.style.alignSelf = .center
        
        let insets = UIEdgeInsets(top: 0, left: 70, bottom: 0, right: 0)
        var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
        if !isIncomingMessage {
            let dateCombiner = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .spaceBetween, alignItems: .center, children: [dateNode,sendIconNode])
            
            dateLayout = ASInsetLayoutSpec(insets: insets, child: dateCombiner)
        }
        let buttonsStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4.0, justifyContent: .start, alignItems: .start, children: [saveContact,spaceHorizontal, messageContact])
        
        let textStack = ASStackLayoutSpec(direction: .vertical, spacing: 2.0, justifyContent: .start, alignItems: .stretch, children: [contactName, contactPhone, dateLayout])
        
        let mainStack = ASStackLayoutSpec(direction: .horizontal, spacing: 8.0, justifyContent: .start, alignItems: .center, children: [image, textStack])
        
        let mainVerticalStack = ASStackLayoutSpec(direction: .vertical, spacing: 4.0, justifyContent: .start, alignItems: .center, children: [mainStack,spaceVertical,buttonsStack])
        if (!is_answer){
        if (is_resend == 1){
            let stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5), child: mainVerticalStack)])
            return stackLayout
        }
        else {
        return ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), child: mainVerticalStack)
        }
        }
        else {
            let width = (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.1
                self.nameMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
                self.answerTextMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
            let answerkLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceAround, alignItems: .stretch, children: [nameMessageNode,answerTextMessageNode])
            answerkLayout.style.flexShrink = 1
            var answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout])
            var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
            if (is_has_image){
                self.networAnswerkImageMessageNode.style.width = ASDimension(unit: .points, value: 40)
                self.networAnswerkImageMessageNode.style.height = ASDimension(unit: .points, value: 40)
                answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout,networAnswerkImageMessageNode])
            }
            let localinsets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
            let insertLayout = ASInsetLayoutSpec(insets: localinsets, child: answerStack)
            backgroundAnswerNode.style.width = ASDimension(unit: .points, value: 2)
            backgroundAnswerNode.style.height = ASDimension(unit: .points, value: 40)
            var stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [insertLayout,ASInsetLayoutSpec(insets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), child: mainVerticalStack)])
            if (is_resend == 1){
                stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,insertLayout,ASInsetLayoutSpec(insets: UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5), child: mainVerticalStack)])
            }
            // answerTextMessageNode.style.width = ASDimension(unit: .points, value: self.view.bounds.width)
            //   answerTextMessageNode.frame = CGRect(0, 0, textMessageNode.frame.width, 30)
            stackLayout.style.maxWidth = ASDimension(unit: .points, value: width)
            stackLayout.style.maxHeight = ASDimension(unit: .fraction, value: 1)
            return stackLayout
        }
    }
    
    // MARK: UIGestureRecognizer Selector Methods

    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        print("messageTapSelector")
        if (is_selecting == false){
                   let alert = UIAlertController(title:"choise_action".localized(), message: nil, preferredStyle: .actionSheet)
            
            let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                self.ouput?.messageReplyAction(text: self.contactNameString, messageId: self.message_id!, title: self.senderName,image:self.contactImageNode.image!)
                
            })
            let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                UIPasteboard.general.string = self.contactPhoneString
            })
            let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id!)
                let vc = ReplyController(chatType: .single,chatIds:messagesList)
                vc.title = "resend".localized()
                self.currentViewController!.show(vc, sender: nil)
            })
            let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id!)
                self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            
            alert.addAction(replyAction)
            alert.addAction(copyAction)
            alert.addAction(resendAction)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            alert.view.tintColor = UIColor.navBlueColor
            self.currentViewController?.present(alert, animated: true, completion: nil)
        }
    }
    @objc func startChatWithContact() {
        print("sffsfas",Int(self.contact_user_id!))
        NetworkManager.makeRequest(.getProfile(user_id: Int(self.contact_user_id!)), success: { (json) in
            let data = json["data"]
            let item = DialogItem()
            item.dialog_id   = self.contact_user_id!
            item.chat_id     = data["chat_id"].intValue
            item.chat_kind   = data["chat_kind"].stringValue
            item.action_name = data["action_name"].stringValue
            item.user_id     = data["user_id"].intValue
            item.chat_name   = data["nickname"].stringValue
            item.avatar      = data["avatar"].stringValue
            item.phone       = data["phone"].stringValue
            item.chat_text   = data["chat_text"].stringValue
            item.chat_date   = data["chat_date"].stringValue
            item.contact_user_name = data["nickname"].stringValue
            print("asffsa",item)
            let chatInfo = DialogInfo(dialog: item)
            let vc = ChatController(type: .single, info: chatInfo ,dialogId: self.contact_user_id! + "U")
            vc.hidesBottomBarWhenPushed = true
            self.currentViewController!.show(vc, sender: nil)
            
        })
    }
    @objc func inviteContact() {
        let text = "Привет, я использую Dalagram для переписки.Присоединяйся!Скачать его можно здесь : https://www.dalagram.com/"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        currentViewController!.present(activityViewController, animated: true, completion: nil)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func addPhoneNumber() {
        if #available(iOS 9.0, *) {
            let store = CNContactStore()
            let contact = CNMutableContact()
            let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :contactPhoneString ))
            
            contact.phoneNumbers = [homePhone]
            contact.givenName = contactNameString
            let controller = CNContactViewController(forUnknownContact : contact)
            controller.contactStore = store
            controller.delegate = self
            controller.navigationController?.setBlueNavBar()
            currentViewController?.navigationController?.setNavigationBarHidden(false, animated: true)
            currentViewController?.navigationController?.navigationBar.barTintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            currentViewController!.show(controller, sender: nil)
        }
    }
    
    
}

