//
// Copyright (c) 2016 eBay Software Foundation
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

import UIKit
import AsyncDisplayKit
import SKPhotoBrowser

//MARK: ImageContentNode
/**
 ImageContentNode for NMessenger. Extends ContentNode.
 Defines content that is an image.
 */
open class ImageContentNode: ContentNode, UploadStatusDelegate {

    // MARK: Public Variables
    /** UIImage as the image of the cell*/
    open var image: UIImage? {
        get {
            return imageMessageNode.image
        } set {
            imageMessageNode.image = newValue
        }
    }
    open var message_id: Int? {
        didSet {
            //      self.updateAttributedText()
        }
    }
    var RemoveOuput: RemoveMessageOutput?
    fileprivate var is_selecting: Bool = false
    var ouput: TextContentNodeOutput?
    fileprivate var is_read: Int = 0
    fileprivate var is_sticker : Int = 0
    var ouputfirstReply: TextContentFirstReply?
    // MARK: Private Variables
    /** ASImageNode as the content of the cell*/
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var imageMessageNode: ASImageNode = ASImageNode()
    open fileprivate(set) var dateNode: ASTextNode = ASTextNode()
    open fileprivate(set) var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
    fileprivate var senderName: String = ""
    
    fileprivate var networkImage: UIImage? = nil
    // MARK: Initialisers
    
    /**
     Initialiser for the cell.
     - parameter image: Must be UIImage. Sets image for cell.
     Calls helper method to setup cell
     */
    public init(image: UIImage, date: String, currentVC: UIViewController,senderName:String,is_sticker:Int = 0, bubbleConfiguration: BubbleConfigurationProtocol? = nil) {
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.is_sticker = is_sticker
        self.setupImageNode(image, date: date)
        self.currentViewController = currentVC
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.networkImage = image
        self.senderName = senderName
    }
    
    // MARK: Initialiser helper method
    /** Override updateBubbleConfig to set bubble mask */
    open override func updateBubbleConfig(_ newValue: BubbleConfigurationProtocol) {
        var maskedBubbleConfig = newValue
        maskedBubbleConfig.isMasked = true
        super.updateBubbleConfig(maskedBubbleConfig)
    }
    
    /**
     Sets the image to be display in the cell. Clips and rounds the corners.
     - parameter image: Must be UIImage. Sets image for cell.
     */
    fileprivate func setupImageNode(_ image: UIImage, date: String)
    {
        
        imageMessageNode.image = image
        imageMessageNode.clipsToBounds = true
        imageMessageNode.contentMode = UIViewContentMode.scaleAspectFill
        if (is_sticker == 1){
            imageMessageNode.contentMode = .scaleAspectFit
        }
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.white]
        dateNode.attributedText = NSAttributedString(string: date, attributes: dateTextAttr)
        dateNode.backgroundColor = UIColor.darkGray
        dateNode.cornerRadius = 5.0
        dateNode.clipsToBounds = true
        dateNode.textContainerInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        
        self.imageMessageNode.accessibilityIdentifier = "imageNode"
        self.imageMessageNode.isAccessibilityElement = true
        
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        self.addSubnode(sendIconNode)
        
        self.addSubnode(imageMessageNode)
        self.addSubnode(dateNode)
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = CGPoint(x: 20, y: 20)
        self.view.addSubview(activityIndicator)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: AppManager.is_readNotification, object: nil)
    }
    @objc func makeIs_read() {
        print("makeIs_read")
        self.is_read = 1
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
            self.ouputfirstReply?.messagefirstReplyAction(text: "Фотография", messageId: self.message_id!, title: self.senderName,image:self.imageMessageNode.image!)
        }
            }
            
        }
        is_selecting.toggle()
    }
    
    // MARK: Upload Status Delegates
    
    func uploadBegin() {
        print("start")
        activityIndicator.startAnimating()
    }
    
    func uploadEnd() {
        print("ended")
        activityIndicator.stopAnimating()
    }
    
    // MARK: Override AsycDisaplyKit Methods
    
    /**
     Overriding layoutSpecThatFits to specifiy relatiohsips between elements in the cell
     */
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        if (is_sticker == 0){
        let width = constrainedSize.max.width / 1.2
        
        self.imageMessageNode.style.maxWidth = ASDimension(unit: .points, value: width - 6)
        self.imageMessageNode.style.height = ASDimension(unit: .points, value: width/4*3 - 6)
        
        let ratio = networkImage!.size.height / networkImage!.size.width
        
        let imageRatioSpec = ASRatioLayoutSpec(ratio:CGFloat(ratio), child:self.imageMessageNode)
        
        imageRatioSpec.style.maxWidth = ASDimension(unit: .points, value: width - 6)
        
        imageRatioSpec.style.maxWidth = ASDimension(unit: .points, value: width - 6)
        let insets = UIEdgeInsets(top: CGFloat.infinity, left: CGFloat.infinity, bottom: 8, right: 8)
        let inserLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 3, left: 3, bottom: 3, right: 3), child: imageRatioSpec)
        var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
        if !isIncomingMessage {
            let dateCombiner = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .spaceBetween, alignItems: .center, children: [dateNode,sendIconNode])
            
            dateLayout = ASInsetLayoutSpec(insets: insets, child: dateCombiner)
        }
        
        return ASOverlayLayoutSpec(child: inserLayout, overlay: dateLayout)
        }
        else {
            let width = constrainedSize.max.width / 2
            self.imageMessageNode.style.width = ASDimension(unit: .points, value: width)
            self.imageMessageNode.style.height = ASDimension(unit: .points, value: width)
            
            let insets = UIEdgeInsets(top: CGFloat.infinity, left: CGFloat.infinity, bottom: 8, right: 8)
            let dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
            
            return ASOverlayLayoutSpec(child: self.imageMessageNode, overlay: dateLayout)
        }
    }
    
    // MARK: UILongPressGestureRecognizer Selector Methods
    
    /**
     Overriding canBecomeFirstResponder to make cell first responder
     */
    override open func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    /**
     Override method from superclass
     */
    open override func messageNodeLongPressSelector(_ recognizer: UITapGestureRecognizer) {
        if recognizer.state == UIGestureRecognizerState.began {
            
            let touchLocation = recognizer.location(in: view)
            if self.imageMessageNode.frame.contains(touchLocation) {
                
                view.becomeFirstResponder()
                
                delay(0.1, closure: {
                    let menuController = UIMenuController.shared
                    menuController.menuItems = [UIMenuItem(title: "Copy", action: #selector(ImageContentNode.copySelector))]
                    menuController.setTargetRect(self.imageMessageNode.frame, in: self.view)
                    menuController.setMenuVisible(true, animated:true)
                })
            }
        }
    }
    
    /**
     Override messageTapSelector Method
     */
    
    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        if (is_selecting == false){
            let touchLocation = recognizer.location(in: view)
            if (is_sticker == 0){
            if (self.imageMessageNode.frame.contains(touchLocation)){
                let photo = SKPhoto.photoWithImage(self.imageMessageNode.image!)
            let browser = SKPhotoBrowser(photos: [photo])
            browser.initializePageIndex(0)
            self.currentViewController?.present(browser, animated: true, completion: nil)
                }
        }
        else {
                let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
            
            let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                self.ouput?.messageReplyAction(text: "photo".localized(), messageId: self.message_id!, title: self.senderName,image:self.imageMessageNode.image!)
            })
                let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                    
                let dataToShare = [self.imageMessageNode.image!]
                
                let activityController = UIActivityViewController(activityItems: dataToShare, applicationActivities: nil)
                self.currentViewController!.present(activityController, animated: true, completion: nil)
            })
                let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id!)
                let vc = ReplyController(chatType: .single,chatIds:messagesList)
                    vc.title = "resend".localized()
                self.currentViewController!.show(vc, sender: nil)
            })
                let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
                })
                let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
                
                alert.addAction(replyAction)
                alert.addAction(copyAction)
                alert.addAction(resendAction)
                alert.addAction(deleteAction)
                alert.addAction(cancelAction)
            alert.view.tintColor = UIColor.navBlueColor
            self.currentViewController?.present(alert, animated: true, completion: nil)
        }
    }
    }
    
    /**
     Copy Selector for UIMenuController
     Puts the node's image on UIPasteboard
     */
    @objc open func copySelector() {
        if let image = self.image {
            UIPasteboard.general.image = image
        }
    }
    
}

