//
//  TwoImagesContentNode.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/6/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import SKPhotoBrowser

//MARK: ImageContentNode
/**
 ImageContentNode for NMessenger. Extends ContentNode.
 Defines content that is an image.
 */
open class TwoImagesContentNode: ContentNode, UploadStatusDelegate {
    
    // MARK: Public Variables
    /** UIImage as the image of the cell*/
    open var image: UIImage? {
        get {
            return imageMessageNode1.image
        } set {
            imageMessageNode1.image = newValue
        }
    }
    open var message_id: Int? {
        didSet {
            //      self.updateAttributedText()
        }
    }
    // MARK: Private Variables
    /** ASImageNode as the content of the cell*/
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var imageMessageNode1: ASImageNode = ASImageNode()
    open fileprivate(set) var imageMessageNode2: ASImageNode = ASImageNode()
    open fileprivate(set) var dateNode: ASTextNode = ASTextNode()
    open fileprivate(set) var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
    fileprivate var is_selecting: Bool = false
    var ouput: TextContentNodeOutput?
    fileprivate var is_read: Int = 0
    var ouputfirstReply: TextContentFirstReply?
    var RemoveOuput: RemoveMessageOutput?
    fileprivate var senderName: String = ""
    // MARK: Initialisers
    
    /**
     Initialiser for the cell.
     - parameter image: Must be UIImage. Sets image for cell.
     Calls helper method to setup cell
     */
    public init(image1: UIImage,image2: UIImage, date: String, currentVC: UIViewController,senderName:String, bubbleConfiguration: BubbleConfigurationProtocol? = nil) {
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.setupImageNode(image1,image2, date: date)
        self.currentViewController = currentVC
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.senderName = senderName
    }
    
    // MARK: Initialiser helper method
    /** Override updateBubbleConfig to set bubble mask */
    open override func updateBubbleConfig(_ newValue: BubbleConfigurationProtocol) {
        var maskedBubbleConfig = newValue
        maskedBubbleConfig.isMasked = true
        super.updateBubbleConfig(maskedBubbleConfig)
    }
    
    /**
     Sets the image to be display in the cell. Clips and rounds the corners.
     - parameter image: Must be UIImage. Sets image for cell.
     */
    fileprivate func setupImageNode(_ image1: UIImage,_ image2: UIImage, date: String)
    {
        imageMessageNode1.image = image1
        imageMessageNode1.clipsToBounds = true
        imageMessageNode1.contentMode = UIViewContentMode.scaleAspectFill
        //for image 2
        imageMessageNode2.image = image2
        imageMessageNode2.clipsToBounds = true
        imageMessageNode2.contentMode = UIViewContentMode.scaleAspectFill
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.white]
        dateNode.attributedText = NSAttributedString(string: date, attributes: dateTextAttr)
        dateNode.backgroundColor = UIColor.darkGray
        dateNode.cornerRadius = 5.0
        dateNode.clipsToBounds = true
        dateNode.textContainerInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        
        self.imageMessageNode1.accessibilityIdentifier = "imageNode"
        self.imageMessageNode1.isAccessibilityElement = true
        
        self.imageMessageNode2.accessibilityIdentifier = "imageNode"
        self.imageMessageNode2.isAccessibilityElement = true
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        self.addSubnode(sendIconNode)
        
        self.addSubnode(imageMessageNode1)
        self.addSubnode(imageMessageNode2)
        self.addSubnode(dateNode)
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = CGPoint(x: 20, y: 20)
        self.view.addSubview(activityIndicator)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: AppManager.is_readNotification, object: nil)
    }
    @objc func makeIs_read() {
        print("makeIs_read")
        self.is_read = 1
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
            self.ouputfirstReply?.messagefirstReplyAction(text: "Фотография", messageId: self.message_id!, title: self.senderName,image:self.imageMessageNode1.image!)
        }
            }
            
        }
        is_selecting.toggle()
    }
    
    // MARK: Upload Status Delegates
    
    func uploadBegin() {
        print("start")
        activityIndicator.startAnimating()
    }
    
    func uploadEnd() {
        print("ended")
        activityIndicator.stopAnimating()
    }
    
    // MARK: Override AsycDisaplyKit Methods
    
    /**
     Overriding layoutSpecThatFits to specifiy relatiohsips between elements in the cell
     */
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
            let width = (constrainedSize.max.width / 2) / 1.2
        self.imageMessageNode1.style.width = ASDimension(unit: .points, value: width)
        self.imageMessageNode1.style.height = ASDimension(unit: .points, value: width/4*6)
        
        self.imageMessageNode2.style.width = ASDimension(unit: .points, value: width)
        self.imageMessageNode2.style.height = ASDimension(unit: .points, value: width/4*6)
        
        let insets = UIEdgeInsets(top: CGFloat.infinity, left: CGFloat.infinity, bottom: 8, right: 8)
        var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
        if !isIncomingMessage {
            let dateCombiner = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .spaceBetween, alignItems: .center, children: [dateNode,sendIconNode])
            
            dateLayout = ASInsetLayoutSpec(insets: insets, child: dateCombiner)
        }
        let date_layout = ASOverlayLayoutSpec(child: self.imageMessageNode2, overlay: dateLayout)
        
        return ASStackLayoutSpec(direction: .horizontal, spacing: 8.0, justifyContent: .start, alignItems: .center, children: [self.imageMessageNode1,date_layout])
    }
    
    // MARK: UILongPressGestureRecognizer Selector Methods
    
    /**
     Overriding canBecomeFirstResponder to make cell first responder
     */
    override open func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    /**
     Override method from superclass
     */
    open override func messageNodeLongPressSelector(_ recognizer: UITapGestureRecognizer) {
            if (is_selecting == false){
        if recognizer.state == UIGestureRecognizerState.began {
            
            let touchLocation = recognizer.location(in: view)
            if self.imageMessageNode1.frame.contains(touchLocation) {
                
                view.becomeFirstResponder()
                
                delay(0.1, closure: {
                    let menuController = UIMenuController.shared
                    menuController.menuItems = [UIMenuItem(title: "Copy", action: #selector(ImageContentNode.copySelector))]
                    menuController.setTargetRect(self.imageMessageNode1.frame, in: self.view)
                    menuController.setMenuVisible(true, animated:true)
                })
            }
        }
        }
    }
    
    /**
     Override messageTapSelector Method
     */
    
    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        if (is_selecting == false){
            let touchLocation = recognizer.location(in: view)
            if (self.imageMessageNode1.frame.contains(touchLocation))||(self.imageMessageNode2.frame.contains(touchLocation)){
                var index = 0
                if self.imageMessageNode1.frame.contains(touchLocation) {
                    index = 0
                }
                else if self.imageMessageNode2.frame.contains(touchLocation) {
                    index = 1
                }

                var images = [SKPhoto]()
                let photo1 = SKPhoto.photoWithImage(imageMessageNode1.image!)
                let photo2 = SKPhoto.photoWithImage(imageMessageNode2.image!)
                images.append(photo1)
                images.append(photo2)
                let browser = SKPhotoBrowser(photos: images)
                browser.initializePageIndex(index)
                self.currentViewController?.present(browser, animated: true, completion: nil)
            }
            else {
            let alert = UIAlertController(title:"choise_action".localized(), message: nil, preferredStyle: .actionSheet)
                
                let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                    self.ouput?.messageReplyAction(text: "photo".localized(), messageId: self.message_id!, title: self.senderName,image:self.imageMessageNode1.image!)
                })
                let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                    
                    let dataToShare = [self.imageMessageNode1.image!,self.imageMessageNode2.image!]
                    
                    let activityController = UIActivityViewController(activityItems: dataToShare, applicationActivities: nil)
                    self.currentViewController!.present(activityController, animated: true, completion: nil)
                })
                let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    let vc = ReplyController(chatType: .single,chatIds:messagesList)
                    vc.title = "resend".localized()
                    self.currentViewController!.show(vc, sender: nil)
                })
                let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
                })
                let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
                
                alert.addAction(replyAction)
                alert.addAction(copyAction)
                alert.addAction(resendAction)
                alert.addAction(deleteAction)
                alert.addAction(cancelAction)
                alert.view.tintColor = UIColor.navBlueColor
                self.currentViewController?.present(alert, animated: true, completion: nil)
            }
        }
        
        
    }
    
    /**
     Copy Selector for UIMenuController
     Puts the node's image on UIPasteboard
     */
    @objc open func copySelector() {
        if let image = self.image {
            UIPasteboard.general.image = image
        }
    }
    
}
