//
// Copyright (c) 2016 eBay Software Foundation
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

import UIKit
import AsyncDisplayKit

//MARK: TextMessageNode
/**
 TextMessageNode class for N Messenger. Extends MessageNode.
 Defines content that is a text.
 */
protocol TextContentNodeOutput {
    func messageReplyAction(text: String,messageId: Int,title:String,image:UIImage)
}
protocol TextContentFirstReply {
    func messagefirstReplyAction(text: String,messageId: Int,title:String,image:UIImage)
}
protocol TextContentCount {
    func changeTextContentCount(id: Int, textMessageString: String, dateString: String,is_read:Int,senderName:String,is_resend:Int,maxLineCount:Int, bubbleConfiguration: BubbleConfigurationProtocol, viewsCount: Int,message:GeneralMessengerCell)
}
open class TextContentNode: ContentNode, ASTextNodeDelegate {

    var ouput: TextContentNodeOutput?
    var RemoveOuput: RemoveMessageOutput?
    var ouputfirstReply: TextContentFirstReply?
    
    var outputContentCount: TextContentCount?
    // MARK: Public Variables
    /** Insets for the node */
    open var insets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8) {
        didSet {
            setNeedsLayout()
        }
    }
    
    /** UIFont for incoming text messages*/
    open var incomingTextFont = UIFont.n1B1Font() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIFont for outgoinf text messages*/
    open var outgoingTextFont = UIFont.n1B1Font() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for incoming text messages*/
    open var incomingTextColor = UIColor.n1DarkestGreyColor() {
        didSet {
            self.updateAttributedText()
        }
    }
    /** UIColor for outgoinf text messages*/
    open var outgoingTextColor = UIColor.black {
        didSet {
            self.updateAttributedText()
        }
    }
    /** String to present as the content of the cell*/
    open var textMessageString: NSAttributedString? {
        get {
            return self.textMessageNode.attributedText
        } set {
            self.textMessageNode.attributedText = newValue
        }
    }
    
    open var message_id: Int? {
        didSet {
            self.updateAttributedText()
        }
    }
    open var message: GeneralMessengerCell? {
        didSet {
            self.updateAttributedText()
        }
    }
    /** Overriding from super class
     Set backgroundBubble.bubbleColor and the text color when valus is set
     */
    open override var isIncomingMessage: Bool
        {
        didSet {
            if isIncomingMessage {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getIncomingColor()
                self.updateAttributedText()
            } else {
                self.backgroundBubble?.bubbleColor = self.bubbleConfiguration.getOutgoingColor()
                self.updateAttributedText()
            }
        }
    }
    
    // MARK: Private Variables
    /** ASTextNode as the content of the cell*/
    open fileprivate(set) var textMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var dateMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var viewsMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var viewsIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var nextButtonnode: ASButtonNode = ASButtonNode()
    
    open fileprivate(set) var resendBackground: ASButtonNode = ASButtonNode()
    
    /** Bool as mutex for handling attributed link long presses*/
    fileprivate var lockKey: Bool = false
    fileprivate var is_read: Int = 0
    fileprivate var is_selecting: Bool = false
    fileprivate var senderName: String = ""
    fileprivate var textMessage: String = ""
    fileprivate var dateString: String = ""
    fileprivate var viewCount: Int = 0
    fileprivate var is_resend: Int = 0
    fileprivate var maxLineCount: Int = 15
    // MARK: Initialisers
    
    /**
     Initialiser for the cell.
     - parameter textMessageString: Must be String. Sets text for cell.
     Calls helper method to setup cell
     */
    public init(id: Int, textMessageString: String, dateString: String,is_read:Int = 0,senderName:String, bubbleConfiguration: BubbleConfigurationProtocol? = nil, viewsCount: Int = 0) {
        
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.is_read = is_read
        self.setupTextNode(textMessageString, dateString: dateString, viewsCount: viewsCount)
        self.message_id = id
        self.textMessage = textMessageString
    }
    /**
     Initialiser for the cell.
     - parameter textMessageString: Must be String. Sets text for cell.
     - parameter currentViewController: Must be an UIViewController. Set current view controller holding the cell.
     Calls helper method to setup cell
     */
    public init(id: Int, textMessageString: String, dateString: String,is_read:Int = 0, currentViewController: UIViewController,senderName:String,is_resend:Int = 0,maxLineCount: Int = 15, bubbleConfiguration: BubbleConfigurationProtocol? = nil, viewsCount: Int = 0)
    {
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.currentViewController = currentViewController
        self.is_read = is_read
        self.message_id = id
        if (maxLineCount != 0){
            self.maxLineCount = maxLineCount
        }
        self.setupTextNode(textMessageString, dateString: dateString, viewsCount: viewsCount)
        self.dateString = dateString
        self.viewCount = viewsCount
        self.is_resend = is_resend
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? TextContentCount {
            self.outputContentCount = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.textMessage = textMessageString
        self.senderName = senderName
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: AppManager.is_readNotification, object: nil)
    }
    func textWidth(text: String, font: UIFont?) -> CGFloat {
        let attributes = font != nil ? [NSAttributedStringKey.font: font] : [:]
        return text.size(withAttributes: attributes as [NSAttributedStringKey : Any]).width
    }
    @objc func makeIs_read() {
         print("makeIs_read")
         self.is_read = 1
         self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
            if let stringText = self.textMessageNode.attributedText?.string {
                let image: UIImage = UIImage(named: "icon_create")!
                self.ouputfirstReply?.messagefirstReplyAction(text: stringText, messageId: self.message_id!, title: self.senderName,image:image)
            }
                }
                
            }
    }
        is_selecting.toggle()
    }
    // MARK: Initialiser helper method
    
    /**
     Creates the text to be display in the cell. Finds links and phone number in the string and creates atrributed string.
      - parameter textMessageString: Must be String. Sets text for cell.
     */
    fileprivate func setupTextNode(_ textMessageString: String, dateString: String, viewsCount: Int)
    {
        self.backgroundBubble = self.bubbleConfiguration.getBubble()
        textMessageNode.delegate = self
        textMessageNode.isUserInteractionEnabled = true
        textMessageNode.linkAttributeNames = ["LinkAttribute","PhoneNumberAttribute"]
        textMessageNode.addTarget(self, action:#selector(messageClicked), forControlEvents: .touchUpInside)
        let fontAndSizeAndTextColor = [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width - 140
        if (textWidth(text:textMessageString,font:UIFont.n1B1Font()) < screenWidth) && (textMessageNode.lineCount < 3){
            var outputString = NSMutableAttributedString(string: "\(textMessageString)", attributes: fontAndSizeAndTextColor )
            let types: NSTextCheckingResult.CheckingType = [.link, .phoneNumber]
            let detector = try! NSDataDetector(types: types.rawValue)
            let matches = detector.matches(in: "\(textMessageString)", options: [], range: NSMakeRange(0, textMessageString.count))
            for match in matches {
                if let url = match.url {
                    outputString.addAttribute(NSAttributedStringKey(rawValue: "LinkAttribute"), value: url, range: match.range)
                    outputString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: match.range)
                    outputString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: match.range)
                }
                if let phoneNumber = match.phoneNumber  {
                    outputString.addAttribute(NSAttributedStringKey(rawValue: "PhoneNumberAttribute"), value: phoneNumber, range: match.range)
                    outputString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: match.range)
                    outputString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: match.range)
                }
            }
            self.textMessageNode.attributedText = outputString
        }
        else {
            let outputString = NSMutableAttributedString(string: "\(textMessageString)\n", attributes: fontAndSizeAndTextColor )
            let types: NSTextCheckingResult.CheckingType = [.link, .phoneNumber]
            let detector = try! NSDataDetector(types: types.rawValue)
            let matches = detector.matches(in: textMessageString, options: [], range: NSMakeRange(0, textMessageString.count))
            for match in matches {
                if let url = match.url {
                    outputString.addAttribute(NSAttributedStringKey(rawValue: "LinkAttribute"), value: url, range: match.range)
                    outputString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: match.range)
                    outputString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: match.range)
                }
                if let phoneNumber = match.phoneNumber  {
                    outputString.addAttribute(NSAttributedStringKey(rawValue: "PhoneNumberAttribute"), value: phoneNumber, range: match.range)
                    outputString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: match.range)
                    outputString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: match.range)
                }
            }
            self.textMessageNode.attributedText = outputString
            
        }
        if textMessageString.contains("http") {
            if (textWidth(text:textMessageString,font:UIFont.n1B1Font()) < screenWidth) && (textMessageNode.lineCount < 3){
                self.textMessageNode.attributedText = "\(textMessageString)".attributedHtmlString
            }
            else {
                self.textMessageNode.attributedText = "\(textMessageString)\n".attributedHtmlString
            }
        }
        self.textMessageNode.accessibilityIdentifier = "labelMessage"
        self.textMessageNode.isAccessibilityElement = true
        self.addSubnode(textMessageNode)
        
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12), NSAttributedStringKey.foregroundColor: UIColor.n1Black50Color()]
        self.dateMessageNode.attributedText = NSMutableAttributedString(string: dateString, attributes: dateTextAttr)
        self.dateMessageNode.accessibilityIdentifier = "dateMessage"
        self.addSubnode(dateMessageNode)
        if (is_read == 0){
        self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        self.addSubnode(sendIconNode)
        
        self.viewsMessageNode.attributedText = NSMutableAttributedString(string:  "\(viewsCount)", attributes: dateTextAttr)
        self.viewsMessageNode.accessibilityIdentifier = "viewsMessage"
        self.addSubnode(viewsMessageNode)
        print("afsffas",textMessageNode.maximumNumberOfLines)
        self.viewsIconNode.image = UIImage(named: "icon_views")
        self.addSubnode(viewsIconNode)
        
        // Show view icon only for channel chat
        
        if viewsCount == 0 {
             self.viewsIconNode.isHidden = true
             self.viewsMessageNode.isHidden = true
        }
        print("asfsasfas",self.textMessageNode.lineCount)
        if ((self.textMessageNode.attributedText?.length)!  < 150){
            self.nextButtonnode.isHidden = true
            self.nextButtonnode.setTitle("  ", with: UIFont.systemFont(ofSize: 12), with: UIColor.n1Black50Color(), for: .normal)
        }
        else {
            self.nextButtonnode.isHidden = false
            if (self.maxLineCount  == 15){
                self.nextButtonnode.setTitle("next_btn".localized(), with: UIFont.systemFont(ofSize: 15), with: UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0), for: .normal)
                
                
            }
            else {
                self.nextButtonnode.setTitle("back_btn".localized(), with: UIFont.systemFont(ofSize: 15), with: UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0), for: .normal)
            }
        }
        self.textMessageNode.maximumNumberOfLines = UInt(maxLineCount)
        self.resendBackground.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        self.resendBackground.setImage(UIImage(named:"icon_resended"), for: .normal)
        self.resendBackground.setTitle("  Пересланное", with: UIFont.systemFont(ofSize: 13), with: UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0), for: .normal)
       self.resendBackground.contentHorizontalAlignment = .left
       self.addSubnode(resendBackground)
        nextButtonnode.addTarget(self, action: #selector(nextButtonClick), forControlEvents: .touchUpInside)
        self.addSubnode(resendBackground)
        self.addSubnode(self.nextButtonnode)
    }
    @objc func nextButtonClick() {
        if ( self.textMessageNode.lineCount == 15){
         //   self.textMessageNode.maximumNumberOfLines = 0
            self.nextButtonnode.setTitle("Меньше", with: UIFont.systemFont(ofSize: 15), with: UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0), for: .normal)
       //     setNeedsDisplay()
            self.outputContentCount?.changeTextContentCount(id: message_id!, textMessageString: textMessage, dateString: dateString, is_read: is_read, senderName: senderName,is_resend: self.is_resend,maxLineCount:self.maxLineCount, bubbleConfiguration: bubbleConfiguration, viewsCount: viewCount,message: message!)
        }
        else {
            self.nextButtonnode.setTitle("Больше", with: UIFont.systemFont(ofSize: 15), with: UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0), for: .normal)
            self.textMessageNode.maximumNumberOfLines = 15
      //     setNeedsDisplay()
            self.outputContentCount?.changeTextContentCount(id: message_id!, textMessageString: textMessage, dateString: dateString, is_read: is_read, senderName: senderName,is_resend: self.is_resend,maxLineCount:self.maxLineCount,bubbleConfiguration: bubbleConfiguration, viewsCount: viewCount,message:message!)
        
        }
        
    }
    //MARK: Helper Methods
    /** Updates the attributed string to the correct incoming/outgoing settings and lays out the component again*/
    
    fileprivate func updateAttributedText() {
        let tmpString = NSMutableAttributedString(attributedString: self.textMessageNode.attributedText!)
        tmpString.addAttributes([NSAttributedStringKey.foregroundColor: isIncomingMessage ? incomingTextColor : outgoingTextColor, NSAttributedStringKey.font: isIncomingMessage ? incomingTextFont : outgoingTextFont], range: NSMakeRange(0, tmpString.length))
        self.textMessageNode.attributedText = tmpString
        self.sendIconNode.isHidden = isIncomingMessage
        setNeedsLayout()
    }
    
    // MARK: Override AsycDisaplyKit Methods
    
    /**
     Overriding layoutSpecThatFits to specifiy relatiohsips between elements in the cell
     */
    
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let insertLayout = ASInsetLayoutSpec(insets: UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0), child: nextButtonnode)
        
        let width = (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.1

        let date = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .end, alignItems: .center, children: [dateMessageNode, sendIconNode])
        
        var dateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .spaceBetween, alignItems: .center, children: [insertLayout,date])
        
        // Show views icon for channel
        print("fasfsafsaa",isIncomingMessage)
        
        if isIncomingMessage {
            dateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .spaceBetween, alignItems: .center, children: [insertLayout,dateMessageNode])
        }
        if !viewsIconNode.isHidden {
            dateStack = ASStackLayoutSpec(direction: .horizontal, spacing: 6, justifyContent: .end, alignItems: .center, children: [nextButtonnode,viewsIconNode,viewsMessageNode, dateMessageNode, sendIconNode])
        }
        var stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: -18, justifyContent: .end, alignItems: .stretch, children: [textMessageNode, dateStack])
            if (self.maxLineCount  == 15) && (self.textMessageNode.attributedText?.length)!  > 150{
            
            stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 5, justifyContent: .end, alignItems: .stretch, children: [textMessageNode, dateStack])
            }
        if (is_resend == 1){
           stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,textMessageNode, dateStack])
        }
         var insetsLast = UIEdgeInsets(top: 8, left: 8, bottom: 10, right: 8)
        print("asfsfsa",textWidth(text:textMessage,font:UIFont.n1B1Font()),width)
        var widthForText = width - 80
        if (!viewsIconNode.isHidden){
            widthForText = width - 80
        }
        if (textWidth(text:textMessage,font:UIFont.n1B1Font()) < widthForText) && (textMessageNode.lineCount < 3){
        stackLayout = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: .start, alignItems: .center, children: [textMessageNode, dateStack])
        insetsLast = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            if (is_resend == 1){
               let verticalLayout = ASStackLayoutSpec(direction: .horizontal, spacing: 0, justifyContent: .start, alignItems: .center, children: [textMessageNode, dateStack])
               stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: -3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground, verticalLayout])
            }
        }
        
        stackLayout.style.maxWidth = ASDimension(unit: .points, value: width)
        stackLayout.style.maxHeight = ASDimension(unit: .fraction, value: 1)
        return ASInsetLayoutSpec(insets: insetsLast, child: stackLayout)
        
    }
    
    @objc func messageClicked() {
        if (is_selecting == false){
            let alert = UIAlertController(title:"choise_action".localized(), message: nil, preferredStyle: .actionSheet)
            
            let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                if let stringText = self.textMessageNode.attributedText?.string {
                    let image: UIImage = UIImage(named: "icon_create")!
                    self.ouput?.messageReplyAction(text: stringText, messageId: self.message_id!, title: self.senderName,image:image)
                }
            })
            let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                UIPasteboard.general.string = self.textMessageNode.attributedText?.string
            })
            let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id!)
                let vc = ReplyController(chatType: .single,chatIds:messagesList)
                vc.title = "resend".localized()
                self.currentViewController!.show(vc, sender: nil)
            })
            let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id!)
                self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            
            alert.addAction(replyAction)
            alert.addAction(copyAction)
            alert.addAction(resendAction)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
            alert.view.tintColor = UIColor.navBlueColor
            self.currentViewController?.present(alert, animated: true, completion: nil)
        }
       
    }
    // MARK: ASTextNodeDelegate
    
    /**
     Implementing shouldHighlightLinkAttribute - returning true for both link and phone numbers
     */
    
    public func textNode(_ textNode: ASTextNode, shouldHighlightLinkAttribute attribute: String, value: Any, at point: CGPoint) -> Bool {
        if attribute == "LinkAttribute"
        {
            return true
        }
        else if attribute == "PhoneNumberAttribute"
        {
            return true
        }
        return false

    }
    
    /*open func textNode(_ textNode: ASTextNode, shouldHighlightLinkAttribute attribute: String, value: AnyObject, at point: CGPoint) -> Bool {
        if attribute == "LinkAttribute"
        {
            return true
        }
        else if attribute == "PhoneNumberAttribute"
        {
            return true
        }
        return false
    }*/
    
    /**
     Implementing tappedLinkAttribute - handle tap event on links and phone numbers
     */

    //open func textNode(_ textNode: ASTextNode, tappedLinkAttribute attribute: String, value: AnyObject, at point: CGPoint, textRange: NSRange) {
    public func textNode(_ textNode: ASTextNode, tappedLinkAttribute attribute: String, value: Any, at point: CGPoint, textRange: NSRange) {
        if attribute == "LinkAttribute"
        {
            if !self.lockKey
            {
                if let tmpString = self.textMessageNode.attributedText
                {
                    let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                    attributedString.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.lightGray, range: textRange)
                    self.textMessageNode.attributedText = attributedString
                    UIApplication.shared.openURL(value as! URL)
                    delay(0.4) {
                        if let tmpString = self.textMessageNode.attributedText
                        {
                            let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                            attributedString.removeAttribute(NSAttributedStringKey.backgroundColor, range: textRange)
                            self.textMessageNode.attributedText = attributedString
                        }
                    }
                }
            }
        }
        else if attribute == "PhoneNumberAttribute"
        {
            let phoneNumber = value as! String
            UIApplication.shared.openURL(URL(string: "tel://\(phoneNumber)")!)
        }
    }
    
    /**
     Implementing shouldLongPressLinkAttribute - returning true for both link and phone numbers
     */
    //open func textNode(_ textNode: ASTextNode, shouldLongPressLinkAttribute attribute: String, value: AnyObject, at point: CGPoint) -> Bool {
    public func textNode(_ textNode: ASTextNode, shouldLongPressLinkAttribute attribute: String, value: Any, at point: CGPoint) -> Bool {
        if attribute == "LinkAttribute"
        {
            return true
        }
        else if attribute == "PhoneNumberAttribute"
        {
            return true
        }
        return false
    }
    
    /**
     Implementing longPressedLinkAttribute - handles long tap event on links and phone numbers
     */
    //open func textNode(_ textNode: ASTextNode, longPressedLinkAttribute attribute: String, value: AnyObject, at point: CGPoint, textRange: NSRange) {
    public func textNode(_ textNode: ASTextNode, longPressedLinkAttribute attribute: String, value: Any, at point: CGPoint, textRange: NSRange) {
        if attribute == "LinkAttribute"
        {
            self.lockKey = true
            if let tmpString = self.textMessageNode.attributedText
            {
                let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                attributedString.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.lightGray, range: textRange)
                self.textMessageNode.attributedText = attributedString

                let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                let openAction = UIAlertAction(title: "Open", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                let addToReadingListAction = UIAlertAction(title: "Add to Reading List", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let copyAction = UIAlertAction(title: "Copy", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                optionMenu.addAction(openAction)
                optionMenu.addAction(addToReadingListAction)
                optionMenu.addAction(copyAction)
                optionMenu.addAction(cancelAction)
                
                if let tmpCurrentViewController = self.currentViewController
                {
                    DispatchQueue.main.async(execute: { () -> Void in
                        tmpCurrentViewController.present(optionMenu, animated: true, completion: nil)
                    })
                    
                }
                
            }
            delay(0.4) {
                if let tmpString = self.textMessageNode.attributedText
                {
                    let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                    attributedString.removeAttribute(NSAttributedStringKey.backgroundColor, range: textRange)
                    self.textMessageNode.attributedText = attributedString
                }
            }
        }
        else if attribute == "PhoneNumberAttribute"
        {
            let phoneNumber = value as! String
            self.lockKey = true
            if let tmpString = self.textMessageNode.attributedText
            {
                let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                attributedString.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.lightGray, range: textRange)
                self.textMessageNode.attributedText = attributedString
                let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                
                let callPhoneNumberAction = UIAlertAction(title: "Call \(phoneNumber)", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                let facetimeAudioAction = UIAlertAction(title: "Facetime Audio", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let sendMessageAction = UIAlertAction(title: "Send Message", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let addToContactsAction = UIAlertAction(title: "Add to Contacts", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let copyAction = UIAlertAction(title: "Copy", style: .default, handler: {
                    (alert: UIAlertAction) -> Void in
                    self.lockKey = false
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (alert: UIAlertAction) -> Void in
                    //print("Cancelled")
                    self.lockKey = false
                })
                
                optionMenu.addAction(callPhoneNumberAction)
                optionMenu.addAction(facetimeAudioAction)
                optionMenu.addAction(sendMessageAction)
                optionMenu.addAction(addToContactsAction)
                optionMenu.addAction(copyAction)
                optionMenu.addAction(cancelAction)
                
                if let tmpCurrentViewController = self.currentViewController
                {
                    DispatchQueue.main.async(execute: { () -> Void in
                        tmpCurrentViewController.present(optionMenu, animated: true, completion: nil)
                    })
                    
                }
                
            }
            delay(0.4) {
                if let tmpString = self.textMessageNode.attributedText
                {
                    let attributedString =  NSMutableAttributedString(attributedString: tmpString)
                    attributedString.removeAttribute(NSAttributedStringKey.backgroundColor, range: textRange)
                    self.textMessageNode.attributedText = attributedString
                }
            }
        }
    }
    
    // MARK: UILongPressGestureRecognizer Selector Methods
    
    
    /**
     Overriding canBecomeFirstResponder to make cell first responder
     */
    override open func canBecomeFirstResponder() -> Bool {
        return true
    }

    /**
     Overriding resignFirstResponder to resign responder
     */
    override open func resignFirstResponder() -> Bool {
        return view.resignFirstResponder()
    }
    
    open override func canPerformAction(_ action: Selector, withSender sender: Any) -> Bool {
        return true
    }
    
    /**
     Override method from superclass
     */
    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        if (is_selecting == false){
            let alert = UIAlertController(title:"choise_action".localized(), message: nil, preferredStyle: .actionSheet)
                
                let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                    if let stringText = self.textMessageNode.attributedText?.string {
                        let image: UIImage = UIImage(named: "icon_create")!
                        self.ouput?.messageReplyAction(text: stringText, messageId: self.message_id!, title: self.senderName,image:image)
                    }
                })
                let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                    UIPasteboard.general.string = self.textMessageNode.attributedText?.string
                })
                let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    let vc = ReplyController(chatType: .single,chatIds:messagesList)
                    vc.title = "resend".localized()
                    self.currentViewController!.show(vc, sender: nil)
                 })
            let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                var messagesList = [Int]()
                messagesList.append(self.message_id!)
                self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            
            alert.addAction(replyAction)
            alert.addAction(copyAction)
            alert.addAction(resendAction)
            alert.addAction(deleteAction)
            alert.addAction(cancelAction)
                alert.view.tintColor = UIColor.navBlueColor
                self.currentViewController?.present(alert, animated: true, completion: nil)
        }
        
    }
    
    /**
     Copy Selector for UIMenuController
     */
    @objc open func copySelector() {
        UIPasteboard.general.string = self.textMessageNode.attributedText!.string
    }
    
}


//    let menuController = UIMenuController.shared
//    menuController.menuItems = [UIMenuItem(title: "Скопировать", action: #selector(TextContentNode.copySelector)), UIMenuItem(title: "Еще", action: #selector(TextContentNode.copySelector))]
//    print(self.textMessageNode.frame)
//    menuController.setTargetRect(self.textMessageNode.frame, in: self.view)
//    menuController.setMenuVisible(true, animated: true)
