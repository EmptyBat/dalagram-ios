//
// Copyright (c) 2016 eBay Software Foundation
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

import UIKit
import AsyncDisplayKit
import AVKit

open class VideoContentNode: ContentNode, UploadStatusDelegate,ASTextNodeDelegate,ASNetworkImageNodeDelegate {
    
    // MARK: Public Variables
    /** UIImage as the image of the cell*/
    open var image: UIImage? {
        get {
            return imageMessageNode.image
        } set {
            imageMessageNode.image = newValue
        }
    }
    open var message_id: Int? {
        didSet {
            //      self.updateAttributedText()
        }
    }
    private var videoUrl: URL?
    
    // MARK: Private Variables
    /** ASImageNode as the content of the cell*/
    open fileprivate(set) var sendIconNode: ASImageNode = ASImageNode()
    open fileprivate(set) var loaderNode = ASDisplayNode()
    open fileprivate(set) var imageMessageNode: ASImageNode = ASImageNode()
    open fileprivate(set) var videoNode : ASVideoNode = ASVideoNode()
    open fileprivate(set) var loadingGifNode: ASImageNode = ASImageNode()
    open fileprivate(set) var dateNode: ASTextNode = ASTextNode()
    open fileprivate(set) var playIcon: ASImageNode = ASImageNode()
    open fileprivate(set) var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
    fileprivate var is_selecting: Bool = false
    fileprivate var is_read: Int = 0
    var ouput: TextContentNodeOutput?
    
    var ouputfirstReply: TextContentFirstReply?
    fileprivate var senderName: String = ""
    open fileprivate(set) var resendBackground: ASButtonNode = ASButtonNode()
    fileprivate var is_resend: Int = 0
    
    //answer content
    open var incomingTextFont = UIFont.n1B1Font() {
        didSet {
            //    self.updateAttributedText()
        }
    }
    /** UIFont for outgoinf text messages*/
    open var outgoingTextFont = UIFont.n1B1Font() {
        didSet {
            //  self.updateAttributedText()
        }
    }
    /** UIColor for incoming text messages*/
    open var incomingTextColor = UIColor.n1DarkestGreyColor() {
        didSet {
            //    self.updateAttributedText()
        }
    }
    /** UIColor for outgoinf text messages*/
    open var outgoingTextColor = UIColor.black {
        didSet {
            //    self.updateAttributedText()
        }
    }
    open var insets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8) {
        didSet {
            setNeedsLayout()
        }
    }
    var ouputAnswerMessage: AnswerMessage?
    open fileprivate(set) var answerTextMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var nameMessageNode: ASTextNode = ASTextNode()
    open fileprivate(set) var backgroundAnswerNode: ASDisplayNode = ASDisplayNode()
    fileprivate var is_has_image: Bool  = false
    fileprivate var AnswerMessage_id: Int = 0
    fileprivate var is_answer: Bool  = false
    fileprivate var AnswertextMessageString: String = ""
    fileprivate var senderTextMessageString: String = ""
    fileprivate var answerUrl: String = ""
    open fileprivate(set) var networAnswerkImageMessageNode:ASNetworkImageNode = ASNetworkImageNode()
    var RemoveOuput: RemoveMessageOutput?
    // MARK: Initialisers
    
    /**
     Initialiser for the cell.
     - parameter image: Must be UIImage. Sets image for cell.
     Calls helper method to setup cell
     */
    public init(id: Int,AnswerMessage_id:Int = 0,image: UIImage? = nil, videoUrl: URL, date: String, currentVC: UIViewController,senderName:String,is_read:Int = 0,is_resend:Int = 0,textMessageString: String = "",senderTextMessageString: String = "",url:String = "",is_answer:Bool = false, bubbleConfiguration: BubbleConfigurationProtocol? = nil) {
        super.init(bubbleConfiguration: bubbleConfiguration)
        self.videoUrl = videoUrl
        self.currentViewController = currentVC
        if (is_answer == true){
            self.is_answer = is_answer
            self.AnswertextMessageString = textMessageString
            self.senderTextMessageString = senderTextMessageString
            self.answerUrl = url
            if let vc = currentViewController as? AnswerMessage {
                self.ouputAnswerMessage = vc
            }
            if (url != ""){
                is_has_image = true
            }
            self.AnswerMessage_id = AnswerMessage_id
        }
        self.setupImageNode(image, date: date)
        if let vc = currentViewController as? TextContentNodeOutput {
            self.ouput = vc
        }
        if let vc = currentViewController as? TextContentFirstReply {
            self.ouputfirstReply = vc
        }
        if let vc = currentViewController as? RemoveMessageOutput {
            self.RemoveOuput = vc
        }
        self.message_id = id
        self.is_resend = is_resend
        self.senderName = senderName
        self.is_read = is_read
    }
    
    // MARK: Initialiser helper method
    /** Override updateBubbleConfig to set bubble mask */
    open override func updateBubbleConfig(_ newValue: BubbleConfigurationProtocol) {
        var maskedBubbleConfig = newValue
        maskedBubbleConfig.isMasked = true
        super.updateBubbleConfig(maskedBubbleConfig)
    }
    
    /**
     Sets the image to be display in the cell. Clips and rounds the corners.
     - parameter image: Must be UIImage. Sets image for cell.
     */

    fileprivate func setupImageNode(_ image: UIImage?, date: String)
    {
        if let previewImage = image {
            imageMessageNode.image = previewImage
        } else {
           // imageMessageNode.image = UIImage(named: "userprofile")
            DispatchQueue.global().async {
                if let videoUrl = self.videoUrl {
                    let asset = AVAsset(url: videoUrl)
                    let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                    let time = CMTimeMake(1, 60)
                    let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                    if img != nil {
                        let frameImg  = UIImage(cgImage: img!)
                        DispatchQueue.main.async(execute: {
                           self.imageMessageNode.image = frameImg
                        })
                    }
                }
            }
        }
       
        imageMessageNode.clipsToBounds = true
        imageMessageNode.contentMode = UIViewContentMode.scaleAspectFill
        
        let dateTextAttr = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.white]
        dateNode.attributedText = NSAttributedString(string: date, attributes: dateTextAttr)
        dateNode.backgroundColor = UIColor.darkGray
        dateNode.cornerRadius = 5.0
        dateNode.clipsToBounds = true
        dateNode.textContainerInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        
        playIcon.image = UIImage(named: "icon_play")
        playIcon.contentMode = UIViewContentMode.scaleAspectFit

        playIcon.isUserInteractionEnabled = true
        playIcon.addTarget(self, action:  #selector(playButton), forControlEvents:.touchUpInside)
        self.imageMessageNode.accessibilityIdentifier = "imageNode"
        self.imageMessageNode.isAccessibilityElement = true
        self.resendBackground.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
        self.resendBackground.setImage(UIImage(named:"icon_resended"), for: .normal)
        self.resendBackground.setTitle("  Пересланное", with: UIFont.systemFont(ofSize: 13), with: UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0), for: .normal)
        self.resendBackground.contentHorizontalAlignment = .left
        if (is_read == 0){
            self.sendIconNode.image = UIImage(named: "icon_mark_double")
        }
        else {
            self.sendIconNode.image = UIImage(named: "icon_mark_green")
        }
        
        if (is_answer){
            //answer Content Node
            backgroundAnswerNode.backgroundColor =  UIColor(red: 89/255.0, green: 198/255.0, blue: 75/255.0, alpha: 1.0)
            self.addSubnode(backgroundAnswerNode)
            let nameString = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15), NSAttributedStringKey.foregroundColor: UIColor(red: 54/255, green: 143/255, blue: 28/255, alpha: 1.0)]
            let fontAndSizeAndTextColor = [ NSAttributedStringKey.font: self.isIncomingMessage ? incomingTextFont : outgoingTextFont, NSAttributedStringKey.foregroundColor: self.isIncomingMessage ? incomingTextColor : outgoingTextColor]
            let TextString = NSMutableAttributedString(string: AnswertextMessageString, attributes: fontAndSizeAndTextColor )
            self.nameMessageNode.delegate = self
            self.nameMessageNode.isUserInteractionEnabled = true
            self.nameMessageNode.attributedText = NSMutableAttributedString(string:  senderTextMessageString, attributes: nameString)
            self.nameMessageNode.accessibilityIdentifier = "labelNameAnswerMessage"
            self.nameMessageNode.isAccessibilityElement = true
            self.nameMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            self.answerTextMessageNode.truncationMode = .byWordWrapping
            self.answerTextMessageNode.maximumNumberOfLines = 3
            
            self.answerTextMessageNode.delegate = self
            self.answerTextMessageNode.isUserInteractionEnabled = true
            self.answerTextMessageNode.attributedText = TextString
            if senderTextMessageString.contains("http") {
                self.answerTextMessageNode.attributedText = senderTextMessageString.attributedHtmlString
            }
            self.answerTextMessageNode.accessibilityIdentifier = "labelanswerMessage"
            self.answerTextMessageNode.isAccessibilityElement = true
            self.answerTextMessageNode.addTarget(self, action:#selector(answerMessageClicked), forControlEvents: .touchUpInside)
            if !isIncomingMessage {
                self.answerTextMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
                self.nameMessageNode.backgroundColor =  self.bubbleConfiguration.getIncomingColor()
            } else {
                self.answerTextMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
                self.nameMessageNode.backgroundColor =  UIColor(red: 206/255, green: 237/255, blue: 186/255, alpha: 1.0)
            }
            print("afsasffas",answerUrl,AnswertextMessageString,senderTextMessageString,senderName)
            self.nameMessageNode.maximumNumberOfLines = 1
            self.addSubnode(nameMessageNode)
            self.addSubnode(answerTextMessageNode)
            networAnswerkImageMessageNode = ASNetworkImageNode(cache: ASPINRemoteImageDownloader.shared(), downloader: ASPINRemoteImageDownloader.shared())
            networAnswerkImageMessageNode.setURL(URL(string: answerUrl), resetToDefault: false)
            networAnswerkImageMessageNode.shouldCacheImage = true
            networAnswerkImageMessageNode.delegate = self
            self.addSubnode(networAnswerkImageMessageNode)
        }
        //
        self.addSubnode(sendIconNode)

        self.addSubnode(resendBackground)
        self.addSubnode(imageMessageNode)
        self.addSubnode(dateNode)
        self.addSubnode(playIcon)
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = CGPoint(x: 20, y: 20)
        loaderNode.view.addSubview(activityIndicator)
        self.addSubnode(loaderNode)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_selecting), name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(makeIs_read), name: AppManager.is_readNotification, object: nil)
    }
    @objc func answerMessageClicked() {
        self.ouputAnswerMessage!.answerMessageClicked(chatId:AnswerMessage_id)
    }
    @objc func playButton() {
        if (is_selecting == false){
            if let url = videoUrl {
                print(url)
                let controller = AVPlayerViewController()
                if url.absoluteString.contains("http") {
                    print("http")
                    controller.player =  AVPlayer(url: url)
                    currentViewController?.present(controller, animated: true, completion: {
                        controller.player?.play()
                    })
                } else {
                    controller.player =  AVPlayer(playerItem: AVPlayerItem(asset: AVAsset(url: url)))
                    currentViewController?.present(controller, animated: true, completion: {
                        controller.player?.play()
                    })
                }
            }
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: AppManager.is_readNotification, object: nil)
    }
    @objc func makeIs_read() {
        print("makeIs_read")
        self.is_read = 1
        self.sendIconNode.image = UIImage(named: "icon_mark_green")
    }
    @objc func makeIs_selecting(_ notification: NSNotification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if  dict["messageId"] as? Int == message_id{
                if (is_selecting == false){
            let image: UIImage = UIImage(named: "icon_create")!
            self.ouputfirstReply?.messagefirstReplyAction(text: "Видео", messageId: self.message_id!, title: self.senderName,image:image)
        }
            }
            
        }
        is_selecting.toggle()
    }
    
    // MARK: Upload Status Delegates
    
    func uploadBegin() {
        print("start")
        activityIndicator.startAnimating()
    }
    
    func uploadEnd() {
        print("ended")
        activityIndicator.stopAnimating()
    }
    
    // MARK: Override AsycDisaplyKit Methods
    
    /**
     Overriding layoutSpecThatFits to specifiy relatiohsips between elements in the cell
     */
    override open func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let width = constrainedSize.max.width / 1.2
        
        self.imageMessageNode.style.width = ASDimension(unit: .points, value: width)
        self.imageMessageNode.style.height = ASDimension(unit: .points, value: width/4*3)
        
        self.loadingGifNode.style.width = ASDimension(unit: .points, value: 100)
        self.loadingGifNode.style.height = ASDimension(unit: .points, value: 50)
        
        let insets = UIEdgeInsets(top: CGFloat.infinity, left: CGFloat.infinity, bottom: 8, right: 8)
        var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
        if !isIncomingMessage {
            let dateCombiner = ASStackLayoutSpec(direction: .horizontal, spacing: 3, justifyContent: .spaceBetween, alignItems: .center, children: [dateNode,sendIconNode])
            
            dateLayout = ASInsetLayoutSpec(insets: insets, child: dateCombiner)
        }
        playIcon.style.width = ASDimension(unit: .points, value: 40)
        playIcon.style.height = ASDimension(unit: .points, value: 40)
        
        let centerSpec = ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: [], child: playIcon)
        let overlay = ASOverlayLayoutSpec(child: self.imageMessageNode, overlay: dateLayout)
        if (!is_answer){
        if (is_resend == 1){
        let stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,ASOverlayLayoutSpec(child: overlay, overlay: centerSpec)])
            return stackLayout
        }
        else {
        return ASOverlayLayoutSpec(child: overlay, overlay: centerSpec)
        }
        }
        else {
            let width = (constrainedSize.max.width - self.insets.left - self.insets.right) / 1.1
                self.nameMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
                self.answerTextMessageNode.style.width = ASDimension(unit: .points, value:  constrainedSize.max.width / 1.2)
            let answerkLayout = ASStackLayoutSpec(direction: .vertical, spacing: 0, justifyContent: .spaceAround, alignItems: .stretch, children: [nameMessageNode,answerTextMessageNode])
            answerkLayout.style.flexShrink = 1
            var answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout])
            var dateLayout = ASInsetLayoutSpec(insets: insets, child: dateNode)
            if (is_has_image){
                self.networAnswerkImageMessageNode.style.width = ASDimension(unit: .points, value: 40)
                self.networAnswerkImageMessageNode.style.height = ASDimension(unit: .points, value: 40)
                answerStack = ASStackLayoutSpec(direction: .horizontal, spacing: 4, justifyContent: .start, alignItems: .start, children: [backgroundAnswerNode,answerkLayout,networAnswerkImageMessageNode])
            }
            let localinsets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
            let insertLayout = ASInsetLayoutSpec(insets: localinsets, child: answerStack)
            backgroundAnswerNode.style.width = ASDimension(unit: .points, value: 2)
            backgroundAnswerNode.style.height = ASDimension(unit: .points, value: 40)
            var stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [insertLayout,ASOverlayLayoutSpec(child: overlay, overlay: centerSpec)])
            if (is_resend == 1){
                stackLayout = ASStackLayoutSpec(direction: .vertical, spacing: 3, justifyContent: .spaceAround, alignItems: .stretch, children: [resendBackground,insertLayout,ASOverlayLayoutSpec(child: overlay, overlay: centerSpec)])
            }
            // answerTextMessageNode.style.width = ASDimension(unit: .points, value: self.view.bounds.width)
            //   answerTextMessageNode.frame = CGRect(0, 0, textMessageNode.frame.width, 30)
            stackLayout.style.maxWidth = ASDimension(unit: .points, value: width)
            stackLayout.style.maxHeight = ASDimension(unit: .fraction, value: 1)
            return stackLayout
        }
    }
    
    // MARK: UILongPressGestureRecognizer Selector Methods
    
    /**
     Overriding canBecomeFirstResponder to make cell first responder
     */
    override open func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    /**
     Override method from superclass
     */
    open override func messageNodeLongPressSelector(_ recognizer: UITapGestureRecognizer) {
     /*   if recognizer.state == UIGestureRecognizerState.began {
            
            let touchLocation = recognizer.location(in: view)
            if self.imageMessageNode.frame.contains(touchLocation) {
                
                view.becomeFirstResponder()
                
                delay(0.1, closure: {
                    let menuController = UIMenuController.shared
                    menuController.menuItems = [UIMenuItem(title: "Copy", action: #selector(ImageContentNode.copySelector))]
                    menuController.setTargetRect(self.imageMessageNode.frame, in: self.view)
                    menuController.setMenuVisible(true, animated:true)
                })
            }
        }*/
    }
    
    
    open override func messageTapSelector(_ recognizer: UITapGestureRecognizer) {
        if (is_selecting == false){
            let touchLocation = recognizer.location(in: view)
            if (!self.playIcon.frame.contains(touchLocation)){
            let alert = UIAlertController(title:"choise_action".localized(), message: nil, preferredStyle: .actionSheet)
                
                let replyAction = UIAlertAction(title: "reply".localized(), style: .default, handler: { _ in
                    self.ouput?.messageReplyAction(text: "video".localized(), messageId: self.message_id!, title: self.senderName,image:self.imageMessageNode.image!)
                })
                let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                        //Show activity indicator
                        
                        DispatchQueue.global(qos: .background).async {
                            if let urlData = NSData(contentsOf: self.videoUrl!){
                                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                                let filePath="\(documentsPath)/tempFile.mov"
                                DispatchQueue.main.async {
                                    urlData.write(toFile: filePath, atomically: true)
                                    
                                    //Hide activity indicator
                                    
                                    let activityVC = UIActivityViewController(activityItems: [NSURL(fileURLWithPath: filePath)], applicationActivities: nil)
                                    activityVC.excludedActivityTypes = [.addToReadingList, .assignToContact]
                                    self.currentViewController!.present(activityVC, animated: true, completion: nil)
                                }
                            }
                    }
                })
                let resendAction = UIAlertAction(title: "resend".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    let vc = ReplyController(chatType: .single,chatIds:messagesList)
                    vc.title = "resend".localized()
                    self.currentViewController!.show(vc, sender: nil)
                })
                let deleteAction = UIAlertAction(title: "delete".localized(), style: .default, handler: { _ in
                    var messagesList = [Int]()
                    messagesList.append(self.message_id!)
                    self.RemoveOuput?.RemoveMessageAction(messageId: self.message_id!)
                })
                let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
                
                alert.addAction(replyAction)
                alert.addAction(copyAction)
                alert.addAction(resendAction)
                alert.addAction(deleteAction)
                alert.addAction(cancelAction)
                alert.view.tintColor = UIColor.navBlueColor
                self.currentViewController?.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    /**
     Copy Selector for UIMenuController
     Puts the node's image on UIPasteboard
     */
    @objc open func copySelector() {
        if let image = self.image {
            UIPasteboard.general.image = image
        }
    }
    
}

