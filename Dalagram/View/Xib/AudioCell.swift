//
//  AudioCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/29/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import AVFoundation
class AudioCell: UITableViewCell,AVAudioPlayerDelegate {
    @IBOutlet weak var size_lb: UILabel!
    private var audioPlayer : AVAudioPlayer!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var title_lb: UILabel!
    var audioUrl: URL? {
        didSet {
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        let origImage = UIImage(named: "icon_mini_play")
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        playButton.setImage(tintedImage, for: .normal)
        playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func playButton(_ sender: Any) {
        if (self.audioPlayer != nil){
            if (self.audioPlayer.isPlaying){
                let origImage = UIImage(named: "icon_mini_play")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                playButton.setImage(tintedImage, for: .normal)
                playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                self.audioPlayer.pause()
            }
            else {
                self.audioPlayer = try! AVAudioPlayer(contentsOf:audioUrl!)
                self.audioPlayer.prepareToPlay()
                self.audioPlayer.delegate = self
                self.audioPlayer.play()
                let origImage = UIImage(named: "icon_mini_pause")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                playButton.setImage(tintedImage, for: .normal)
                playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
        }
            else {
                self.audioPlayer = try! AVAudioPlayer(contentsOf:audioUrl!)
                self.audioPlayer.prepareToPlay()
                self.audioPlayer.delegate = self
                self.audioPlayer.play()
            let origImage = UIImage(named: "icon_mini_pause")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            playButton.setImage(tintedImage, for: .normal)
            playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        let origImage = UIImage(named: "icon_mini_play")
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        playButton.setImage(tintedImage, for: .normal)
        playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
    }
}
