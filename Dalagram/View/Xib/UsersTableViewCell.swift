//
//  UsersTableViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 5/9/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class UsersTableViewCell: UITableViewCell {
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var userCountry: UILabel!
    @IBOutlet weak var userJob: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupUsers(_ data: Users) {
        print("afssfafsa",data)
        let contactName = data.user_name != "" ? data.user_name : data.nickname
        statusLabel.text    = "\(!data.last_visit.isEmpty ? data.last_visit : "Был(а) в сети недавно")"
        firstName.text = data.nickname
        if !data.avatar.isEmpty {
            userCreds.isHidden = true
            userCreds.text = ""
            avatarView.kf.setImage(with: URL(string: data.avatar.encodeUrl()!), placeholder: #imageLiteral(resourceName: "bg_gradient_2"))
        } else {
            avatarView.image = UIImage(named: "bg_gradient_2")
            userCreds.isHidden = false
            print("afsfasfas",data.user_name)
            if (data.nickname != ""){
            userCreds.text = "\(data.nickname.first!)"
        }
        }
        avatarView.layer.cornerRadius = avatarView.frame.width/2
        userCountry.text = data.city
        userJob.text = data.speciality_name
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
