//
//  NotificationCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/11/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var icon_status: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var statusLB: UILabel!
    
    @IBOutlet weak var avatar_view: UIImageView!
    @IBOutlet weak var user_credsLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setupNotification(_ data: UserNotification) {
        let contactName = data.user_name != "" ? data.user_name : data.contact_user_name
        statusLB.text    = "\(!data.last_visit.isEmpty ? data.last_visit : "Был(а) в сети недавно")"
        if !data.avatar.isEmpty {
            user_credsLB.isHidden = true
            user_credsLB.text = ""
            avatar_view.kf.setImage(with: URL(string: data.avatar.encodeUrl()!), placeholder: #imageLiteral(resourceName: "bg_gradient_2"))
            avatar_view.layer.cornerRadius = avatar_view.frame.width/2
        } else {
            avatar_view.image = UIImage(named: "bg_gradient_2")
            user_credsLB.isHidden = false
            print("afsfasfas",data.user_name)
            user_credsLB.text = "\(data.user_name.first!)"
        }
        switch data.action_id {
        case 1:
           icon_status.image = UIImage(named: "icon_sended_status")
        case 2:
           icon_status.image = UIImage(named: "icon_added_status")
        case 3:
           icon_status.image = UIImage(named: "icon_added_status")
        case 4:
           icon_status.image = UIImage(named: "icon_commented_status")
        case 5:
           icon_status.image = UIImage(named: "icon_liked_status")
        case 6:
           icon_status.image = UIImage(named: "icon_liked_status")
            
        default:
            icon_status.image = UIImage(named: "icon_added_status")
        }
        let split = "\(contactName) \(data.action)"
        let range = (split as NSString).range(of: data.action)
        
        let attribute = NSMutableAttributedString.init(string: split)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0) , range: range)
        titleLB.attributedText = attribute
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
