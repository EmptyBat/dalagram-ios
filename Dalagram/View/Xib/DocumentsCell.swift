//
//  DocumentsCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/29/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit

class DocumentsCell: UITableViewCell {

    @IBOutlet weak var file_extension_lb: UILabel!
    @IBOutlet weak var file_image: UIImageView!
    @IBOutlet weak var size_lb: UILabel!
    @IBOutlet weak var title_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
