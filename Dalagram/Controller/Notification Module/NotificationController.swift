//
//  NotificationController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/11/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
import SVProgressHUD
class NotificationController: UITableViewController {
    var isLoading = false
    var bathCount = 1
    var viewModelProfile = ProfileViewModel()
    let disposeBag = DisposeBag()
    var NotificationData: [UserNotification] = []
    lazy var titleView: DialogNavigationTitleView = {
        let titleView = DialogNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    
    
    lazy var navigationTitle: UILabel = {
        let label = UILabel()
        label.text = "News"
        label.font = UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.init(0.01))
        label.textColor = UIColor.white
        return label
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        setBlueNavBar()
        configureUI()
        configureNavBar()
        setupData()
        configureEvents()
        setEmptyBackTitle()
    }
    func configureUI(){
        tableView.registerNib(NotificationCell.self)
        // MARK: - UIBarButtonItem Configurations
        let createSearch = UIBarButtonItem(image: UIImage(named: "icon_search"), style: .plain, target: self, action: #selector(searchAction))
        createSearch.tintColor = UIColor.white
        
        let createShare = UIBarButtonItem(image: UIImage(named: "icon_mbriShare"), style: .plain, target: self, action: #selector(CompanyProductAction))
        createShare.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItems = [createSearch, createShare]
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        self.tableView.reloadData()
        
    }
    @objc func searchAction() {
        let vc = GlobalSearchTableViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }
    private func loadData(){
        isLoading = true
        let parameters = ["token": User.getToken(), "page": bathCount, "per_page": 20] as [String : Any]
        SVProgressHUD.show()
        NetworkManager.makeRequest(.getNotification(parameters), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = UserNotification(json: subJson)
                self.NotificationData.append(new)
            }
            if (json["data"].count != 0){
            self.showNoContentView(dataCount: self.NotificationData.count ?? 0,contentText:true)
            self.tableView.reloadData()
            self.isLoading = false
            SVProgressHUD.dismiss()
            }
        })
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return NotificationData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
            cell.setupNotification(NotificationData[indexPath.row])
        return cell
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = self.NotificationData.count - 1
        if indexPath.row  == lastElement &&  self.NotificationData.count > 19 {
            if (!isLoading) {
                bathCount += 1
                self.loadData()
            }
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SVProgressHUD.show()
        let publication = self.NotificationData[indexPath.row]
            switch publication.action_id {
            case 1:
                let vc = NewsController()
                vc.author_id = publication.user_id
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case 2:
                let vc = NewsController()
                vc.author_id = publication.user_id
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            case 4,6:
                NetworkManager.makeRequest(.getPublicationDetail(publication_id: publication.publication_id), success: { (json)  in
                    var item = NewsTransfer(json:  json["data"])
                    for (_, subJson):(String, JSON) in json["data"] {
                        item = NewsTransfer(json: subJson)
                    }
                    let newsController = NewsHistory()
                    newsController.publication_id = item.publication_id
                    newsController.file_list.append(item.file_list[0])
                    newsController.avatar = item.avatar
                    newsController.is_i_liked = item.is_i_liked
                    newsController.nickname = item.nickname
                    
                    newsController.publication_desc = item.publication_desc
                    newsController.publication_date = item.publication_date
                    newsController.view_count = item.view_count
                    newsController.share_count = item.share_count
                    newsController.comment_count = item.comment_count
                    newsController.like_count = item.like_count
                let vc = NewsControllerDetail.instantiate()
                vc.hidesBottomBarWhenPushed = true
                vc.newsHistory = newsController
                vc.showComments = true
                self.navigationController?.pushViewController(vc, animated: true)
                      SVProgressHUD.dismiss()
                      })
            case 5:
                NetworkManager.makeRequest(.getPublicationDetail(publication_id: publication.publication_id), success: { (json)  in
                    var item = NewsTransfer(json:  json["data"])
                    for (_, subJson):(String, JSON) in json["data"] {
                        item = NewsTransfer(json: subJson)
                    }
                    let newsController = NewsHistory()
                    newsController.publication_id = item.publication_id
                    for name in item.file_list {
                        newsController.file_list.append(name)
                    }
                    newsController.avatar = item.avatar
                    newsController.is_i_liked = item.is_i_liked
                    newsController.nickname = item.nickname
                    
                    newsController.publication_desc = item.publication_desc
                    newsController.publication_date = item.publication_date
                    newsController.view_count = item.view_count
                    newsController.share_count = item.share_count
                    newsController.comment_count = item.comment_count
                    newsController.like_count = item.like_count
                    let vc = NewsControllerDetail.instantiate()
                    vc.hidesBottomBarWhenPushed = true
                    vc.newsHistory = newsController
                    self.navigationController?.pushViewController(vc, animated: true)
                    SVProgressHUD.dismiss()
                })
            default:
                let vc = NewsController()
                vc.author_id = publication.user_id
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
                self.tableView.deselectRow(at: indexPath, animated: true)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NotificationController {
    
    // MARK: - Configuring NavBar TitleView
    
    func configureNavBar() {
        
        let titleItem = UIBarButtonItem(customView: titleView)
        self.navigationItem.leftBarButtonItem = titleItem
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationTitle.text = ""
        
        let imagePlaceholder = #imageLiteral(resourceName: "placeholder")
        titleView.userAvatarView.image = imagePlaceholder
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        titleView.userAvatarView.isUserInteractionEnabled = true
        titleView.userAvatarView.addGestureRecognizer(tapGestureRecognizer)
    }
    // MARK: - NavBar TitleView Action
    
    @objc func userTitleViewPressed() {
        
        let transition = CATransition()
        transition.duration = 0.35
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: avatarUrl.encodeUrl()!), placeholder: nil)
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let user_id = viewModelProfile.user_id.value
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.show(vc, sender: nil)
    }
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
            self!.tableView.reloadData()
            
        })
    }
}
