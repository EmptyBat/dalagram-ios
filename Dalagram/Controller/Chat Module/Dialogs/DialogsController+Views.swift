//
//  DialogsController+ViewConfiguration.swift
//  Dalagram
//
//  Created by Toremurat on 10.07.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit

// MARK: - Configuring UI

extension DialogsController {
    
    // MARK: - Configure Main UI
    
    func configureUI() {
        
        // MARK: - Navigation Bar
        setEmptyBackTitle()
        setBlueNavBar()
        
        //MARK: - TableView Configurations
        tableView.tableHeaderView = searchBar
        tableView.registerNib(ChatCell.self)
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.init(white: 0.85, alpha: 1.0)
        // MARK: - UIBarButtonItem Configurations
        let createSearch = UIBarButtonItem(image: UIImage(named: "icon_search"), style: .plain, target: self, action: #selector(searchAction))
        createSearch.tintColor = UIColor.white
        
        let createShare = UIBarButtonItem(image: UIImage(named: "icon_mbriShare"), style: .plain, target: self, action: #selector(CompanyProductAction))
        createShare.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItems = [createSearch, createShare]
        
        // MARK: - UINavigationBar TitleView Configurations
        let titleView = UIView()
        titleView.addSubview(navigationTitle)
        navigationTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        titleView.addSubview(navigationLoader)
        navigationLoader.snp.makeConstraints { (make) in
            make.left.equalTo(navigationTitle.snp.right).offset(8.0)
            make.centerY.equalTo(navigationTitle)
        }
        self.navigationItem.titleView = titleView
        view.backgroundColor = UIColor.init(white: 1.0, alpha: 0.98)
        //creating New Chat button On TableVew
        let createButton = UIButton()
        let image: UIImage = UIImage(named: "icon_fab")!
        createButton.setImage(image, for: .normal)
        self.tableView.addSubview(createButton)
        createButton.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            createButton.rightAnchor.constraint(equalTo: tableView.safeAreaLayoutGuide.rightAnchor, constant: -16).isActive = true
        } else {
        }
        if #available(iOS 11.0, *) {
            createButton.bottomAnchor.constraint(equalTo: tableView.safeAreaLayoutGuide.bottomAnchor,constant: -12).isActive = true
        } else {
        }
        createButton.heightAnchor.constraint(equalToConstant: 80).isActive = true
        createButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        createButton.addTarget(self, action: #selector(createChatAction), for: .touchUpInside)
        
    }
    // MARK: - Create Chat UIBarButton Action
    @objc func searchAction() {
        searchChannels = true
        self.searchController = UISearchController(searchResultsController: nil)
        // Set any properties (in this case, don't hide the nav bar and don't show the emoji keyboard option)
        self.searchController!.hidesNavigationBarDuringPresentation = false
        self.searchController!.dimsBackgroundDuringPresentation = false
        self.searchController!.searchBar.keyboardType = UIKeyboardType.default
        //  self.searchController!.searchBar.searchBarStyle =  .minimal
        self.searchController!.searchBar.placeholder = "search".localized()
        self.searchController!.searchBar.sizeToFit()
        // Make this class the delegate and present the search
        self.searchBar.delegate = nil
        self.searchController!.searchBar.delegate = self
        self.present(self.searchController!, animated: true, completion: nil)
    }
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }
    @objc func createChatAction() {
        let alert = UIAlertController(title: "Выберите", message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        let groupAction = UIAlertAction(title: "Новая группа", style: .default) { (act) in
            let vc = NewGroupController()
            vc.title = "Новая группа"
            vc.chatType = .group
            self.show(vc, sender: nil)
        }
        let channelAction = UIAlertAction(title: "Новый канал", style: .default) { (act) in
            let vc = NewGroupController()
            vc.chatType = .channel
            vc.title = "Новый канал"
            self.show(vc, sender: nil)
        }
        let singleAction = UIAlertAction(title: "Новый чат", style: .default) { (act) in
            let vc = ChatContactsController(chatType: .single)
            vc.title = "Новый диалог"
            self.show(vc, sender: nil)
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(groupAction)
        alert.addAction(channelAction)
        alert.addAction(singleAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - SwipeCellKit Action
    
    @objc func showDeleteActionSheet(id: String, dialogItem: DialogItem) {
        let alert = UIAlertController(title: "Выберите", message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        let deleteAction = UIAlertAction(title: "Удалить", style: .default) { (act) in
            self.viewModel.removeDialog(by: id, dialogItem: dialogItem)
        }
        let clearAction = UIAlertAction(title: "Очистить", style: .default) { (act) in
            self.viewModel.clearDialog(by: id, dialogItem: dialogItem)
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(deleteAction)
        alert.addAction(clearAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
