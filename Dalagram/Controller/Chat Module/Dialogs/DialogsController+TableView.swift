//
//  DialogsController+TableView.swift
//  Dalagram
//
//  Created by Toremurat on 10.07.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SwipeCellKit

// MARK: - DialogsController TableView Delegate & DataSource

extension DialogsController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                if !(searchState){
        return viewModel.dialogs?.count ?? 0
        }
        else {
                    return viewModel.searchDialog.count ?? 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !(searchState){
        let cell: ChatCell = tableView.dequeReusableCell(for: indexPath)
        cell.delegate = self
        cell.setupDialog(viewModel.dialogs![indexPath.row])
            return cell
        }
         else {
            let cell: ChatCell = tableView.dequeReusableCell(for: indexPath)
            cell.delegate = self
            cell.setupDialog(viewModel.searchDialog[indexPath.row])
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // FIXME:  OPTIONALS FORCE UNWRAPPING
         var currentDialog = viewModel.dialogs![indexPath.row]
        if (searchState){
            currentDialog = viewModel.searchDialog[indexPath.row]
        }
        let chatInfo = DialogInfo(dialog: currentDialog.dialogItem!)
          print("afsfasfa",chatInfo)
        if chatInfo.user_id != 0 { // Single Chat
            let vc = ChatController(type: .single, info: chatInfo, dialogId: currentDialog.id,avatarUrl:self.avatarUrl)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else if chatInfo.group_id != 0 { // Group Chat
            let vc = ChatController(type: .group, info: chatInfo, dialogId: currentDialog.id,avatarUrl:self.avatarUrl)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else if chatInfo.channel_id != 0 { // Channel
            let vc = ChatController(type: .channel, info: chatInfo, dialogId: currentDialog.id,avatarUrl:self.avatarUrl)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        viewModel.resetMessagesCounter(for: currentDialog)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

// MARK: - SwipeCellKit Delegates

extension DialogsController: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        var dialog = viewModel.dialogs![indexPath.row]
        if (searchState){
            dialog = viewModel.searchDialog[indexPath.row]
        }
        let deleteAction = SwipeAction(style: .default, title: nil) { action, indexPath in
            if let item = dialog.dialogItem {
                self.showDeleteActionSheet(id: dialog.id, dialogItem: item)
            }
        }
        let pinAction = SwipeAction(style: .default, title: nil) { [weak self] action, indexPath in
            var param = ["partner_id":"\(dialog.dialogItem!.user_id)"]
            if dialog.dialogItem?.user_id != 0 {
                param = ["partner_id":"\(dialog.dialogItem!.user_id)"]
            } else if  dialog.dialogItem?.group_id != 0 {
                param = ["group_id":"\(dialog.dialogItem!.group_id)"]
            } else if dialog.dialogItem?.channel_id != 0 {
                param = ["channel_id":"\(dialog.dialogItem!.channel_id)"]
            }
            self?.viewModel.PinDialog(by: dialog.id, params: param){ () -> () in
                self?.loadData()
            }
        }
       // let muteAction = SwipeAction(style: .default, title: dialog.isMute ? "Вкл. звук" : "Без звука") { [weak self] action, indexPath in
         let muteAction = SwipeAction(style: .default, title:nil) { [weak self] action, indexPath in
            var param = ["is_mute": "\(!dialog.isMute)","partner_id":"\(dialog.dialogItem!.user_id)"]
            if dialog.dialogItem?.user_id != 0 {
                param = ["is_mute": "\(!dialog.isMute)","partner_id":"\(dialog.dialogItem!.user_id)"]
            } else if  dialog.dialogItem?.group_id != 0 {
                param = ["is_mute": "\(!dialog.isMute)","group_id":"\(dialog.dialogItem!.group_id)"]
            } else if dialog.dialogItem?.channel_id != 0 {
                  param = ["is_mute": "\(!dialog.isMute)","channel_id":"\(dialog.dialogItem!.channel_id)"]
            }
            self?.viewModel.muteDialog(by: dialog.id, params: param)
            self?.tableView.reloadData()
        }
        deleteAction.font = UIFont.systemFont(ofSize: 13)
        deleteAction.hidesWhenSelected = true
        deleteAction.image = UIImage(named: "icon_chatDelete")
        deleteAction.backgroundColor = UIColor.red
        
        muteAction.font = UIFont.systemFont(ofSize: 13)
        muteAction.hidesWhenSelected = true
        muteAction.backgroundColor = UIColor(red: 251/255, green: 150/255, blue: 1/255, alpha: 1.0)
        if (dialog.dialogItem?.is_mute == 0) {
        muteAction.image = UIImage(named: "icon_chatMute")
        }
        else {
        muteAction.image = UIImage(named: "icon_sound")
        }
        
        pinAction.font = UIFont.systemFont(ofSize: 13)
        pinAction.hidesWhenSelected = true
        pinAction.backgroundColor =  UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
        if (dialog.dialogItem?.is_bookmark == 0) {
            pinAction.image = UIImage(named: "icon_Pin")
        }
        else {
            pinAction.image = UIImage(named: "icon_Unpin")
        }
        return [deleteAction, muteAction,pinAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        options.transitionStyle = .border
        return options
    }
}

