//
//  DialogsController.swift
//  Dalagram
//
//  Created by Toremurat on 19.05.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift
import SwipeCellKit
import RxSwift

class DialogsController: UITableViewController {

    // MARK: - UIElements
    lazy var titleView: DialogNavigationTitleView = {
        let titleView = DialogNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "search".localized()
        searchBar.delegate = self
        searchBar.sizeToFit()
        return searchBar
    }()
    
    lazy var navigationTitle: UILabel = {
        let label = UILabel()
        label.text = "Чаты"
        label.font = UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.init(0.01))
        label.textColor = UIColor.white
        return label
    }()
    
    lazy var navigationLoader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(activityIndicatorStyle: .white)
        loader.hidesWhenStopped = true
        return loader
    }()
    
    // MARK: - Variables
    
    var viewModel = DialogsViewModel()
    var notificationToken: NotificationToken? = nil
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var searchController :UISearchController?
    var avatarUrl = ""
    var searchState = false
    var searchChannels = false
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        socketConnectionEvents()
        socketTypingEvent()
        socketreadEvent()
        configureNavBar()
        setupData()
        configureEvents()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SocketIOManager.shared.socket.off("message")
        SocketIOManager.shared.socket.off("read")
        listenMessageUpdates()
        loadData()
    }
    deinit {
        notificationToken?.invalidate()
        NotificationCenter.default.removeObserver(self, name: AppManager.loadDialogsNotification, object: nil)
    }
    
    // MARK: Listen Socket Connection Evenets
    
    func socketConnectionEvents() {
        viewModel.connectToSocket(onConnect: { [weak self] in
           // self?.navigationTitle.text = "Чаты"
            self?.navigationLoader.stopAnimating()
        }, onDisconnect: { [weak self] in
            guard let vc = self else { return }
         //   vc.navigationTitle.text = "Соединение"
            vc.navigationLoader.startAnimating()
        })
    }
    // MARK: - Socket Event: Message
    func listenMessageUpdates() {
        // Listen "message" event and update tableView
        viewModel.socketMessageEvent { [weak self] in
            self?.tableView.reloadData()
        }
        
        // Updating No Content View
        if viewModel.dialogs?.count != 0 {
            showNoContentView(dataCount: viewModel.dialogs?.count ?? 0)
        }
        
        notificationToken = viewModel.dialogs?.observe({ [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                print("initial")
            case .update(_):
                print("update")
              
            case .error(let error):
                fatalError("\(error)")
            }
        })
    }
    
    // MARK: - Recieve Socket Event "Typing"
    func socketreadEvent() {
        SocketIOManager.shared.socket.on("read") { (data, ack) in
            print("afafafsfsa rread")
            if let data = data[0] as? NSDictionary {
                if let sender_id = data["sender_id"] as? Int, let dialog_id = data["dialog_id"] as? String {
                    if dialog_id.contains("U") { // User
                        self.makeIsRead(dialog_id: "\(sender_id)U")
                    } else if dialog_id.contains("G") { // Group
                        self.makeIsRead(dialog_id: dialog_id)
                        
                    } else if dialog_id.contains("C") { // Channel
                        self.makeIsRead(dialog_id: dialog_id)
                    }
                }
            }
        }
    }
    func makeIsRead(dialog_id: String) {
        guard let dialogs = self.viewModel.dialogs else { return }
        for index in 0 ..< dialogs.count {
            if dialog_id == dialogs[index].id {
              loadData()
                          print("afafafsfsa zxczcxzcx")
            }
        }
    }
    func socketTypingEvent() {
        SocketIOManager.shared.socket.on("typing") { (data, ack) in
            if let data = data[0] as? NSDictionary {
                if let sender_id = data["sender_id"] as? Int, let dialog_id = data["dialog_id"] as? String {
                    if dialog_id.contains("U") { // User
                        self.showTypingIndicator(dialog_id: "\(sender_id)U")
                    } else if dialog_id.contains("G") { // Group
                        self.showTypingIndicator(dialog_id: dialog_id)
                        
                    } else if dialog_id.contains("C") { // Channel
                        self.showTypingIndicator(dialog_id: dialog_id)
                    }
                }
            }
        }
    }
    
    func showTypingIndicator(dialog_id: String) {
        guard let dialogs = self.viewModel.dialogs else { return }
        for index in 0 ..< dialogs.count {
            if dialog_id == dialogs[index].id {
                self.startTypingIndicator(isTyping: true, cellIndex: index)
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: { [weak self] in
                    guard let vc = self else { return }
                    vc.startTypingIndicator(isTyping: false, cellIndex: index, lastMessage: dialogs[index].dialogItem!.chat_text)
                })
            }
        }
    }
    
    func startTypingIndicator(isTyping: Bool, cellIndex: Int, lastMessage: String = "") {
        let index = IndexPath.init(row: cellIndex, section: 0)
        if let cell = self.tableView.cellForRow(at: index) as? ChatCell {
            cell.messageLabel.text = isTyping ? "Печатает..." : lastMessage
        }
    }
    
    // MARK: - Load Data
    
    @objc func loadData() {
        viewModel.getUserDialogs { [weak self] in
            guard let vc = self else { return }
            vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
              vc.tableView.reloadData()
        }
    }
}
extension DialogsController {
        
        // MARK: - Configuring NavBar TitleView
        
        func configureNavBar() {
            
            let titleItem = UIBarButtonItem(customView: titleView)
            self.navigationItem.leftBarButtonItem = titleItem
            self.navigationItem.leftItemsSupplementBackButton = true
            self.navigationTitle.text = ""

            let imagePlaceholder = #imageLiteral(resourceName: "placeholder")
            titleView.userAvatarView.image = imagePlaceholder
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            titleView.userAvatarView.isUserInteractionEnabled = true
            titleView.userAvatarView.addGestureRecognizer(tapGestureRecognizer)
        }
        // MARK: - NavBar TitleView Action
        
        @objc func userTitleViewPressed() {
            
            let transition = CATransition()
            transition.duration = 0.35
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionFade
            self.navigationController?.view.layer.add(transition, forKey: nil)
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: avatarUrl.encodeUrl()!), placeholder: nil)
            self!.avatarUrl = avatarUrl
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let user_id = viewModelProfile.user_id.value
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.show(vc, sender: nil)
    }
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
        })
    }
}
