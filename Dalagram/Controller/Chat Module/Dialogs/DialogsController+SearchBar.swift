//
//  DialogsController+SearchBar.swift
//  Dalagram
//
//  Created by Toremurat on 10.07.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit

extension DialogsController {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

extension DialogsController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        self.searchState = true
        if searchText == "" {
            print("nillsa")
            self.searchState = false
            viewModel.getUserDialogs { [weak self] in
                guard let vc = self else { return }
                
                vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            }
            return
        }
        viewModel.searchDialog(by: searchText,searchChannel:searchChannels, onReload: { [weak self] in
            self?.tableView.reloadData()
        })
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchChannels = false
        self.searchBar.delegate = self

        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        viewModel.getUserDialogs { [weak self] in
            guard let vc = self else { return }
            vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            self?.tableView.reloadData()
        }
        self.searchBar.delegate = self
         searchChannels = false

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("vzxzvxzvx",searchText)
        if searchText == "" {
            print("nillsa")
            self.searchState = false
            viewModel.getUserDialogs { [weak self] in
                guard let vc = self else { return }
                vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            }
            return
        }
        print(searchText)
        self.searchState = true
        viewModel.searchDialog(by: searchText,searchChannel:searchChannels, onReload: { [weak self] in
          //     print("affasfas",self?.viewModel.searchDialog.count)
               self?.tableView.reloadData()
        
            
        })
    }
}
