//
//  DialogsViewModel.swift
//  Dalagram
//
//  Created by Toremurat on 15.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class DialogsViewModel {

    var dialogs: Results<Dialog>?
    var searchDialog = [Dialog]()
    let realm = try! Realm()
    
    init() {
        dialogs = realm.objects(Dialog.self).sorted(byKeyPath: "updatedDate")
    }
    
    // MARK: - Dialogs GET Request
    
    func getUserDialogs(page: Int = 1, per_page: Int = 40, success: @escaping (() -> Void) = {}) {
        let parameters = ["token": User.getToken(), "page": page, "per_page": per_page] as [String : Any]
        NetworkManager.makeRequest(.getDialogs(parameters), success: { (json)  in
            print("afsasa",json)
            for (_, subJson):(String, JSON) in json["data"] {
                print("afasfsasaf",subJson)
                let primaryKeyId = subJson["dialog_id"].stringValue
                let dialogItem = DialogItem.initWith(json: subJson, id: primaryKeyId)
                Dialog.initWith(dialogItem: dialogItem, id: primaryKeyId)
            }
            
            success()
        })
    }
    
    // MARK: - Socket Event: Message
    
    func socketMessageEvent(onEvent: @escaping () -> Void) {
        SocketIOManager.shared.socket.on("message") { [weak self] (dataArray, ack)  in
            print("socketMessageEvent \(dataArray)")
            let dict = dataArray[0] as! NSDictionary
            if let text = dict.value(forKey: "chat_text") as? String ?? dict.value(forKey: "chat_image") as? String,
                let dialog_id = dict.value(forKey: "dialog_id") as? String,
                let recipiend_id = dict.value(forKey: "recipient_user_id") as? Int,
                let sender_user_id = dict.value(forKey: "sender_user_id") as? Int {
                
                print(recipiend_id, sender_user_id, User.currentUser()!.user_id, dialog_id)
                
                // Rooms are not available now, we should check for user_id of sender and recipient
                if recipiend_id == User.currentUser()!.user_id || sender_user_id == User.currentUser()!.user_id {
                    // Edit existing dialog
                    let commonDialogId = "\(sender_user_id)U"
                    if let currentDialog = self?.dialogs?.filter(NSPredicate(format: "id = %@", commonDialogId)).first {

                        let realm = try! Realm()
                        try! realm.write {
                            // Update current dialog parameters
                            currentDialog.dialogItem?.chat_text = text
                            currentDialog.dialogItem?.chat_date = "Сейчас"
                            if sender_user_id != User.currentUser()!.user_id {
                                currentDialog.messagesCount = currentDialog.messagesCount + 1
                            }
                            if  dict["file_list"] != nil && dict.value(forKey: "is_has_file") as! Int == 1{
                                let dict_list = dict.value(forKey: "file_list") as! NSArray
                                let file_list = dict_list[0] as! NSDictionary
                            if file_list.count != 0 {
                                currentDialog.dialogItem?.is_has_file = 1
                                currentDialog.dialogItem?.file_format = (file_list.value(forKey: "file_format") as? String)!
                                    switch (file_list.value(forKey: "file_format") as? String)! {
                                    case "file":
                                         currentDialog.dialogItem?.chat_text = "Документ"
                                    case "video":
                                         currentDialog.dialogItem?.chat_text = "Видеозапись"
                                    case "image":
                                      currentDialog.dialogItem?.chat_text  = "Фото"
                                    default: break
                                    }
                                    // Contact Message
                                
                                }
                                
                               
                            }
                            else {
                                if dict.value(forKey: "is_contact") as? Int == 1 {
                                    
                                    currentDialog.dialogItem?.is_contact = 1
                                    currentDialog.dialogItem?.chat_text = "Контакт"
                                    
                                }
                                else {
                                    currentDialog.dialogItem?.is_contact = 0
                                }
                                currentDialog.dialogItem?.file_format = ""
                                currentDialog.dialogItem?.is_has_file = 0
                            }
                        }
                        onEvent()
                    } else {

                        // Dialog don't exist, need to pull from server and create it
                        self?.getUserDialogs()
                    }
                }
            }
        }
    }
    
    // MARK: - Connect to Socket
    
    func connectToSocket(onConnect: @escaping () -> Void, onDisconnect:  @escaping () -> Void ) {
        SocketIOManager.shared.socket.on(clientEvent: .connect) {data, ack in
            onConnect()
        }
        SocketIOManager.shared.socket.on(clientEvent: .disconnect) { (data, ack) in
            onDisconnect()
        }
        SocketIOManager.shared.socket.on(clientEvent: .reconnectAttempt) { (data, ack) in
            onDisconnect()
        }
    }
    
    // MARK: - Reset Messages Counter
    
    func resetMessagesCounter(for item: Dialog) {
        if item.messagesCount != 0 {
            try! realm.write {
                item.messagesCount = 0
            }
        }
    }
    
    // MARK: - Mute Dialog
    
    func muteDialog(by identifier: String, muteValue: Bool = true,params : [String :Any]) {
        if let item = realm.object(ofType: Dialog.self, forPrimaryKey: "\(identifier)") {
            try! realm.write {
                if (item.dialogItem?.is_mute == 0){
                    item.dialogItem?.is_mute = 1
                }
                else {
                    item.dialogItem?.is_mute = 0
                }
                NetworkManager.makeRequest(.muteDialog(params), success: { (json)  in
        
                })
            }
        }
    }
    func PinDialog(by identifier: String, pinValue: Bool = true,params : [String :Any],completion:@escaping ()->()) {
        if let item = realm.object(ofType: Dialog.self, forPrimaryKey: "\(identifier)") {
            try! realm.write {
                if (item.dialogItem?.is_bookmark == 0){
                item.dialogItem?.is_bookmark = 1
                }
                else {
                item.dialogItem?.is_bookmark = 0
                }
                NetworkManager.makeRequest(.addBookmark(params), success: { (json)  in
                    completion( )
                })
            }
        }
    }
    
    // MARK: - Delete Dialog
    
    func removeDialog(by identifier: String, dialogItem: DialogItem) {
        if let item = realm.object(ofType: Dialog.self, forPrimaryKey: "\(identifier)") {
            try! realm.write {
                realm.delete(item)
            }
        }
        for item in realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", identifier)) {
            try! realm.write {
                realm.delete(item)
            }
        }
        /// Endpoing Parameters for signle/group/channel dialogs
        var endpointParam: [String: Any] = [:]
        
        if dialogItem.user_id != 0 {
            endpointParam["partner_id"] = dialogItem.user_id
        } else if dialogItem.group_id != 0 {
            endpointParam["group_id"] = dialogItem.group_id
        } else if dialogItem.channel_id != 0 {
            endpointParam["channel_id"] = dialogItem.channel_id
        }
        
        NetworkManager.makeRequest(.removeChat(endpointParam), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
        })
    }
    
    // MARK: - Clear Dialog History
    
    func clearDialog(by identifier: String, dialogItem: DialogItem) {
        
        /// Endpoing Parameters for signle/group/channel dialogs
        var endpointParam: [String: Any] = ["is_delete_all": 1]
        
        if dialogItem.user_id != 0 {
            endpointParam["partner_id"] = dialogItem.user_id
        } else if dialogItem.group_id != 0 {
            endpointParam["group_id"] = dialogItem.group_id
        } else if dialogItem.channel_id != 0 {
            endpointParam["channel_id"] = dialogItem.channel_id
        }
        
        NetworkManager.makeRequest(.removeChat(endpointParam), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
        })
        
        for object in realm.objects(DialogHistory.self)
            .filter(NSPredicate(format: "id = %@", identifier)) {
            try! realm.write {
                realm.delete(object)
            }
        }
        if let item = realm.object(ofType: Dialog.self, forPrimaryKey: "\(identifier)") {
            try! realm.write {
                item.dialogItem?.chat_text = ""
            }
        }
    }
    
    // MARK: - Search dialog by Message text and Contact name
    // CONTAINS[c] is case insensitive keyword
    
    func searchDialog(by text: String,searchChannel : Bool, onReload: @escaping() -> Void?) {
        var parameters = ["token": User.getToken(), "page": 1, "per_page": 40,"q":text] as [String : Any]
        if (searchChannel){
        parameters = ["token": User.getToken(), "page": 1, "per_page": 40,"search":text] as [String : Any]
        }
        print("asffaszxz",parameters)
         self.searchDialog.removeAll()
        NetworkManager.makeRequest(.getDialogs(parameters), success: { (json)  in
            print("afsasa",json)
            self.searchDialog.removeAll()
            for (_, subJson):(String, JSON) in json["data"] {
                print("afasfsasaf",subJson)
                let primaryKeyId = subJson["dialog_id"].stringValue
                let item = DialogItem()
                item.dialog_id   = primaryKeyId
                item.chat_id     = subJson["chat_id"].intValue
                item.chat_kind   = subJson["chat_kind"].stringValue
                item.action_name = subJson["action_name"].stringValue
                item.group_id    = subJson["group_id"].intValue
                item.channel_id  = subJson["channel_id"].intValue
                item.is_mute     = subJson["is_mute"].intValue
                item.is_bookmark = subJson["is_bookmark"].intValue
                item.user_id     = subJson["user_id"].intValue
                item.chat_name   = subJson["chat_name"].stringValue
                item.avatar      = subJson["avatar"].stringValue
                item.phone       = subJson["phone"].stringValue
                item.chat_text   = subJson["chat_text"].stringValue
                item.chat_date   = subJson["chat_date"].stringValue
                item.is_read     = subJson["is_read"].intValue
                item.is_admin    = subJson["is_admin"].intValue
                item.is_contact  = subJson["is_contact"].intValue
                
                item.contact_user_name = subJson["nickname"].stringValue
                item.is_own_last_message = subJson["is_own_last_message"].boolValue
                item.partner_block_me   = subJson["partner_block_me"].intValue
                item.i_block_partner    = subJson["i_block_partner"].intValue
                
                
                if subJson["is_has_file"].intValue == 1 {
                    item.is_has_file = subJson["is_has_file"].intValue
                    item.file_format = subJson["file_list"][0]["file_format"].stringValue
                }
                let object  = Dialog()
                object.id   = primaryKeyId
                object.dialogItem = item
                self.searchDialog.append(object)
            }
            
            onReload()
        })
    }
    
    func getAllDialogs(onReload: @escaping () -> Void) {
        self.dialogs = realm.objects(Dialog.self).sorted(byKeyPath: "updatedDate")
        onReload()
    }
    
}
