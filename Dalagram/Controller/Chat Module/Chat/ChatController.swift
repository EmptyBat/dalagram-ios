//
//  ChatController.swift
//  Dalagram
//
//  Created by Toremurat on 16.07.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RealmSwift
import RxSwift
import AVFoundation
import SVProgressHUD
import SwiftyJSON
//ChatController
protocol UploadStatusDelegate {
    func uploadBegin()
    func uploadEnd()
}
class ChatController: NMessengerViewController,UIToolbarDelegate {
    
    // MARK: - Views
    
    lazy var titleView: UserNavigationTitleView = {
        let titleView = UserNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    
    // MARK: - Variables
    let segmentedControlPadding: CGFloat = 10
    let segmentedControlHeight: CGFloat = 30
    var bootstrapWithRandomMessages : Int = 30
    let disposeBag = DisposeBag()
    var selectionMode : Bool = false
    var messagesList = [Int]()
    var messagesCells = [GeneralMessengerCell]()
    var messagesGroup = [MessageGroup]()
    var type: DialogType = .single
    var info: DialogInfo? = nil
    var isMute: Int  = 0
    var dialogId: String = ""
    private(set) var lastMessageGroup: MessageGroup? = nil
    
    var viewModel: ChatViewModel?
    var searchController :UISearchController?
    private var myToolbar: UIToolbar!
    private var searchResultCount: UIBarButtonItem!
    private var recordingSession: AVAudioSession!
    private var audoRecorder: AVAudioRecorder!
    private var NeedToRoot = false
    private var searchResult = [Int]()
    private var searchIndex = 0
    var viewModelDialog = DialogsViewModel()
    // MARK: - ViewModel & Controller Initializer
    
    
    //data for save
    var firstAnswerTitle = ""
    var firstAnswerText = ""
    var firstAnswerImage : UIImage? = nil
    var firstAnswerMessageId  = 0
    convenience init(type: DialogType = .single, info: DialogInfo, dialogId: String,avatarUrl:String = "",chatIds:[Int] = []) {
        self.init()
        self.info = info
        self.type = type
        self.dialogId = dialogId
        if (type == .group)||(type == .channel){
            self.info!.user_id = (User.currentUser()?.user_id)!
        }
        self.viewModel = ChatViewModel(dialogId, info: self.info!, chatType: type,avatarUrl:avatarUrl)
        self.viewModel!.chatVC = self
        if (avatarUrl == ""){
            configureEvents()
        }
        self.configureNavBar(info)
        if (chatIds.count != 0){
            self.viewModel!.ResendMessagesRequest(chatIds: chatIds,success: {
            //    self.loadMessages()
            })
            NeedToRoot = true
        }
        let realm = try! Realm()
        let obj = realm.objects(Dialog.self).filter(NSPredicate(format: "id = %@", self.dialogId))
        if  let mute = obj.first?.dialogItem?.is_mute {
        isMute = mute
        }
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setEmptyBackTitle()
        setRequestNotifications()
        socketMessageEvent()
        socketTypingEvent()
        if (type == .single){
        socketOnlineEvent()
        }
        else if (type == .channel){
        getChannelMembers()
        }
        else {
        getGroupMembers()
        }
        self.messengerView.messengerNode.backgroundColor = UIColor(patternImage: UIImage(named: "background_last")!)
        inputBarViewEvents()
        checkAdminPermisions(type: type, info: info!)
        createToolbar()
        automaticallyAdjustsScrollViewInsets = false
        if (NeedToRoot == true){
            var navigationArray = self.navigationController?.viewControllers //To get all UIViewController stack as Array
            navigationArray!.remove(at: (navigationArray?.count)! - 3)
            navigationArray!.remove(at: (navigationArray?.count)! - 2)// To remove previous UIViewController
            self.navigationController?.viewControllers = navigationArray!
        }
    }
    func checkAdminPermisions(type: DialogType = .single, info: DialogInfo){
        if (info.is_admin == 0)&&type == .channel{
            self.inputBarView.isHidden = true
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                let bottomPadding = window?.safeAreaInsets.bottom
                self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, UIScreen.main.bounds.height - bottomPadding! - 115)
                }
                
            else {
            self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height - 50)
            }
            }
        
            self.messengerView.layoutIfNeeded()
            createHideNotificationButton()
        }
    }
    func createHideNotificationButton(){
        var  disableActionTitle = ""
        if (isMute == 0){
            disableActionTitle = "disable_notification".localized()
        }
        else {
            disableActionTitle = "enable_notification".localized()
        }
        let button = UIButton(type: .custom)
        button.setTitle(disableActionTitle, for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.backgroundColor = UIColor.white
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let bottomPadding = window?.safeAreaInsets.bottom
            button.frame = CGRect(x: 0, y: (UIScreen.main.bounds.height - 115) - bottomPadding!, width: UIScreen.main.bounds.width, height: 50)
        }
        else  {
            button.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 115, width: UIScreen.main.bounds.width, height: 50)
        }
        button.addTarget(self, action: #selector(disableNotificationAction), for: .touchUpInside)
        self.view.addSubview(button)
    }
    func createToolbar(){
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "icon_reply-1"), for: .normal)
        button.sizeToFit()
        button.addTarget(self, action: #selector(resendMessage), for: .touchUpInside)
        let reply = UIBarButtonItem(customView: button)
        let resend = UIBarButtonItem(barButtonSystemItem: .reply, target: self, action: #selector(replyMessage))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let trash = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(removeAction))
        toolbarItems = [createRemoveButton(),spacer,spacer,spacer,reply,spacer,resend, spacer,trash]
    }
    @objc func reloadData(){
       
        
    }
    func createRemoveButton()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setTitle("Удалить все", for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cancelButton.addTarget(self, action: #selector(removeAllAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    @objc func disableNotificationAction(){
            var param = ["is_mute": "\(self.isMute)","partner_id":"\(self.info!.user_id)"]
            if self.info!.user_id != 0 {
                param = ["is_mute": "\(self.isMute)","partner_id":"\(self.info!.user_id)"]
            } else if  self.info!.group_id != 0 {
                param = ["is_mute": "\(self.isMute)","group_id":"\(self.info!.group_id)"]
            } else if self.info!.channel_id != 0 {
                param = ["is_mute": "\(self.isMute)","channel_id":"\(self.info!.channel_id)"]
            }
            self.viewModelDialog.muteDialog(by: self.dialogId, params: param)
    }
    @objc func replyMessage(){
        cancelAction()
        self.viewModel?.replyMessageId = firstAnswerMessageId
        //  self.viewModel?.replyMessageId = messageId
        let imageForTest: UIImage = UIImage(named: "icon_create")!
        if (firstAnswerImage == imageForTest){
            self.inputBarView.changeReplyArea(with: firstAnswerText,title: firstAnswerTitle)
        }
        else {
            self.inputBarView.changeReplyArea(with: firstAnswerText,title: firstAnswerTitle,image:firstAnswerImage)
        }
        self.messengerView.scrollToLastMessage(animated: true)
        self.inputBarView.isReplyMessage.value = true
    
    }
    @objc func resendMessage(){
        let vc = ReplyController(chatType: .single,chatIds:messagesList)
        vc.title = "resend".localized()
        self.show(vc, sender: nil)
        cancelAction()
    }
    @objc func removeAllAction(){
        let alert = UIAlertController(title: "are_u_sure".localized(), message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        let clearAction = UIAlertAction(title: "clear_chat".localized(), style: .default, handler: { [unowned self] (act) in
            self.viewModel!.clearChatRequest()
            self.cancelAction()
        })
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(clearAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @objc func complete(){
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (self.messengerView.allMessages().count == 0){
            setupBubbleMessages()
        }
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: AppManager.dialogDetailsNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: AppManager.diloagHistoryNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: AppManager.loadDialogsNotification, object: nil)
            SocketIOManager.shared.socket.off("message")
            SocketIOManager.shared.socket.off("read")
            viewModel?.removeEmptyDialogHistory()
        }
    }
    @objc func removeAction(){
        self.viewModel?.clearMessagesRequest(messagesList:messagesList,success: {
            DialogHistory.removeChatIds(dialog_id: self.dialogId, messagesList: self.messagesList)
        self.cancelAction()
        self.messengerView.clearALLMessages()
        self.setupBubbleMessages()
        })
      /*  for index1 in 0..<messagesGroup.count {
                for index2 in 0..<self.messagesCells.count {
        if (self.messagesGroup[index1].messages.contains(self.messagesCells[index2])){
        self.messagesGroup[index1].removeMessageFromGroup(self.messagesCells[index2], completion:self.complete)
                    }
            }
                    }*/
              //  print("value",indexOfA)
              //  if (messagesGroup[index2].messages[backIndex] == messagesCells[index1]){
                //    print("before",self.messagesGroup[index2].messages.count)
                   
                    
                //self.messagesGroup[index2].removeMessageFromGroup(messagesCells[index1], completion:nil)
                    
                    //    print("after",self.messagesGroup[index2].messages.count)
                //    self.messagesList.remove(at: index1)
                  //  self.messagesCells.remove(at: index1)
              //  }
            
          //  self.messagesList.remove(at: index1)
          //  self.messagesCells.remove(at: index1)
        
   /*     for index in 0..<messageForRemove.count {
        self.messagesGroup[messageForRemove[index]].removeMessageFromGroup(messagesCells[messageRemovePlace[index]], completion:nil)
            
           let message = messagesCells[messageRemovePlace[index]]
            let removeMessage = messagesGroup[messageForRemove[index]]
            self.messagesList.remove(at: messageRemovePlace[index])
            self.messagesCells.remove(at: messageRemovePlace[index])
        }*/
    }
    //load more system
    func batchFetchContent() {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.createNextBatch()
        }
    }
    
    private func endBatchFetchDirectly(messageCells: [MessageNode]) {
        self.messengerView.endBatchFetchWithMessages(messageCells)
    }
    private func endBatchFetchAdjusted(messageCells: [MessageGroup]) {
        let currentHeight = self.messengerView.messengerNode.view.contentSize.height
        self.messengerView.endBatchFetchWithMessages(messageCells)
        self.messengerView.addMessagesWithBlock([], scrollsToMessage: false, withAnimation: .none) {
            DispatchQueue.main.async {
                let addedHeight = self.messengerView.messengerNode.view.contentSize.height - currentHeight
                self.messengerView.messengerNode.view.contentOffset.y = addedHeight
            }
        }
    }
    private var currentBatchCounter: Int = 10
    private var isLoading: Bool = false
    private func createNextBatch() {
        if (isLoading == false){
            isLoading = true
        viewModel?.BatchloadMessagesFromRealm(currentBatchCounter:currentBatchCounter) { [weak self] in
            guard let vc = self, let vm = self?.viewModel else { return }
            var checkValue = false
            if (vm.messageGroups.count == 0){
                checkValue = true
            }
            else {
                let lastPosition = self!.messengerView.allMessages().first
                vc.messengerView.endBatchFetchWithMessages(vm.messageGroups)
           //     vc.messengerView.scrollToMessage(lastPosition!, atPosition: UITableViewScrollPosition.none, animated: true)
                vc.messagesGroup = vm.messageGroups
                vc.currentBatchCounter += 10
            }
            vc.viewModel?.getDialogMessagesForStore(page:(vc.currentBatchCounter/10)+1,requestSuccess: { [weak self] () in
                vc.isLoading = false
                if (checkValue){
                vc.viewModel?.BatchloadMessagesFromRealm(currentBatchCounter:vc.currentBatchCounter) { [weak self] in
                    guard let vc = self, let vm = self?.viewModel else { return }
                    if (vm.messageGroups.count
                        != 0){
                        let lastPosition = self!.messengerView.allMessages().first
                        vc.messengerView.endBatchFetchWithMessages(vm.messageGroups)
                   //     vc.messengerView.scrollToMessage(lastPosition!, atPosition: UITableViewScrollPosition.none, animated: true)
                        vc.messagesGroup = vm.messageGroups
                        vc.currentBatchCounter += 10
                    }
                }
                }
        })
        }
        }
    }
    func getChannelMembers(){
        NetworkManager.makeRequest(.getChannelDetails(channel_id: self.info!.channel_id), success: { [weak self] (json) in
                print(json)
                guard let vc = self else { return }
                let data = json["data"]
                var userCount = 0
                for (_, subJson):(String, JSON) in data["channel_users"] {
                    userCount += 1
                }
            if(userCount  == 1){
                vc.titleView.userStatusLabel.text = "\(userCount) " + "members_count_1".localized()
            }
            else if (userCount > 4) {
                vc.titleView.userStatusLabel.text = "\(userCount) " + "members_count_2".localized()
            }
            else {
                vc.titleView.userStatusLabel.text = "\(userCount) " + "members_count_3".localized()
            }
            })
        }
    func getGroupMembers() {
        NetworkManager.makeRequest(.getGroupDetails(group_id: self.info!.group_id), success: { [weak self] (json) in
            guard let vc = self else { return }
             var userCount = 0
            for (_, subJson):(String, JSON) in json["data"]["group_users"] {
                let newContact = JSONContact(json: subJson)
                userCount += 1
            }
            if(userCount  == 1){
                vc.titleView.userStatusLabel.text = "\(userCount) " + "members_count_1".localized()
            }
            else if (userCount > 4) {
                vc.titleView.userStatusLabel.text = "\(userCount) " + "members_count_2".localized()
            }
            else {
                vc.titleView.userStatusLabel.text = "\(userCount) " + "members_count_3".localized()
            }
        })
        
    }

    
    // MARK: - Setup Request Notifications
    
    func setRequestNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(loadDialogDetails), name: AppManager.dialogDetailsNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadMessages), name: AppManager.diloagHistoryNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadData), name: AppManager.loadDialogsNotification, object: nil)
    }
    @objc func loadData() {
       self.messengerView.clearALLMessages()
    }
    // MARK: - Send Socket Event "Typing"
    
    override func sendTypingEvent() {
        viewModel?.socketTypingEmit()
    }
    
    // MARK: - Recieve Socket Event "Typing"
    
    func socketTypingEvent() {
        guard let vm = viewModel else { return }
        SocketIOManager.shared.socket.on("typing") { (data, ack) in
            if let data = data[0] as? NSDictionary {
               if let sender_id = data["sender_id"] as? Int {
                    if sender_id == vm.info.user_id {
                        self.startTypingIndicator(isTyping: true)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: { [weak self] in
                            guard let vc = self else { return }
                            vc.startTypingIndicator(isTyping: false)
                        })
                    }
                }
            }
        }
    }
    
    @objc func startTypingIndicator(isTyping: Bool) {
        if isTyping {
            titleView.userStatusLabel.text = "Печатает..."
        } else {
            titleView.userStatusLabel.text = "В сети"
        }
    }
    
    // MARK: - Socket Event: Online
    
    func socketOnlineEvent() {
        guard let vm = self.viewModel else { return }
        SocketIOManager.shared.socket.on("online") { [weak self] (params, ack) in
            guard let vc = self else { return }
            let dict = params[0] as! NSDictionary
           
            if let sender_id = dict.value(forKey: "sender_id") as? Int {
                if vm.info.user_id == sender_id {
                    vc.titleView.userStatusLabel.text = "В сети"
                    if let _ = dict.value(forKey: "is_online") as? Bool {
                        vc.titleView.userStatusLabel.text = "был(а) недавно"
                    }
                }
            }
        }
    }
    
    // MARK: - Socket Event: Message
    
    func socketMessageEvent() {
        viewModel?.socketMessageEvent(onSuccess: { [weak self] () in
            guard let vc = self else { return }
          /*  vc.messengerView.addMessages(newMessages, scrollsToMessage: false)
            vc.lastMessageGroup = newMessages.last
            vc.messengerView.scrollToLastMessage(animated: true)*/
            print("affsfas calling")
            self!.viewModel?.getLastDialogMessages(page:1,per_page:1,requestSuccess: { [weak self] (newMessages) in
                guard let vc = self else { return }
                print("fsasfafsa",newMessages)
                let message = newMessages.last
                vc.messengerView.addMessage(message!, scrollsToMessage: false)
                vc.lastMessageGroup = newMessages.last
                vc.messengerView.scrollToLastMessage(animated: true)
            })
         //   vc.lastMessageGroup = newMessages.last
           // vc.messengerView.scrollToLastMessage(animated: true)
        })
            //vc.sendText(text, date: Date().getStringTime(), isIncomingMessage: true)
           // let textContent = TextContentNode(id: 0, textMessageString: text, dateString: Date().getStringTime(), currentViewController: vc, bubbleConfiguration: vc.sharedBubbleConfiguration)
            //textContent.sendIconNode.image = nil
           // textContent.sendIconNode.image = UIImage(named: "icon_mark_single")
           // let newMessage = MessageNode(content: textContent)
           // newMessage.cellPadding = vc.messagePadding
           // newMessage.currentViewController = self
           // vc.postText(newMessage, isIncomingMessage: true)
    }
    // MARK: - Setup messages from Realm
    
    func setupBubbleMessages() {
        viewModel?.BatchloadMessagesFromRealm(currentBatchCounter:0) { [weak self] in
            guard let vc = self, let vm = self?.viewModel else { return }
            vc.messengerView.addMessages(vm.messageGroups, scrollsToMessage: false)
            vc.lastMessageGroup = vm.messageGroups.last
            vc.messengerView.scrollToLastMessage(animated: false)
            vc.messagesGroup = vm.messageGroups
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            vc.loadMessages()
            }
        }
    }
    // MARK: - Load Chat History Request
    @objc func loadMessages(animated:Bool = false) {
        viewModel?.getDialogMessages(requestSuccess: { [weak self] (newMessages) in
            guard let vc = self else { return }
            vc.messagesGroup = newMessages
            print("afsfsafas",newMessages)
            vc.messengerView.addMessages(newMessages, scrollsToMessage: false)
            vc.lastMessageGroup = newMessages.last
            vc.messengerView.scrollToLastMessage(animated: animated)
           // print("asfffeqffsafa",(self!.viewModel?.dialogId)!)
            SocketIOManager.shared.isRead(dialog_id: (self!.viewModel?.dialogId)!)
        })
    }
    
    // MARK: - Dialog Detail Request
    
    @objc func loadDialogDetails() {
        viewModel?.getDialogDetails { [weak self] (detail) in
            self?.titleView.userNameLabel.text = detail.name
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: detail.avatar), placeholder: #imageLiteral(resourceName: "bg_gradient_3"))
        }
    }
    
    // MARK: - NMessenger Send Image Action
    
    override func sendContact(imageUrl: String, name: String, phone: String, date: String, userId: Int, isIncomingMessage: Bool) -> GeneralMessengerCell {
        guard (self.info?.partner_block_me == 0) else {
            WhisperHelper.showErrorMurmur(title: "Пользователь заблокировал вас")
            return GeneralMessengerCell()
        }
        return self.postContact(imageUrl: imageUrl, name: name, phone: phone, date: date, userId: userId, isIncomingMessage: isIncomingMessage)
    }
    override func sendSticker(_ image: UIImage,_ sticker_id: String, isIncomingMessage: Bool) -> GeneralMessengerCell {
        guard (self.info?.partner_block_me == 0) else {
            WhisperHelper.showErrorMurmur(title: "Пользователь заблокировал вас")
            return GeneralMessengerCell()
        }
        return self.postSticker(image,sticker_id: sticker_id, isIncomingMessage: isIncomingMessage)
    }
    override func sendImage(_ image: UIImage, isIncomingMessage: Bool) -> GeneralMessengerCell {
        guard (self.info?.partner_block_me == 0) else {
            WhisperHelper.showErrorMurmur(title: "Пользователь заблокировал вас")
            return GeneralMessengerCell()
        }
        return self.postImage(image, isIncomingMessage: isIncomingMessage)
    }
    
    override func sendVideo(_ thumbImage: UIImage, videoUrl: URL, videoData: Data, isIncomingMessage: Bool) -> GeneralMessengerCell {
        guard (self.info?.partner_block_me == 0) else {
            WhisperHelper.showErrorMurmur(title: "Пользователь заблокировал вас")
            return GeneralMessengerCell()
        }
        return self.postVideo(thumbImage, videoUrl: videoUrl,videoData: videoData, isIncomingMessage: isIncomingMessage)
    }
    override func sendAudio(_ duration: String, data: Data, date: String,audioUrl: URL,chat_id:String,fileTime:String, isIncomingMessage:Bool) -> GeneralMessengerCell {
        guard (self.info?.partner_block_me == 0) else {
            WhisperHelper.showErrorMurmur(title: "Пользователь заблокировал вас")
            return GeneralMessengerCell()
        }
        return self.postAudio(duration, data: data, date: date,audioUrl: audioUrl,chat_id:chat_id,fileTime:fileTime,  isIncomingMessage: isIncomingMessage)
    }
    override func sendCollectionViewWithNodes(_ nodes: MessageNode,_ nodesSend: [ASDisplayNode],_ nodeImageContent:ImageContentNode? = nil,_ nodeTwoImageContent:TwoImagesContentNode? = nil,_ nodeThreeImageContent:ThreeImagesContentNode? = nil,_ nodeFourImageContent:FourImagesContentNode? = nil, numberOfRows:CGFloat, isIncomingMessage:Bool) -> GeneralMessengerCell {
        guard (self.info?.partner_block_me == 0) else {
            WhisperHelper.showErrorMurmur(title: "Пользователь заблокировал вас")
            return GeneralMessengerCell()
        }
        return self.postCollectionView(nodes,nodesSend: nodesSend,nodeImageContent,nodeTwoImageContent,nodeThreeImageContent,nodeFourImageContent, numberOfRows: numberOfRows, isIncomingMessage: isIncomingMessage)
    }
    
    override func sendDocument(_ fileName: String, fileExtension: String, fileData: Data,  isIncomingMessage: Bool) -> GeneralMessengerCell {
        guard (self.info?.partner_block_me == 0) else {
            WhisperHelper.showErrorMurmur(title: "Пользователь заблокировал вас")
            return GeneralMessengerCell()
        }
        return self.postDocument(fileName:fileName, fileExtension: fileExtension, fileData: fileData, isIncomingMessage: isIncomingMessage)
    }
    
    override func sendText(_ text: String, date: String, isIncomingMessage: Bool) -> GeneralMessengerCell {
        guard (self.info?.partner_block_me == 0) else {
            WhisperHelper.showErrorMurmur(title: "Пользователь заблокировал вас")
            return GeneralMessengerCell()
        }
        //create a new text message
        guard inputBarView.isReplyMessage.value == false else {
            viewModel?.socketMessageEmit(text: text, onSuccess: {
                
                self.viewModel?.getLastDialogMessages(page:1,per_page:1,requestSuccess: { [weak self] (newMessages) in
                    guard let vc = self else { return }
                    print("fsasfafsa",newMessages)
                    let message = newMessages.last
                    vc.messengerView.addMessage(message!, scrollsToMessage: false)
                    vc.lastMessageGroup = newMessages.last
                    vc.messengerView.scrollToLastMessage(animated: true)
                })
                })
            let textContent = TextContentNode(id: 0, textMessageString: text, dateString: date, currentViewController: self,senderName:"Я", bubbleConfiguration: self.sharedBubbleConfiguration)
             let newMessage = MessageNode(content: textContent)
            textContent.message = newMessage
            inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
            return newMessage
        }
        let textContent = TextContentNode(id: 0, textMessageString: text, dateString: date, currentViewController: self,senderName:"Я", bubbleConfiguration: self.sharedBubbleConfiguration)
        textContent.sendIconNode.image = nil
        textContent.sendIconNode.image = UIImage(named: "icon_mark_double")
        let newMessage = MessageNode(content: textContent)
        textContent.message = newMessage
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.chatVC = self
        self.postText(newMessage, isIncomingMessage: false)
        
        // We set double mark icon when messages saved to db
        viewModel?.socketMessageEmit(text: text, onSuccess: {
    
            self.viewModel?.returnLastChatId(page:1,per_page:1,requestSuccess: { [weak self] (chatId) in
                    guard let vc = self else { return }
                    print(chatId)
                    newMessage.chatId =  chatId["chat_id"].intValue
                    textContent.message_id =  chatId["chat_id"].intValue
                })
          //  textContent.sendIconNode.image = UIImage(named: "icon_mark_double")
            textContent.setNeedsDisplay()
            self.inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
        })
        
        return newMessage
    }
    
    // MARK: - NMessenger Post Text Delegate
    private func postAudio(_ duration: String, data: Data, date: String,audioUrl: URL,chat_id:String,fileTime:String, isIncomingMessage:Bool) -> GeneralMessengerCell {
        let audioContent = AudioContentNode(id:0,duration: duration, date: date, currentVC: self,audioUrl: audioUrl,chat_id :chat_id,fileTime:fileTime,avatarUrl:avatarUrl, senderName: "Я")
        let newMessage = MessageNode(content: audioContent)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.avatarNode = createReply()
        newMessage.chatVC = self
        newMessage.isIncomingMessage = isIncomingMessage
        if (!isIncomingMessage)
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 17,
                             left:  -17,
                             bottom: 17,
                             right: 17)
        }
        else
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 17,
                             left: 17,
                             bottom: 17,
                             right:  -17)
        }
     //   let audioContent = AudioContentNode(duration: duration, date: date, currentVC: self, bubbleConfiguration: self.sharedBubbleConfiguration)
        let timeInSeconds: TimeInterval = Date().timeIntervalSince1970
        guard inputBarView.isReplyMessage.value == false else {
            viewModel?.uploadChatFile(with: data, format: "audio",fileTime :fileTime,fileName:"\(timeInSeconds)AudioRecordingvoice", success: {
            self.viewModel?.getLastDialogMessages(page:1,per_page:1,requestSuccess: { [weak self] (newMessages) in
                    guard let vc = self else { return }
                    let message = newMessages.last
                    vc.messengerView.addMessage(message!, scrollsToMessage: false)
                    vc.lastMessageGroup = newMessages.last
                    vc.messengerView.scrollToLastMessage(animated: true)
                    
                })
            })
            self.inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
            return GeneralMessengerCell()
        }
        self.addMessageToMessenger(newMessage)

        viewModel?.uploadChatFile(with: data, format: "audio",fileTime :fileTime,fileName:"\(timeInSeconds)AudioRecordingvoice", success: {
            self.viewModel?.returnLastChatId(page:1,per_page:1,requestSuccess: { [weak self] (chatId) in
                guard let vc = self else { return }
                print(chatId)
                newMessage.chatId =  chatId["chat_id"].intValue
                audioContent.message_id =  chatId["chat_id"].intValue
                
            })
            self.inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
            print("audio uploaded to server")
        })
        return newMessage
    }
    private func postSticker(_ image: UIImage,sticker_id:String, isIncomingMessage:Bool) -> GeneralMessengerCell {
        let imageContent = ImageContentNode(image: image, date: Date().getStringTime(), currentVC: self,senderName:"Я",is_sticker:1, bubbleConfiguration: self.stickerBubbleConfiguration)
        let newMessage = MessageNode(content: imageContent)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.avatarNode = createReply()
        newMessage.chatVC = self
        newMessage.isIncomingMessage = isIncomingMessage
        if (!isIncomingMessage)
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 60,
                             left: -40,
                             bottom: 60,
                             right: 40)
        }
        else
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 60,
                             left: 40,
                             bottom: 60,
                             right: -40)
        }
        guard inputBarView.isReplyMessage.value == false else {
            viewModel?.sendStickerEmit(sticker_id: sticker_id, onSuccess: {
            self.viewModel?.getLastDialogMessages(page:1,per_page:1,requestSuccess: { [weak self] (newMessages) in
                guard let vc = self else { return }
                let message = newMessages.last
                vc.messengerView.addMessage(message!, scrollsToMessage: false)
                vc.lastMessageGroup = newMessages.last
                vc.messengerView.scrollToLastMessage(animated: true)
                
            })
            })
                self.inputBarView.isReplyMessage.value = false
                self.inputBarView.changeReplyArea(with: "")
                return GeneralMessengerCell()
        }
        self.addMessageToMessenger(newMessage)
        
           viewModel?.sendStickerEmit(sticker_id: sticker_id, onSuccess: {
            self.viewModel?.returnLastChatId(page:1,per_page:1,requestSuccess: { [weak self] (chatId) in
                guard let vc = self else { return }
                print(chatId)
                imageContent.message_id =  chatId["chat_id"].intValue
                newMessage.chatId =  chatId["chat_id"].intValue 
            })
        })
        return newMessage
    }
    private func postImage(_ image: UIImage, isIncomingMessage:Bool) -> GeneralMessengerCell {
        let imageContent = ImageContentNode(image: image, date: Date().getStringTime(), currentVC: self,senderName:"Я", bubbleConfiguration: self.sharedBubbleConfiguration)
        imageContent.uploadBegin()
        let newMessage = MessageNode(content: imageContent)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.avatarNode = createReply()
        newMessage.chatVC = self
        newMessage.isIncomingMessage = isIncomingMessage
        if (!isIncomingMessage)
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 60,
                             left: -40,
                             bottom: 60,
                             right: 40)
        }
        else
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 60,
                             left: 40,
                             bottom: 60,
                             right: -40)
        }
        self.addMessageToMessenger(newMessage)

        // Upload to server process
        if let data = UIImageJPEGRepresentation(image, 0.5) {
            viewModel?.uploadChatFile(with: data, format: "image", success: {
                print("image uploaded to server")
                imageContent.uploadEnd()
                self.viewModel?.returnLastChatId(page:1,per_page:1,requestSuccess: { [weak self] (chatId) in
                    guard let vc = self else { return }
                    print(chatId)
                    imageContent.message_id =  chatId["chat_id"].intValue
                    newMessage.chatId =  chatId["chat_id"].intValue
                    
                })
                self.inputBarView.isReplyMessage.value = false
                self.inputBarView.changeReplyArea(with: "")
            })
        }
        return newMessage
    }
    
    private func postDocument(fileName: String, fileExtension: String, fileData: Data, isIncomingMessage: Bool) -> GeneralMessengerCell {
        let node = DocumentContentNode(id:0,text: fileName, date: Date().getStringTime(), currentVC: self, senderName: "Я", bubbleConfiguration: self.sharedBubbleConfiguration)
        node.uploadBegin()
        let newMessage = MessageNode(content: node)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.isIncomingMessage = isIncomingMessage
        newMessage.chatVC = self
        let serverFileName = fileName.replacingOccurrences(of: " ", with: "_")
        guard inputBarView.isReplyMessage.value == false else {
          viewModel?.uploadChatFile(with: fileData, format: "document", fileExtension: fileExtension, fileName: serverFileName, success: {
                self.viewModel?.getLastDialogMessages(page:1,per_page:1,requestSuccess: { [weak self] (newMessages) in
                    guard let vc = self else { return }
                    let message = newMessages.last
                    vc.messengerView.addMessage(message!, scrollsToMessage: false)
                    vc.lastMessageGroup = newMessages.last
                    vc.messengerView.scrollToLastMessage(animated: true)
                    
                })
            })
            self.inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
            return GeneralMessengerCell()
        }
        self.addMessageToMessenger(newMessage)

        viewModel?.uploadChatFile(with: fileData, format: "document", fileExtension: fileExtension, fileName: serverFileName, success: {
            print("document uploaded to server")
            self.viewModel?.returnLastChatId(page:1,per_page:1,requestSuccess: { [weak self] (chatId) in
                guard let vc = self else { return }
                print(chatId)
                node.message_id =  chatId["chat_id"].intValue
                newMessage.chatId =  chatId["chat_id"].intValue
                
            })
            self.inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
            node.uploadEnd()
        })
        return newMessage
    }
    private func postVideo(_ image: UIImage, videoUrl: URL, videoData: Data, isIncomingMessage: Bool) -> GeneralMessengerCell {
        print(image)
        let videoContent = VideoContentNode(id:0,image: image, videoUrl: videoUrl, date: Date().getStringTime(), currentVC: self,senderName:"Я", bubbleConfiguration: self.sharedBubbleConfiguration)
        videoContent.uploadBegin()
        let newMessage = MessageNode(content: videoContent)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.avatarNode = createReply()
        newMessage.chatVC = self
        newMessage.isIncomingMessage = isIncomingMessage
        if (!isIncomingMessage)
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 65,
                             left:  -43,
                             bottom: 65,
                             right: 43)
        }
        else
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 65,
                             left: 43,
                             bottom: 65,
                             right:  -43)
        }
        var videoExtension = videoUrl.lastPathComponent
        videoExtension = NSString(string: videoExtension).pathExtension
        print(videoExtension)
        guard inputBarView.isReplyMessage.value == false else {
            viewModel?.uploadChatFile(with: videoData, format: "video", fileExtension: videoExtension, success: {
            self.viewModel?.getLastDialogMessages(page:1,per_page:1,requestSuccess: { [weak self] (newMessages) in
                    guard let vc = self else { return }
                    let message = newMessages.last
                    vc.messengerView.addMessage(message!, scrollsToMessage: false)
                    vc.lastMessageGroup = newMessages.last
                    vc.messengerView.scrollToLastMessage(animated: true)
                    
                })
            })
            self.inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
            return GeneralMessengerCell()
        }
        self.addMessageToMessenger(newMessage)
        
        // Upload to server process
        viewModel?.uploadChatFile(with: videoData, format: "video", fileExtension: videoExtension, success: {
            videoContent.uploadEnd()
            self.viewModel?.returnLastChatId(page:1,per_page:1,requestSuccess: { [weak self] (chatId) in
                guard let vc = self else { return }
                print(chatId)
                newMessage.chatId =  chatId["chat_id"].intValue
                videoContent.message_id =   chatId["chat_id"].intValue
                
            })
            self.inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
        })
        return newMessage
    }
    
    private func postContact(imageUrl: String, name: String, phone: String, date: String, userId: Int, isIncomingMessage:Bool) -> GeneralMessengerCell {
        let contactContent = ContactContentNode(id :0,imageUrl: imageUrl, name: name, phone: phone, date: date, currentVC: self, senderName: "Я", bubbleConfiguration: self.sharedBubbleConfiguration, userId: userId)
        let newMessage = MessageNode(content: contactContent)
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.isIncomingMessage = isIncomingMessage
        newMessage.chatVC = self
        guard inputBarView.isReplyMessage.value == false else {
              viewModel?.sendMessageApi(isContact: true, contactPhone: phone, contactName: name,success:{ [weak self] () in
                self!.viewModel?.getLastDialogMessages(page:1,per_page:1,requestSuccess: { [weak self] (newMessages) in
                    guard let vc = self else { return }
                    let message = newMessages.last
                    vc.messengerView.addMessage(message!, scrollsToMessage: false)
                    vc.lastMessageGroup = newMessages.last
                    vc.messengerView.scrollToLastMessage(animated: true)
                    
                })
            })
            self.inputBarView.isReplyMessage.value = false
            self.inputBarView.changeReplyArea(with: "")
            return GeneralMessengerCell()
        }
        self.addMessageToMessenger(newMessage)
        
        viewModel?.sendMessageApi(isContact: true, contactPhone: phone, contactName: name,success:{ [weak self] () in
            self!.viewModel?.returnLastChatId(page:1,per_page:1,requestSuccess: { [weak self] (json) in
            guard let vc = self else { return }
            print("asfasfaafs",json)
            newMessage.chatId = json["chat_id"].intValue
            contactContent.is_exist =  json["contact"]["is_exist"].intValue
            contactContent.contact_user_id = json["contact"]["user_id"].stringValue
            contactContent.message_id = json["chat_id"].intValue
           })
            self!.inputBarView.isReplyMessage.value = false
            self!.inputBarView.changeReplyArea(with: "")
        })
        return newMessage
    }
    
    private func postCollectionView(_ nodes: MessageNode,nodesSend: [ASDisplayNode],_ nodeImageContent:ImageContentNode? = nil,_ nodeTwoImageContent:TwoImagesContentNode? = nil,_ nodeThreeImageContent:ThreeImagesContentNode? = nil,_ nodeFourImageContent:FourImagesContentNode? = nil, numberOfRows:CGFloat, isIncomingMessage:Bool) -> GeneralMessengerCell {
        
        let newMessage = nodes
        newMessage.cellPadding = messagePadding
        newMessage.currentViewController = self
        newMessage.avatarNode = createReply()
        newMessage.chatVC = self
        newMessage.isIncomingMessage = isIncomingMessage
        if (!isIncomingMessage)
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 60,
                             left:  -40,
                             bottom: 60,
                             right: 40)
        }
        else
        {
            newMessage.avatarInsets =
                UIEdgeInsets(top: 60,
                             left: 40,
                             bottom: 60,
                             right:  -40)
        }
        print("afssaf calling count",nodesSend.count)
        // Upload to server process
       var files: Array<[String: String]> = []
        for (index,imageNode) in nodesSend.enumerated() {
            if let imageContent = imageNode as? ImageContentNode {
                imageContent.uploadBegin()
                if let data = UIImageJPEGRepresentation(imageContent.image!, 0.5) {
                    viewModel?.uploadMultipleChatFile(with: data, format: "image"){ [weak self] (file) in
                        files.append(file)
                        imageContent.uploadEnd()
                        if (index == nodesSend.count-1){
                            guard self!.inputBarView.isReplyMessage.value == false else {
                                self!.viewModel?.sendMultipleChatFile(format: "image",files:files,success: {
                                    self!.viewModel?.getLastDialogMessages(page:1,per_page:1,requestSuccess: { [weak self] (newMessages) in
                                        guard let vc = self else { return }
                                        let message = newMessages.last
                                        vc.messengerView.addMessage(message!, scrollsToMessage: false)
                                        vc.lastMessageGroup = newMessages.last
                                        vc.messengerView.scrollToLastMessage(animated: true)
                                        
                                    })
                                })
                                self!.inputBarView.isReplyMessage.value = false
                                self!.inputBarView.changeReplyArea(with: "")
                                return
                            }
                            self!.addMessageToMessenger(newMessage)
                            self!.viewModel?.sendMultipleChatFile(format: "image",files:files,success: {
                            print("message uploaded to server")
                                self!.viewModel?.returnLastChatId(page:1,per_page:1,requestSuccess: { [weak self] (chatId) in
                                    guard let vc = self else { return }
                                    print(chatId)
                                    newMessage.chatId =  chatId["chat_id"].intValue
                                    if let twoImage = nodeTwoImageContent {
                                    twoImage.message_id =  chatId["chat_id"].intValue
                                        print("twoImage")
                                    }
                                    else if let threeImage = nodeThreeImageContent{
                                    threeImage.message_id =  chatId["chat_id"].intValue
                                           print("threeImage")
                                    }
                                    else if let fourImage = nodeFourImageContent{
                                    fourImage.message_id =  chatId["chat_id"].intValue
                                            print("fourImage")
                                    }
                                })
                                self!.inputBarView.isReplyMessage.value = false
                                self!.inputBarView.changeReplyArea(with: "")
                        })
                    }
                    }
                }
            }
        }
        
        return newMessage
    }
    
    private func postText(_ message: MessageNode, isIncomingMessage: Bool) {
        if self.lastMessageGroup == nil || self.lastMessageGroup?.isIncomingMessage == !isIncomingMessage {
            self.lastMessageGroup = self.createMessageGroup()
            self.lastMessageGroup!.isIncomingMessage = isIncomingMessage
            self.messengerView.addMessageToMessageGroup(message, messageGroup: self.lastMessageGroup!, scrollsToLastMessage: false)
            self.messengerView.addMessage(self.lastMessageGroup!, scrollsToMessage: true, withAnimation: isIncomingMessage ? .left : .right)
            
          
        } else {
            self.messengerView.addMessageToMessageGroup(message, messageGroup: self.lastMessageGroup!, scrollsToLastMessage: true)
        }
    }
    
    /**
     Creates a new message group for *lastMessageGroup*
     -returns: MessageGroup
     */
    
    private func createMessageGroup() -> MessageGroup {
        let newMessageGroup = MessageGroup()
        newMessageGroup.currentViewController = self
        newMessageGroup.cellPadding = self.messagePadding
        return newMessageGroup
    }
    private func createReply() -> ASNetworkImageNode {
        let avatar = ASNetworkImageNode()
        avatar.backgroundColor = UIColor.lightGray
        avatar.style.preferredSize = CGSize(width: 26, height: 26)
        avatar.layer.cornerRadius = 13
        avatar.image = #imageLiteral(resourceName: "icon_reply")
        return avatar
    }
    func inputBarViewEvents() {
        self.inputBarView.isReplyMessage.asObservable().subscribe(onNext: { [weak self] (value) in
            self?.viewModel?.isReplyMessage = value
            print(value)
        }).disposed(by: disposeBag)
    }
    
}

extension ChatController {
    
    // MARK: - Configuring NavBar TitleView
    
    func configureNavBar(_ data: DialogInfo) {
        let titleItem = UIBarButtonItem(customView: titleView)
        self.navigationItem.leftBarButtonItem = titleItem
        self.navigationItem.leftItemsSupplementBackButton = true
        let userNameText = data.user_name.isEmpty ? data.phone : data.user_name
        titleView.userNameLabel.text = userNameText
        guard let vm = viewModel else { return }
        var imagePlaceholder = #imageLiteral(resourceName: "bg_gradient_2")
        var statusPlaceholder = ""
        
        switch vm.chatType {
        case .single:
            statusPlaceholder = data.last_visit
            imagePlaceholder = #imageLiteral(resourceName: "bg_gradient_2")
        case .group:
            statusPlaceholder = "Группа"
            imagePlaceholder = #imageLiteral(resourceName: "bg_gradient_1")
        case .channel:
            statusPlaceholder = "Канал"
            imagePlaceholder = #imageLiteral(resourceName: "bg_gradient_3")
        }
        titleView.userStatusLabel.text = statusPlaceholder
        titleView.userAvatarView.kf.setImage(with: URL(string: data.avatar.encodeUrl()!), placeholder: imagePlaceholder)
        
        let createSettings = UIBarButtonItem(image: UIImage(named: "icon_setting"), style: .plain, target: self, action: #selector(settingAction))
        createSettings.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItems = [createSettings]
        
    }
    @objc func settingAction(){
        let alert = UIAlertController(title: "choise".localized(), message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        var  disableActionTitle = ""
        if (isMute == 0){
            disableActionTitle = "disable_notification".localized()
        }
        else {
            disableActionTitle = "enable_notification".localized()
        }
        let disableAction = UIAlertAction(title: disableActionTitle, style: .default) { (act) in
            var param = ["is_mute": "\(self.isMute)","partner_id":"\(self.info!.user_id)"]
            if self.info!.user_id != 0 {
                param = ["is_mute": "\(self.isMute)","partner_id":"\(self.info!.user_id)"]
            } else if  self.info!.group_id != 0 {
                param = ["is_mute": "\(self.isMute)","group_id":"\(self.info!.group_id)"]
            } else if self.info!.channel_id != 0 {
                param = ["is_mute": "\(self.isMute)","channel_id":"\(self.info!.channel_id)"]
            }
            self.viewModelDialog.muteDialog(by: self.dialogId, params: param)
        }
        var titleForBlock = ""
        
        if (self.info?.i_block_partner == 1){
            titleForBlock = "unblock".localized()
        }
        else {
            titleForBlock = "block".localized()
            
        }
        let blockAction = UIAlertAction(title: titleForBlock, style: .default) { (act) in
            self.viewModel!.blockUserRequest()
        }
        let clearAction = UIAlertAction(title: "clear_chat".localized(), style: .default)
        { (act) in
            let alert = UIAlertController(title: "are_u_sure".localized(), message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        let clearAction = UIAlertAction(title: "clear_chat".localized(), style: .default, handler: { [unowned self] (act) in
            self.viewModel!.clearChatRequest()
            self.cancelAction()
        })
            
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            alert.addAction(clearAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            
    }
    let searchAction = UIAlertAction(title: "search".localized(), style: .default) { (act) in
        // Create the search controller and specify that it should present its results in this same view
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchResultCount = UIBarButtonItem(title: "0 из 0", style: .plain, target: self, action:nil)
        self.searchResultCount.setTitleTextAttributes([
            NSAttributedStringKey.font : UIFont(name: "Helvetica-Bold", size: 13)!,
            NSAttributedStringKey.foregroundColor : UIColor.gray],
                                                      for: UIControlState.normal)
        // Set any properties (in this case, don't hide the nav bar and don't show the emoji keyboard option)
        self.searchController!.hidesNavigationBarDuringPresentation = false
        self.searchController!.dimsBackgroundDuringPresentation = false
        self.searchController!.searchBar.keyboardType = UIKeyboardType.default
      //  self.searchController!.searchBar.searchBarStyle =  .minimal
        self.searchController!.searchBar.placeholder = "search".localized()
        self.searchController!.searchBar.sizeToFit()
        // Make this class the delegate and present the search
        self.searchController!.searchBar.delegate = self
      //  self.inputBarView.isHidden = true
        //  self.messengerView.frame..constraint(equalTo
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            self.createUpArrayButton(),
            self.createDownArrayButton(),
            self.searchResultCount]
        numberToolbar.sizeToFit()
        self.searchController!.searchBar.inputAccessoryView = numberToolbar
        self.present(self.searchController!, animated: true, completion: nil)
        self.inputBarView.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                let bottomPadding = window?.safeAreaInsets.bottom
               self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height + 50 + bottomPadding!)
                }
                else {
                self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height + 50)
                }
        }
        }
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(disableAction)
        alert.addAction(blockAction)
        alert.addAction(clearAction)
        alert.addAction(searchAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @objc func cancelAction(){
        UIView.animate(withDuration: 0.3) {
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    let bottomPadding = window?.safeAreaInsets.bottom
                    self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height - 50 - bottomPadding!)
                }
                else {
                self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height - 50)
                }
            
        }
        selectionMode = false
        
        navigationController?.setToolbarHidden(true, animated: true)
        self.inputBarView.isHidden = false
        self.messagesList.removeAll()
        self.messagesCells.removeAll()
        NotificationCenter.default.post(name: AppManager.selectNotification, object: nil)
        NotificationCenter.default.post(name: AppManager.selectionCanceled, object: nil)
        self.navigationItem.title = nil
        configureNavBar(info!)
        self.navigationItem.setHidesBackButton(false, animated:true)
    }
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setToolbarHidden(true, animated: true)
    }
    // MARK: - NavBar TitleView Action
    
    @objc func userTitleViewPressed() {
        
        let transition = CATransition()
        transition.duration = 0.35
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)

        guard let vm = viewModel else { return }
        switch vm.chatType {
        case .single:
            let vc = UserProfileController.fromStoryboard()
            vc.contact = viewModel!.info
            vc.dialogId = viewModel!.dialogId
            vc.user_id = viewModel!.info.user_id
            self.show(vc, sender: nil)
        case .group:
            let vc = EditGroupController.fromStoryboard()
            vc.groupInfo = viewModel!.info
            vc.user_id = (User.currentUser()?.user_id)!
            self.show(vc, sender: nil)
        case .channel:
            let vc = EditChannelController.fromStoryboard()
            vc.channelInfo = viewModel!.info
            vc.user_id = (User.currentUser()?.user_id)!
            self.show(vc, sender: nil)
        }
    }
}

extension ChatController: TextContentNodeOutput {
    
    func messageReplyAction(text: String,messageId: Int,title:String,image:UIImage) {
        print(title)
        self.viewModel?.replyMessageId = messageId
      //  self.viewModel?.replyMessageId = messageId
        let imageForTest: UIImage = UIImage(named: "icon_create")!
        if (image == imageForTest){
            self.inputBarView.changeReplyArea(with: text,title: title)
        }
        else {
            self.inputBarView.changeReplyArea(with: text,title: title,image:image)
        }
        self.inputBarView.isReplyMessage.value = true
        
    }
    func configureEvents() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
            self!.viewModel?.avatarUrl = self!.viewModelProfile.avatar.value.encodeUrl()!
        })
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
}
extension ChatController: TextContentFirstReply {
    
    func messagefirstReplyAction(text: String,messageId: Int,title:String,image:UIImage) {
        firstAnswerTitle = title
        firstAnswerText = text
        firstAnswerImage = image
        firstAnswerMessageId = messageId
        }
}
extension ChatController: RemoveMessageOutput {
    
    func RemoveMessageAction(messageId:Int) {
        self.viewModel?.clearMessagesRequest(messagesList:[messageId],success: {
            DialogHistory.removeChatIds(dialog_id: self.dialogId, messagesList: [messageId])
            self.messagesList.removeAll()
            self.messagesCells.removeAll()
            self.messengerView.clearALLMessages()
            self.setupBubbleMessages()
    })
}
}
extension ChatController: MessageSelectedNodeOutput {
    
     func messageSelectedAction(messageId: Int,messageNode:GeneralMessengerCell) {
        if (selectionMode == false) {
        
        selectionMode = true
        navigationController?.setToolbarHidden(false, animated: true)
        self.inputBarView.isHidden = true
        //  self.messengerView.frame..constraint(equalTo
            UIView.animate(withDuration: 0.3) {
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    let bottomPadding = window?.safeAreaInsets.bottom
                    
               self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height + 50 + bottomPadding!)
                }
                else {
                self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height + 50)
                }
            }
            let message:[String: Int] = ["messageId": messageId]
        NotificationCenter.default.post(name: AppManager.selectNotification, object: nil, userInfo: message)
            messagesList.append(messageId)
            messagesCells.append(messageNode)
            createSelectionNavigationBar()
            self.view.endEditing(true)
        }
        else {
            if (messagesList.count < 2){
                toolbarItems?.remove(at: 4)
            }
            else {
                createToolbar()
            }
            var needAdd = false
            for (i,num) in messagesList.enumerated().reversed() {
                if (messageId == messagesList[i]){
                    needAdd = true
                    messagesCells.remove(at: i)
                    messagesList.remove(at: i)
                }
            }
            if (needAdd == false){
            messagesList.append(messageId)
            messagesCells.append(messageNode)
            }
            print("asffssa")
            self.navigationItem.title = "Выбрано: \(messagesList.count)"
        }
      //  print(text)
      //  self.viewModel?.replyMessageId = messageId
       // self.inputBarView.changeReplyArea(with: text)
      //  self.inputBarView.isReplyMessage.value = true
    }
    func createSelectionNavigationBar(){
        let cancelButton = UIButton(type: .custom)
        cancelButton.setTitle("cancel".localized(), for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        cancelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        self.navigationItem.leftBarButtonItem = item
        self.navigationItem.title = "Выбрано:\(messagesList.count)"
        self.navigationItem.setHidesBackButton(true, animated:true)
    }
}
extension ChatController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        self.viewModel?.getSearchMessages(search:searchText,requestSuccess: { [weak self] (result) in
            self!.searchResultCount.title = "0 из \(result.count)"
        self!.searchController!.searchBar.inputAccessoryView?.updateFocusIfNeeded()
            self!.searchResult = result.reversed()
            self!.searchIndex = 0
        })
     //   viewModel.searchDialog(by: searchText, onReload: { [weak self] in
       //     self?.tableView.reloadData()
    //    })
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.inputBarView.isHidden = false
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let bottomPadding = window?.safeAreaInsets.bottom
            self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height - 50 - bottomPadding!)
        }
        else {
            self.messengerView.frame = CGRect(self.messengerView.frame.origin.x, self.messengerView.frame.origin.y,self.messengerView.frame.width, self.messengerView.frame.height - 50 )
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
    //        viewModel.getAllDialogs(onReload: { [weak self] in
       //         self?.tableView.reloadData()
       //     })
            return
        }
        self.viewModel?.getSearchMessages(search:searchText,requestSuccess: { [weak self] (result) in
        self!.searchResultCount.title = "0 из \(result.count)"
        self!.searchController!.searchBar.inputAccessoryView?.updateFocusIfNeeded()
            self!.searchResult = result.reversed()
            print("asffsaasfaf", self!.searchResult ,result )
            self!.searchIndex = 0
        })
        print(searchText)
    }
    
    func createUpArrayButton()->UIBarButtonItem{
        let upButton = UIButton(type: .custom)
        let image: UIImage = UIImage(named: "icon_arrow_up")!
        upButton.setImage(image, for: .normal)
        upButton.setTitleColor(UIColor.red, for: .normal)
        upButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        upButton.addTarget(self, action: #selector(upArrowAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: upButton)
        return item
    }
    func createDownArrayButton()->UIBarButtonItem{
        let downButton = UIButton(type: .custom)
        let image: UIImage = UIImage(named: "icon_arrow_down")!
        downButton.setImage(image, for: .normal)
        downButton.setTitleColor(UIColor.red, for: .normal)
        downButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        downButton.addTarget(self, action: #selector(downArrowAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: downButton)
        return item
    }
    @objc func upArrowAction() {
        if (searchIndex < searchResult.count){
        self.viewModel?.getPageByChatId(chatId:searchResult[searchIndex],requestSuccess: { [weak self] (result) in            print("asasffas",self?.viewModel?.searchModel.last!.messageGroups.count,self?.viewModel?.searchModel.last!.messegesChatId.count,result.count)
            print("afsxxcsfas",self!.searchIndex,result)
            var searchNext = false
        self!.searchController!.searchBar.inputAccessoryView?.updateFocusIfNeeded()
                var finded = false
                for index2 in 0..<self!.viewModel!.searchModel.count {
                    for index3 in 0..<self!.viewModel!.searchModel[index2].messegesChatId.count {
                    for index4 in 0..<self!.viewModel!.searchModel[index2].messegesChatId[index3].count {
                        if (!finded){
                    if (self!.viewModel!.searchModel[index2].messegesChatId[index3][index4] == self!.searchResult[self!.searchIndex]){
                        self!.searchIndex += 1
                        self!.searchResultCount.title = "\(self!.searchIndex) из \(self!.searchResult.count)"
                               print("affsfоооороas working",(self!.viewModel!.searchModel[index2].messegesChatId[index3][index4],index2,index3,index4))
                            self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index2].messageGroups[index3], atPosition: UITableViewScrollPosition.none, animated: true)
                        finded = true
                           SVProgressHUD.dismiss()
                        break
                        }
                    else {
                        if (index4 == self!.viewModel!.searchModel[index2].messegesChatId[index3].count - 1)&&(index3 == self!.viewModel!.searchModel[index2].messegesChatId.count - 1)&&(index2 == self!.viewModel!.searchModel.count - 1)&&(finded == false){
                            print("afssfafaf calling here or not")
                                     if  (result.count > 1) && (result[0] < result[1]){
                                      self!.createNextSearchBatch(page:result[1],requestSuccess: { [weak self] () in                                               
                                                print("currentBatchCounter calling ==1")
                                                self!.getAllMessages(requestSuccess: { [weak self] () in
                                                    for index7 in 0..<self!.viewModel!.searchModel.count {
                                                        for index8 in 0..<self!.viewModel!.searchModel[index7].messegesChatId.count {
                                                            for index9 in 0..<self!.viewModel!.searchModel[index7].messegesChatId[index8].count {
                                            print("zxzxzxz",self!.viewModel!.searchModel[index7].messegesChatId[index8][index9],self!.searchIndex)
                                                    if (!finded){
                                                                if (self!.viewModel!.searchModel[index7].messegesChatId[index8][index9] == self!.searchResult[self!.searchIndex]){
                            
                                                        self!.searchIndex += 1
                                                        finded = true
                                                                    self!.searchResultCount.title = "\(self!.searchIndex) из \(self!.searchResult.count)"
                                                                    print("affsfas workinzxxzxzg",self!.viewModel!.searchModel[index7].messegesChatId[index8][index9],index7,index8,index9)
                                                                    self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index7].messageGroups[index8], atPosition: UITableViewScrollPosition.none, animated: true)
                                                              SVProgressHUD.dismiss()
                                                                    print("afsfsafa",index7,self!.viewModel!.searchModel.count)
                                                                    break
                                                                    
                                                                    
                                                                }
                                                       
                                                                }
                                                            }}}
                                                })
                                            
                                        })
                                    
                                }
                                else {
                                    self!.createNextSearchBatch(page:result[0],requestSuccess: { [weak self] () in
                                                self!.getAllMessages(requestSuccess: { [weak self] () in
                                                    for index7 in 0..<self!.viewModel!.searchModel.count {
                                                        for index8 in 0..<self!.viewModel!.searchModel[index7].messegesChatId.count {
                                                            for index9 in 0..<self!.viewModel!.searchModel[index7].messegesChatId[index8].count {
                                                        if (!finded){
                                                                if (self!.viewModel!.searchModel[index7].messegesChatId[index8][index9] == self!.searchResult[self!.searchIndex]){
                                                        finded = true
                                                        self!.searchIndex += 1
                                                                    self!.searchResultCount.title = "\(self!.searchIndex) из \(self!.searchResult.count)"
                                                                    print("affsfas working",self!.viewModel!.searchModel[index7].messegesChatId[index8][index9],index7,index8,index9)
                                                                    self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index7].messageGroups[index8], atPosition: UITableViewScrollPosition.none, animated: true)
                                                                     SVProgressHUD.dismiss()
                                                                    break
                                                                }
                                                            }}}
                                                    }
                                                })
                                            
                                        })
                                    
                                }
                            }

                        }
                        }
                        }}
            }
            
        })

        }
    }
    @objc func downArrowAction() {
        if (searchIndex > 1){
            self.searchIndex -= 1
            self.searchResultCount.title = "\(self.searchIndex) из \(self.searchResult.count)"
            self.viewModel?.getPageByChatId(chatId:searchResult[searchIndex],requestSuccess: { [weak self] (result) in
                print("asasffas",self?.viewModel?.searchModel.last!.messageGroups.count,self?.viewModel?.searchModel.last!.messegesChatId.count,result.count)
                var searchNext = false
                self!.searchController!.searchBar.inputAccessoryView?.updateFocusIfNeeded()
                var finded = false
                for index2 in 0..<self!.viewModel!.searchModel.count {
                    print("asfsfafas",index2,self!.viewModel!.searchModel.count)
                    for index3 in 0..<self!.viewModel!.searchModel[index2].messegesChatId.count {
                        for index4 in 0..<self!.viewModel!.searchModel[index2].messegesChatId[index3].count {
                            if (self!.viewModel!.searchModel[index2].messegesChatId[index3][index4] == self!.searchResult[self!.searchIndex]){
                                print("affsfоооороas working",(self!.viewModel!.searchModel[index2].messegesChatId[index3][index4],index2,index3,index4))
                                self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index2].messageGroups[index3], atPosition: UITableViewScrollPosition.none, animated: true)
                                finded = true
                                SVProgressHUD.dismiss()
                                break
                            }
                            else {
                                if (index4 == self!.viewModel!.searchModel[index2].messegesChatId[index3].count - 1)&&(index3 == self!.viewModel!.searchModel[index2].messegesChatId.count - 1)&&(index2 == self!.viewModel!.searchModel.count - 1)&&(finded == false){
                                           if  (result.count > 1) && (result[0] < result[1]){
                                        self!.createNextSearchBatch(page:result[1],requestSuccess: { [weak self] () in
                                            
                                                    print("currentBatchCounter calling ==1")
                                                    self!.getAllMessages(requestSuccess: { [weak self] () in
                                                        for index7 in 0..<self!.viewModel!.searchModel.count {
                                                            for index8 in 0..<self!.viewModel!.searchModel[index7].messegesChatId.count {
                                                                for index9 in 0..<self!.viewModel!.searchModel[index7].messegesChatId[index8].count {
                                                                    print("asfsafasf",self!.searchResult,self!.searchIndex)
                                                                    if (self!.viewModel!.searchModel[index7].messegesChatId[index8][index9] == self!.searchResult[self!.searchIndex]){
                                                                        print("affsfas working",self!.viewModel!.searchModel[index7].messegesChatId[index8][index9],index7,index8,index9)
                                                                        self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index7].messageGroups[index8], atPosition: UITableViewScrollPosition.none, animated: true)
                                                                        SVProgressHUD.dismiss()
                                                                        print("afsfsafa",index7,self!.viewModel!.searchModel.count)
                                                                        break
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    
                                                                }}}
                                                    })
                                                
                                            })
                                        
                                    }
                                    else {
                                            self!.createNextSearchBatch(page:result[0],requestSuccess: { [weak self] () in
                                                    print("currentBatchCounter calling == 2")
                                                    self!.getAllMessages(requestSuccess: { [weak self] () in
                                                        for index7 in 0..<self!.viewModel!.searchModel.count {
                                                            for index8 in 0..<self!.viewModel!.searchModel[index7].messegesChatId.count {
                                                                for index9 in 0..<self!.viewModel!.searchModel[index7].messegesChatId[index8].count {
                                                                    if (self!.viewModel!.searchModel[index7].messegesChatId[index8][index9] == self!.searchResult[self!.searchIndex]){
                                                                        print("affsfas working",self!.viewModel!.searchModel[index7].messegesChatId[index8][index9],index7,index8,index9)
                                                                        self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index7].messageGroups[index8], atPosition: UITableViewScrollPosition.none, animated: true)
                                                                        SVProgressHUD.dismiss()
                                                                        break
                                                                    }
                                                                }}}
                                                    })
                                                
                                            })
                                        
                                    }
                                }
                                
                            }
                            
                        }}
                }
                
            })
    }
    }
    func getAllMessages(requestSuccess: @escaping () -> Void) {
        viewModel?.getMultipleMessagesFromRealm(currentBatchCounter:0) { [weak self] in
            guard let vc = self, let vm = self?.viewModel else { return }
            vc.messengerView.clearALLMessages()
            print("afssfaf working")
            vc.messengerView.addMessages(vm.messageGroups, scrollsToMessage: false)
            vc.lastMessageGroup = vm.messageGroups.last
         //   vc.messengerView.scrollToLastMessage(animated: false)
            vc.messagesGroup = vm.messageGroups
            requestSuccess()
        }
    }
    private func createNextSearchBatch(page:Int,requestSuccess: @escaping () -> Void) {
            currentBatchCounter = page * 10
        var checkValue = false
        let realm = try! Realm()
        let historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
            if (historyObjects.count >= page * 10){
                requestSuccess()
                print("checkValue",checkValue)
            }
            else {
                checkValue = true
            }
        if (checkValue){
        viewModel?.getDialogMessagesForStore(page:1,per_page:page * 10,search:true,requestSuccess: { [weak self] () in
                    requestSuccess()
                
            })
            }
}
}

extension ChatController: AnswerMessage {
    
    func answerMessageClicked(chatId: Int) {
        self.viewModel?.getPageByChatId(chatId:chatId,requestSuccess: { [weak self] (result) in            print("asasffas",self?.viewModel?.searchModel.last!.messageGroups.count,self?.viewModel?.searchModel.last!.messegesChatId.count,result.count)
            var searchNext = false
            var finded = false
            for index2 in 0..<self!.viewModel!.searchModel.count {
                for index3 in 0..<self!.viewModel!.searchModel[index2].messegesChatId.count {
                    for index4 in 0..<self!.viewModel!.searchModel[index2].messegesChatId[index3].count {
                        if (self!.viewModel!.searchModel[index2].messegesChatId[index3][index4] == chatId){
                            print("affsfоооороas working",(self!.viewModel!.searchModel[index2].messegesChatId[index3][index4],index2,index3,index4))
                            self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index2].messageGroups[index3], atPosition: UITableViewScrollPosition.none, animated: true)
                            finded = true
                            SVProgressHUD.dismiss()
                        }
                        else {
                            if (index4 == self!.viewModel!.searchModel[index2].messegesChatId[index3].count - 1)&&(index3 == self!.viewModel!.searchModel[index2].messegesChatId.count - 1)&&(index2 == self!.viewModel!.searchModel.count - 1)&&(finded == false){
                                print("afssfafaf calling here or not")
                                if  (result.count > 1) && (result[0] < result[1]){
                                        self!.createNextSearchBatch(page:result[1],requestSuccess: { [weak self] () in
                                            
                                                print("currentBatchCounter calling ==1")
                                                self!.getAllMessages(requestSuccess: { [weak self] () in
                                                    for index7 in 0..<self!.viewModel!.searchModel.count {
                                                        for index8 in 0..<self!.viewModel!.searchModel[index7].messegesChatId.count {
                                                            for index9 in 0..<self!.viewModel!.searchModel[index7].messegesChatId[index8].count {
                                                                print("asfsafasf",chatId,self!.searchIndex)
                                                                if (self!.viewModel!.searchModel[index7].messegesChatId[index8][index9] == chatId){
                                                                    print("affsfas working",self!.viewModel!.searchModel[index7].messegesChatId[index8][index9],index7,index8,index9)
                                                                    self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index7].messageGroups[index8], atPosition: UITableViewScrollPosition.none, animated: true)
                                                                    SVProgressHUD.dismiss()
                                                        break
                                                                    print("afsfsafa",index7,self!.viewModel!.searchModel.count)
                                                                    
                                                                    
                                                                }
                                                                
                                                                
                                                            }}}
                                                })
                                            
                                        })
                                }
                                else {
                                    self!.createNextSearchBatch(page:result[0],requestSuccess: { [weak self] () in
                                            print("affasfs calling second")
                                                print("currentBatchCounter calling == 2")
                                                self!.getAllMessages(requestSuccess: { [weak self] () in
                                                    for index7 in 0..<self!.viewModel!.searchModel.count {
                                                        for index8 in 0..<self!.viewModel!.searchModel[index7].messegesChatId.count {
                                                            for index9 in 0..<self!.viewModel!.searchModel[index7].messegesChatId[index8].count {
                                                                if (self!.viewModel!.searchModel[index7].messegesChatId[index8][index9] == chatId){
                                                                    print("affsfas working",self!.viewModel!.searchModel[index7].messegesChatId[index8][index9],index7,index8,index9)
                                                                    self?.messengerView.scrollToMessage(self!.viewModel!.searchModel[index7].messageGroups[index8], atPosition: UITableViewScrollPosition.none, animated: true)
                                                                    SVProgressHUD.dismiss()
                                                        break
                                                                }
                                                            }}}
                                                })
                                            
                                        })
                                    
                                }
                            }
                            
                        }
                        
                    }}
            }
            
           
        })
    }
}
extension ChatController: TextContentCount {
    
    func changeTextContentCount(id: Int, textMessageString: String, dateString: String,is_read:Int,senderName:String,is_resend: Int,maxLineCount:Int, bubbleConfiguration: BubbleConfigurationProtocol, viewsCount: Int,message:GeneralMessengerCell) {
        for index2 in 0..<self.viewModel!.searchModel.count {
            for index3 in 0..<self.viewModel!.searchModel[index2].messegesChatId.count {
                for index4 in 0..<self.viewModel!.searchModel[index2].messegesChatId[index3].count {
                    if (self.viewModel!.searchModel[index2].messegesChatId[index3][index4] == id){
                        var maxLine = 15
                        print("afsasfsa",maxLineCount)
                        if (maxLineCount > 15){
                        maxLine = 15
                        }
                        else {
                        maxLine = 10000
                        }
                        let textContent = TextContentNode(id: id,textMessageString: textMessageString, dateString: dateString,is_read:is_read, currentViewController: self,senderName:senderName,is_resend: is_resend,maxLineCount: maxLine, bubbleConfiguration: bubbleConfiguration)
                        let messageNode = MessageNode(content: textContent)
                       textContent.message = messageNode
                    self.viewModel!.searchModel[index2].messageGroups[index3].replaceMessage(message, withMessage: messageNode, completion: nil)

                    }
                }
            }
        }

}
}
