//
//  ChatViewModel.swift
//  Dalagram
//
//  Created by Toremurat on 01.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift
import SwiftyJSON
import SocketIO
import Photos
import SVProgressHUD
import AsyncDisplayKit
import Alamofire
class ChatViewModel {
    
    var messageGroups = [MessageGroup]()
    var searchModel = [SearchModel]()
    var messages: Results<DialogHistory>?
    
    var info: DialogInfo!
    var chatType: DialogType = .single
    let realm = try! Realm()
    var isReplyMessage: Bool = false
    var replyMessageId: Int = 0
    var dialogs: Results<Dialog>?
    var dialogId: String = ""
    var socket: SocketIOClient!
    var chatVC: ChatController!
    var lastRecievedChatId: Int = 0
    var viewModelProfile = ProfileViewModel()
    let disposeBag = DisposeBag()
    var avatarUrl  = ""
    var isToday: Bool = false
    // MARK: - ViewModel Initializer
    
    init(_ dialogId: String, info: DialogInfo, chatType: DialogType,avatarUrl:String) {
        self.dialogId = dialogId
        self.info = info
        print("asffsa",info)
        self.chatType = chatType
        self.avatarUrl = avatarUrl
        dialogs = realm.objects(Dialog.self).sorted(byKeyPath: "updatedDate")
    }
    // MARK: - Load Data to MessageGroups
    func loadNewMessageFromRealmSocket(success: @escaping ([MessageGroup]) -> Void) {
        var newMessageGroups = [MessageGroup]()
        //        for message in realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId)) {
        //var         print(message.chat_id, lastRecievedChatId)
        //
        //                addRealmResults(with: message, newMessageGroup: newMessageGroups)
        //            }
        //        }
        newMessageGroups = addRealmFromSocket(isFirstLoading: true)
        success(newMessageGroups)
    }
    
    func loadNewMessageFromRealm(success: @escaping ([MessageGroup]) -> Void) {
        var newMessageGroups = [MessageGroup]()
//        for message in realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId)) {
//            print(message.chat_id, lastRecievedChatId)
//
//                addRealmResults(with: message, newMessageGroup: newMessageGroups)
//            }
//        }
        newMessageGroups = addRealmResults(isFirstLoading: false)
        success(newMessageGroups)
    }
    
    func loadMessagesFromRealm(success: @escaping () -> Void) {
//        let realm = try! Realm()
//        let historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
//        for message in historyObjects {
//            addRealmResults(with: message, newMessageGroup: messageGroups)
//        }
        
        messageGroups = addRealmResults()
        success()
    }
    func BatchloadMessagesFromRealm(currentBatchCounter: Int,success: @escaping () -> Void) {
        //        let realm = try! Realm()
        //        let historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
        //        for message in historyObjects {
        //            addRealmResults(with: message, newMessageGroup: messageGroups)
        //        }
        
        messageGroups = BatchLoaderRealm(currentBatchCounter:currentBatchCounter)
        success()
    }
    func getMultipleMessagesFromRealm(currentBatchCounter: Int,success: @escaping () -> Void) {
        //        let realm = try! Realm()
        //        let historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
        //        for message in historyObjects {
        //            addRealmResults(with: message, newMessageGroup: messageGroups)
        //        }
        
        messageGroups = LoadMultipleRealm(currentBatchCounter:currentBatchCounter)
        success()
    }
    private func LoadMultipleRealm(currentBatchCounter: Int) -> [MessageGroup] {
        let realm = try! Realm()
        var historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
        historyObjects = historyObjects.sorted(byKeyPath : "chat_id")
        var messageGroups: [MessageGroup] = []
        var searchChatId = [Int]()
        guard currentBatchCounter<=historyObjects.count else {
            return messageGroups
        }
        //  var reversed = Array(historyObjects.reversed())
        searchModel.removeAll()
        var reversed = [DialogHistory]()
        
        for arrayIndex in 0..<historyObjects.count {
            reversed.append(historyObjects[(historyObjects.count - 1) - arrayIndex])
        }
        reversed = Array(reversed).reversed()
        //   reversed = Array(reversed.reversed())
        var previousDate = ""
        for index in (0..<reversed.count){
            let message = reversed[index]
            if message.file_list.count == 2 {
                var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                if self.chatType == .group {
                    if let user = User.currentUser() {
                        isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                    }
                }
                else if self.chatType == .channel {
                    isIncomingMessage = true
                }
                // Images
                let imageNode = TwoImagesNode(id:message.chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                var messageNode = MessageNode(content: imageNode)
                if (message.answer_chat_id != 0){
                    if (message.is_has_file == 1){
                        var senderName = message.sender_name
                        if (message.sender_user_id == info.user_id){
                            senderName = "Вы"
                        }
                        var fileFormatTitle = message.answer_chatText
                        switch message.answerFileFormat {
                        case "audio":
                            fileFormatTitle = "Аудио"
                        case "file":
                            fileFormatTitle = "Документ"
                        case "video":
                            fileFormatTitle = "Видеозапись"
                        case "image":
                            fileFormatTitle = "Фото"
                        default: break
                        }
                        let imageNode = TwoImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: imageNode)
                    }}
                messageNode.cellPadding = chatVC.messagePadding
                messageNode.avatarNode = createReply()
                messageNode.chatVC = chatVC
                messageNode.chatId = message.chat_id
                if (!isIncomingMessage)
                {
                    messageNode.avatarInsets =
                        UIEdgeInsets(top: 60,
                                     left: -40,
                                     bottom: 60,
                                     right: 40)
                }
                else
                {
                    messageNode.avatarInsets =
                        UIEdgeInsets(top: 60,
                                     left: 40,
                                     bottom: 60,
                                     right: -40)
                }
                let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                searchChatId.append(message.chat_id)
                newMessageGroup.isIncomingMessage = isIncomingMessage
                if chatType != .single {
                    //     newMessageGroup.isIncomingMessage = true
                }
                messageGroups.append(newMessageGroup)
                let searchModelLocal = SearchModel()
                searchModelLocal.messageGroups.append(newMessageGroup)
                searchModelLocal.messegesChatId.append(searchChatId)
                searchModel.append(searchModelLocal)
                
            }
                
            else  if message.file_list.count == 3 {
                var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                if self.chatType == .group {
                    if let user = User.currentUser() {
                        isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                        print("afsaffas",message.sender_user_id," ",user.user_id )
                        
                    }
                }
                else if self.chatType == .channel {
                    isIncomingMessage = true
                }
                let imageNode = ThreeImagesNode(id:message.chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,imageURL3:message.file_list[2].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                
                var messageNode = MessageNode(content: imageNode)
                if (message.answer_chat_id != 0){
                    if (message.is_has_file == 1){
                        var senderName = message.sender_name
                        if (message.sender_user_id == info.user_id){
                            senderName = "Вы"
                        }
                        var fileFormatTitle = message.answer_chatText
                        switch message.answerFileFormat {
                        case "audio":
                            fileFormatTitle = "Аудио"
                        case "file":
                            fileFormatTitle = "Документ"
                        case "video":
                            fileFormatTitle = "Видеозапись"
                        case "image":
                            fileFormatTitle = "Фото"
                        default: break
                        }
                        let imageNode = ThreeImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,imageURL3:message.file_list[2].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: imageNode)
                    }}
                messageNode.cellPadding = chatVC.messagePadding
                messageNode.avatarNode = createReply()
                messageNode.chatVC = chatVC
                messageNode.chatId = message.chat_id
                if (!isIncomingMessage)
                {
                    messageNode.avatarInsets =
                        UIEdgeInsets(top: 60,
                                     left: -40,
                                     bottom: 60,
                                     right: 40)
                }
                else
                {
                    messageNode.avatarInsets =
                        UIEdgeInsets(top: 60,
                                     left: 40,
                                     bottom: 60,
                                     right: -40)
                }
                
                
                let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                searchChatId.append(message.chat_id)
                newMessageGroup.isIncomingMessage = isIncomingMessage
                if chatType != .single {
                    //       newMessageGroup.isIncomingMessage = true
                }
                messageGroups.append(newMessageGroup)
                let searchModelLocal = SearchModel()
                searchModelLocal.messageGroups.append(newMessageGroup)
                searchModelLocal.messegesChatId.append(searchChatId)
                searchModel.append(searchModelLocal)
            }
            else  if message.file_list.count >= 4 {
                var urls: [String] = []
                for url in message.file_list {
                    urls.append(url.file_url)
                }
                var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                if self.chatType == .group {
                    if let user = User.currentUser() {
                        isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                        print("afsaffas",message.sender_user_id," ",user.user_id )
                        
                    }
                }
                else if self.chatType == .channel {
                    isIncomingMessage = true
                }
                let imageNode = FourImagesNode(id:message.chat_id,urls:urls,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                var messageNode = MessageNode(content: imageNode)
                if (message.answer_chat_id != 0){
                    if (message.is_has_file == 1){
                        var senderName = message.sender_name
                        if (message.sender_user_id == info.user_id){
                            senderName = "Вы"
                        }
                        var fileFormatTitle = message.answer_chatText
                        switch message.answerFileFormat {
                        case "audio":
                            fileFormatTitle = "Аудио"
                        case "file":
                            fileFormatTitle = "Документ"
                        case "video":
                            fileFormatTitle = "Видеозапись"
                        case "image":
                            fileFormatTitle = "Фото"
                        default: break
                        }
                        let imageNode = FourImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,urls:urls,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: imageNode)
                    }}
                messageNode.cellPadding = chatVC.messagePadding
                messageNode.avatarNode = createReply()
                messageNode.chatVC = chatVC
                messageNode.chatId = message.chat_id
                if (!isIncomingMessage)
                {
                    messageNode.avatarInsets =
                        UIEdgeInsets(top: 60,
                                     left: -40,
                                     bottom: 60,
                                     right: 40)
                }
                else
                {
                    messageNode.avatarInsets =
                        UIEdgeInsets(top: 60,
                                     left: 40,
                                     bottom: 60,
                                     right: -40)
                }
                
                let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                searchChatId.append(message.chat_id)
                newMessageGroup.isIncomingMessage = isIncomingMessage
                if chatType != .single {
                    //       newMessageGroup.isIncomingMessage = true
                }
                messageGroups.append(newMessageGroup)
                let searchModelLocal = SearchModel()
                searchModelLocal.messageGroups.append(newMessageGroup)
                searchModelLocal.messegesChatId.append(searchChatId)
                searchModel.append(searchModelLocal)
                
            }
            else if message.file_list.count > 0 {
                for file in message.file_list {
                    if file.file_format == "file" {
                        var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                        if self.chatType == .group {
                            if let user = User.currentUser() {
                                isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                print("afsaffas",message.sender_user_id," ",user.user_id )
                                
                            }
                        }
                        else if self.chatType == .channel {
                            isIncomingMessage = true
                        }
                        // Documents
                        var documentNode = DocumentContentNode(id:message.chat_id,text: file.file_name, date: message.chat_time, documentUrl: URL(string: file.file_url.encodeUrl()!)!, currentVC: chatVC, senderName: message.sender_name,is_read:message.is_read,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        var messageNode = MessageNode(content: documentNode)
                        if (message.answer_chat_id != 0){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            documentNode = DocumentContentNode(id:message.chat_id,text: file.file_name, date: message.chat_time, documentUrl: URL(string: file.file_url.encodeUrl()!)!, currentVC: chatVC, senderName:message.sender_name, is_read:message.is_read,is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: documentNode)
                        }
                        messageNode.cellPadding = chatVC.messagePadding
                        messageNode.currentViewController = chatVC
                        messageNode.chatVC = chatVC
                        messageNode.chatId = message.chat_id
                        let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                        newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                        searchChatId.append(message.chat_id)
                        newMessageGroup.isIncomingMessage = isIncomingMessage
                        if chatType != .single {
                            //           newMessageGroup.isIncomingMessage = true
                        }
                        messageGroups.append(newMessageGroup)
                        let searchModelLocal = SearchModel()
                        searchModelLocal.messageGroups.append(newMessageGroup)
                        searchModelLocal.messegesChatId.append(searchChatId)
                        searchModel.append(searchModelLocal)
                        
                    } else if file.file_format == "image" {
                        // Images
                        var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                        if self.chatType == .group {
                            if let user = User.currentUser() {
                                isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                print("afsaffas",message.sender_user_id," ",user.user_id )
                                
                            }
                        }
                        else if self.chatType == .channel {
                            isIncomingMessage = true
                        }
                        
                        let imageNode = NetworkImageContentNode(id:message.chat_id,imageURL: file.file_url, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        var messageNode = MessageNode(content: imageNode)
                        if (message.answer_chat_id != 0){
                            if (message.is_has_file == 1){
                                var senderName = message.sender_name
                                if (message.sender_user_id == info.user_id){
                                    senderName = "Вы"
                                }
                                var fileFormatTitle = message.answer_chatText
                                switch message.answerFileFormat {
                                case "audio":
                                    fileFormatTitle = "Аудио"
                                case "file":
                                    fileFormatTitle = "Документ"
                                case "video":
                                    fileFormatTitle = "Видеозапись"
                                case "image":
                                    fileFormatTitle = "Фото"
                                default: break
                                }
                                let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: file.file_url, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                messageNode = MessageNode(content: imageNode)
                            }}
                        messageNode.cellPadding = chatVC.messagePadding
                        messageNode.avatarNode = createReply()
                        messageNode.chatVC = chatVC
                        messageNode.chatId = message.chat_id
                        
                        if (!isIncomingMessage)
                        {
                            messageNode.avatarInsets =
                                UIEdgeInsets(top: 60,
                                             left: -40,
                                             bottom: 60,
                                             right: 40)
                        }
                        else
                        {
                            messageNode.avatarInsets =
                                UIEdgeInsets(top: 60,
                                             left: 40,
                                             bottom: 60,
                                             right: -40)
                        }
                        
                        let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                        newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                        searchChatId.append(message.chat_id)
                        newMessageGroup.isIncomingMessage = isIncomingMessage
                        
                        
                        if chatType != .single {
                            //         newMessageGroup.isIncomingMessage = true
                        }
                        messageGroups.append(newMessageGroup)
                        let searchModelLocal = SearchModel()
                        searchModelLocal.messageGroups.append(newMessageGroup)
                        searchModelLocal.messegesChatId.append(searchChatId)
                        searchModel.append(searchModelLocal)
                        
                    } else if file.file_format == "video" {
                        // Videos
                        var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                        if self.chatType == .group {
                            if let user = User.currentUser() {
                                isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                
                            }
                        }
                        else if self.chatType == .channel {
                            isIncomingMessage = true
                        }
                        if let videoUrl = URL(string: file.file_url) {
                            var videoContent = VideoContentNode(id:message.chat_id,videoUrl: videoUrl, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            var messageNode = MessageNode(content: videoContent)
                            if (message.answer_chat_id != 0){
                                var senderName = message.sender_name
                                if (message.sender_user_id == info.user_id){
                                    senderName = "Вы"
                                }
                                var fileFormatTitle = message.answer_chatText
                                switch message.answerFileFormat {
                                case "audio":
                                    fileFormatTitle = "Аудио"
                                case "file":
                                    fileFormatTitle = "Документ"
                                case "video":
                                    fileFormatTitle = "Видеозапись"
                                case "image":
                                    fileFormatTitle = "Фото"
                                default: break
                                }
                                videoContent = VideoContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,videoUrl: videoUrl, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name, is_read:message.is_read,is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                messageNode = MessageNode(content: videoContent)
                            }
                            messageNode.cellPadding = chatVC.messagePadding
                            messageNode.avatarNode = createReply()
                            messageNode.chatVC = chatVC
                            messageNode.chatId = message.chat_id
                            if (!isIncomingMessage)
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 65,
                                                 left:  -43,
                                                 bottom: 65,
                                                 right: 43)
                            }
                            else
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 65,
                                                 left: 43,
                                                 bottom: 65,
                                                 right:  -43)
                            }
                            let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                            newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                            searchChatId.append(message.chat_id)
                            newMessageGroup.isIncomingMessage = isIncomingMessage
                            messageGroups.append(newMessageGroup)
                            let searchModelLocal = SearchModel()
                            searchModelLocal.messageGroups.append(newMessageGroup)
                            searchModelLocal.messegesChatId.append(searchChatId)
                            searchModel.append(searchModelLocal)
                            
                        }
                    }
                    else if file.file_format == "audio" {
                        var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                        if self.chatType == .group {
                            if let user = User.currentUser() {
                                isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                print("afsaffas",message.sender_user_id," ",user.user_id )
                                
                            }
                        }
                        else if self.chatType == .channel {
                            isIncomingMessage = true
                        }
                        // audios
                        if let audioUrl = URL(string: file.file_url) {
                            
                            var audioContent = AudioContentNode(id:message.chat_id,duration: file.file_time, date: message.chat_time, currentVC: chatVC, audioUrl: audioUrl,chat_id:"\(message.chat_id)",fileTime:file.file_time,info:info,avatarUrl:avatarUrl,incommingMessage:isIncomingMessage,senderName:message.sender_name,is_resend:message.is_resend,is_read:message.is_read, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            
                            var messageNode = MessageNode(content: audioContent)
                                if (message.answer_chat_id != 0){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    audioContent = AudioContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,duration: file.file_time, date: message.chat_time, currentVC: chatVC, audioUrl: audioUrl,chat_id:"\(message.chat_id)",fileTime:file.file_time,info:info,avatarUrl:avatarUrl,incommingMessage:isIncomingMessage,senderName:message.sender_name,is_resend:message.is_resend, is_read:message.is_read,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: audioContent)
                                }
                            messageNode.cellPadding = chatVC.messagePadding
                            messageNode.avatarNode = createReply()
                            messageNode.chatVC = chatVC
                            messageNode.chatId = message.chat_id
                            
                            if (!isIncomingMessage)
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 17,
                                                 left:  -17,
                                                 bottom: 17,
                                                 right: 17)
                            }
                            else
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 17,
                                                 left: 17,
                                                 bottom: 17,
                                                 right:  -17)
                            }
                            
                            
                            let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                            newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                            searchChatId.append(message.chat_id)
                            newMessageGroup.isIncomingMessage = isIncomingMessage
                            messageGroups.append(newMessageGroup)
                            let searchModelLocal = SearchModel()
                            searchModelLocal.messageGroups.append(newMessageGroup)
                            searchModelLocal.messegesChatId.append(searchChatId)
                            searchModel.append(searchModelLocal)
                            
                        }
                    }
                    
                    
                    
                }
                
            } else if message.chat_kind == "action" {
                // Group, Channel Actions (i.e someone added to the group)
                let actionNode = ActionContentNode(textMessageString: getActionCredentials(message), bubbleConfiguration: ActionBubbleConfiguration())
                
                let messageNode = MessageNode(content: actionNode)
                messageNode.cellPadding = chatVC.messagePadding
                
                let newMessageGroup = createAction()
                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                searchChatId.append(message.chat_id)
                newMessageGroup.isCenterMessage = true
                
                messageGroups.append(newMessageGroup)
                let searchModelLocal = SearchModel()
                searchModelLocal.messageGroups.append(newMessageGroup)
                searchModelLocal.messegesChatId.append(searchChatId)
                searchModel.append(searchModelLocal)
                
                
            } else if message.chat_kind == "message" {
                // Messages for Chat, Group, Channel
                var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                if self.chatType == .group {
                    if let user = User.currentUser() {
                        isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                        print("afsaffas",message.sender_user_id," ",user.user_id )
                        
                    }
                }
                else if self.chatType == .channel {
                    isIncomingMessage = true
                }
                var textContent = TextContentNode(id: message.chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read, currentViewController: chatVC,senderName:message.sender_name,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                if chatType == .channel {
                    textContent = TextContentNode(id: message.chat_id, textMessageString: message.chat_text, dateString: message.chat_time, currentViewController: chatVC,senderName:message.sender_name,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration, viewsCount: message.view_count)
                }
                
                var messageNode = MessageNode(content: textContent)
                textContent.message = messageNode
                if message.is_contact == 1 {
                    var contactContent = ContactContentNode(id: message.chat_id,imageUrl: "", name: message.contact_name, phone: message.contact_phone, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_resend:message.is_resend,is_exist:message.is_exist,contact_user_id: message.contact_user_id,is_read:message.is_read, bubbleConfiguration: chatVC.sharedBubbleConfiguration, userId: 0)
                    messageNode = MessageNode(content: contactContent)
                    if (message.answer_chat_id != 0){
                        var senderName = message.sender_name
                        if (message.sender_user_id == info.user_id){
                            senderName = "Вы"
                        }
                        var fileFormatTitle = message.answer_chatText
                        switch message.answerFileFormat {
                        case "audio":
                            fileFormatTitle = "Аудио"
                        case "file":
                            fileFormatTitle = "Документ"
                        case "video":
                            fileFormatTitle = "Видеозапись"
                        case "image":
                            fileFormatTitle = "Фото"
                        default: break
                        }
                        contactContent = ContactContentNode(id: message.chat_id,AnswerMessage_id: message.answer_chat_id,imageUrl: "", name: message.contact_name, phone: message.contact_phone, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_resend:message.is_resend,is_exist:message.is_exist,contact_user_id: message.contact_user_id, is_read:message.is_read,bubbleConfiguration: chatVC.sharedBubbleConfiguration, userId: 0,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true)
                        messageNode = MessageNode(content: contactContent)
                    }
                }
                if message.is_sticker == 1 {
                    let imageNode = NetworkImageContentNode(id:message.chat_id,imageURL: message.sticker_image.encodeUrl()!, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,is_sticker:1, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    messageNode = MessageNode(content: imageNode)
                }
                var senderName = message.sender_name
                if (message.sender_user_id == info.user_id){
                    senderName = "Вы"
                    
                }
                if (message.answer_chat_id != 0){
                    if (message.is_has_file == 1){
                        var fileFormatTitle = message.answer_chatText
                        switch message.answerFileFormat {
                        case "audio":
                            fileFormatTitle = "Аудио"
                        case "file":
                            fileFormatTitle = "Документ"
                        case "video":
                            fileFormatTitle = "Видеозапись"
                        case "image":
                            fileFormatTitle = "Фото"
                        default: break
                        }

                        let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString:message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:fileFormatTitle,senderName:senderName,url:message.answerFileUrl, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: answerTextContent)
                        if chatType == .channel {
                            let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration,viewsCount: message.view_count)
                            messageNode = MessageNode(content: answerTextContent)
                        }
                    }
                    else {
                        
                        let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: answerTextContent)
                        if chatType == .channel {
                            let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration,viewsCount: message.view_count)
                            print("vzxzxvzx",message.view_count)
                            messageNode = MessageNode(content: answerTextContent)
                        }
                    }
                    if (message.is_sticker == 1){
                        let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.sticker_image.encodeUrl()!, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,is_sticker:1, textMessageString:"Стикер",senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        print("afafssffas calls")
                        messageNode = MessageNode(content: imageNode)
                    }
                }
                else if (message.is_has_link == 1){
                    let answerTextContent = LinkContentNode(id: message.chat_id,title:message.link_title,textMessageString: message.link_description, dateString: message.chat_time,is_read:message.is_read,senderName:message.sender_name,url:message.link_image,link:message.chat_text,currentViewController: chatVC,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    messageNode = MessageNode(content: answerTextContent)
                }
                
                messageNode.cellPadding = chatVC.messagePadding
                messageNode.currentViewController = chatVC
                messageNode.chatVC = chatVC
                messageNode.chatId = message.chat_id
                
                
                if messageGroups.last == nil || messageGroups.last?.isIncomingMessage == !isIncomingMessage {
                    let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                    if isIncomingMessage && chatType == .group {
                        newMessageGroup.avatarNode = createAvatar(with: message.sender_avatar)
                        newMessageGroup.chatVC = chatVC
                        newMessageGroup.dialogId = dialogId
                        newMessageGroup.user_id = message.sender_user_id
                        newMessageGroup.info = self.info
                    }
                    print("aafsfa",message.chat_text)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                } else {
                    messageGroups.last?.addMessageToGroup(messageNode, completion: nil)
                    //searchModel.last?.messageGroups[(searchModel.last?.messageGroups.count)! - 1] =  (messageGroups.last)!
                    //searchModel.last!.messegesChatId[searchModel.last!.messegesChatId.count - 1].append(message.chat_id)
                    
                    searchModel.last?.messageGroups[(searchModel.last?.messageGroups.count)! - 1] =  (messageGroups.last)!
                    searchModel.last!.messegesChatId[searchModel.last!.messegesChatId.count - 1].append(message.chat_id)
                    
                }
                
                // We save last local chat_id. It need when we pull history from server (getDialogMessages:Method)
                lastRecievedChatId = message.chat_id
            }
            //print("messageGroups.count",messageGroups.count ,index,reversed.count)
            
            previousDate = message.chat_date
        }
                print("assaffsafas",searchModel)
        print("messageGroups.dcount",messageGroups.count ,index,reversed.count)
        return messageGroups
        
    }
    private func BatchLoaderRealm(currentBatchCounter: Int) -> [MessageGroup] {
        let endIndex = currentBatchCounter + 10
        let realm = try! Realm()
        var historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
        historyObjects = historyObjects.sorted(byKeyPath : "chat_id")
        var messageGroups: [MessageGroup] = []
        var searchChatId = [Int]()
        guard currentBatchCounter<=historyObjects.count else {
            return messageGroups
        }
        
      //  var reversed = Array(historyObjects.reversed())
        var reversed = Array(historyObjects.reversed())
                  print("12fsasa")
            print("vvzvzx",historyObjects)
        if (reversed.count - currentBatchCounter) >= 10 {
         reversed = Array(reversed[currentBatchCounter..<endIndex].reversed())
    }
    else {
       reversed = Array(reversed[currentBatchCounter..<reversed.count].reversed())
    }
             print("asfsfaffasfssasaasf",reversed)
     //   reversed = Array(reversed.reversed())
         var previousDate = ""
         for index in 0..<reversed.count {
            let message = reversed[index]
                    print("asffasf1212sa",message.chat_id)
                if message.file_list.count == 2 {
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    // Images
                    let imageNode = TwoImagesNode(id:message.chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = TwoImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }
                    let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType != .single {
                   //     newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                    
                }
                    
                else  if message.file_list.count == 3 {
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    let imageNode = ThreeImagesNode(id:message.chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,imageURL3:message.file_list[2].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = ThreeImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,imageURL3:message.file_list[2].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }


                    let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                     searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType != .single {
                 //       newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                }
                else  if message.file_list.count >= 4 {
                    var urls: [String] = []
                    for url in message.file_list {
                        urls.append(url.file_url)
                    }
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    let imageNode = FourImagesNode(id:message.chat_id,urls:urls,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = FourImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,urls:urls,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }

                    let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                     searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType != .single {
                 //       newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)

                }
                else if message.file_list.count > 0 {
                    for file in message.file_list {
                        if file.file_format == "file" {
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            // Documents
                            var documentNode = DocumentContentNode(id:message.chat_id,text: file.file_name, date: message.chat_time, documentUrl: URL(string: file.file_url.encodeUrl()!)!, currentVC: chatVC, senderName: message.sender_name,is_read:message.is_read,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            var messageNode = MessageNode(content: documentNode)
                            if (message.answer_chat_id != 0){
                                var senderName = message.sender_name
                                if (message.sender_user_id == info.user_id){
                                    senderName = "Вы"
                                }
                                var fileFormatTitle = message.answer_chatText
                                switch message.answerFileFormat {
                                case "audio":
                                    fileFormatTitle = "Аудио"
                                case "file":
                                    fileFormatTitle = "Документ"
                                case "video":
                                    fileFormatTitle = "Видеозапись"
                                case "image":
                                    fileFormatTitle = "Фото"
                                default: break
                                }
                                documentNode = DocumentContentNode(id:message.chat_id,text: file.file_name, date: message.chat_time, documentUrl: URL(string: file.file_url.encodeUrl()!)!, currentVC: chatVC, senderName:message.sender_name, is_read:message.is_read,is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                messageNode = MessageNode(content: documentNode)
                            }
                            messageNode.cellPadding = chatVC.messagePadding
                            messageNode.currentViewController = chatVC
                            messageNode.chatVC = chatVC
                            messageNode.chatId = message.chat_id
                            let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                            newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                             searchChatId.append(message.chat_id)
                            newMessageGroup.isIncomingMessage = isIncomingMessage
                            if chatType != .single {
                     //           newMessageGroup.isIncomingMessage = true
                            }
                            messageGroups.append(newMessageGroup)
                            let searchModelLocal = SearchModel()
                            searchModelLocal.messageGroups.append(newMessageGroup)
                            searchModelLocal.messegesChatId.append(searchChatId)
                            searchModel.append(searchModelLocal)
                            
                        } else if file.file_format == "image" {
                            // Images
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            
                            let imageNode = NetworkImageContentNode(id:message.chat_id,imageURL: file.file_url, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            var messageNode = MessageNode(content: imageNode)
                            if (message.answer_chat_id != 0){
                                if (message.is_has_file == 1){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: file.file_url, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                messageNode = MessageNode(content: imageNode)
                                }}
                            messageNode.cellPadding = chatVC.messagePadding
                            messageNode.avatarNode = createReply()
                            messageNode.chatVC = chatVC
                            messageNode.chatId = message.chat_id
                        
                            if (!isIncomingMessage)
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 60,
                                                 left: -40,
                                                 bottom: 60,
                                                 right: 40)
                            }
                            else
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 60,
                                                 left: 40,
                                                 bottom: 60,
                                                 right: -40)
                            }

                            let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                            newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                             searchChatId.append(message.chat_id)
                            newMessageGroup.isIncomingMessage = isIncomingMessage
                 
                            
                            if chatType != .single {
                       //         newMessageGroup.isIncomingMessage = true
                            }
                            messageGroups.append(newMessageGroup)
                            let searchModelLocal = SearchModel()
                            searchModelLocal.messageGroups.append(newMessageGroup)
                            searchModelLocal.messegesChatId.append(searchChatId)
                            searchModel.append(searchModelLocal)
                     
                        } else if file.file_format == "video" {
                            // Videos
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            
                            if let videoUrl = URL(string: file.file_url) {
                                var videoContent = VideoContentNode(id:message.chat_id,videoUrl: videoUrl, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                var messageNode = MessageNode(content: videoContent)
                                if (message.answer_chat_id != 0){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    videoContent = VideoContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,videoUrl: videoUrl, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name, is_read:message.is_read,is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: videoContent)
                                }
                                messageNode.cellPadding = chatVC.messagePadding
                                messageNode.avatarNode = createReply()
                                messageNode.chatVC = chatVC
                                messageNode.chatId = message.chat_id
                                if (!isIncomingMessage)
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 65,
                                                     left:  -43,
                                                     bottom: 65,
                                                     right: 43)
                                }
                                else
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 65,
                                                     left: 43,
                                                     bottom: 65,
                                                     right:  -43)
                                }
                                let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                                 searchChatId.append(message.chat_id)
                                newMessageGroup.isIncomingMessage = isIncomingMessage
                                messageGroups.append(newMessageGroup)
                                let searchModelLocal = SearchModel()
                                searchModelLocal.messageGroups.append(newMessageGroup)
                                searchModelLocal.messegesChatId.append(searchChatId)
                                searchModel.append(searchModelLocal)
             
                            }
                        }
                        else if file.file_format == "audio" {
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            // audios
                            if let audioUrl = URL(string: file.file_url) {
                                
                                var audioContent = AudioContentNode(id:message.chat_id,duration: file.file_time, date: message.chat_time, currentVC: chatVC, audioUrl: audioUrl,chat_id:"\(message.chat_id)",fileTime:file.file_time,info:info,avatarUrl:avatarUrl,incommingMessage:isIncomingMessage,senderName:message.sender_name,is_resend:message.is_resend,is_read:message.is_read, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                
                                var messageNode = MessageNode(content: audioContent)
                                if (message.answer_chat_id != 0){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    audioContent = AudioContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,duration: file.file_time, date: message.chat_time, currentVC: chatVC, audioUrl: audioUrl,chat_id:"\(message.chat_id)",fileTime:file.file_time,info:info,avatarUrl:avatarUrl,incommingMessage:isIncomingMessage,senderName:message.sender_name,is_resend:message.is_resend, is_read:message.is_read,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: audioContent)
                                }
                                messageNode.cellPadding = chatVC.messagePadding
                                messageNode.avatarNode = createReply()
                                messageNode.chatVC = chatVC
                                messageNode.chatId = message.chat_id
       
                                if (!isIncomingMessage)
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 17,
                                                     left:  -17,
                                                     bottom: 17,
                                                     right: 17)
                                }
                                else
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 17,
                                                     left: 17,
                                                     bottom: 17,
                                                     right:  -17)
                                }
                                

                                let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                                 searchChatId.append(message.chat_id)
                                newMessageGroup.isIncomingMessage = isIncomingMessage
                                messageGroups.append(newMessageGroup)
                                let searchModelLocal = SearchModel()
                                searchModelLocal.messageGroups.append(newMessageGroup)
                                searchModelLocal.messegesChatId.append(searchChatId)
                                searchModel.append(searchModelLocal)
                           
                            }
                        }
                        
                        
                        
                    }
                    
                } else if message.chat_kind == "action" {
                    // Group, Channel Actions (i.e someone added to the group)
                    let actionNode = ActionContentNode(textMessageString: getActionCredentials(message), bubbleConfiguration: ActionBubbleConfiguration())
                    
                    let messageNode = MessageNode(content: actionNode)
                    messageNode.cellPadding = chatVC.messagePadding
                    
                    let newMessageGroup = createAction()
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isCenterMessage = true
                    
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
             
                    
                } else if message.chat_kind == "message" {
                    // Messages for Chat, Group, Channel
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                                                        print("afsaffas",message.sender_user_id," ",user.user_id ,isIncomingMessage)
                            
                        }
                    }
                    else if self.chatType == .channel {
                       isIncomingMessage = true
                    }
                    var textContent = TextContentNode(id: message.chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read, currentViewController: chatVC,senderName:message.sender_name,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    if chatType == .channel {
                        textContent = TextContentNode(id: message.chat_id, textMessageString: message.chat_text, dateString: message.chat_time, currentViewController: chatVC,senderName:message.sender_name,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration, viewsCount: message.view_count)
                    }
                    
                    var messageNode = MessageNode(content: textContent)
                    textContent.message = messageNode
                    if message.is_contact == 1 {
                        var contactContent = ContactContentNode(id: message.chat_id,imageUrl: "", name: message.contact_name, phone: message.contact_phone, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_resend:message.is_resend,is_exist:message.is_exist,contact_user_id: message.contact_user_id,is_read:message.is_read, bubbleConfiguration: chatVC.sharedBubbleConfiguration, userId: 0)
                        messageNode = MessageNode(content: contactContent)
                        if (message.answer_chat_id != 0){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            contactContent = ContactContentNode(id: message.chat_id,AnswerMessage_id: message.answer_chat_id,imageUrl: "", name: message.contact_name, phone: message.contact_phone, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_resend:message.is_resend,is_exist:message.is_exist,contact_user_id: message.contact_user_id, is_read:message.is_read,bubbleConfiguration: chatVC.sharedBubbleConfiguration, userId: 0,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true)
                            messageNode = MessageNode(content: contactContent)
                        }
                    }
                    print("afsasfsfa",message)
                    if message.is_sticker == 1 {
                        let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.sticker_image.encodeUrl()!, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,is_sticker:1, bubbleConfiguration: chatVC.stickerBubbleConfiguration)
                        messageNode = MessageNode(content: imageNode)
                        messageNode.contentNode?.backgroundBubble?.bubbleColor = UIColor.green
                        messageNode.setNeedsDisplay()
                        messageNode.contentNode?.setNeedsDisplay()
                    }
                    var senderName = message.sender_name
                    if (message.sender_user_id == info.user_id){
                        senderName = "Вы"
                        
                    }
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString:message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:fileFormatTitle,senderName:senderName,url:message.answerFileUrl, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: answerTextContent)
                            if chatType == .channel {
                                let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration,viewsCount: message.view_count)
                                messageNode = MessageNode(content: answerTextContent)
                            }
                        }
                        else {
                            
                            let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: answerTextContent)
                            if chatType == .channel {
                                let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration,viewsCount: message.view_count)
                                print("vzxzxvzx",message.view_count)
                                messageNode = MessageNode(content: answerTextContent)
                            }
                            
                        }
                        if (message.is_sticker == 1){
                            let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.sticker_image.encodeUrl()!, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,is_sticker:1, textMessageString:"Стикер",senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            print("afafssffas calls")
                            messageNode = MessageNode(content: imageNode)
                        }
                    }
                    else if (message.is_has_link == 1){
                        let answerTextContent = LinkContentNode(id: message.chat_id,title:message.link_title,textMessageString: message.link_description, dateString: message.chat_time,is_read:message.is_read,senderName:message.sender_name,url:message.link_image,link:message.chat_text,currentViewController: chatVC,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: answerTextContent)
                    }

                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.currentViewController = chatVC
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if messageGroups.last == nil || messageGroups.last?.isIncomingMessage == !isIncomingMessage {
                        let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                        if isIncomingMessage && chatType == .group {
                            newMessageGroup.avatarNode = createAvatar(with: message.sender_avatar)
                            newMessageGroup.chatVC = chatVC
                            newMessageGroup.dialogId = dialogId
                            newMessageGroup.user_id = message.sender_user_id
                            newMessageGroup.info = self.info
                        }
                        print("aafsfa",message.chat_text)
                        newMessageGroup.isIncomingMessage = isIncomingMessage
                        newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                        searchChatId.append(message.chat_id)
                        messageGroups.append(newMessageGroup)
                        let searchModelLocal = SearchModel()
                        searchModelLocal.messageGroups.append(newMessageGroup)
                        searchModelLocal.messegesChatId.append(searchChatId)
                        searchModel.append(searchModelLocal)
                    } else {
                        messageGroups.last?.addMessageToGroup(messageNode, completion: nil)
                        searchModel.last?.messageGroups[(searchModel.last?.messageGroups.count)! - 1] =  (messageGroups.last)!
                        searchModel.last!.messegesChatId[searchModel.last!.messegesChatId.count - 1].append(message.chat_id)
                    }

                    // We save last local chat_id. It need when we pull history from server (getDialogMessages:Method)
                     lastRecievedChatId = message.chat_id
                }
              //print("messageGroups.count",messageGroups.count ,index,reversed.count)
             lastRecievedChatId = message.chat_id
            previousDate = message.chat_date
           
        }
        print("messageGroups.dcount",messageGroups.count ,index,reversed.count)
        return messageGroups
        
    }
    private func addRealmResults(isFirstLoading: Bool = true) -> [MessageGroup] {
        let realm = try! Realm()
        var historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
        historyObjects = historyObjects.sorted(byKeyPath : "chat_id")
        let reversed = Array(historyObjects)
        var messageGroups: [MessageGroup] = []
        var searchChatId = [Int]()
        var previousDate = ""
          for (index,message) in reversed.enumerated() {
             if message.chat_id > lastRecievedChatId || isFirstLoading {
                if message.file_list.count == 2 {
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    // Images
                    let imageNode = TwoImagesNode(id:message.chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = TwoImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }
                    let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType != .single {
                        //     newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                    
                }
                    
                else  if message.file_list.count == 3 {
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    let imageNode = ThreeImagesNode(id:message.chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,imageURL3:message.file_list[2].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = ThreeImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,imageURL3:message.file_list[2].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }
                    
                    
                    let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType != .single {
                        //       newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                }
                else  if message.file_list.count >= 4 {
                    var urls: [String] = []
                    for url in message.file_list {
                        urls.append(url.file_url)
                    }
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    let imageNode = FourImagesNode(id:message.chat_id,urls:urls,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = FourImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,urls:urls,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }
                    
                    let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType != .single {
                        //       newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                    
                }
                else if message.file_list.count > 0 {
                    for file in message.file_list {
                        if file.file_format == "file" {
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            // Documents
                            var documentNode = DocumentContentNode(id:message.chat_id,text: file.file_name, date: message.chat_time, documentUrl: URL(string: file.file_url.encodeUrl()!)!, currentVC: chatVC, senderName: message.sender_name,is_read:message.is_read,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            var messageNode = MessageNode(content: documentNode)
                            if (message.answer_chat_id != 0){
                                var senderName = message.sender_name
                                if (message.sender_user_id == info.user_id){
                                    senderName = "Вы"
                                }
                                var fileFormatTitle = message.answer_chatText
                                switch message.answerFileFormat {
                                case "audio":
                                    fileFormatTitle = "Аудио"
                                case "file":
                                    fileFormatTitle = "Документ"
                                case "video":
                                    fileFormatTitle = "Видеозапись"
                                case "image":
                                    fileFormatTitle = "Фото"
                                default: break
                                }
                                documentNode = DocumentContentNode(id:message.chat_id,text: file.file_name, date: message.chat_time, documentUrl: URL(string: file.file_url.encodeUrl()!)!, currentVC: chatVC, senderName:message.sender_name, is_read:message.is_read,is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                messageNode = MessageNode(content: documentNode)
                            }
                            messageNode.cellPadding = chatVC.messagePadding
                            messageNode.currentViewController = chatVC
                            messageNode.chatVC = chatVC
                            messageNode.chatId = message.chat_id
                            let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                            newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                            searchChatId.append(message.chat_id)
                            newMessageGroup.isIncomingMessage = isIncomingMessage
                            if chatType != .single {
                                //           newMessageGroup.isIncomingMessage = true
                            }
                            messageGroups.append(newMessageGroup)
                            let searchModelLocal = SearchModel()
                            searchModelLocal.messageGroups.append(newMessageGroup)
                            searchModelLocal.messegesChatId.append(searchChatId)
                            searchModel.append(searchModelLocal)
                            
                        } else if file.file_format == "image" {
                            // Images
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            
                            let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: file.file_url, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            var messageNode = MessageNode(content: imageNode)
                            if (message.answer_chat_id != 0){
                                if (message.is_has_file == 1){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: file.file_url, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: imageNode)
                                }}
                            messageNode.cellPadding = chatVC.messagePadding
                            messageNode.avatarNode = createReply()
                            messageNode.chatVC = chatVC
                            messageNode.chatId = message.chat_id
                            
                            if (!isIncomingMessage)
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 60,
                                                 left: -40,
                                                 bottom: 60,
                                                 right: 40)
                            }
                            else
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 60,
                                                 left: 40,
                                                 bottom: 60,
                                                 right: -40)
                            }
                            
                            let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                            newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                            searchChatId.append(message.chat_id)
                            newMessageGroup.isIncomingMessage = isIncomingMessage
                            
                            
                            if chatType != .single {
                                //         newMessageGroup.isIncomingMessage = true
                            }
                            messageGroups.append(newMessageGroup)
                            let searchModelLocal = SearchModel()
                            searchModelLocal.messageGroups.append(newMessageGroup)
                            searchModelLocal.messegesChatId.append(searchChatId)
                            searchModel.append(searchModelLocal)
                            
                        } else if file.file_format == "video" {
                            // Videos
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            
                            if let videoUrl = URL(string: file.file_url) {
                                var videoContent = VideoContentNode(id:message.chat_id,videoUrl: videoUrl, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                var messageNode = MessageNode(content: videoContent)
                                if (message.answer_chat_id != 0){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    videoContent = VideoContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,videoUrl: videoUrl, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name, is_read:message.is_read,is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: videoContent)
                                }
                                messageNode.cellPadding = chatVC.messagePadding
                                messageNode.avatarNode = createReply()
                                messageNode.chatVC = chatVC
                                messageNode.chatId = message.chat_id
                                if (!isIncomingMessage)
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 65,
                                                     left:  -43,
                                                     bottom: 65,
                                                     right: 43)
                                }
                                else
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 65,
                                                     left: 43,
                                                     bottom: 65,
                                                     right:  -43)
                                }
                                let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                                searchChatId.append(message.chat_id)
                                newMessageGroup.isIncomingMessage = isIncomingMessage
                                messageGroups.append(newMessageGroup)
                                let searchModelLocal = SearchModel()
                                searchModelLocal.messageGroups.append(newMessageGroup)
                                searchModelLocal.messegesChatId.append(searchChatId)
                                searchModel.append(searchModelLocal)
                                
                            }
                        }
                        else if file.file_format == "audio" {
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            // audios
                            if let audioUrl = URL(string: file.file_url) {
                                
                                var audioContent = AudioContentNode(id:message.chat_id,duration: file.file_time, date: message.chat_time, currentVC: chatVC, audioUrl: audioUrl,chat_id:"\(message.chat_id)",fileTime:file.file_time,info:info,avatarUrl:avatarUrl,incommingMessage:isIncomingMessage,senderName:message.sender_name,is_resend:message.is_resend,is_read:message.is_read, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                
                                var messageNode = MessageNode(content: audioContent)
                                if (message.answer_chat_id != 0){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    audioContent = AudioContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,duration: file.file_time, date: message.chat_time, currentVC: chatVC, audioUrl: audioUrl,chat_id:"\(message.chat_id)",fileTime:file.file_time,info:info,avatarUrl:avatarUrl,incommingMessage:isIncomingMessage,senderName:message.sender_name,is_resend:message.is_resend, is_read:message.is_read,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: audioContent)
                                }
                                messageNode.cellPadding = chatVC.messagePadding
                                messageNode.avatarNode = createReply()
                                messageNode.chatVC = chatVC
                                messageNode.chatId = message.chat_id
                                
                                if (!isIncomingMessage)
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 17,
                                                     left:  -17,
                                                     bottom: 17,
                                                     right: 17)
                                }
                                else
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 17,
                                                     left: 17,
                                                     bottom: 17,
                                                     right:  -17)
                                }
                                
                                
                                let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                                searchChatId.append(message.chat_id)
                                newMessageGroup.isIncomingMessage = isIncomingMessage
                                messageGroups.append(newMessageGroup)
                                let searchModelLocal = SearchModel()
                                searchModelLocal.messageGroups.append(newMessageGroup)
                                searchModelLocal.messegesChatId.append(searchChatId)
                                searchModel.append(searchModelLocal)
                                
                            }
                        }
                        
                        
                        
                    }
                    
                } else if message.chat_kind == "action" {
                    // Group, Channel Actions (i.e someone added to the group)
                    let actionNode = ActionContentNode(textMessageString: getActionCredentials(message), bubbleConfiguration: ActionBubbleConfiguration())
                    
                    let messageNode = MessageNode(content: actionNode)
                    messageNode.cellPadding = chatVC.messagePadding
                    
                    let newMessageGroup = createAction()
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isCenterMessage = true
                    
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                    
                    
                } else if message.chat_kind == "message" {
                    // Messages for Chat, Group, Channel
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    var textContent = TextContentNode(id: message.chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read, currentViewController: chatVC,senderName:message.sender_name,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    if chatType == .channel {
                        textContent = TextContentNode(id: message.chat_id, textMessageString: message.chat_text, dateString: message.chat_time, currentViewController: chatVC,senderName:message.sender_name,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration, viewsCount: message.view_count)
                    }
                    
                    var messageNode = MessageNode(content: textContent)
                    textContent.message = messageNode
                    if message.is_contact == 1 {
                        var contactContent = ContactContentNode(id: message.chat_id,imageUrl: "", name: message.contact_name, phone: message.contact_phone, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_resend:message.is_resend,is_exist:message.is_exist,contact_user_id: message.contact_user_id,is_read:message.is_read, bubbleConfiguration: chatVC.sharedBubbleConfiguration, userId: 0)
                        messageNode = MessageNode(content: contactContent)
                        if (message.answer_chat_id != 0){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            contactContent = ContactContentNode(id: message.chat_id,AnswerMessage_id: message.answer_chat_id,imageUrl: "", name: message.contact_name, phone: message.contact_phone, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_resend:message.is_resend,is_exist:message.is_exist,contact_user_id: message.contact_user_id, is_read:message.is_read,bubbleConfiguration: chatVC.sharedBubbleConfiguration, userId: 0,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true)
                            messageNode = MessageNode(content: contactContent)
                        }
                    }
                    if message.is_sticker == 1 {
                        let imageNode = NetworkImageContentNode(id:message.chat_id,imageURL: message.sticker_image.encodeUrl()!, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,is_sticker:1, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: imageNode)
                    }
                    var senderName = message.sender_name
                    if (message.sender_user_id == info.user_id){
                        senderName = "Вы"
                    }
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString:message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:fileFormatTitle,senderName:senderName,url:message.answerFileUrl, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: answerTextContent)
                            if chatType == .channel {
                                let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration,viewsCount: message.view_count)
                                messageNode = MessageNode(content: answerTextContent)
                            }
                        }
                        else {
                            
                            let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: answerTextContent)
                            if chatType == .channel {
                                let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration,viewsCount: message.view_count)
                                print("vzxzxvzx",message.view_count)
                                messageNode = MessageNode(content: answerTextContent)
                            }
                        }
                        if (message.is_sticker == 1){
                            let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.sticker_image.encodeUrl()!, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,is_sticker:1, textMessageString:"Стикер",senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            print("afafssffas calls")
                            messageNode = MessageNode(content: imageNode)
                        }
                    }
                    else if (message.is_has_link == 1){
                        let answerTextContent = LinkContentNode(id: message.chat_id,title:message.link_title,textMessageString: message.link_description, dateString: message.chat_time,is_read:message.is_read,senderName:message.sender_name,url:message.link_image,link:message.chat_text,currentViewController: chatVC,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: answerTextContent)
                    }
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.currentViewController = chatVC
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    
                    
                    if messageGroups.last == nil || messageGroups.last?.isIncomingMessage == !isIncomingMessage {
                        let newMessageGroup = createMessageGroup(currentDate: message.chat_date, previousDate: previousDate)
                        if isIncomingMessage && chatType == .group {
                            newMessageGroup.avatarNode = createAvatar(with: message.sender_avatar)
                            newMessageGroup.chatVC = chatVC
                            newMessageGroup.dialogId = dialogId
                            newMessageGroup.user_id = message.sender_user_id
                            newMessageGroup.info = self.info
                        }
                        print("aafsfa",message.chat_text)
                        newMessageGroup.isIncomingMessage = isIncomingMessage
                        newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                        searchChatId.append(message.chat_id)
                        messageGroups.append(newMessageGroup)
                        let searchModelLocal = SearchModel()
                        searchModelLocal.messageGroups.append(newMessageGroup)
                        searchModelLocal.messegesChatId.append(searchChatId)
                        searchModel.append(searchModelLocal)
                    } else {
                        messageGroups.last?.addMessageToGroup(messageNode, completion: nil)
                        searchModel.last?.messageGroups[(searchModel.last?.messageGroups.count)! - 1] =  (messageGroups.last)!
                        searchModel.last!.messegesChatId[searchModel.last!.messegesChatId.count - 1].append(message.chat_id)
                    }
                
                // We save last local chat_id. It need when we pull history from server (getDialogMessages:Method)
                lastRecievedChatId = message.chat_id
            }
            //print("messageGroups.count",messageGroups.count ,index,reversed.count)
            }
            previousDate = message.chat_date
            
        }
        return messageGroups
        
    }
    private func addRealmFromSocket(isFirstLoading: Bool = true) -> [MessageGroup] {
        let realm = try! Realm()
        let historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
        var messageGroups: [MessageGroup] = []
        var searchChatId = [Int]()
        if let message = historyObjects.last {
            if message.chat_id > lastRecievedChatId || isFirstLoading {
                if message.file_list.count == 2 {
                    // Images
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    let imageNode = TwoImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = TwoImagesNode(id:message.chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }

                    let newMessageGroup = createAction()
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType == .single {
                      //  newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                    lastRecievedChatId = message.chat_id
                    
                }
                else  if message.file_list.count == 3 {
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    let imageNode = ThreeImagesNode(id:message.chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,imageURL3:message.file_list[2].file_url,date: message.chat_date, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = ThreeImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.file_list[0].file_url,imageURL2:message.file_list[1].file_url,imageURL3:message.file_list[2].file_url,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }

                    let newMessageGroup = createAction()
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType != .single {
                   //     newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                    lastRecievedChatId = message.chat_id
                }
                else  if message.file_list.count >= 4 {
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group {
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            print("afsaffas",message.sender_user_id," ",user.user_id )
                            
                        }
                    }
                    else if self.chatType == .channel {
                        isIncomingMessage = true
                    }
                    var urls: [String] = []
                    for url in message.file_list {
                        urls.append(url.file_url)
                    }
                    let imageNode = FourImagesNode(id:message.chat_id,urls:urls,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    var messageNode = MessageNode(content: imageNode)
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let imageNode = FourImagesNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,urls:urls,date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: imageNode)
                        }}
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.avatarNode = createReply()
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    if (!isIncomingMessage)
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: -40,
                                         bottom: 60,
                                         right: 40)
                    }
                    else
                    {
                        messageNode.avatarInsets =
                            UIEdgeInsets(top: 60,
                                         left: 40,
                                         bottom: 60,
                                         right: -40)
                    }

                    let newMessageGroup = createAction()
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isIncomingMessage = isIncomingMessage
                    if chatType != .single {
                    //    newMessageGroup.isIncomingMessage = true
                    }
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                    lastRecievedChatId = message.chat_id
                }
                else if message.file_list.count > 0 {
                    for file in message.file_list {
                        
                        if file.file_format == "file" {
                            // Documents
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            var documentNode = DocumentContentNode(id:message.chat_id,text: file.file_name, date: message.chat_time, documentUrl: URL(string: file.file_url.encodeUrl()!)!, currentVC: chatVC, senderName: message.sender_name,is_read:message.is_read,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            var messageNode = MessageNode(content: documentNode)
                            if (message.answer_chat_id != 0){
                                var senderName = message.sender_name
                                if (message.sender_user_id == info.user_id){
                                    senderName = "Вы"
                                }
                                var fileFormatTitle = message.answer_chatText
                                switch message.answerFileFormat {
                                case "audio":
                                    fileFormatTitle = "Аудио"
                                case "file":
                                    fileFormatTitle = "Документ"
                                case "video":
                                    fileFormatTitle = "Видеозапись"
                                case "image":
                                    fileFormatTitle = "Фото"
                                default: break
                                }
                                documentNode = DocumentContentNode(id:message.chat_id,text: file.file_name, date: message.chat_time, documentUrl: URL(string: file.file_url.encodeUrl()!)!, currentVC: chatVC, senderName:message.sender_name, is_read:message.is_read,is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                messageNode = MessageNode(content: documentNode)
                            }
                            messageNode.cellPadding = chatVC.messagePadding
                            messageNode.currentViewController = chatVC
                            messageNode.chatVC = chatVC
                            messageNode.chatId = message.chat_id
                            let newMessageGroup = createAction()
                            newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                            searchChatId.append(message.chat_id)
                            newMessageGroup.isIncomingMessage = isIncomingMessage
                            if chatType != .single {
                         //       newMessageGroup.isIncomingMessage = true
                            }
                            messageGroups.append(newMessageGroup)
                            let searchModelLocal = SearchModel()
                            searchModelLocal.messageGroups.append(newMessageGroup)
                            searchModelLocal.messegesChatId.append(searchChatId)
                            searchModel.append(searchModelLocal)
                            lastRecievedChatId = message.chat_id
                            
                        } else if file.file_format == "image" {
                            // Images
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            let imageNode = NetworkImageContentNode(id:message.chat_id,imageURL:file.file_url, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            var messageNode = MessageNode(content: imageNode)
                            if (message.answer_chat_id != 0){
                                if (message.is_has_file == 1){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: file.file_url, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: imageNode)
                                }}
                            messageNode.cellPadding = chatVC.messagePadding
                            messageNode.avatarNode = createReply()
                            messageNode.chatVC = chatVC
                            messageNode.chatId = message.chat_id
                            if (!isIncomingMessage)
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 60,
                                                 left: -40,
                                                 bottom: 60,
                                                 right: 40)
                            }
                            else
                            {
                                messageNode.avatarInsets =
                                    UIEdgeInsets(top: 60,
                                                 left: 40,
                                                 bottom: 60,
                                                 right: -40)
                            }

                           let newMessageGroup = createAction()
                            newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                            searchChatId.append(message.chat_id)
                            newMessageGroup.isIncomingMessage = isIncomingMessage
                            if chatType != .single {
                   //             newMessageGroup.isIncomingMessage = true
                            }
                            messageGroups.append(newMessageGroup)
                            let searchModelLocal = SearchModel()
                            searchModelLocal.messageGroups.append(newMessageGroup)
                            searchModelLocal.messegesChatId.append(searchChatId)
                            searchModel.append(searchModelLocal)
                            lastRecievedChatId = message.chat_id
                        } else if file.file_format == "video" {
                            // Videos
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            if let videoUrl = URL(string: file.file_url) {
                                var videoContent = VideoContentNode(id:message.chat_id,videoUrl: videoUrl, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                var messageNode = MessageNode(content: videoContent)
                                if (message.answer_chat_id != 0){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    videoContent = VideoContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,videoUrl: videoUrl, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name, is_read:message.is_read,is_resend:message.is_resend,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: videoContent)
                                }
                                messageNode.cellPadding = chatVC.messagePadding
                                messageNode.avatarNode = createReply()
                                messageNode.chatVC = chatVC
                                messageNode.chatId = message.chat_id
                                if (!isIncomingMessage)
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 65,
                                                     left:  -43,
                                                     bottom: 65,
                                                     right: 43)
                                }
                                else
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 65,
                                                     left: 43,
                                                     bottom: 65,
                                                     right:  -43)
                                }
                                let newMessageGroup = createAction()
                                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                                searchChatId.append(message.chat_id)
                                newMessageGroup.isIncomingMessage = isIncomingMessage
                                messageGroups.append(newMessageGroup)
                                let searchModelLocal = SearchModel()
                                searchModelLocal.messageGroups.append(newMessageGroup)
                                searchModelLocal.messegesChatId.append(searchChatId)
                                searchModel.append(searchModelLocal)
                                lastRecievedChatId = message.chat_id
                            }
                        }
                        else if file.file_format == "audio" {
                            // audios
                            var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                            if self.chatType == .group {
                                if let user = User.currentUser() {
                                    isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                                    print("afsaffas",message.sender_user_id," ",user.user_id )
                                    
                                }
                            }
                            else if self.chatType == .channel {
                                isIncomingMessage = true
                            }
                            if let audioUrl = URL(string: file.file_url) {
                                
                                var audioContent = AudioContentNode(id:message.chat_id,duration: file.file_time, date: message.chat_time, currentVC: chatVC, audioUrl: audioUrl,chat_id:"\(message.chat_id)",fileTime:file.file_time,info:info,avatarUrl:avatarUrl,incommingMessage:message.sender_user_id != info.user_id ? false : true,senderName:message.sender_name,is_resend:message.is_resend,is_read:message.is_read, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
        
                                var messageNode = MessageNode(content: audioContent)
                                if (message.answer_chat_id != 0){
                                    var senderName = message.sender_name
                                    if (message.sender_user_id == info.user_id){
                                        senderName = "Вы"
                                    }
                                    var fileFormatTitle = message.answer_chatText
                                    switch message.answerFileFormat {
                                    case "audio":
                                        fileFormatTitle = "Аудио"
                                    case "file":
                                        fileFormatTitle = "Документ"
                                    case "video":
                                        fileFormatTitle = "Видеозапись"
                                    case "image":
                                        fileFormatTitle = "Фото"
                                    default: break
                                    }
                                    audioContent = AudioContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,duration: file.file_time, date: message.chat_time, currentVC: chatVC, audioUrl: audioUrl,chat_id:"\(message.chat_id)",fileTime:file.file_time,info:info,avatarUrl:avatarUrl,incommingMessage:isIncomingMessage,senderName:message.sender_name,is_resend:message.is_resend, is_read:message.is_read,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                                    messageNode = MessageNode(content: audioContent)
                                }
                                messageNode.chatVC = chatVC
                                messageNode.chatId = message.chat_id
                                messageNode.cellPadding = chatVC.messagePadding
                                messageNode.avatarNode = createReply()
                                if (!isIncomingMessage)
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 17,
                                                     left:  -17,
                                                     bottom: 17,
                                                     right: 17)
                                }
                                else
                                {
                                    messageNode.avatarInsets =
                                        UIEdgeInsets(top: 17,
                                                     left: 17,
                                                     bottom: 17,
                                                     right:  -17)
                                }
                                
                                let newMessageGroup = createAction()
                                newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                                searchChatId.append(message.chat_id)
                                newMessageGroup.isIncomingMessage = isIncomingMessage
                                messageGroups.append(newMessageGroup)
                                let searchModelLocal = SearchModel()
                                searchModelLocal.messageGroups.append(newMessageGroup)
                                searchModelLocal.messegesChatId.append(searchChatId)
                                searchModel.append(searchModelLocal)
                                lastRecievedChatId = message.chat_id
                            }
                        }
                        
                        
                        
                    }
                    
                } else if message.chat_kind == "action" {
                    // Group, Channel Actions (i.e someone added to the group)
                    let actionNode = ActionContentNode(textMessageString: getActionCredentials(message), bubbleConfiguration: ActionBubbleConfiguration())
                    
                    let messageNode = MessageNode(content: actionNode)
                    messageNode.cellPadding = chatVC.messagePadding
                    
                    let newMessageGroup = createAction()
                    newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                    searchChatId.append(message.chat_id)
                    newMessageGroup.isCenterMessage = true
                    
                    messageGroups.append(newMessageGroup)
                    let searchModelLocal = SearchModel()
                    searchModelLocal.messageGroups.append(newMessageGroup)
                    searchModelLocal.messegesChatId.append(searchChatId)
                    searchModel.append(searchModelLocal)
                    lastRecievedChatId = message.chat_id
                    
                } else  {
                    // Messages for Chat, Group, Channel
                    var isIncomingMessage = message.sender_user_id != info.user_id ? false : true
                    if self.chatType == .group || self.chatType == .channel{
                        if let user = User.currentUser() {
                            isIncomingMessage = message.sender_user_id == user.user_id ? false : true
                            
                        }
                    }
                    var textContent = TextContentNode(id: message.chat_id,textMessageString: message.chat_text, dateString: message.chat_time, currentViewController: chatVC,senderName:message.sender_name,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                    if chatType == .channel {
                        textContent = TextContentNode(id: message.chat_id, textMessageString: message.chat_text, dateString: message.chat_time, currentViewController: chatVC,senderName:message.sender_name,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration, viewsCount: message.view_count)
                    }
                    
                    var messageNode = MessageNode(content: textContent)
                    textContent.message = messageNode
                    if message.is_contact == 1 {
                        var contactContent = ContactContentNode(id: message.chat_id,imageUrl: "", name: message.contact_name, phone: message.contact_phone, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_resend:message.is_resend,is_exist:message.is_exist,contact_user_id: message.contact_user_id, is_read:message.is_read,bubbleConfiguration: chatVC.sharedBubbleConfiguration, userId: 0)
                        messageNode = MessageNode(content: contactContent)
                        if (message.answer_chat_id != 0){
                            var senderName = message.sender_name
                            if (message.sender_user_id == info.user_id){
                                senderName = "Вы"
                            }
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            contactContent = ContactContentNode(id: message.chat_id,AnswerMessage_id: message.answer_chat_id,imageUrl: "", name: message.contact_name, phone: message.contact_phone, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_resend:message.is_resend,is_exist:message.is_exist,contact_user_id: message.contact_user_id, is_read:message.is_read,bubbleConfiguration: chatVC.sharedBubbleConfiguration, userId: 0,textMessageString:fileFormatTitle,senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true)
                            messageNode = MessageNode(content: contactContent)
                        }
                    }
                    if message.is_sticker == 1 {
                        let imageNode = NetworkImageContentNode(id:message.chat_id,imageURL: message.sticker_image.encodeUrl()!, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,is_sticker:1, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: imageNode)
                    }
                    var senderName = message.sender_name
                    if (message.sender_user_id == info.user_id){
                        senderName = "Вы"
                        
                    }
                    if (message.answer_chat_id != 0){
                        if (message.is_has_file == 1){
                            var fileFormatTitle = message.answer_chatText
                            switch message.answerFileFormat {
                            case "audio":
                                fileFormatTitle = "Аудио"
                            case "file":
                                fileFormatTitle = "Документ"
                            case "video":
                                fileFormatTitle = "Видеозапись"
                            case "image":
                                fileFormatTitle = "Фото"
                            default: break
                            }
                            let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString:message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:fileFormatTitle,senderName:senderName,url:message.answerFileUrl, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: answerTextContent)
                        }
                        else {
                            let answerTextContent = AnswerTextContentNode(id: message.chat_id,AnswerMessage_id:message.answer_chat_id,textMessageString: message.chat_text, dateString: message.chat_time,is_read:message.is_read,senderTextMessageString:message.answer_chatText,senderName:senderName, currentViewController: chatVC,is_resend:message.is_resend,is_contact:message.answer_is_contact, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            messageNode = MessageNode(content: answerTextContent)
                        }
                        if (message.is_sticker == 1){
                            let imageNode = NetworkImageContentNode(id:message.chat_id,AnswerMessage_id: message.answer_chat_id,imageURL: message.sticker_image.encodeUrl()!, date: message.chat_time, currentVC: chatVC,senderName:message.sender_name,is_read:message.is_read, is_resend:message.is_resend,is_sticker:1, textMessageString:"Стикер",senderTextMessageString:senderName,url:message.answerFileUrl,is_answer:true, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                            print("afafssffas calls")
                            messageNode = MessageNode(content: imageNode)
                        }
                    }
                    else if (message.is_has_link == 1){
                        let answerTextContent = LinkContentNode(id: message.chat_id,title:message.link_title,textMessageString: message.link_description, dateString: message.chat_time,is_read:message.is_read,senderName:message.sender_name,url:message.link_image,link:message.chat_text,currentViewController: chatVC,is_resend:message.is_resend, bubbleConfiguration: chatVC.sharedBubbleConfiguration)
                        messageNode = MessageNode(content: answerTextContent)
                    }
                   
                    messageNode.cellPadding = chatVC.messagePadding
                    messageNode.currentViewController = chatVC
                    messageNode.chatVC = chatVC
                    messageNode.chatId = message.chat_id
                    lastRecievedChatId = message.chat_id
                    
                    if messageGroups.last == nil || messageGroups.last?.isIncomingMessage == !isIncomingMessage {
                        let newMessageGroup = createAction()
                        if isIncomingMessage && chatType == .group {
                            newMessageGroup.avatarNode = createAvatar(with: message.sender_avatar)
                            newMessageGroup.chatVC = chatVC
                            newMessageGroup.dialogId = dialogId
                            newMessageGroup.user_id = message.sender_user_id
                            newMessageGroup.info = self.info
                        }
                        newMessageGroup.isIncomingMessage = isIncomingMessage
                        newMessageGroup.addMessageToGroup(messageNode, completion: nil)
                        searchChatId.append(message.chat_id)
                        messageGroups.append(newMessageGroup)
                        let searchModelLocal = SearchModel()
                        searchModelLocal.messageGroups.append(newMessageGroup)
                        searchModelLocal.messegesChatId.append(searchChatId)
                        searchModel.append(searchModelLocal)
                    } else {
                        messageGroups.last?.addMessageToGroup(messageNode, completion: nil)
                        searchModel.last?.messageGroups[(searchModel.last?.messageGroups.count)! - 1] =  (messageGroups.last)!
                        searchModel.last!.messegesChatId[searchModel.last!.messegesChatId.count - 1].append(message.chat_id)
                    }
                      
                    // We save last local chat_id. It need when we pull history from server (getDialogMessages:Method)
                    lastRecievedChatId = message.chat_id
                    
                }
            }
        }
        return messageGroups
        
    }
    func getThumbnail(with sourceURL: URL) -> UIImage? {
        let asset = AVAsset(url: sourceURL)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        var time = asset.duration
        time.value = min(time.value, 2)

        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print(error)
            return UIImage(named: "some generic thumbnail")
        }
    }
    
    private func getActionCredentials(_ data: DialogHistory) -> String {
        // I.e User One added to ther group User Two
        var text = data.sender_name + " " + data.action_name
        if data.recipient_user_id != 0 {
            text += " \(data.recipient_name)"
        }
        return text
    }
    
    private func createMessageGroup(currentDate:String,previousDate:String) -> MessageGroup {
        let newMessageGroup = MessageGroup()
           if (currentDate != previousDate) {
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            if let dateString = dateFormatter.date(from: currentDate){
                if (isToday == false){
                    isToday = true
                    let messageTimestamp = MessageSentIndicator()
                    messageTimestamp.messageSentText = "Сегодня"
                    newMessageGroup.addMessageToGroup(messageTimestamp, completion: nil)
                }
            }
            else {
                let messageTimestamp = MessageSentIndicator()
                messageTimestamp.messageSentText = currentDate
                newMessageGroup.addMessageToGroup(messageTimestamp, completion: nil)
            }
            
        }
        newMessageGroup.currentViewController = chatVC
        newMessageGroup.cellPadding = chatVC.messagePadding
        return newMessageGroup
}
    private func createAction() -> MessageGroup {
        let newMessageGroup = MessageGroup()
        newMessageGroup.currentViewController = chatVC
        newMessageGroup.cellPadding = chatVC.messagePadding
        return newMessageGroup
        
    }
    
    private func createReply() -> ASNetworkImageNode {
        let avatar = ASNetworkImageNode()
        avatar.backgroundColor = UIColor.lightGray
        avatar.style.preferredSize = CGSize(width: 26, height: 26)
        avatar.layer.cornerRadius = 13
        avatar.image = #imageLiteral(resourceName: "icon_reply")

        return avatar
    }
    private func createAvatar(with imageUrl: String = "") -> ASNetworkImageNode {
        let avatar = ASNetworkImageNode()
        avatar.backgroundColor = UIColor.lightGray
        avatar.style.preferredSize = CGSize(width: 26, height: 26)
        avatar.layer.cornerRadius = 13
        avatar.image = #imageLiteral(resourceName: "bg_gradient_3")
        if !imageUrl.isEmpty {
            avatar.image = nil
            avatar.url = URL(string: imageUrl.encodeUrl()!)
        }
        return avatar
    }
    // MARK: API ENDPOINT Get Detail of Chat
    func returnLastChatId(page: Int = 1, per_page: Int = 40, requestSuccess: @escaping (JSON) -> Void) {
        
        var parameters = ["token": User.getToken(), "page": page, "per_page": per_page] as [String : Any]
        
        switch chatType {
        case .single:
            parameters["recipient_user_id"] = info.user_id
        case .group:
            parameters["group_id"] = info.group_id
        case .channel:
            parameters["channel_id"] = info.channel_id
        }
        
        NetworkManager.makeRequest(.getDialogDetails(parameters), success: { (json)  in
            if self.lastRecievedChatId <  json["data"].arrayValue.last!["chat_id"].intValue {
                DialogHistory.initWith(json: json["data"].arrayValue.last!, dialog_id: self.dialogId)
                requestSuccess(json["data"])
                
            }
        })
    }
    func getLastDialogMessages(page: Int = 1, per_page: Int = 40, requestSuccess: @escaping ([MessageGroup]) -> Void) {
        
        var parameters = ["token": User.getToken(), "page": page, "per_page": per_page] as [String : Any]
        
        switch chatType {
        case .single:
            parameters["recipient_user_id"] = info.user_id
        case .group:
            parameters["group_id"] = info.group_id
        case .channel:
            parameters["channel_id"] = info.channel_id
        }
        
        NetworkManager.makeRequest(.getDialogDetails(parameters), success: { (json)  in
            if self.lastRecievedChatId <  json["data"].arrayValue.last!["chat_id"].intValue {
                DialogHistory.initWith(json: json["data"].arrayValue.last!, dialog_id: self.dialogId)
                }
                //DialogHistory.initWith(json: subJson, dialog_id: self.dialogId)
            
            self.loadNewMessageFromRealmSocket(success: { newMessages in
                requestSuccess(newMessages)
            })
        })
    }
    func getDialogMessages(page: Int = 1, per_page: Int = 10, requestSuccess: @escaping ([MessageGroup]) -> Void) {
        
        var parameters = ["token": User.getToken(), "page": page, "per_page": per_page] as [String : Any]
        
        switch chatType {
        case .single:
            parameters["recipient_user_id"] = info.user_id
        case .group:
            parameters["group_id"] = info.group_id
        case .channel:
             parameters["channel_id"] = info.channel_id
        }
    
        NetworkManager.makeRequest(.getDialogDetails(parameters), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                if self.lastRecievedChatId < subJson["chat_id"].intValue {
                    print("afsfs1212afas", self.lastRecievedChatId,parameters)
                    DialogHistory.initWith(json: subJson, dialog_id: self.dialogId)
                                        print("asffffsafsa",subJson)
                }
                if self.lastRecievedChatId == subJson["chat_id"].intValue {
                    DialogHistory.initMakeIs_read(dialog_id: self.dialogId)
                    NotificationCenter.default.post(name: AppManager.is_readNotification, object: nil)
                }
                //DialogHistory.initWith(json: subJson, dialog_id: self.dialogId)
            }
            self.loadNewMessageFromRealm(success: { newMessages in
                requestSuccess(newMessages)
            })
        })
    }
    
    func getDialogMessagesForStore(page: Int = 1, per_page: Int = 10,search:Bool = false, requestSuccess: @escaping() -> Void) {
        if (search == false){
        let realm = try! Realm()
        let historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId))
        print("afsfasfas",historyObjects.count,page)
        guard historyObjects.count <= (page * 10) else {
            requestSuccess()
            return
        }
        }
        var parameters = ["token": User.getToken(), "page": page, "per_page": per_page] as [String : Any]
        
        switch chatType {
        case .single:
            parameters["recipient_user_id"] = info.user_id
        case .group:
            parameters["group_id"] = info.group_id
        case .channel:
            parameters["channel_id"] = info.channel_id
        }
        print("zzzzzxxvwew",parameters)
        NetworkManager.makeRequest(.getDialogDetails(parameters), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                DialogHistory.initWith(json: subJson, dialog_id: self.dialogId)
                print("asffsafsa",subJson)
                //DialogHistory.initWith(json: subJson, dialog_id: self.dialogId)
            }
            
        //    self.loadNewMessageFromRealm(success: { newMessages in
               requestSuccess()
        //    })
        })
    }
    func getPageByChatId(chatId:Int,page: Int = 1, per_page: Int = 10, requestSuccess: @escaping ([Int]) -> Void) {
        let parameters = ["token": User.getToken(),"page": page, "per_page": per_page, "chat_id": chatId] as [String : Any]

        SVProgressHUD.show()
        let url = "https://api.dalagram.com/api/chat/page?token=\(User.getToken())&page=\(page)&per_page=\(per_page)&chat_id=\(chatId)"
        print("afafs",url)
        AF.request(url,
                          method: .get,
                          parameters: nil,
                          encoding: URLEncoding.default,
                          headers:nil)
            .validate().responseJSON { response in
                switch response.result {
                       case .success(let value):
                            var chatIdList = [Int]()
                           let json = try! JSON(data: (response.data)!)
                                  chatIdList.append(json["page"].intValue)
                               if (json["page2"].exists()){
                                   chatIdList.append(json["page2"].intValue)
                               }
                           requestSuccess(chatIdList)
                       case .failure(let error): break
                           print("Ошибка при запросе данных \(String(describing: response.error))")
                                           return
                       }
        }
    }
    func getSearchMessages(page: Int = 1, per_page: Int = 100,search:String, requestSuccess: @escaping ([Int]) -> Void) {
        
        var parameters = ["token": User.getToken(), "page": page, "per_page": per_page,"search":search] as [String : Any]
        
        switch chatType {
        case .single:
            parameters["recipient_user_id"] = info.user_id
        case .group:
            parameters["group_id"] = info.group_id
        case .channel:
            parameters["channel_id"] = info.channel_id
        }
        var chatIdList = [Int]()
        NetworkManager.makeRequest(.getDialogDetails(parameters), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
              chatIdList.append(subJson["chat_id"].intValue)
            }
            requestSuccess(chatIdList)
        })
    }
    // MARK: API ENDPOINT Get Detail Info of Chat
    
    func getDialogDetails(success: @escaping (DialogDetail) -> Void) {
        switch chatType {
        case .group:
            NetworkManager.makeRequest(.getGroupDetails(group_id: info.group_id), success: { (json)  in
                success(DialogDetail(type: .group, json: json["data"]))
                print(json)
            })
        case .channel:
            NetworkManager.makeRequest(.getChannelDetails(channel_id: info.channel_id), success: { (json) in
                success(DialogDetail(type: .channel, json: json["data"]))
            })
        default:
            break
        }
    }
    
    // MARK: API ENDPOINT: Upload chat file
    func uploadMultipleChatFile(with imageData: Data, format: String, fileExtension: String = "",fileTime: String = "", fileName: String = "", success: @escaping ([String: String]) -> Void) {
        NetworkManager.makeRequest(.uploadChatFile(imageData, format: format, fileExtension: fileExtension, fileName: fileName), success: { (json) in
            print(json)
            let jsonDictionary = ["file_url": json["file_url"].stringValue,
                                  "file_format": json["file_format"].stringValue,
                                  "file_name": json["file_name"].stringValue,
                                  "file_time": fileTime]
            success(jsonDictionary)
           
        })
    }
    func sendMultipleChatFile(format: String,files: Array<[String: String]>, success: @escaping () -> Void) {
            var parameters: [String: Any] = [:]
            switch format {
            case "image":
                parameters["chat_text"] = ""
            case "document":
                parameters["chat_text"] = ""
            case "video":
                parameters["chat_text"] = ""
            case "audio":
                parameters["chat_text"] = ""
            default: break
            }
            
            switch self.chatType {
            case .single:
                parameters["recipient_user_id"] = self.info.user_id
            case .group:
                parameters["group_id"] = self.info.group_id
            case .channel:
                parameters["channel_id"] = self.info.channel_id
            }
        if self.isReplyMessage && self.replyMessageId != 0 {
            parameters["answer_chat_id"] = self.replyMessageId
        }
        print("asfasf",files)
            NetworkManager.makeRequest(.sendFileMessage(params: parameters, files: files), success: { (json) in
                if let user = User.currentUser() {
                    let senderJson: NSDictionary = ["user_id": "\(user.user_id)"]
                    SocketIOManager.shared.socket.emit("message", ["chat_text":  parameters["chat_text"],
                                                                   "sender": senderJson,
                                                                   "sender_user_id": user.user_id,
                                                                   "dialog_id": self.dialogId,
                                                                   "sender_name": user.user_name,
                                                                   "recipient_user_id": self.info.user_id,
                                                                   "is_has_file": 1,
                                                                   "file_list":files])
                    print("makeRequest(.sendFileMessage",parameters)
                }
                success()
            })

    }
    func uploadChatFile(with imageData: Data, format: String, fileExtension: String = "",fileTime: String = "", fileName: String = "", success: @escaping () -> Void) {
           print("12121asas",fileName)
        NetworkManager.makeRequest(.uploadChatFile(imageData, format: format, fileExtension: fileExtension, fileName: fileName), success: { (json) in
            print(json)
            let jsonDictionary = ["file_url": json["file_url"].stringValue,
                                  "file_format": json["file_format"].stringValue,
                                  "file_name": fileName,
                                  "file_time": fileTime]
            print("afsfassfafas",jsonDictionary)
            var parameters: [String: Any] = [:]
            switch format {
            case "image":
                parameters["chat_text"] = ""
            case "document":
                parameters["chat_text"] = ""
            case "video":
                parameters["chat_text"] = ""
            case "audio":
                parameters["chat_text"] = ""
            default: break
            }
            
            switch self.chatType {
            case .single:
                parameters["recipient_user_id"] = self.info.user_id
            case .group:
                parameters["group_id"] = self.info.group_id
            case .channel:
                parameters["channel_id"] = self.info.channel_id
            }
            if self.isReplyMessage && self.replyMessageId != 0 {
                parameters["answer_chat_id"] = self.replyMessageId
            }
            print("asfzdsdssdsd1sasaf",parameters)
            NetworkManager.makeRequest(.sendFileMessage(params: parameters, files: [jsonDictionary]), success: { (json) in
                if let user = User.currentUser() {
                    let senderJson: NSDictionary = ["user_id": "\(user.user_id)"]
                SocketIOManager.shared.socket.emit("message", ["chat_text":  parameters["chat_text"],
                                                               "sender": senderJson,
                                                               "sender_user_id": user.user_id,
                                                               "dialog_id": self.dialogId,
                                                               "sender_name": user.user_name,
                                                               "recipient_user_id": self.info.user_id,
                                                               "is_has_file": 1,
                                                        "file_list":jsonDictionary])
                    print("makeRequest(.sendFileMessage",parameters)
                }
                success()
            })
        })
    }
    
    // MARK: - Socket Event: Message
   /* func socketMessageEvent(onSuccess: @escaping ([MessageGroup]) -> Void) {
        SocketIOManager.shared.socket.on("message") { dataArray, ack in
            let dict = dataArray[0] as! NSDictionary
            
            var jsonObj = JSON(dict)
            print("json ==",jsonObj)
            DialogHistory.initWith(json: jsonObj, dialog_id: self.dialogId)
            //DialogHistory.initWith(json: subJson, dialog_id: self.dialogId)
        }
        self.loadNewMessageFromRealm(success: { newMessages in
            onSuccess(newMessages)
        })
    }*/

    func socketMessageEvent(onSuccess: @escaping () -> Void) {
        SocketIOManager.shared.socket.on("message") { dataArray, ack in
            let dict = dataArray[0] as! NSDictionary
            let jsonObj = JSON(dict)
            if let text = dict.value(forKey: "chat_text") as? String,
                let recipiend_id = dict.value(forKey: "recipient_user_id") as? Int {
                if self.chatType == .single{
                if let user = User.currentUser() {
                    if recipiend_id == user.user_id{
                        if let currentDialog = self.dialogs?.filter(NSPredicate(format: "id = %@", self.dialogId)).first {
                            let realm = try! Realm()
                            try! realm.write {
                                currentDialog.messagesCount = 0
                            }
                        }
                            onSuccess()
                            SocketIOManager.shared.isRead(dialog_id: self.dialogId)
                    }
                }
                }
                    else if (self.chatType == .group || self.chatType == .channel){
                    guard jsonObj["dialog_id"].stringValue == self.dialogId else {
                        return
                    }
                        let sender_user_id = jsonObj["sender"]["user_id"].intValue
                        if sender_user_id != User.currentUser()?.user_id{
                            if let currentDialog = self.dialogs?.filter(NSPredicate(format: "id = %@", self.dialogId)).first {
                                let realm = try! Realm()
                                try! realm.write {
                                    currentDialog.messagesCount = 0
                                }
                            }
                            onSuccess()
                            SocketIOManager.shared.isRead(dialog_id: self.dialogId)
                        }
                        
                    }

            }
        }
        SocketIOManager.shared.socket.on("read") { dataArray, ack in
            let dict = dataArray[0] as! NSDictionary
            print("affa",dict)
            let SocketDialog_id = dict["dialog_id"] as? String
            let sender_id = dict["sender_id"] as? Int
            if let user = User.currentUser() {
                print("afsfa",user,sender_id,SocketDialog_id,user.user_id)
            if ("\(sender_id)U" == self.dialogId)&&(SocketDialog_id != "\(user.user_id)"){
               DialogHistory.initMakeIs_read(dialog_id: self.dialogId)
            NotificationCenter.default.post(name: AppManager.is_readNotification, object: nil)
               
            }
                }
                
        }
    }
    
    // MARK: - Socket Event: Typing
    
    func socketTypingEvet(onSuccess: @escaping (_ isTyping: Bool) -> Void) {
        SocketIOManager.shared.socket.on("message") { dataArray, ack in
            let dict = dataArray[0] as! NSDictionary
            if let value = dict.value(forKey: "typingValue") as? Bool {
                onSuccess(value)
            }
        }
    }
    
    // MARK: - Socket Emit: Message
    
    func socketMessageEmit(text: String, onSuccess: @escaping ()-> Void) {
        if let user = User.currentUser() {
            let senderJson: NSDictionary = ["user_id": "\(user.user_id)"]
            SocketIOManager.shared.socket.emit("message", ["chat_text": text,
                                                           "sender": senderJson,
                                                           "sender_user_id": user.user_id,
                                                           "dialog_id": dialogId,
                                                           "user_name": user.user_name,
                                                           "recipient_user_id": info.user_id])
            sendMessageApi(chat_text: text, success: {
                onSuccess()
                print("asfsffsa sendMessageApi",text)
            })
            
        } else {
            WhisperHelper.showErrorMurmur(title: "User does not exist")
        }
    }
    func sendStickerEmit(sticker_id: String, onSuccess: @escaping ()-> Void) {
        if let user = User.currentUser() {
            let senderJson: NSDictionary = ["user_id": "\(user.user_id)"]
            SocketIOManager.shared.socket.emit("message", ["sender": senderJson,
                                                             "sender_user_id": user.user_id,
                                                           "dialog_id": dialogId,
                                                           "user_name": user.user_name,
                                                           "recipient_user_id": info.user_id,
                                                           "sticker_id":sticker_id])
            var parameters = ["sticker_id":"\(sticker_id)"] as [String: Any]
            switch chatType {
            case .single:
                parameters["recipient_user_id"] = info.user_id
            case .group:
                parameters["group_id"] = info.group_id
            case .channel:
                parameters["channel_id"] = info.channel_id
            }
            parameters["sticker_id"] = sticker_id
            parameters["chat_text"] = "Стикер"
            if isReplyMessage && replyMessageId != 0 {
                parameters["answer_chat_id"] = replyMessageId
            }
            
            
            NetworkManager.makeRequest(.sendMessage(parameters), success: { (json) in
              
                
                onSuccess()
            })
            
        } else {
            WhisperHelper.showErrorMurmur(title: "User does not exist")
        }
    }
    func socketFilesEmit(text: String, files: String, senderId: Int, recipientId: Int, senderName: String = "") {
            if let user = User.currentUser() {
        let senderJson: NSDictionary = ["user_id": "\(user.user_id)"]
        SocketIOManager.shared.socket.emit("message", ["chat_text": text,
                                                       "sender": senderJson,
                                                       "sender_user_id": user.user_id,
                                                       "dialog_id": dialogId,
                                                       "sender_name": senderName,
                                                       "recipient_user_id": recipientId])
        sendMessageApi(chat_text: text, files: files)
    }
    }
    
    func socketTypingEmit() {
        if let user = User.currentUser() {
            SocketIOManager.shared.socket.emit("typing", ["dialog_id": dialogId, "sender_id": user.user_id])
        }
    }
    
    // MARK: - Send Message POST Request
    // In order to see dialogs and full history, need to send request for each message
    
    func sendMessageApi(chat_text: String = "", files: String = "", isContact: Bool = false, contactPhone: String = "", contactName: String = "",sticker_id:String = "", success: @escaping ()->Void = {}) {
        
        var parameters = ["chat_text": chat_text] as [String: Any]
        switch chatType {
            case .single:
                parameters["recipient_user_id"] = info.user_id
            case .group:
                parameters["group_id"] = info.group_id
            case .channel:
                parameters["channel_id"] = info.channel_id
        }
        if isContact {
            parameters["is_contact"] = 1
            parameters["contact_name"] = contactName
            parameters["contact_phone"] = contactPhone
        }
        if isReplyMessage && replyMessageId != 0 {
            parameters["answer_chat_id"] = replyMessageId
        }
        
        if !files.isEmpty {
            parameters["file_list"] = files
        }
        
        NetworkManager.makeRequest(.sendMessage(parameters), success: { (json) in
            if files.isEmpty {
            //    DialogHistory.initWith(json: json["data"][0], dialog_id: self.dialogId)
            }
            if (isContact){
                if let user = User.currentUser() {
                    let senderJson: NSDictionary = ["user_id": "\(user.user_id)"]
                    SocketIOManager.shared.socket.emit("message", ["chat_text": "Контакт",
                                                                   "sender": senderJson,
                                                                   "sender_user_id": user.user_id,
                                                                   "dialog_id": self.dialogId,
                                                                   "sender_name": user.user_name,
                                                                   "recipient_user_id": self.info.user_id,
                        "is_contact" : 1,
                        "contact_name":contactName,
                        "contact_phone":contactPhone])
            }
            }
            success()
        })
    }
    
    // MARK: - Update Particular Node Cell Data
    
    @objc func updateNodeData() {
        if let group = messageGroups.last {
            if let message = group.messages.last {
                if let messageNode = message as? MessageNode {
                    if let textNode = messageNode.contentNode as? TextContentNode {
                        textNode.sendIconNode.image = nil
                        textNode.sendIconNode.image = #imageLiteral(resourceName: "icon_mark_double")
                        textNode.setNeedsDisplay()
                    }
                }
            }
        }
    }
    func clearMessagesRequest(messagesList:[Int] = [],success: @escaping ()->Void = {}) {
        NetworkManager.makeRequest(.removeChat(["partner_id" : dialogId, "chat_ids":messagesList]), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            NotificationCenter.default.post(name: AppManager.loadDialogsNotification, object: nil)
            success()
        })
    }
    func clearChatRequest() {
        NetworkManager.makeRequest(.removeChat(["partner_id" : dialogId, "is_delete_all": 1]), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            NotificationCenter.default.post(name: AppManager.loadDialogsNotification, object: nil)
            
        })
        removeCurrentDialogHistory()
    }
    func ResendMessagesRequest(chatIds:[Int], success: @escaping ()->Void = {})  {
        NetworkManager.makeRequest(.resendMessage(chat_id:chatIds,user_id:dialogId), success: { (json) in
            print("asfsfas testing",chatIds)
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
         //   NotificationCenter.default.post(name: AppManager.loadDialogsNotification, object: nil)
        success()
        })
        removeCurrentDialogHistory()
    }
    func blockUserRequest() {
        NetworkManager.makeRequest(.blockUser(["partner_id" : dialogId]), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            //            NotificationCenter.default.post(name: AppManager.loadDialogsNotification, object: nil)
        })
    }
    func removeCurrentDialogHistory() {
        let realm = try! Realm()
        for object in realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId)) {
            try! realm.write {
                realm.delete(object)
            }
        }
    }
    
    // MARK: Remove DialogHistory with chat_id 0
    
    func removeEmptyDialogHistory() {
        let realm = try! Realm()
        for object in realm.objects(DialogHistory.self).filter("chat_id = 0") {
            try! realm.write {
                realm.delete(object)
            }
        }
    }

}
extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
