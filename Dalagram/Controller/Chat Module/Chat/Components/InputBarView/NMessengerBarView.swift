//
// Copyright (c) 2016 eBay Software Foundation
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

import UIKit
import AVFoundation
import Photos
import Gallery
import AsyncDisplayKit
import MobileCoreServices
import ISEmojiView
import AudioToolbox
//MARK: InputBarView
/**
 InputBarView class for NMessenger.
 Define the input bar for NMessenger. This is where the user would type text and open the camera or photo library.
 */
enum SendButtonState {
    case audio
    case text
}

open class NMessengerBarView: InputBarView, UITextViewDelegate, GalleryControllerDelegate, AVAudioPlayerDelegate, AVAudioRecorderDelegate,EmojiViewDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var stickerButton: UIButton!
    
    @IBOutlet weak var audioRecordingView: UIView!
    
    lazy var audioTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.font = UIFont.systemFont(ofSize: 15.0)
        return label
    }()
    lazy var emojiKeyboard: EmojiKeyboardView = {
        let keyboard = EmojiKeyboardView()
        keyboard.delegate = self
        return keyboard
    }()
    var timer: Timer?
    var seconds: Int = 0
    var isAudioCancelled: Bool = false
    //AVAudioPlayer
    var recordingSession : AVAudioSession!
    var audioRecorder    :AVAudioRecorder!
    var settings         = [String : Int]()
    var keyboardTypeSmails = false
    var keyboardTypeSticker = false
    @IBOutlet weak var keyboardTypeButton: UIButton!
    @IBOutlet weak var attachButton: UIButton!
    //@IBOutlet for InputBarView
    @IBOutlet open weak var inputBarView: UIView!
    //@IBOutlet for send button
    @IBOutlet open weak var replyLabel: UILabel!
    @IBOutlet open weak var replyView: UIView!
    @IBOutlet open weak var sendButton: UIButton!
    //@IBOutlets NSLayoutConstraint input area view height
    @IBOutlet open weak var textInputAreaViewHeight: NSLayoutConstraint!
    //@IBOutlets NSLayoutConstraint input view height
    @IBOutlet open weak var textInputViewHeight: NSLayoutConstraint!
    
    //MARK: Public Parameters
    
    //CGFloat to the fine the number of rows a user can type
    open var numberOfRows:CGFloat = 10
    
    //String as placeholder text in input view
    open var inputTextViewPlaceholder: String = "enter_mesage".localized()
    {
        willSet(newVal)
        {
            self.textInputView.text = newVal
        }
    }
    
    // Send Button State (Audio, Text)
    var sendButtonState: SendButtonState = .audio {
        didSet {
            sendButton.setImage(sendButtonState == .audio ? #imageLiteral(resourceName: "icon_audio") : #imageLiteral(resourceName: "icon_send"), for: .normal)
        }
    }
    
    //MARK: Private Parameters
    
    //CGFloat as defualt height for input view
    fileprivate let textInputViewHeightConst:CGFloat = 35
    
    // MARK: Initialisers
    
    public required init() {
        super.init()
    }

    /**
     Initialiser the view.
     - parameter controller: Must be NMessengerViewController. Sets controller for the view.
     Calls helper method to setup the view
     */
    public required init(controller: NMessengerViewController) {
        super.init(controller: controller)
        loadFromBundle()
    }
    /**
     Initialiser the view.
     - parameter controller: Must be NMessengerViewController. Sets controller for the view.
     - parameter frame: Must be CGRect. Sets frame for the view.
     Calls helper method to setup the view
     */
    public required init(controller:NMessengerViewController,frame: CGRect) {
        super.init(controller: controller,frame: frame)
        loadFromBundle()
    }
    /**
     - parameter aDecoder: Must be NSCoder
     Calls helper method to setup the view
     */
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadFromBundle()
    }
    
    // MARK: Initialiser helper methods
    /**
     Loads the view from nib file InputBarView and does intial setup.
     */
    fileprivate func loadFromBundle() {
        
        // Gallery Picker Configs
        Config.Grid.FrameView.borderColor = UIColor.navBlueColor
        Config.Camera.imageLimit = 6
        Config.initialTab = Config.GalleryTab.imageTab
        Config.Camera.recordLocation = false
        Config.VideoEditor.savesEditedVideoToLibrary = false
        
        _ = Bundle(for: NMessengerViewController.self).loadNibNamed("NMessengerBarView", owner: self, options: nil)?[0] as! UIView
        self.addSubview(inputBarView)
        inputBarView.frame = self.bounds
        textInputView.delegate = self
        
        // Customize
        self.textInputView.layer.borderColor = UIColor(white: 0.8, alpha: 0.8).cgColor
        self.textInputView.layer.borderWidth = 0.5
        self.textInputView.layer.cornerRadius = 5.0
        self.textInputAreaView.backgroundColor = UIColor.lightGrayColor
        self.replyAreaHeighConstraint.constant = 0.0
    
        configureAudioView()
        setAudioConfig()

    }
    func setAudioConfig(){
        
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("sound.m4a")
        print(soundURL)
        return soundURL as NSURL?
    }
    func startRecording() {
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
        }
    }
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        if success {
            print(success)
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    // MARK: - Configure Audio Recording View
    
    func configureAudioView() {
        sendButtonState = .audio
        
        let swipeToCloseLabel: UILabel = UILabel()
        swipeToCloseLabel.text = "Влево - отмена"
        
        audioRecordingView.backgroundColor = UIColor.lightGrayColor
        audioRecordingView.addSubview(audioTimeLabel)
        audioRecordingView.addSubview(swipeToCloseLabel)
        
        audioTimeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(16.0)
            make.centerY.equalToSuperview()
        }
        
        swipeToCloseLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.centerX.equalToSuperview()
        }
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(audioButonLongPress))
        sendButton.addGestureRecognizer(longGesture)
        
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(audioButtonSwipeAction))
        swipeGesture.direction = .left
        sendButton.addGestureRecognizer(swipeGesture)
        
        let doubleGesture = UITapGestureRecognizer(target: self, action: #selector(sendButonDoublePress))
        doubleGesture.numberOfTapsRequired = 2
        sendButton.addGestureRecognizer(doubleGesture)
        
    }
    
    func scheduleAudioTimer() {
        seconds = 0
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimerAction), userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    // MARK: - Update Timer Action
    
    @objc func updateTimerAction() {
        seconds = seconds + 1
        let minute = seconds/60
        let realSeconds = seconds % 60 == 0 ? 0 : seconds % 60
        let prefixSec = realSeconds < 10 ? "0" : ""
        audioTimeLabel.text = "\(minute):\(prefixSec)\(realSeconds)"
        print(seconds)
    }
    
    // MARK: - Send Button Touch Actions
    
    @objc func audioButonLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {
        
        if sendButtonState == .audio {
           
            switch gestureRecognizer.state {
                case .began:
                    UIView.animate(withDuration: 0.3, animations: {
                        self.sendButton.transform = CGAffineTransform.identity.scaledBy(x: 1.4, y: 1.4)
                    }, completion: { (finish) in
                    })
                    print("began recording")
                    isAudioCancelled = false
                    scheduleAudioTimer()
                    self.audioRecordingView.alpha = 0
                    self.audioRecordingView.isHidden = false
                    UIView.animate(withDuration: 0.3) {
                        self.audioRecordingView.alpha = 1
                    }
              //       DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                
                //    }
                    
                  self.startRecording()
                case .changed:
                    //print("changed recording")
                    let longPressView = gestureRecognizer.view
                    let longPressPoint = gestureRecognizer.location(in: longPressView)
                    print(longPressPoint)
                    
                    if longPressPoint.x < -20.0 {
                        audioButtonSwipeAction()
                    }
                   
                    break
                case .ended:
                    UIView.animate(withDuration: 0.3, animations: {
                        self.sendButton.transform = CGAffineTransform.identity
                    })
                    print("end recording")
                    if !isAudioCancelled {
                        hideAudioMessageView()
                        print("send audio message")
                        if audioRecorder != nil {
                         self.finishRecording(success: true)
                        }
                            do {
                                   let data = try! Data(contentsOf: (audioRecorder.url))
                                 print("send audioRecorder.url",audioRecorder.url)
                                _ = self.controller.sendAudio(audioTimeLabel.text!, data: data, date: Date().getStringTime(),audioUrl: (audioRecorder.url),chat_id:"",fileTime:"\(seconds)", isIncomingMessage: false)
                            } catch let error as NSError {
                                print("audioPlayer error: \(error.localizedDescription)")
                            }
                    }
                default:
                    break
            }
        }
    }
    
    @objc func audioButtonSwipeAction() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        print("swipped")
        isAudioCancelled = true
        hideAudioMessageView()
    }
    
    func hideAudioMessageView() {
        timer?.invalidate()
        timer = nil
        UIView.animate(withDuration: 0.3, animations: {
            self.audioRecordingView.alpha = 0
        }) { (completed) in
            self.audioRecordingView.isHidden = completed
        }
    }
    
    @objc func sendButonDoublePress() {
        switch sendButtonState {
        case .audio:
            sendButtonState = .text
        case .text:
            sendButtonState = .audio
        }
    }
    
   
    
    // MARK: - Close Reply Button Action
    @IBAction func closeReplyAreaAction(_ sender: UIButton) {
        changeReplyArea(with: "")
        isReplyMessage.value = false
    }
    
    //MARK: TextView delegate methods
    
    /**
     Implementing textViewShouldBeginEditing in order to set the text indicator at position 0
     */
    open func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {

        if textView.text == inputTextViewPlaceholder {
            textView.text = ""
        }
       
        textView.textColor = UIColor.n1DarkestGreyColor()
        UIView.animate(withDuration: 0.1, animations: {
            self.sendButton.isEnabled = true
        })
        DispatchQueue.main.async(execute: {
            textView.selectedRange = NSMakeRange(textView.text.count, textView.text.count)
        })
        return true
    }
    /**
     Implementing textViewShouldEndEditing in order to re-add placeholder and hiding send button when lost focus
    */
    open func textViewShouldEndEditing(_ textView: UITextView) -> Bool {

        if self.textInputView.text.isEmpty {
            self.addInputSelectorPlaceholder()
        }
        self.textInputView.resignFirstResponder()
        return true
    }
    /**
     Implementing shouldChangeTextInRange in order to remove placeholder when user starts typing and to show send button
     Re-sizing the text area to default values when the return button is tapped
     Limit the amount of rows a user can write to the value of numberOfRows
    */
    
    open func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    
        
        return true
    }
    
    /**
     Implementing textViewDidChange in order to resize the text input area
     */
    open func textViewDidChange(_ textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        var newFrame = textView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        textInputViewHeight.constant = newFrame.size.height
        textInputAreaViewHeight.constant = newFrame.size.height + 10
        // Emit "Typing" Event
        if (textView.text.count >= 1){
            sendButtonState = .text
            self.attachButton.isHidden = true
            self.stickerButton.isHidden = true
        }
        else {
            self.attachButton.isHidden = false
            self.stickerButton.isHidden = false
            sendButtonState = .audio
        }
        self.controller.sendTypingEvent()
    }
    
    //MARK: TextView helper methods
    /**
     Adds placeholder text and change the color of textInputView
     */
    
    fileprivate func addInputSelectorPlaceholder() {
        self.textInputView.text = self.inputTextViewPlaceholder
        self.textInputView.textColor = UIColor.lightGray
    }
    
    //MARK: @IBAction selectors
    /**
     Send button selector
     Sends the text in textInputView to the controller
     */
    
    @IBAction open func sendButtonClicked(_ sender: AnyObject) {
        if sendButtonState == .text && self.textInputView.text != inputTextViewPlaceholder {
            textInputViewHeight.constant = textInputViewHeightConst;            textInputAreaViewHeight.constant = textInputViewHeightConst + 10
            if self.textInputView.text != ""
            {
                _ = self.controller.sendText(self.textInputView.text , date: Date().getStringTime(), isIncomingMessage: false)
                self.textInputView.text = ""
            }
        }
        self.attachButton.isHidden = false
        self.stickerButton.isHidden = false
      //  _ = self.controller.sendAudio(audioTimeLabel.text!, data: Data(), date: Date().getStringTime(), isIncomingMessage: false)
    }
    @IBAction func keyboardTypeButton(_ sender: Any) {
        if (keyboardTypeSmails == false) {
            self.textInputView.resignFirstResponder()
            let keyboardSettings = KeyboardSettings(bottomType: .categories)
            let emojiView = EmojiView(keyboardSettings: keyboardSettings)
            emojiView.translatesAutoresizingMaskIntoConstraints = false
            emojiView.delegate = self
            self.textInputView.inputView = emojiView
            keyboardTypeSmails = true
            keyboardTypeButton.setImage(UIImage(named:"icon_emoticon"), for: .normal)
            self.textInputView.reloadInputViews()
            self.textInputView.becomeFirstResponder()
        }
        else {
            self.textInputView.resignFirstResponder()
            keyboardTypeSticker = false
            stickerButton.setImage(UIImage(named:"icon_keyboard"), for: .normal)
            self.textInputView.inputView = nil
            self.textInputView.keyboardType = .default
            self.textInputView.reloadInputViews()
            self.textInputView.becomeFirstResponder()
        }
         //   self.inputBarView.endEditing(true)
            //self.textInputView.becomeFirstResponder()
    }
    /**
     Attach button selector
     Requests camera and photo library permission if needed
     Open camera and/or photo library to take/select a photo
     */
    @IBAction func stickerClicked(_ sender: Any) {
        if (keyboardTypeSticker == false){
            self.textInputView.resignFirstResponder()
            keyboardTypeSticker = true
            stickerButton.setImage(UIImage(named:"icon_sticker_white"), for: .normal)
            self.textInputView.inputView = emojiKeyboard
            self.textInputView.becomeFirstResponder()
        }
        else {
            self.textInputView.resignFirstResponder()
            keyboardTypeSticker = false
            stickerButton.setImage(UIImage(named:"icon_keyboard"), for: .normal)
            self.textInputView.inputView = nil
            self.textInputView.keyboardType = .default
            self.textInputView.reloadInputViews()
            self.textInputView.becomeFirstResponder()
            
        }
    }

    @IBAction open func plusClicked(_ sender: AnyObject?) {
        let alert = UIAlertController(title: "choise", message: nil, preferredStyle: .actionSheet)
        let mediaAction = UIAlertAction(title: "photo_video".localized(), style: .default) { _ in
            let gallery = GalleryController()
            gallery.delegate = self
            self.controller.present(gallery, animated: true, completion: nil)
        }
        let fileAction = UIAlertAction(title: "documents".localized(), style: .default) { _ in
            let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            importMenu.delegate = self
            importMenu.modalPresentationStyle = .formSheet
            self.controller.present(importMenu, animated: true, completion: nil)
        }
        let contactAction = UIAlertAction(title: "contacts".localized(), style: .default) { _ in
            let vc = OwnContactsController()
            let navController = UINavigationController(rootViewController: vc)
            vc.completionBlock = { contactObj in
                // contactObj is ContactFacade object as completion parameter
                if let contact = contactObj.phoneContact {
                    _ = self.controller.sendContact(imageUrl: "", name: contact.getFullName(), phone: contact.phone, date: Date().getStringTime(), userId: 0, isIncomingMessage: false)
                }
                else if let contact = contactObj.dalagramContact {
                    _ = self.controller.sendContact(imageUrl: contact.avatar, name: contact.contact_name, phone: contact.user_name, date: Date().getStringTime(), userId: contact.user_id, isIncomingMessage: false)
                }
            }
            self.controller.present(navController, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(mediaAction)
        alert.addAction(fileAction)
        alert.addAction(contactAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.darkBlueNavColor
        self.controller.present(alert, animated: true, completion: nil)
       
    }
    // callback when tap a emoji on keyboard
    public func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
         self.textInputView.insertText(emoji)
    }
    
    // callback when tap change keyboard button on keyboard
    public func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
         self.textInputView.inputView = nil
         self.textInputView.keyboardType = .default
         self.textInputView.reloadInputViews()
    }
    
    // callback when tap delete button on keyboard
    public func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
         self.textInputView.deleteBackward()
    }
    
    // callback when tap dismiss button on keyboard
    public func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
         self.textInputView.resignFirstResponder()
    }
    // MARK: - Gallery DidSelectImages
    
    public func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        print("Selected Gallery Image", images.count)
        if images.count == 1 {
            // Single Image
            if let selectedImage = images.first {
                selectedImage.resolve(completion: { (resolvedImage) in
                    if let image = resolvedImage {
                        _ = self.controller.sendImage(image, isIncomingMessage: false)
                        self.controller.dismiss(animated: true, completion: nil)
                    }
                })
            }
        }
        else if images.count == 2{
            // Mutiple Images == 2 ImageContentNode in
            Image.resolve(images: images) { (resolvedImages) in
                if let images = resolvedImages as? [UIImage] {
                    let imageContentNodeSend = images.map { (selectedImage) -> ImageContentNode in
                          let imageContentNodeSend = ImageContentNode(image: selectedImage, date: Date().getStringTime(), currentVC: self.controller, senderName: "Я")
                        return imageContentNodeSend
                    }
                     let imageContentNode = TwoImagesContentNode(image1: images[0],image2:images[1], date: Date().getStringTime(), currentVC: self.controller, senderName: "Я")
                    let newMessage = MessageNode(content: imageContentNode)
                    _ = self.controller.sendCollectionViewWithNodes(newMessage,imageContentNodeSend,nil,imageContentNode, numberOfRows: CGFloat(1), isIncomingMessage: false)
                    
                    self.controller.dismiss(animated: true, completion: nil)
                }
            }
        }
        else if images.count == 3{
            // Mutiple Images == 2 ImageContentNode in
            Image.resolve(images: images) { (resolvedImages) in
                if let images = resolvedImages as? [UIImage] {
                    let imageContentNodeSend = images.map { (selectedImage) -> ImageContentNode in
                        let imageContentNodeSend = ImageContentNode(image: selectedImage, date: Date().getStringTime(), currentVC: self.controller, senderName: "Я")
                        return imageContentNodeSend
                    }
                    let imageContentNode = ThreeImagesContentNode(image1: images[0],image2:images[1],image3:images[2], date: Date().getStringTime(), currentVC: self.controller, senderName: "Я")
                    let newMessage = MessageNode(content: imageContentNode)
                    _ = self.controller.sendCollectionViewWithNodes(newMessage,imageContentNodeSend,nil,nil,imageContentNode, numberOfRows: CGFloat(1), isIncomingMessage: false)
                    self.controller.dismiss(animated: true, completion: nil)
                }
            }
        }
        else if images.count >= 4{
            // Mutiple Images == 2 ImageContentNode in
            Image.resolve(images: images) { (resolvedImages) in
                if let images = resolvedImages as? [UIImage] {
                    let imageContentNodeSend = images.map { (selectedImage) -> ImageContentNode in
                        let imageContentNodeSend = ImageContentNode(image: selectedImage, date: Date().getStringTime(), currentVC: self.controller, senderName: "Я")
                        return imageContentNodeSend
                    }
                    let imageContentNode = FourImagesContentNode(_images:images, date: Date().getStringTime(), currentVC: self.controller, senderName: "Я")
                    let newMessage = MessageNode(content: imageContentNode)
                    _ = self.controller.sendCollectionViewWithNodes(newMessage,imageContentNodeSend,nil,nil,nil,imageContentNode, numberOfRows: CGFloat(1), isIncomingMessage: false)
                    self.controller.dismiss(animated: true, completion: nil)
                    
                }
            }
        }
        else {
            // Mutiple Images
            
            }
                    
                    //  let newMessage = MessageNode(content: imageNodes)
                  //  _ = self.controller.sendCollectionViewWithNodes(newMessage,imageNodes, numberOfRows: CGFloat(imageNodes.count), isIncomingMessage: false)
                  //  self.controller.dismiss(animated: true, completion: nil)
        
        
        
    }
    
    // MARK: - Gallery DidSelectVideo
    
    public func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        print("Select video delegate")
        var thumbVideoImage: UIImage?
        
        video.fetchThumbnail(size: CGSize(width: 300, height: 300), completion: { (image) in
            if let thumbImage = image {
                thumbVideoImage = thumbImage
            }
        })
        
        video.fetchAVAsset { (asset) in
            if let urlAsset = asset as? AVURLAsset {
                print("url video fetched")
                do {
                    let data = try Data(contentsOf: urlAsset.url)
                    print("url thumg fetched", urlAsset.url)
                    if let image = thumbVideoImage {
                        _ = self.controller.sendVideo(image, videoUrl: urlAsset.url, videoData: data, isIncomingMessage: false)
                        self.controller.dismiss(animated: true, completion: nil)
                    }
                } catch {
                    WhisperHelper.showErrorMurmur(title: "Ошибка загрузки видео")
                }
            }
        }
    }
    
    public func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        print("requestLightbox delegate")
    }
    
    public func galleryControllerDidCancel(_ controller: GalleryController) {
        self.controller.dismiss(animated: true, completion: nil)
    }

}

// MARK: - UIDocumentMenuDelegate

extension NMessengerBarView: UIDocumentMenuDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        let fileExtension = NSString(string: fileName).pathExtension
        
        do {
            let data = try Data(contentsOf: url)
            _ = self.controller.sendDocument(fileName, fileExtension: fileExtension, fileData: data, isIncomingMessage: false)
        } catch {
            WhisperHelper.showErrorMurmur(title: "Ошибка загрузки документа")
            print("error converting from file url to data")
        }
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.controller.present(documentPicker, animated: true, completion: nil)
    }
    
    public func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
        print("view was cancelled")
        self.controller.dismiss(animated: true, completion: nil)
    }
    
}
extension NMessengerBarView: EmojiKeyboardViewDelegate {
    func KeyboardView(_ KeyboardView: EmojiKeyboardView, use emoji: EmojiViewModel) {
        let imageName = "bg_gradient_2"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        let textView = UITextView()
        EmojiHelper.getEmojiImage(for: imageView,image_id:textView, with: emoji)
        print("asffsafsa",textView.text!)
        _ = self.controller.sendSticker(imageView.image!, textView.text!, isIncomingMessage: false)
    }
    func emojiKeyBoardViewDidPressBackSpace(_ keyboardView: EmojiKeyboardView) {
        self.textInputView.deleteBackward()
    }
    
    func sendContent(_ keyboardView: EmojiKeyboardView) {
    }
    
}
