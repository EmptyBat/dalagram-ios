//
//  SingInController.swift
//  Dalagram
//
//  Created by Toremurat on 18.05.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import InputMask
import RxSwift
import RxCocoa
import DropDown
import Localize_Swift
class SignInController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var lang_dynamic: UIButton!
    @IBOutlet weak var registButton: UIButton!
    @IBOutlet weak var forgetButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var languageButton: UIButton!
    

    // MARK: - Variables
    
    var viewModel = AuthViewModel()
    var maskDelegate: MaskedTextFieldDelegate? = nil
    var disBag = DisposeBag()
    let dropDown = DropDown()
    
    // MARK: - Initializer
    
    static func instantiate() -> SignInController {
        return SignInController.fromStoryboard(name: "Auth", bundle: nil)
    }
    
    // MARK: - Life cycle
    @IBAction func loginButton(_ sender: Any) {
        self.viewModel.attempSignIn({ [unowned self] () in
       AppDelegate.shared().configureRootController(isLogged: true)
        })
    }
    @IBAction func forgetButton(_ sender: Any) {
    }
    @IBAction func registButton(_ sender: Any) {
    }
    @IBAction func languageButton(_ sender: Any) {
        dropDown.show()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Вход"
        configureUI()
        setBlueNavBar()
        phoneField.rx.text.orEmpty.bind(to: viewModel.phone).disposed(by: disBag)
        passwordField.rx.text.orEmpty.bind(to: viewModel.password).disposed(by: disBag)
        setLanguage()
    }
    @objc func languageBtn(){
        dropDown.show()
    }
    // MARK: - Configuring UI
    private func setLanguage(){
        self.title = "log_in".localized()
        self.registButton.setTitle("first_time".localized(), for: .normal)
        self.forgetButton.setTitle("forget_password".localized(), for: .normal)
        self.loginButton.setTitle("log_in".localized(), for: .normal)
        self.lang_dynamic.setTitle("language".localized(), for: .normal)
        self.phoneField.placeholder = "phone".localized()
        self.passwordField.placeholder = "password".localized()
    }
    func configureUI() {
        self.hideKeyboardWhenTappedAround()
        setEmptyBackTitle()
        loginButton.layer.cornerRadius = 16
        phoneField.keyboardType = .phonePad
        phoneField.returnKeyType = .next
        phoneField.becomeFirstResponder()
        
        passwordField.delegate = self
        passwordField.keyboardType = .default
        passwordField.returnKeyType = .done
       
        // InputMask for PhoneField
        maskDelegate = MaskedTextFieldDelegate(format: "{+7} ([000]) [000] [00] [00]")
        maskDelegate?.put(text: "+7 ", into: phoneField)
        maskDelegate?.listener = self
        phoneField.delegate = maskDelegate
        
        
        // DropDown for LanguageButton
        dropDown.anchorView = languageButton
        dropDown.dataSource = ["English", "Russia", "Kazakh"]
        dropDown.selectionAction = {[unowned self] (index: Int, item: String) in
        self.languageButton.setTitle(item, for: UIControlState.normal)
            if (index == 0){
                Localize.setCurrentLanguage("en")
            }
            else if (index == 1){
                Localize.setCurrentLanguage("ru")
            }
            else {
                Localize.setCurrentLanguage("kk-KZ")
            }
            self.setLanguage()
        }
    }
    // MARK: - DropDown action
}

// MARK: - PhoeField Listener

extension SignInController: MaskedTextFieldDelegateListener {
    
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
      /*  if value.count == 12 {
            alert(title: "Подтверждение телефона",
                message: "Убедитесь что правильно ввели Ваш номер \(value)",
                cancelButton: "Отмена",
                actionButton: "Подтвердить", handler: { [unowned self] () -> (Void) in
                self.viewModel.attempSignIn({ [unowned self] (value) in
                    self.showNextController(isNewUser: value)
                })
            })
        }
 */
        
    }

    // MARK: - Show Next Controller Action
    
    func showNextController(isNewUser: Bool) {
        let vc = ConfirmController.instantiate()
        viewModel.isNewUser.value = isNewUser
        vc.viewModel = viewModel
        self.show(vc, sender: nil)
    }

}
