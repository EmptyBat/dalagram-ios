//
//  SignUpController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/13/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import InputMask
import RxSwift
import RxCocoa
import DropDown
class SignUpController: UIViewController {
    @IBOutlet weak var title_lb: UILabel!
    @IBOutlet weak var desc_lb: UILabel!
    var viewModel = AuthViewModel()
    var maskDelegate: MaskedTextFieldDelegate? = nil
    var disBag = DisposeBag()
    var PhoneFieldValue = ""
    @IBOutlet weak var getCodeButton: UIButton!
    
    @IBOutlet weak var phoneField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Подтверждение"
        configureUI()
        setBlueNavBar()
        phoneField.rx.text.orEmpty.bind(to: viewModel.phone).disposed(by: disBag)

        
        // Do any additional setup after loading the view.
    }
    func configureUI() {
        setEmptyBackTitle()
        self.hideKeyboardWhenTappedAround()
        phoneField.keyboardType = .phonePad
        phoneField.returnKeyType = .next
        phoneField.becomeFirstResponder()
        getCodeButton.layer.cornerRadius = 16
        // InputMask for PhoneField
        maskDelegate = MaskedTextFieldDelegate(format: "{+7} ([000]) [000] [00] [00]")
        maskDelegate?.put(text: "+7 ", into: phoneField)
        maskDelegate?.listener = self
        phoneField.delegate = maskDelegate
        self.desc_lb.text = "for_security".localized()
        self.title = "confirm".localized()
        self.title_lb.text = "account_validation".localized()
        self.getCodeButton.setTitle("get_code".localized(), for: .normal)
        
    }
    
    // MARK: - DropDown action
    
}

// MARK: - PhoeField Listener

extension SignUpController: MaskedTextFieldDelegateListener {
    
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
         PhoneFieldValue = value
    }
    
    // MARK: - Show Next Controller Action
    
    func showNextController(isNewUser: Bool) {
        let vc = ConfirmController.instantiate()
        viewModel.isNewUser.value = isNewUser
        vc.viewModel = viewModel
        self.show(vc, sender: nil)
    }

    @IBAction func getCodeButton(_ sender: Any) {
        phoneField.resignFirstResponder()
        if phoneField.text!.count == 18 {
                self.viewModel.attempSignUp({ [unowned self] (value) in
                self.showNextController(isNewUser: value)
        
            })
        
        }
        else {
              WhisperHelper.showErrorMurmur(title: "Введите корректный номер телефона")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
