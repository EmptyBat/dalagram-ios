//
//  SignUpInformationController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/14/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit

class SignUpInformationController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var fermale_btn: UIButton!
    @IBOutlet weak var male_btn: UIButton!
    @IBOutlet weak var desc_lb: UILabel!
    @IBOutlet weak var title_lb: UILabel!
    @IBOutlet weak var FirstNameField: UITextField!
    @IBOutlet weak var SecondNameField: UITextField!
    @IBOutlet weak var BirthdayField: UITextField!
    
    @IBOutlet weak var statusField: UITextField!
    @IBOutlet weak var nickNameField: UITextField!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var fermaleButton: UIButton!
    @IBOutlet weak var RegistrButton: UIButton!
    var sex = "1"
    var birthday = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        BirthdayField.addTarget(self, action: #selector(birthdayClicked), for: .touchDown)
        // Do any additional setup after loading the view.
    }
    func configureUI() {
        self.title_lb.text = "first_time_in_d".localized()
        self.desc_lb.text = "sign_up_or_reg".localized()
        self.male_btn.setTitle("male".localized(), for: .normal)
        self.fermale_btn.setTitle("fermale".localized(), for: .normal)
        self.FirstNameField.placeholder = "first_name".localized()
        self.SecondNameField.placeholder = "second_name".localized()
        self.BirthdayField.placeholder = "birthday".localized()
        self.nickNameField.placeholder = "nick_name".localized()
        self.statusField.placeholder = "status".localized()
        self.RegistrButton.setTitle("registr".localized(), for: .normal)
        
        setEmptyBackTitle()
        FirstNameField.keyboardType = .default
        FirstNameField.returnKeyType = .next
        nickNameField.becomeFirstResponder()
        BirthdayField.delegate = self
        RegistrButton.layer.cornerRadius = 16
        // InputMask for PhoneField
        self.hideKeyboardWhenTappedAround()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == BirthdayField {
            return false;
        }
        return true
    }
    @objc func birthdayClicked() {
        let alert = UIAlertController(title: "Choose a birthday", message: "\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        alert.isModalInPopover = true
        
        let pickerFrame = UIDatePicker(frame: CGRect(x: 5, y: 25, width: 250, height: 240))
        pickerFrame.datePickerMode = .date
        pickerFrame.maximumDate = Date()
        alert.view.addSubview(pickerFrame)
        pickerFrame.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        alert.view.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
       // alert.view.backgroundColor = UIColor(red: 82/255, green: 185/255, blue: 98/255, alpha: 1.0)
        alert.addAction(UIAlertAction(title: "Pick", style: .default, handler: { (UIAlertAction) in

        }))
        self.present(alert,animated: true, completion: nil )
    }
    @objc func dateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print("\(day) \(month) \(year)")
            birthday = "\(day).\(month).\(year)"
            BirthdayField.text = birthday
        }
    }
    @IBAction func FermaleButton(_ sender: Any) {
    self.fermaleButton.setImage(#imageLiteral(resourceName: "checkbox_checked"), for: .normal)
    self.maleButton.setImage(#imageLiteral(resourceName: "checkbox_default"), for: .normal)
        sex = "0"
    }
    @IBAction func MaleButton(_ sender: Any) {
    self.fermaleButton.setImage(#imageLiteral(resourceName: "checkbox_default"), for: .normal)
    self.maleButton.setImage(#imageLiteral(resourceName: "checkbox_checked"), for: .normal)
        sex = "1"
    }
    @IBAction func RegistrButton(_ sender: Any) {
        guard  nickNameField.text != "" && FirstNameField.text != "" && SecondNameField.text != "" && birthday != "" && statusField.text != ""   else {
            WhisperHelper.showErrorMurmur(title: "Заполните все поля!")
            return
        }
             self.performSegue(withIdentifier: "next", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination: CredentialsController = segue.destination as! CredentialsController
        destination.nickName = nickNameField.text!
        destination.firstName = FirstNameField.text!
        destination.secondName = SecondNameField.text!
        destination.user_status = statusField.text!
        destination.birthday = birthday
        destination.sex = sex
    }
    }

