//
//  ForgetConfirmController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/13/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit

class ForgetConfirmController: UIViewController {
    @IBOutlet weak var reset_title_lb: UILabel!
    
    @IBOutlet weak var resetMessage: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    var email = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        self.title = "Забыл Пароль"
        resetMessage.text = "Письмо со ссылкой для восстановления пароля отправлено на \(email) .Пожалуйста перейдите по ссылке для завершения процедуры."
        // Do any additional setup after loading the view.
    }
    @IBAction func doneButton(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    func configureUI() {
        setEmptyBackTitle()
        doneButton.layer.cornerRadius = 16
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
