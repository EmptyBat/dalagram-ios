//
//  ForgetPasswordController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/13/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import InputMask
import RxSwift
import RxCocoa
import DropDown

class ForgetPasswordController: UIViewController {
    @IBOutlet weak var forgot_btn: UIButton!
    @IBOutlet weak var reset_password_lb: UILabel!
    @IBOutlet weak var desc_lb: UILabel!
    var viewModel = AuthViewModel()
    @IBOutlet weak var getCodeBtn: UIButton!
    @IBOutlet weak var phoneField: UITextField!
    @IBAction func getCodeBtn(_ sender: Any) {
        if (isValidEmail(testStr:phoneField.text!)){
            if let email = phoneField.text{
        viewModel.forgetPassword(email: email, {
        self.performSegue(withIdentifier: "next", sender: nil)
        })
            }
        }
        else {
          WhisperHelper.showErrorMurmur(title: "Введите корректные E-mail адрес")
        }
    }
    @IBAction func forgotBtn(_ sender: Any) {
      navigationController?.popToRootViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Забыл Пароль"
        configureUI()
    }
    func configureUI() {
        self.forgot_btn.setTitle("remember_password".localized(), for: .normal)
        self.reset_password_lb.text = "reset_password".localized()
        self.desc_lb.text = "reset_desc".localized()
        self.title = "forgot_password_no_q".localized()
        self.getCodeBtn.setTitle("get_code".localized(), for: .normal)
        self.hideKeyboardWhenTappedAround()
        setEmptyBackTitle()
        getCodeBtn.layer.cornerRadius = 16
        phoneField.keyboardType = .emailAddress
        phoneField.returnKeyType = .done
        phoneField.becomeFirstResponder()
        
    // MARK: - DropDown action
}
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination: ForgetConfirmController = segue.destination as! ForgetConfirmController
        destination.email = phoneField.text!
    }

}
