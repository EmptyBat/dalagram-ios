//
//  NewsControllerDetail.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/4/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift
import RxSwift
import Alamofire
import AVKit
import SafariServices
import SKPhotoBrowser
import ISEmojiView
import GrowingTextView
import Photos
import Gallery
import MobileCoreServices
import YouTubePlayer
class NewsControllerDetail: UIViewController,GalleryControllerDelegate,EmojiViewDelegate,UITableViewDelegate,UITableViewDataSource,YouTubePlayerDelegate {
    
    @IBOutlet weak var keyboard_btn: UIButton!
    @IBOutlet weak var answerText: UILabel!
    @IBOutlet weak var answerView: UIView!
    var keyboardType = false
    var showComments = false
    var isGallery = false
    var getShareData = false
    var isCreateOpened = false
    var galleryCell :UITableViewCell? = nil
    var answer_id = 0
    var publication_id = 0
    var newsHistory :NewsHistory? = nil
    var newsSequance = [NewsSequence]()
    var viewModel = NewsControllerViewModel(user_id: 0)
    var images = [SKPhoto]()
    var answer_comment_id = 0
    var viewModelProfile :ProfileViewModel? = nil
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var answerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textView: GrowingTextView!
    //@IBOutlet for InputBarView
    @IBOutlet weak var attachBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       configureUI()
       GalleryConfig()
    }
    func configureUI() {
        if (getShareData == true){
            self.publication_id = self.newsHistory!.share_publication_id
            self.loadData()
        }
        else {
            self.publication_id = self.newsHistory!.publication_id
            createSequance()
        }
        // MARK: - Navigation Bar
        setEmptyBackTitle()
        setBlueNavBar()
        self.title = "post".localized()
        //MARK: - TableView Configurations
        tableView.registerNib(ContentInputTableViewCell.self)
        tableView.registerNib(NameCell.self)
        tableView.registerNib(DescriptionCell.self)
        tableView.registerNib(ImageContentCell.self)
        tableView.registerNib(FooterMenuCell.self)
        tableView.registerNib(AudioContentCell.self)
        tableView.registerNib(VideoContentCell.self)
        tableView.registerNib(LinkContentCell.self)
        tableView.registerNib(MultipleImageContentCell.self)
        tableView.registerNib(CommentImageCell.self)
        tableView.registerNib(CommentTextCell.self)
        tableView.registerNib(CommentsVideoCell.self)
        tableView.registerNib(CommentsFileCell.self)
        tableView.registerNib(CommentAudioCell.self)
        tableView.registerNib(SharedDescriptionCell.self)
        tableView.registerNib(SharedImageContentCell.self)
        tableView.registerNib(SharedAudioContentCell.self)
        tableView.registerNib(SharedVideoContentCell.self)
        tableView.registerNib(SharedLinkContentCell.self)
        tableView.registerNib(SharedMultipleImageContentCell.self)
        tableView.registerNib(SharedDocumentContentCell.self)
        
        tableView.register(UINib(nibName: "DocumentsCell", bundle: nil), forCellReuseIdentifier: "document_cell")
        tableView.separatorColor = UIColor.init(white: 0.85, alpha: 1.0)
        tableView.allowsSelection = true
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.isUserInteractionEnabled = true
        tableView.separatorColor = UIColor.clear
        answerViewHeight.constant = 0
        self.view.layoutIfNeeded()
        self.tableView.reloadData()
        self.textView.placeholder = "enter_comment".localized()
        // growing textview
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        // *** Hide keyboard when tapping outside ***
    //    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
   //     tapGesture.cancelsTouchesInView = false
     //   view.addGestureRecognizer(tapGesture)
    }
    deinit {
    NotificationCenter.default.removeObserver(self)
    }
    func GalleryConfig(){
        // Gallery Picker Configs
        Config.Grid.FrameView.borderColor = UIColor.navBlueColor
        Config.Camera.imageLimit = 1
        Config.initialTab = Config.GalleryTab.imageTab
        Config.Camera.recordLocation = false
        Config.VideoEditor.savesEditedVideoToLibrary = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (isCreateOpened){
           loadData()
            isCreateOpened = false
        }
    }
    
    @objc func loadData() {
        SVProgressHUD.show()
        viewModel.getlastNews(page: 1, publication_id: self.publication_id){[weak self] (success) in
            self?.newsHistory = success
            guard let vc = self else { return }
            //     vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            vc.newsSequance.removeAll()
            vc.viewModel.newsCommentsData.removeAll()
            vc.createSequance()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if(isGallery){
            let cell = self.galleryCell as! GalleryTableViewCell
            let controller = cell.currentViewController as! NewsController
            controller.getGallery()
        }
    }
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            textViewBottomConstraint.constant = keyboardHeight + 8
            view.layoutIfNeeded()
        }
    }
    
    @IBAction func rejectButton(_ sender: Any) {
        self.answer_comment_id = 0
        self.answerText.text = ""
        self.answerView.isHidden = true
        answerViewHeight.constant = 0
        self.view.layoutIfNeeded()
        self.textView.text = ""
        
    }
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    func getComments(){
        SVProgressHUD.show()
        viewModel.getComments(publication_id: (newsHistory?.publication_id)!) { [weak self] in
            guard let vc = self else { return }
            for index in 0..<vc.viewModel.newsCommentsData.count {
                if (vc.viewModel.newsCommentsData[index].is_has_file == 1){
                    if (vc.viewModel.newsCommentsData[index].file_list[0].file_format == "image"){
                        let news = NewsSequence()
                        news.index = index
                        news.newsType = .commentImage
                        news.fileIndex = 0
                        self!.newsSequance.append(news)
                    }
                else if (vc.viewModel.newsCommentsData[index].file_list[0].file_format == "video"){
                        let news = NewsSequence()
                        news.index = index
                        news.newsType = .commentVideo
                        news.fileIndex = 0
                        self!.newsSequance.append(news)
                    }
                    else if (vc.viewModel.newsCommentsData[index].file_list[0].file_format == "file"){
                        let news = NewsSequence()
                        news.index = index
                        news.newsType = .commentFile
                        news.fileIndex = 0
                        self!.newsSequance.append(news)
                    }
                    else if (vc.viewModel.newsCommentsData[index].file_list[0].file_format == "audio"){
                        let news = NewsSequence()
                        news.index = index
                        news.newsType = .commentAudio
                        news.fileIndex = 0
                        self!.newsSequance.append(news)
                    }
                    
                    
                }
                else {
                    let news = NewsSequence()
                    news.index = index
                    news.newsType = .commentText
                    self!.newsSequance.append(news)
                }
            
            }
        SVProgressHUD.dismiss()
        self?.tableView.reloadData()
            if self!.showComments {
                self!.scrollToBottom()
                self!.textView.becomeFirstResponder()
            }
    }
    }
    func getLastComment(){
        SVProgressHUD.show()
        viewModel.getComments(publication_id: (newsHistory?.publication_id)!) { [weak self] in
            guard let vc = self else { return }
            if (vc.viewModel.newsCommentsData.last?.is_has_file == 1){
                if (vc.viewModel.newsCommentsData.last!.file_list[0].file_format == "image"){
                        let news = NewsSequence()
                        news.index = vc.viewModel.newsCommentsData.count - 1
                        news.newsType = .commentImage
                        news.fileIndex = 0
                        self!.newsSequance.append(news)
                    }
                    else if (vc.viewModel.newsCommentsData.last!.file_list[0].file_format == "video"){
                        let news = NewsSequence()
                        news.index = vc.viewModel.newsCommentsData.count - 1
                        news.newsType = .commentVideo
                        news.fileIndex = 0
                        self!.newsSequance.append(news)
                    }
                    else if (vc.viewModel.newsCommentsData.last!.file_list[0].file_format == "file"){
                        let news = NewsSequence()
                          news.index = vc.viewModel.newsCommentsData.count - 1
                        news.newsType = .commentFile
                        news.fileIndex = 0
                        self!.newsSequance.append(news)
                    }
                    else if (vc.viewModel.newsCommentsData.last!.file_list[0].file_format == "audio"){
                        let news = NewsSequence()
                          news.index = vc.viewModel.newsCommentsData.count - 1
                        news.newsType = .commentAudio
                        news.fileIndex = 0
                        self!.newsSequance.append(news)
                    }
                    
                    
                }
                else {
                    let news = NewsSequence()
                    news.index = vc.viewModel.newsCommentsData.count - 1
                    news.newsType = .commentText
                    self!.newsSequance.append(news)
                }
                
            
            SVProgressHUD.dismiss()
            self!.tableView.reloadData()
            self!.scrollToBottom()
        }
    }
    func scrollToBottom(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            let indexPath = IndexPath(row: self.newsSequance.count-1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    func createSequance(){
            let index1 = 0
            let news = NewsSequence()
            news.index = index1
            news.newsType = .header
            self.newsSequance.append(news)
            if (!newsHistory!.publication_desc.isEmpty){
                let news = NewsSequence()
                news.index = index1
                news.newsType = .text
                self.newsSequance.append(news)
            }
        if (newsHistory!.is_has_link == 1){
                
                let news = NewsSequence()
                news.index = index1
                news.newsType = .link
                self.newsSequance.append(news)
            }
        if (newsHistory!.is_share == 1){
            var first = true
            if (newsHistory!.file_list.count == 0){
                if (!newsHistory!.share_publication_desc.isEmpty){
                    let news = NewsSequence()
                    news.index = index1
                    news.newsType = .shareDesk
                    self.newsSequance.append(news)
                }
            }
            if (newsHistory!.share_is_has_link == 1){
                
                let news = NewsSequence()
                news.index = index1
                news.newsType = .shareLink
                self.newsSequance.append(news)
            }
            var isMultiple = 0
            for index3 in 0..<self.newsHistory!.file_list.count {
                let file_format = self.newsHistory!.file_list[index3].file_format
                if (file_format == "image") {
                    isMultiple += 1
                }
            }
            var multiplesIndex = [Int]()
            for index2 in 0..<newsHistory!.file_list.count {
                let file_format = newsHistory!.file_list[index2].file_format
                if (file_format == "audio"){
                    let news = NewsSequence()
                    news.index = index1
                    if (first == true){
                        news.newsType = .shareAudio
                        first = false
                    }
                    else {
                        news.newsType = .audio
                    }
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
                else if (file_format == "image") {
                    if (isMultiple > 1){
                        print("asfsafsfafs",isMultiple , index2,index1)
                        if ((isMultiple - 1) == index2){
                            multiplesIndex.append(index2)
                            let photo = SKPhoto.photoWithImageURL(newsHistory!.file_list[index2].file_url.encodeUrl()!)
                            photo.shouldCachePhotoURLImage = true
                            images.append(photo)
                            let news = NewsSequence()
                            news.index = index1
                            if (first == true){
                                news.newsType = .shareMultipleImage
                                first = false
                            }
                            else {
                                news.newsType = .multiple
                            }
                            news.fileIndex = index2
                            news.multipleIndexs = multiplesIndex
                            self.newsSequance.append(news)
                            print("afsfsafsafa",multiplesIndex)
                        }
                        else {
                            let photo = SKPhoto.photoWithImageURL(newsHistory!.file_list[index2].file_url.encodeUrl()!)
                            photo.shouldCachePhotoURLImage = true
                            images.append(photo)
                            multiplesIndex.append(index2)
                            
                        }
                    }
                    else {
                        let news = NewsSequence()
                        news.index = index1
                        if (first == true){
                            news.newsType = .shareImage
                            first = false
                        }
                        else {
                            news.newsType = .image
                        }
                        let photo = SKPhoto.photoWithImageURL(newsHistory!.file_list[index2].file_url.encodeUrl()!)
                        photo.shouldCachePhotoURLImage = true
                        images.append(photo)
                        news.fileIndex = index2
                        self.newsSequance.append(news)
                    }
                    }
                else if (file_format == "video") {
                    let news = NewsSequence()
                    news.index = index1
                    if (first == true){
                        news.newsType = .shareVideo
                        first = false
                    }
                    else {
                        news.newsType = .video
                    }
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
                else if (file_format == "file") {
                    let news = NewsSequence()
                    news.index = index1
                    if (first == true){
                        news.newsType = .shareFile
                        first = false
                    }
                    else {
                        news.newsType = .file
                    }
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
            }
        }
            
        else {
                    var isMultiple = 0
            for index3 in 0..<self.newsHistory!.file_list.count {
                let file_format = self.newsHistory!.file_list[index3].file_format
                if (file_format == "image") {
                    isMultiple += 1
                }
            }
                var multiplesIndex = [Int]()
            for index2 in 0..<newsHistory!.file_list.count {
                let file_format = self.newsHistory!.file_list[index2].file_format
                if (file_format == "audio"){
                    let news = NewsSequence()
                    news.index = index1
                    news.newsType = .audio
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
                else if (file_format == "image") {
                    if (isMultiple > 1){
                        if ((isMultiple - 1) == index2){
                            let photo = SKPhoto.photoWithImageURL(newsHistory!.file_list[index2].file_url.encodeUrl()!)
                            photo.shouldCachePhotoURLImage = true
                            images.append(photo)
                            multiplesIndex.append(index2)
                            let news = NewsSequence()
                            news.index = index1
                            news.newsType = .multiple
                            news.fileIndex = index2
                            news.multipleIndexs = multiplesIndex
                            self.newsSequance.append(news)
                            print("afsfsafsafa",multiplesIndex)
                        }
                        else {
                            let photo = SKPhoto.photoWithImageURL(newsHistory!.file_list[index2].file_url.encodeUrl()!)
                            photo.shouldCachePhotoURLImage = true
                            images.append(photo)
                            multiplesIndex.append(index2)
                            
                        }
                    }
                    else {
                        let photo = SKPhoto.photoWithImageURL(newsHistory!.file_list[index2].file_url.encodeUrl()!)
                        photo.shouldCachePhotoURLImage = true
                        images.append(photo)
                        let news = NewsSequence()
                        news.index = index1
                        news.newsType = .image
                        news.fileIndex = index2
                        self.newsSequance.append(news)
                    }
                }
                else if (file_format == "video") {
                    let news = NewsSequence()
                    news.index = index1
                    news.newsType = .video
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
                else if (file_format == "file") {
                    let news = NewsSequence()
                    news.index = index1
                    news.newsType = .file
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
            }
        }
            let newsFooter = NewsSequence()
            newsFooter.index = index1
            newsFooter.newsType = .footer
            self.newsSequance.append(newsFooter)
        
        self.tableView.reloadData()
        self.getComments()
    }
    // MARK: - Table view data source
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         return nil
    
    }
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsSequance.count
    }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var item = self.newsHistory!
        if (newsSequance[indexPath.row].newsType == .header){
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell", for: indexPath) as! NameCell
            if (item.access_id == 1){
                cell.public_width.constant = 19
                cell.publicIV.isHidden = false
            }
            else {
                cell.public_width.constant = 0
                cell.publicIV.isHidden = true
            }
            cell.layoutIfNeeded()
            cell.Name.text = item.nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClicked))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
            cell.actionButton.removeTarget(self,  action:#selector(pickerAction), for: .touchUpInside)
            if (viewModelProfile?.user_id.value == item.user_id){
                cell.actionButton.addTarget(self, action:#selector(pickerAction), for: .touchUpInside)
                cell.actionButton.tag = indexPath.row
                cell.actionButton.isHidden = false
            }
            else {
                cell.actionButton.addTarget(self, action:#selector(pickerAlienAction), for: .touchUpInside)
                cell.actionButton.tag = indexPath.row
                cell.actionButton.isHidden = false
            }
            // Configure the cell...
            
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .text){
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell", for: indexPath) as! DescriptionCell
            if (self.newsHistory!.is_has_link == 1){
                print("DescriptionCell",self.newsHistory!.publication_desc)
                cell.descriptionLB.attributedText = self.newsHistory!.publication_desc.attributedHtmlString
                
            }
            else {
                cell.descriptionLB.text = self.newsHistory!.publication_desc
            }
            
            // Configure the cell...
            
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .image){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageContentCell", for: indexPath) as! ImageContentCell
            
            cell.ImageContent.kf.setImage(with: URL(string: item.file_list[newsSequance[indexPath.row].fileIndex].file_url.encodeUrl()!), placeholder: nil)
            
            if let imageSource = CGImageSourceCreateWithURL(URL(string: item.file_list[newsSequance[indexPath.row].fileIndex].file_url.encodeUrl()!)! as CFURL, nil) {
                if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                    let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                    let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                    
                    let myViewWidth = self.tableView.frame.size.width
                    
                    let ratio = Double(myViewWidth)/Double(myImageWidth)
                    let scaledHeight = Double(myImageHeight) * ratio
                    
                    cell.imageHeight.constant = CGFloat(scaledHeight)
                    cell.layoutIfNeeded()
                    
                }
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .footer){
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterMenuCell", for: indexPath) as! FooterMenuCell
            if ( self.newsHistory!.view_count != 0) {
                cell.visible_btn.setTitle("\(self.newsHistory!.view_count)", for: .normal)
            }
            if (self.newsHistory!.view_count != 0) {
                cell.visible_btn.setTitle("\(self.newsHistory!.view_count)", for: .normal)
            }
            else {
                cell.visible_btn.setTitle("", for: .normal)
            }
            if (self.newsHistory!.comment_count != 0){
                cell.comments_btn.setTitle("\(self.newsHistory!.comment_count)", for: .normal)
                
            }
            else {

                cell.comments_btn.setTitle("", for: .normal)
            }
            cell.view_height.constant = 0
            cell.comments_btn.tag = indexPath.row
            if (self.newsHistory!.share_count != 0){
                cell.share_btn.setTitle("\(self.newsHistory!.share_count)", for: .normal)
                
            }
            else {
                cell.share_btn.setTitle("", for: .normal)
            }
            
            if (self.newsHistory!.like_count != 0){
                cell.like_btn.setTitle("\(self.newsHistory!.like_count)", for: .normal)
                
            }
            else {
                cell.like_btn.setTitle("", for: .normal)
            }
            if (self.newsHistory!.is_i_liked == 1){
                cell.like_btn.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.like_btn.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.like_btn.addTarget(self, action:#selector(likeACtion), for: .touchUpInside)

            cell.like_btn.tag = indexPath.row
            
            cell.share_btn.addTarget(self, action:#selector(shareAction), for: .touchUpInside)
            
            cell.share_btn.tag = indexPath.row
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .audio){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AudioContentCell", for: indexPath) as! AudioContentCell
            
            cell.titleLb.text = item.file_list[newsSequance[indexPath.row].fileIndex].file_name
            let temp:Int = Int(item.file_list[newsSequance[indexPath.row].fileIndex].file_time)!
            let mins:Int = temp / 60
            let secs:Int = temp % 60
            if (mins >= 10){
                if (secs >= 10){
                    cell.dateLb.text = "\(mins):\(secs)"
                }
                else {
                    cell.dateLb.text = "\(mins):0\(secs)"
                }
            }
            else {
                if (secs >= 10){
                    cell.dateLb.text = "0\(mins):\(secs)"
                }
                else {
                    cell.dateLb.text = "0\(mins):0\(secs)"
                }
            }
            if let i = self.viewModel.realmDialog?.index(where: { $0.publication_id == item.publication_id }) {
                item = self.viewModel.realmDialog![i]
            }
            if item.file_list[newsSequance[indexPath.row].fileIndex].file_url.contains("https"){
                let origImage = UIImage(named: "icon_mini_download_green")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
            else {
                if (cell.audioPlayer != nil){
                    if (cell.audioPlayer.isPlaying){
                        let origImage = UIImage(named: "icon_mini_pause")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                    else {
                        let origImage = UIImage(named: "icon_mini_play")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                }
                else {
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(playAction), for: .touchUpInside)
            // Configure the cell...
            
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .file){
            let origImage = UIImage(named: "icon_file_default")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            let cell = tableView.dequeueReusableCell(withIdentifier: "document_cell", for: indexPath) as! DocumentsCell
            cell.file_image.image = tintedImage
            cell.file_image.tintColor = UIColor.random
            cell.file_extension_lb.text =  item.file_list[newsSequance[indexPath.row].fileIndex].file_name.fileExtension()
            cell.title_lb.text = item.file_list[newsSequance[indexPath.row].fileIndex].file_name
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .video){
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoContentCell", for: indexPath) as! VideoContentCell
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(playVideo), for: .touchUpInside)
            let asset = AVAsset(url:URL(string: item.file_list[self.newsSequance[indexPath.row].fileIndex].file_url.encodeUrl()!)!)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            let time = CMTimeMake(1, 60)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {
                    cell.previewImage.image = frameImg
                })
                
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .link){
            let cell = tableView.dequeueReusableCell(withIdentifier: "LinkContentCell", for: indexPath) as! LinkContentCell
            cell.titleLb.text = item.meta_tagsTitle
            cell.descriptionLb.text = item.meta_tagsDescription
            let removeSymbols =  item.meta_tagsImage.replacingOccurrences(of: "\"", with: "")
              cell.linkImage.kf.setImage(with: URL(string: removeSymbols.encodeUrl()!), placeholder: nil)
            if (!item.meta_tagsImage.isEmpty){
            if let imageSource = CGImageSourceCreateWithURL(URL(string: removeSymbols.encodeUrl()!)! as CFURL, nil) {
                if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                    let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                    let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                    
                    let myViewWidth = self.tableView.frame.size.width
                    
                    let ratio = Double(myViewWidth)/Double(myImageWidth)
                    let scaledHeight = Double(myImageHeight) * ratio
                    
                    cell.imageHeight.constant = CGFloat(scaledHeight)
                    cell.layoutIfNeeded()
                    
                }
                
            }
            }
            else {
                cell.imageHeight.constant = 0
                cell.layoutIfNeeded()
            }
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: item.publication_desc, options: [], range: NSRange(location: 0, length: item.publication_desc.utf16.count))
                 cell.playButton.isHidden = true
            for match in matches {
                guard let range = Range(match.range, in: item.publication_desc) else { continue }
                let url = item.publication_desc[range]
                guard let urlOpen = URL(string: String(url)) else { return  cell}
                if (url.contains("youtu")){
                    print("ararraar ",url)
                    cell.youtubePlayer.isHidden  = false
                    cell.youtubePlayer.delegate = self;
                    cell.youtubePlayer.loadVideoURL(urlOpen)
                    cell.youtubePlayer.play()
                    break
                }
                else {
                    cell.youtubePlayer.isHidden  = true
                }
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .multiple){
            let item = self.newsHistory!
            let image1 = newsSequance[indexPath.row ].multipleIndexs[0]
            let image2 = newsSequance[indexPath.row].multipleIndexs[1]
            let cell = tableView.dequeueReusableCell(withIdentifier: "MultipleImageContentCell", for: indexPath) as! MultipleImageContentCell
            cell.image1.kf.setImage(with: URL(string: item.file_list[image1].file_url.encodeUrl()!), placeholder: nil)
            cell.image2.kf.setImage(with: URL(string: item.file_list[image2].file_url.encodeUrl()!), placeholder: nil)
            if (newsSequance[indexPath.row].multipleIndexs.count > 2){
                cell.moreLabel.text = "+\(newsSequance[indexPath.row].multipleIndexs.count - 2)"
                
            }
            else {
                cell.moreBackground.isHidden = true
            }
            
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .commentText){
          let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTextCell", for: indexPath) as! CommentTextCell
            let comment = self.viewModel.newsCommentsData[newsSequance[indexPath.row].index]
            cell.Name.text = comment.nickname
            cell.date.text = comment.comment_date
            cell.descriptionLB.text = ("  " + comment.comment_text)
            cell.descriptionLB.layer.cornerRadius = 8
            cell.descriptionLB.layer.masksToBounds = true
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            let word = (" " + comment.comment_text)
            
            let needle: Character = ","
            if let idx = word.characters.index(of: needle) {
                let pos = word.characters.distance(from: word.startIndex, to: idx)
                var myStringArr = word.components(separatedBy: ",")
                
                let attrs2 = [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedStringKey.foregroundColor :UIColor.black]
                
                let myAttribute = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13)]
                
                let finalMutableString = NSMutableAttributedString()
                
                let attributedString = NSMutableAttributedString(string: myStringArr[0] + ",", attributes:attrs2)
                
                let attributedString1 = NSMutableAttributedString(string:myStringArr[1], attributes:myAttribute)
                
                finalMutableString.append(attributedString)
                finalMutableString.append(attributedString1)
                
                
                
                cell.descriptionLB.attributedText = finalMutableString
            }
    
            if item.avatar.isEmpty {
                if let firstLetter = comment.nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            if  (comment.is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            cell.likeButton.setTitle("\(comment.like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(answerCommentAction), for: .touchUpInside)
            cell.answerButton.tag = indexPath.row
            if (comment.parent_id != 0){
               cell.avatarLeadingSpace.constant = 30
            }
            else {
                cell.avatarLeadingSpace.constant = 0
            }

            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .commentImage){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentImageCell", for: indexPath) as! CommentImageCell
            let comment = self.viewModel.newsCommentsData[newsSequance[indexPath.row].index]
            cell.Name.text = comment.nickname
            cell.date.text = comment.comment_date
            if item.avatar.isEmpty {
                if let firstLetter = comment.nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommentImageAction))
            cell.imageContent.isUserInteractionEnabled = true
            cell.imageContent.addGestureRecognizer(tapGestureRecognizer)
            cell.imageContent.tag = indexPath.row
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            //image
            cell.imageContent.kf.setImage(with: URL(string: comment.file_list[0].file_url.encodeUrl()!), placeholder: nil)
            if let imageSource = CGImageSourceCreateWithURL(URL(string: comment.file_list[0].file_url.encodeUrl()!)! as CFURL, nil) {
                if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                    let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                    let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                    
                    let myViewWidth = cell.imageContent.frame.width
                    
                    let ratio = Double(myViewWidth)/Double(myImageWidth)
                    let scaledHeight = Double(myImageHeight) * ratio
                    
                    cell.imageHeight.constant = CGFloat(scaledHeight)
                    cell.layoutIfNeeded()
                    
                }
            }
            if  (comment.is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(answerCommentAction), for: .touchUpInside)
            cell.answerButton.tag = indexPath.row
            if (comment.parent_id != 0){
                cell.avatarLeadingSpace.constant = 30
            }
            else {
                cell.avatarLeadingSpace.constant = 0
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .commentVideo){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsVideoCell", for: indexPath) as! CommentsVideoCell
            let comment = self.viewModel.newsCommentsData[newsSequance[indexPath.row].index]
            cell.Name.text = comment.nickname
            cell.date.text = comment.comment_date
            if item.avatar.isEmpty {
                if let firstLetter = comment.nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(CommentVideoAction), for: .touchUpInside)
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            let asset = AVAsset(url:URL(string: comment.file_list[0].file_url.encodeUrl()!)!)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            let time = CMTimeMake(1, 60)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {
                    cell.imageContent.image = frameImg
                })
                
            }
            if  (comment.is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(answerCommentAction), for: .touchUpInside)
            cell.answerButton.tag = indexPath.row
            if (comment.parent_id != 0){
                cell.avatarLeadingSpace.constant = 30
            }
            else {
                cell.avatarLeadingSpace.constant = 0
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .commentFile){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsFileCell", for: indexPath) as! CommentsFileCell
            let comment = self.viewModel.newsCommentsData[newsSequance[indexPath.row].index]
            cell.file_extension_lb.text = comment.file_list[0].file_name.fileExtension()
            cell.title_lb.text = comment.file_list[0].file_name
            cell.Name.text = comment.nickname
            cell.date.text = comment.comment_date
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            if item.avatar.isEmpty {
                if let firstLetter = comment.nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            if  (comment.is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(answerCommentAction), for: .touchUpInside)
            cell.answerButton.tag = indexPath.row
            if (comment.parent_id != 0){
                cell.avatarLeadingSpace.constant = 30
            }
            else {
                cell.avatarLeadingSpace.constant = 0
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .commentAudio){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentAudioCell", for: indexPath) as! CommentAudioCell
            let comment = self.viewModel.newsCommentsData[newsSequance[indexPath.row].index]
            cell.titleLB.text = comment.file_list[0].file_name
            let temp:Int = Int(comment.file_list[0].file_time)!
            let mins:Int = temp / 60
            let secs:Int = temp % 60
            if (mins >= 10){
                if (secs >= 10){
                    cell.durationLB.text = "\(mins):\(secs)"
                }
                else {
                    cell.durationLB.text = "\(mins):0\(secs)"
                }
            }
            else {
                if (secs >= 10){
                    cell.durationLB.text = "0\(mins):\(secs)"
                }
                else {
                    cell.durationLB.text = "0\(mins):0\(secs)"
                }
            }
            if comment.file_list[0].file_url.contains("https"){
                let origImage = UIImage(named: "icon_mini_download_green")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
            else {
                if (cell.audioPlayer != nil){
                    if (cell.audioPlayer.isPlaying){
                        let origImage = UIImage(named: "icon_mini_pause")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                    else {
                        let origImage = UIImage(named: "icon_mini_play")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                }
                else {
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(playAudioCommentAction), for: .touchUpInside)
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            cell.Name.text = comment.nickname
            cell.date.text = comment.comment_date
            if item.avatar.isEmpty {
                if let firstLetter = comment.nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            if  (comment.is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(answerCommentAction), for: .touchUpInside)
            cell.answerButton.tag = indexPath.row
            if (comment.parent_id != 0){
                cell.avatarLeadingSpace.constant = 30
            }
            else {
                cell.avatarLeadingSpace.constant = 0
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .shareDesk){
            let item = self.newsHistory!
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedDescriptionCell", for: indexPath) as! SharedDescriptionCell
            if (self.newsHistory!.share_is_has_link == 1){
                         print("SharedDescriptionCell",self.newsHistory!.share_publication_desc)
                cell.descriptionLB.attributedText = self.newsHistory!.share_publication_desc.attributedHtmlString
                
                
            }
            else {
                    cell.descriptionLB.text = self.newsHistory!.publication_desc
            }
            cell.descriptionLB.text = item.share_publication_desc
            cell.date.text = item.share_publication_date
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
            // Configure the cell...
            
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .shareImage){
            let item = self.newsHistory!
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedImageContentCell", for: indexPath) as! SharedImageContentCell
            
            cell.ImageContent.kf.setImage(with: URL(string: item.file_list[newsSequance[indexPath.row].fileIndex].file_url.encodeUrl()!), placeholder: nil)
            cell.descriptionLB.text = item.share_publication_desc
            cell.date.text = item.share_publication_date
            if let imageSource = CGImageSourceCreateWithURL(URL(string: item.file_list[newsSequance[indexPath.row].fileIndex].file_url.encodeUrl()!)! as CFURL, nil) {
                if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                    let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                    let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                    
                    let myViewWidth = self.tableView.frame.size.width
                    
                    let ratio = Double(myViewWidth)/Double(myImageWidth)
                    let scaledHeight = Double(myImageHeight) * ratio
                    
                    cell.imageHeight.constant = CGFloat(scaledHeight - 16)
                    cell.layoutIfNeeded()
                    
                }
            }
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .shareAudio){
            var item = self.newsHistory!
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedAudioContentCell", for: indexPath) as! SharedAudioContentCell
            
            cell.titleLb.text = item.file_list[newsSequance[indexPath.row].fileIndex].file_name
            cell.descriptionLB.text = item.share_publication_desc
            cell.date.text = item.share_publication_date
            let temp:Int = Int(item.file_list[newsSequance[indexPath.row].fileIndex].file_time)!
            let mins:Int = temp / 60
            let secs:Int = temp % 60
            if (mins >= 10){
                if (secs >= 10){
                    cell.dateLb.text = "\(mins):\(secs)"
                }
                else {
                    cell.dateLb.text = "\(mins):0\(secs)"
                }
            }
            else {
                if (secs >= 10){
                    cell.dateLb.text = "0\(mins):\(secs)"
                }
                else {
                    cell.dateLb.text = "0\(mins):0\(secs)"
                }
            }
            if let i = self.viewModel.realmDialog?.index(where: { $0.publication_id == item.publication_id }) {
                item = self.viewModel.realmDialog![i]
            }
            if item.file_list[newsSequance[indexPath.row].fileIndex].file_url.contains("https"){
                let origImage = UIImage(named: "icon_mini_download_green")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
            else {
                if (cell.audioPlayer != nil){
                    if (cell.audioPlayer.isPlaying){
                        let origImage = UIImage(named: "icon_mini_pause")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                    else {
                        let origImage = UIImage(named: "icon_mini_play")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                }
                else {
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(playAction), for: .touchUpInside)
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
            // Configure the cell...
            
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .shareFile){
            let item = self.newsHistory!
            let origImage = UIImage(named: "icon_file_default")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedDocumentContentCell", for: indexPath) as! SharedDocumentContentCell
            cell.descriptionLB.text = item.share_publication_desc
            cell.date.text = item.share_publication_date
            cell.file_image.image = tintedImage
            cell.file_image.tintColor = UIColor.random
            cell.file_extension_lb.text =  item.file_list[newsSequance[indexPath.row].fileIndex].file_name.fileExtension()
            cell.title_lb.text = item.file_list[newsSequance[indexPath.row].fileIndex].file_name
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .shareVideo){
            let item = self.newsHistory!
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedVideoContentCell", for: indexPath) as! SharedVideoContentCell
            cell.descriptionLB.text = item.share_publication_desc
            cell.date.text = item.share_publication_date
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(playVideo), for: .touchUpInside)
            let asset = AVAsset(url:URL(string: item.file_list[self.newsSequance[indexPath.row].fileIndex].file_url.encodeUrl()!)!)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            let time = CMTimeMake(1, 60)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {
                    cell.previewImage.image = frameImg
                })
                
            }
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .shareLink){
            let item = self.newsHistory!
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedLinkContentCell", for: indexPath) as! SharedLinkContentCell
            cell.descriptionLB.text = item.share_publication_desc
            cell.date.text = item.share_publication_date
            cell.titleLb.text = item.share_meta_tagsTitle
            cell.descriptionLb.text = item.share_meta_tagsDescription
            let removeSymbols =  item.share_meta_tagsImage.replacingOccurrences(of: "\"", with: "")
            cell.linkImage.kf.setImage(with: URL(string: removeSymbols.encodeUrl()!), placeholder: nil)
            if (!item.meta_tagsImage.isEmpty){
                if let imageSource = CGImageSourceCreateWithURL(URL(string: removeSymbols.encodeUrl()!)! as CFURL, nil) {
                    if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                        let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                        let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                        
                        let myViewWidth = self.tableView.frame.size.width
                        
                        let ratio = Double(myViewWidth)/Double(myImageWidth)
                        let scaledHeight = Double(myImageHeight) * ratio
                        
                        cell.imageHeight.constant = CGFloat(scaledHeight)
                        cell.layoutIfNeeded()
                    }
                }
            }
            else {
                cell.imageHeight.constant = 0
                cell.layoutIfNeeded()
            }
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
                        cell.playButton.isHidden = true
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: item.share_publication_desc, options: [], range: NSRange(location: 0, length: item.publication_desc.utf16.count))
            
            for match in matches {
                guard let range = Range(match.range, in: item.share_publication_desc) else { continue }
                let url = item.share_publication_desc[range]
                guard let urlOpen = URL(string: String(url)) else { return  cell}
                if (url.contains("youtu")){
                    cell.youtubePlayer.isHidden  = false
                    cell.youtubePlayer.delegate = self;
                    cell.youtubePlayer.loadVideoURL(urlOpen)
                    cell.youtubePlayer.play()
                    break
                }
                else {
                    cell.youtubePlayer.isHidden  = true
                }
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .shareMultipleImage){
            let item = self.newsHistory!
            let image1 = newsSequance[indexPath.row].multipleIndexs[0]
            let image2 = newsSequance[indexPath.row].multipleIndexs[1]
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedMultipleImageContentCell", for: indexPath) as! SharedMultipleImageContentCell
            let descStr = item.share_publication_desc
            
            if (descStr.count > 150){
                    let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                
                    let range = (first150 as NSString).range(of: "next".localized())
                    
                let attribute = NSMutableAttributedString.init(string: first150)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)
                
                cell.descriptionLB.attributedText = attribute
            }
            else {
                cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row].index].share_publication_desc
            }
            cell.date.text = item.share_publication_date
            cell.image1.kf.setImage(with: URL(string: item.file_list[image1].file_url.encodeUrl()!), placeholder: nil)
            cell.image2.kf.setImage(with: URL(string: item.file_list[image2].file_url.encodeUrl()!), placeholder: nil)
            if (newsSequance[indexPath.row ].multipleIndexs.count > 2){
                cell.moreLabel.text = "+\(newsSequance[indexPath.row ].multipleIndexs.count - 2)"
                
            }
            else {
                cell.moreBackground.isHidden = true
            }
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterMenuCell", for: indexPath)
            
            // Configure the cell...
            
            return cell
        }
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.newsHistory!
        self.tableView.deselectRow(at: indexPath, animated: true)
        if (newsSequance[indexPath.row].newsType == .header){
            
        }
        else if (newsSequance[indexPath.row].newsType == .text){
            if (item.is_has_link == 1){
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                let matches = detector.matches(in: item.publication_desc, options: [], range: NSRange(location: 0, length: item.publication_desc.utf16.count))
                
                for match in matches {
                    guard let range = Range(match.range, in: item.publication_desc) else { continue }
                    let url = item.publication_desc[range]
                    guard let urlOpen = URL(string: String(url)) else { return }
                    UIApplication.shared.open(urlOpen)
                }
            }
        }
        else if (newsSequance[indexPath.row].newsType == .image){
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(newsSequance[indexPath.row].fileIndex)
              SKPhotoBrowserOptions.displayGallery = false
            self.present(browser, animated: true, completion: nil)
        }
        else if (newsSequance[indexPath.row].newsType == .multiple || newsSequance[indexPath.row].newsType == .shareMultipleImage){
            let browser = SKPhotoBrowser(photos: images)
             SKPhotoBrowserOptions.displayGallery = false
            browser.initializePageIndex(newsSequance[indexPath.row].index)
            self.present(browser, animated: true, completion: nil)
        }
        else if (newsSequance[indexPath.row].newsType == .footer){
            
        }
        else if (newsSequance[indexPath.row].newsType == .audio){
            
        }
        else if (newsSequance[indexPath.row].newsType == .file){
            let vc = SFSafariViewController(url: URL(string:item.file_list[newsSequance[indexPath.row].fileIndex].file_url.encodeUrl()!)!)
            self.present(vc, animated: true, completion: nil)
            
        }
        else if (newsSequance[indexPath.row].newsType == .link){
            let removeSymbols =  item.meta_tagsImage.replacingOccurrences(of: "\"", with: "")
            let photo = SKPhoto.photoWithImageURL(removeSymbols.encodeUrl()!)
            let browser = SKPhotoBrowser(photos: [photo])
              SKPhotoBrowserOptions.displayGallery = false
            browser.initializePageIndex(0)
            self.present(browser, animated: true, completion: nil)
        }
        else if (newsSequance[indexPath.row].newsType == .commentFile){
            let comment = self.viewModel.newsCommentsData[newsSequance[indexPath.row].index]
            let vc = SFSafariViewController(url: URL(string:comment.file_list[0].file_url.encodeUrl()!)!)
            self.present(vc, animated: true, completion: nil)
        }
        else if (newsSequance[indexPath.row].newsType == .commentText || newsSequance[indexPath.row].newsType == .commentImage || newsSequance[indexPath.row].newsType == .commentVideo || newsSequance[indexPath.row].newsType == .commentAudio){
             let comment = self.viewModel.newsCommentsData[newsSequance[indexPath.row].index]
            let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
            let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
                
                UIPasteboard.general.string =  comment.comment_text
                
            })
            let changeAction = UIAlertAction(title: "delete_comment".localized(), style: .default, handler: { _ in
                
                self.viewModel.removeComment(comment_id: comment.comment_id) { [weak self] in
                    self?.viewModel.newsCommentsData.filter { $0.comment_id != comment.comment_id }
                    self?.newsSequance.remove(at: indexPath.row)
                    self?.tableView.reloadData()
                }
                
                
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            alert.addAction(changeAction)
            alert.addAction(copyAction)
            alert.addAction(cancelAction)
            alert.view.tintColor = UIColor.navBlueColor
            self.present(alert, animated: true, completion: nil)
        }
    }
    @objc func avatarClickedShare(_ sender: UITapGestureRecognizer) {
        let item = self.newsHistory!
        let vc = NewsController()
        vc.author_id = item.share_user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func avatarClicked(_ sender: UITapGestureRecognizer) {
        let item = self.newsHistory!
        let vc = NewsController()
        vc.author_id = item.user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func playAudioCommentAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = self.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentAudioCell
         let comment = self.viewModel.newsCommentsData[newsSequance[myIndexPath.row].index]
        if comment.file_list[0].file_url.contains("https"){
            cell.loader.startAnimating()
            cell.playButton.isHidden = true
            downloadAudio(url: comment.file_list[0].file_url, comment_id: comment.comment_id,myIndexPath:myIndexPath,requestSuccess: { [weak self] () in
                guard let vc = self else { return }
                let origImage = UIImage(named: "icon_mini_play")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                cell.loader.stopAnimating()
                cell.playButton.isHidden = false
            })
            
        }
        else {
            if (cell.audioPlayer != nil){
                if (cell.audioPlayer.isPlaying){
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    cell.audioPlayer.pause()
                }
                else {
                    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                        let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(comment.file_list[0].file_url).csv")
                        cell.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                        cell.audioPlayer.prepareToPlay()
                        cell.audioPlayer.delegate = cell
                        cell.audioPlayer.play()
                        let origImage = UIImage(named: "icon_mini_pause")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                }
            }
            else {
                
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(comment.file_list[0].file_url).csv")
                    print("zxzxxzxx",audioURL,comment.file_list[0].file_url)
                    cell.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                    cell.audioPlayer.prepareToPlay()
                    cell.audioPlayer.delegate = cell
                    cell.audioPlayer.play()
                    let origImage = UIImage(named: "icon_mini_pause")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
        }
    
    }
    @objc func answerCommentAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let comment = self.viewModel.newsCommentsData[newsSequance[myIndexPath.row].index]
        self.textView.text = "\(comment.nickname),"
        answer_comment_id = comment.comment_id
        answerView.isHidden = false
        answerViewHeight.constant = 35
        self.view.layoutIfNeeded()
        let attrs2 = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16), NSAttributedStringKey.foregroundColor : UIColor(red: 0/255, green: 147/255, blue: 236/255, alpha: 1.0)]
        
        let attributedString1 = NSMutableAttributedString(string:("reply".localized() + ": \(comment.nickname)"), attributes:attrs2)
        
        attributedString1.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: NSRange(location:0,length:8))
        // set label Attribute
     //   labName.attributedText = myMutableString
        answerText.attributedText = attributedString1
        
        
        self.textView.becomeFirstResponder()
        
    }
    @objc func shareAction(_ sender: UIButton) {
        let item = newsHistory
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "SHARECONTROLLER") as! ShareViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.publication_id = item!.publication_id
        self.present(myAlert, animated: true, completion: nil)
    }
    @objc func likeComment(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let comment = self.viewModel.newsCommentsData[newsSequance[myIndexPath.row].index]
        let type = newsSequance[myIndexPath.row].newsType
        viewModel.likeComment(comment_id:comment.comment_id) { [weak self] in
            guard let vc = self else { return }
            var cell = self!.tableView.cellForRow(at: myIndexPath as IndexPath)
            
            switch type {
            case .commentText:
               let cell = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentTextCell
                
                if (comment.is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count - 1)", for: .normal)
                }
            case .commentFile:
                  let cell  = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentsFileCell
                if (comment.is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count - 1)", for: .normal)
                }
            case .commentAudio:
                  let cell  = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentAudioCell
                if (comment.is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count - 1)", for: .normal)
                }
            case .commentImage:
                  let cell  = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentImageCell
                if (comment.is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count - 1)", for: .normal)
                }
            case .commentVideo:
                  let cell  = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentsVideoCell
                if (comment.is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.like_count - 1)", for: .normal)
                }
            default:
                cell = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentsVideoCell
                
            }
            if (comment.is_liked == 0 ){
                self!.viewModel.newsCommentsData[self!.newsSequance[myIndexPath.row].index].like_count += 1
                self!.viewModel.newsCommentsData[self!.newsSequance[myIndexPath.row].index].is_liked = 1
            }
            else {
                self!.viewModel.newsCommentsData[self!.newsSequance[myIndexPath.row].index].like_count -= 1
                self!.viewModel.newsCommentsData[self!.newsSequance[myIndexPath.row].index].is_liked = 0
            }
    
        }
    }
    @objc func CommentVideoAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let comment = self.viewModel.newsCommentsData[newsSequance[myIndexPath.row].index]
        let videoURL = URL(string: comment.file_list[0].file_url.encodeUrl()!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
    }
    }
    @objc func CommentImageAction(_ sender: UITapGestureRecognizer) {
          let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
         let comment = self.viewModel.newsCommentsData[newsSequance[myIndexPath.row].index]
        let photo = SKPhoto.photoWithImageURL(comment.file_list[0].file_url.encodeUrl()!)
        let browser = SKPhotoBrowser(photos: [photo])
        browser.initializePageIndex(0)
          SKPhotoBrowserOptions.displayGallery = false
        self.present(browser, animated: true, completion: nil)
    }
    @objc func likeACtion(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item =  self.newsHistory!
        let cell = self.tableView.cellForRow(at: myIndexPath as IndexPath) as! FooterMenuCell
        viewModel.likeNews(publication_id:item.publication_id) { [weak self] in
            guard let vc = self else { return }
            if (item.is_i_liked == 0){
                cell.like_btn.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                      cell.like_btn.setTitle("\(self!.newsHistory!.like_count + 1)", for: .normal)
            }
            else {
                cell.like_btn.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                      cell.like_btn.setTitle("\(self!.newsHistory!.like_count - 1)", for: .normal)
            }
            let realm = try! Realm()
            if let existingItem = NewsController.getCurrentObject(publication_id: item.publication_id) {
                
                try! realm.write {
                    if (existingItem.is_i_liked == 0 ){
                        existingItem.is_i_liked = 1
                        existingItem.like_count += 1
                    }
                    else {
                        existingItem.is_i_liked = 0
                        existingItem.like_count -= 1
                    }
                }
            }
        }
    }
    @objc func playVideo(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.newsHistory!
        let videoURL = URL(string: item.file_list[self.newsSequance[myIndexPath.row].fileIndex].file_url.encodeUrl()!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
    @objc func pickerAlienAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
        let item = self.newsHistory!
        let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
            
            UIPasteboard.general.string =  item.publication_desc
            
            
        })
        let reportAction = UIAlertAction(title: "report".localized(), style: .default, handler: { _ in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myAlert = storyboard.instantiateViewController(withIdentifier: "REPORTSCONTROLLER") as! ReportViewController
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            myAlert.publication_id = item.publication_id
            self.present(myAlert, animated: true, completion: nil)
            
        })
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(copyAction)
        alert.addAction(reportAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.present(alert, animated: true, completion: nil)
    }
    @objc func reportAction(_ sender: UIButton) {
        let item = self.newsHistory!
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "REPORTSCONTROLLER") as! ReportViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.publication_id = item.publication_id
        self.present(myAlert, animated: true, completion: nil)
    }
    @objc func pickerAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)
        let item = self.newsHistory!
        let removeAction = UIAlertAction(title: "Редактировать", style: .default, handler: { _ in
            let vc = CreateNewsController.instantiate()
            vc.avatarUrl = self.viewModelProfile?.avatar.value ?? ""
            vc.userName = self.viewModelProfile?.name.value ?? ""
            vc.publication_id = item.publication_id
            vc.newsHistory = item
            vc.hidesBottomBarWhenPushed = true
            self.isCreateOpened = true
            self.navigationController?.pushViewController(vc, animated: true)
        })
        let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
            
            UIPasteboard.general.string =  item.publication_desc
            
            
        })
        let changeAction = UIAlertAction(title: "delete_post".localized(), style: .default, handler: { _ in
            
            self.viewModel.removeNews(publication_id: item.publication_id) { [weak self] in
                let realm = try! Realm()
                if let existingItem = NewsController.getCurrentObject(publication_id: item.publication_id) {
                    try! realm.write {
                        realm.delete(existingItem)
                    }
                }
                self!.isCreateOpened = true
                self?.navigationController?.popViewController(animated: true)
            }
            
            
        })
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(changeAction)
        alert.addAction(copyAction)
        alert.addAction(removeAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.present(alert, animated: true, completion: nil)
    }
    @objc func playAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = self.tableView.cellForRow(at: myIndexPath as IndexPath) as! AudioContentCell
        var item = self.newsHistory!
        if let i = self.viewModel.realmDialog?.index(where: { $0.publication_id == item.publication_id }) {
            item = self.viewModel.realmDialog![i]
        }
        if item.file_list[newsSequance[myIndexPath.row].fileIndex].file_url.contains("https"){
            cell.loader.startAnimating()
            cell.playButton.isHidden = true
            downloadFileFromURL(audioUrl: item.file_list[newsSequance[myIndexPath.row].fileIndex].file_url, publication_id: item.publication_id,myIndexPath:myIndexPath,requestSuccess: { [weak self] () in
                guard let vc = self else { return }
                let origImage = UIImage(named: "icon_mini_play")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                cell.loader.stopAnimating()
                cell.playButton.isHidden = false
            })
            
        }
        else {
            if (cell.audioPlayer != nil){
                if (cell.audioPlayer.isPlaying){
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    cell.audioPlayer.pause()
                }
                else {
                    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                        let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(item.file_list[newsSequance[myIndexPath.row].fileIndex].file_url).csv")
                        cell.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                        cell.audioPlayer.prepareToPlay()
                        cell.audioPlayer.delegate = cell
                        cell.audioPlayer.play()
                        let origImage = UIImage(named: "icon_mini_pause")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                }
            }
            else {
                
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(item.file_list[newsSequance[myIndexPath.row].fileIndex].file_url).csv")
                    print("zxzxxzxx",audioURL,item.file_list[newsSequance[myIndexPath.row].fileIndex].file_url)
                    cell.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                    cell.audioPlayer.prepareToPlay()
                    cell.audioPlayer.delegate = cell
                    cell.audioPlayer.play()
                    let origImage = UIImage(named: "icon_mini_pause")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
        }
    }
    func downloadAudio(url: String,comment_id: Int,myIndexPath:NSIndexPath, requestSuccess: @escaping() -> Void) {
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("audioFilesC\(comment_id).csv")
            return (documentsURL, [.removePreviousFile])
        }
        print("asfsafsafsfa",url)
        AF.download(url.encodeUrl()!, to: destination).response { response in
    self.viewModel.newsCommentsData[self.newsSequance[myIndexPath.row].index].file_list[0].file_url = "audioFilesC\(comment_id)"
            requestSuccess()
            
        }
    }
    func downloadFileFromURL(audioUrl:String,publication_id:Int,myIndexPath:NSIndexPath, requestSuccess: @escaping() -> Void) {
        
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            
            documentsURL.appendPathComponent("p_\(publication_id).csv")
            return (documentsURL, [.removePreviousFile])
        }
        AF.download(audioUrl.encodeUrl()!, to: destination).response { response in
            let realm = try! Realm()
            if let existingItem = NewsController.getCurrentObject(publication_id: publication_id) {
                
                if (existingItem.file_list[
                    self.newsSequance[myIndexPath.row].fileIndex].file_url.contains("https")){
                    try! realm.write {
                        let file = ChatFile()
                        file.file_url = "p_\(publication_id)"
                        file.file_data = existingItem.file_list[self.newsSequance[myIndexPath.row].fileIndex].file_data
                        file.file_name = existingItem.file_list[self.newsSequance[myIndexPath.row].fileIndex].file_name
                        file.file_format = existingItem.file_list[self.newsSequance[myIndexPath.row].fileIndex].file_format
                        file.file_time = existingItem.file_list[self.newsSequance[myIndexPath.row].fileIndex].file_time
                        existingItem.file_list[self.newsSequance[myIndexPath.row].fileIndex] = file
                        
                    }
                }
                
            }
            requestSuccess()
            //     self.activityIndicator.stopAnimating()
            //      self.playIconNode.isHidden = false
            //      self.playIconNode.image = UIImage(named: "icon_mini_play")
        }
    }
    static func getCurrentObject(publication_id: Int) -> NewsHistory? {
        let realm = try! Realm()
        let obj = realm.objects(NewsHistory.self).filter("publication_id = \(publication_id)")
        return obj.first
    }
    static func instantiate() -> NewsControllerDetail {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NewsControllerDetail") as! NewsControllerDetail
    }
    @IBAction func sendBtn(_ sender: Any) {
            if self.textView.text != ""
            {
                self.viewModel.sendMessageApi(comment_text: self.textView.text, publication_id: (newsHistory?.publication_id)!, answer_comment_id: answer_comment_id, success: {
                    self.getLastComment()
                    self.answer_comment_id = 0
                    self.dismissKeyboard()
                })
                
        }
        self.textView.text = ""
        self.answerView.isHidden = true
        answerViewHeight.constant = 0
        self.view.layoutIfNeeded()
        
        self.attachBtn.isHidden = false
      
    }
    @IBAction func attachBtn(_ sender: Any) {
        let alert = UIAlertController(title: "choise".localized(), message: nil, preferredStyle: .actionSheet)
        let mediaAction = UIAlertAction(title: "photo_video".localized(), style: .default) { _ in
            let gallery = GalleryController()
            gallery.delegate = self
            self.present(gallery, animated: true, completion: nil)
        }
        let fileAction = UIAlertAction(title: "documents".localized(), style: .default) { _ in
            let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
            importMenu.delegate = self
            importMenu.modalPresentationStyle = .formSheet
            self.present(importMenu, animated: true, completion: nil)
        }

        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(mediaAction)
        alert.addAction(fileAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.darkBlueNavColor
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func keyboardBtn(_ sender: Any) {
        if (keyboardType == false) {
            let keyboardSettings = KeyboardSettings(bottomType: .categories)
            let emojiView = EmojiView(keyboardSettings: keyboardSettings)
            emojiView.translatesAutoresizingMaskIntoConstraints = false
            emojiView.delegate = self
            self.textView.inputView = emojiView
            keyboardType = true
            self.textView.reloadInputViews()
            self.textView.becomeFirstResponder()
        }
        else {
            keyboardType = false
            self.textView.inputView = nil
            self.textView.keyboardType = .default
            self.textView.reloadInputViews()
            self.textView.becomeFirstResponder()
        }
    }
    // callback when tap a emoji on keyboard
    public func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        self.textView.insertText(emoji)
    }
    
    // callback when tap change keyboard button on keyboard
    public func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
        self.textView.inputView = nil
        self.textView.keyboardType = .default
        self.textView.reloadInputViews()
    }
    
    // callback when tap delete button on keyboard
    public func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        self.textView.deleteBackward()
    }
    
    // callback when tap dismiss button on keyboard
    public func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        self.textView.resignFirstResponder()
    }
    // MARK: - Gallery DidSelectImages
    
    public func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        print("Selected Gallery Image", images.count)
        if let selectedImage = images.first {
            selectedImage.resolve(completion: { (resolvedImage) in
                if let image = resolvedImage {
                    if let data = UIImageJPEGRepresentation(image, 0.5) {
                        self.viewModel.uploadCommentFile(with: data, format: "image",publication_id:(self.newsHistory?.publication_id)!,answer_comment_id:self.answer_comment_id, success: {
                            print("image uploaded to server")
                            self.answer_comment_id = 0
                            self.getLastComment()
                        })
                        
                    }
                    self.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
        
        
    
            

                
    
    // MARK: - Gallery DidSelectVideo
    
    public func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        print("Select video delegate")
        var thumbVideoImage: UIImage?
        
        video.fetchThumbnail(size: CGSize(width: 300, height: 300), completion: { (image) in
            if let thumbImage = image {
                thumbVideoImage = thumbImage
            }
        })
        
        video.fetchAVAsset { (asset) in
            if let urlAsset = asset as? AVURLAsset {
                print("url video fetched")
                do {
                    let data = try Data(contentsOf: urlAsset.url)
                    print("url thumg fetched", urlAsset.url)
                    if let image = thumbVideoImage {
                        var videoExtension = urlAsset.url.lastPathComponent
                        videoExtension = NSString(string: videoExtension).pathExtension
                        self.viewModel.uploadCommentFile(with: data, format: "video", fileExtension: videoExtension,publication_id:(self.newsHistory?.publication_id)!,answer_comment_id:self.answer_comment_id, success: {
                            print("video is uploaded")
                               self.answer_comment_id = 0
                              self.getLastComment()
                        })
                        self.dismiss(animated: true, completion: nil)
                    }
                } catch {
                    WhisperHelper.showErrorMurmur(title: "Ошибка загрузки видео")
                }
            }
        }
    }
    
    public func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        print("requestLightbox delegate")
    }
    
    public func galleryControllerDidCancel(_ controller: GalleryController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - UIDocumentMenuDelegate

extension NewsControllerDetail: UIDocumentMenuDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        let fileExtension = NSString(string: fileName).pathExtension
        
        do {
            let data = try Data(contentsOf: url)
          
            viewModel.uploadCommentFile(with: data, format: "document", fileExtension: fileExtension, fileName: fileName,publication_id:(newsHistory?.publication_id)!,answer_comment_id:answer_comment_id, success: {
                self.answer_comment_id = 0
                print("document uploaded to server")
                self.getLastComment()
                })
                
        } catch {
            WhisperHelper.showErrorMurmur(title: "Ошибка загрузки документа")
            print("error converting from file url to data")
        }
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    public func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
        print("view was cancelled")
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension NewsControllerDetail: GrowingTextViewDelegate {
    
    // *** Call layoutIfNeeded on superview for animation when changing height ***
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    func textViewDidChange(_ textView: UITextView) {
        if (textView.text.count >= 1){
            self.attachBtn.isHidden = true
        }
        else {
            self.attachBtn.isHidden = false
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
    
}
