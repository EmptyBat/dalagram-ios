//
//  GalleryController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 5/16/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
import SKPhotoBrowser

class UserGalleryController: UIViewController,SKPhotoBrowserDelegate {
    var galleryData: [Gallery] = []
    var browser = SKPhotoBrowser()
    var currentIndex = 0
    // MARK: - IBOutlets
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MediaCell.self)
        collectionView.backgroundColor = UIColor.white
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 8)
        return collectionView
    }()
    
    // MARK: - Variables
    
    var images = [SKPhoto]()
    var author_id: Int = 0
    var parentNavigationController : UINavigationController?
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setupData()
    }
    
    // MARK: - Configuring UI
    
    func configureUI() {
        self.title = " Галерея"
        collectionView.contentInset = UIEdgeInsets.zero
        self.automaticallyAdjustsScrollViewInsets = false
        view.backgroundColor = UIColor.white
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.edges.equalTo(view.safeAreaLayoutGuide)
            } else {
                make.edges.equalToSuperview()
            }
        }
    }
    func setupData() {
        let parameters = ["token": User.getToken(), "page": 1, "per_page": 1000,"author_id":author_id] as [String : Any]
        NetworkManager.makeRequest(.getGallery(parameters), success: { (json)  in
            print("afsasa",json)
            for (_, subJson):(String, JSON) in json["data"] {
                let new = Gallery(json: subJson)
                self.galleryData.append(new)
                let photo = SKPhoto.photoWithImageURL(new.file_list[0].file_url.encodeUrl()!)
                photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                self.images.append(photo)
            }
            
                self.collectionView.reloadData()
        })
    }
    
}

extension UserGalleryController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MediaCell = collectionView.dequeReusableCell(for: indexPath)
        cell.mediaImageView.kf.setImage(with: URL(string: galleryData[indexPath.row].file_list[0].file_url.encodeUrl()!), placeholder: #imageLiteral(resourceName: "bg_gradient_2"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MediaCell {
            currentIndex = indexPath.row
            SKToolbarOptions.like_count = galleryData[indexPath.row].like_count
            SKToolbarOptions.is_i_liked = galleryData[indexPath.row].is_i_liked
            SKToolbarOptions.resend_count = galleryData[indexPath.row].share_count
            SKToolbarOptions.view_count = galleryData[indexPath.row].view_count
            SKToolbarOptions.comment_count = galleryData[indexPath.row].comment_count
            browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(indexPath.row)
            browser.delegate = self
            self.present(browser, animated: true, completion: nil)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = collectionView.frame.width/2 - 8.0
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
}
extension UserGalleryController {
    func didShowPhotoAtIndex(_ index: Int) {
        //  print("test did show but i know")
        collectionView.visibleCells.forEach({$0.isHidden = false})
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
    }
    func didScrollToIndex(_ browser: SKPhotoBrowser, index: Int){
        currentIndex = index
        SKToolbarOptions.like_count = galleryData[index].like_count
        SKToolbarOptions.is_i_liked = galleryData[index].is_i_liked
        SKToolbarOptions.resend_count = galleryData[index].share_count
        SKToolbarOptions.view_count = galleryData[index].view_count
        SKToolbarOptions.comment_count = galleryData[index].comment_count
        browser.updateToolbarAction()
    }
    func ActionButtonClicked(_ index: Int) {
        if (index == 0){
            browser.backLikeAction(i_liked:galleryData[currentIndex].is_i_liked)
            likePhoto(publication_id:galleryData[index].publication_id) { [weak self] in
            }
            if (galleryData[currentIndex].is_i_liked == 0){
                galleryData[currentIndex].is_i_liked = 1
                galleryData[currentIndex].like_count += 1
            }
            else {
                galleryData[currentIndex].is_i_liked = 0
                galleryData[currentIndex].like_count -= 1
            }
        }
        else if (index == 1){
            let item = galleryData[currentIndex]
            let newsController = NewsHistory()
            newsController.publication_id = item.publication_id
            newsController.file_list.append(item.file_list[0])
            newsController.avatar = item.avatar
            newsController.is_i_liked = item.is_i_liked
            newsController.nickname = item.nickname
            
            newsController.publication_desc = item.publication_desc
            newsController.publication_date = item.publication_date
            newsController.view_count = item.view_count
            newsController.share_count = item.share_count
            newsController.comment_count = item.comment_count
            newsController.like_count = item.like_count
            
            self.dismiss(animated: true, completion: nil)
            
            let vc = NewsControllerDetail.instantiate()
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = newsController
            vc.showComments = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    func willDismissAtPageIndex(_ index: Int) {
        collectionView.visibleCells.forEach({$0.isHidden = false})
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
    }
    
    func willShowActionSheet(_ photoIndex: Int) {
    }
    
    func didDismissAtPageIndex(_ index: Int) {
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = false
    }
    
    func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
        // handle dismissing custom actions
    }
    
    func removePhoto(_ browser: SKPhotoBrowser, index: Int, reload: @escaping (() -> Void)) {
        reload()
    }
    
    func viewForPhoto(_ browser: SKPhotoBrowser, index: Int) -> UIView? {
        return collectionView.cellForItem(at: IndexPath(item: index, section: 0))
    }
    
    func captionViewForPhotoAtIndex(index: Int) -> SKCaptionView? {
        return nil
    }
    func likePhoto(publication_id: Int,completion:@escaping ()->()) {
        let parameters = ["token": User.getToken(),"publication_id":publication_id] as [String : Any]
        NetworkManager.makeRequest(.likeNews(parameters), success: { (json)  in
            completion()
        })
    }
}
