//
//  NewsControllerView.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/18/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
extension NewsController {
    func configureUI() {
        
        // MARK: - Navigation Bar
        setEmptyBackTitle()
        setBlueNavBar()
        self.tableView = UITableView.init(frame: CGRect.zero, style: .grouped)
        //MARK: - TableView Configurations
        tableView.registerNib(ShowAllCommentsTableViewCell.self)
        tableView.registerNib(ShowAllTableViewCell.self)
        tableView.registerNib(ContentInputTableViewCell.self)
        tableView.registerNib(NameCell.self)
        tableView.registerNib(DescriptionCell.self)
        tableView.registerNib(ImageContentCell.self)
        tableView.registerNib(FooterMenuCell.self)
        tableView.registerNib(AudioContentCell.self)
        tableView.registerNib(VideoContentCell.self)
        tableView.registerNib(LinkContentCell.self)
        tableView.registerNib(HeaderTableViewCell.self)
        tableView.registerNib(GalleryTableViewCell.self)
        tableView.registerNib(InfoTableViewCell.self)
        tableView.registerNib(MultipleImageContentCell.self)
        tableView.register(UINib(nibName: "DocumentsCell", bundle: nil), forCellReuseIdentifier: "document_cell")
        tableView.registerNib(CommentImageCell.self)
        tableView.registerNib(CommentTextCell.self)
        tableView.registerNib(CommentsVideoCell.self)
        tableView.registerNib(CommentsFileCell.self)
        tableView.registerNib(CommentAudioCell.self)
        tableView.registerNib(FriendsCollectionViewTableViewCell.self)
        tableView.registerNib(SharedDescriptionCell.self)
        tableView.registerNib(SharedImageContentCell.self)
        tableView.registerNib(SharedAudioContentCell.self)
        tableView.registerNib(SharedVideoContentCell.self)
        tableView.registerNib(SharedLinkContentCell.self)
        tableView.registerNib(SharedMultipleImageContentCell.self)
        tableView.registerNib(SharedDocumentContentCell.self)
        tableView.registerNib(BannerContentCell.self)
        
        tableView.separatorColor = UIColor.init(white: 0.85, alpha: 1.0)
        tableView.allowsSelection = true
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 400
        tableView.separatorColor = UIColor.clear
        self.tableView.reloadData()
        
        // MARK: - UIBarButtonItem Configurations
                let createSearch = UIBarButtonItem(image: UIImage(named: "icon_search"), style: .plain, target: self, action: #selector(searchAction))
                createSearch.tintColor = UIColor.white
                
                let createShare = UIBarButtonItem(image: UIImage(named: "icon_mbriShare"), style: .plain, target: self, action: #selector(CompanyProductAction))
                createShare.tintColor = UIColor.white
                
                self.navigationItem.rightBarButtonItems = [createSearch, createShare]
       
    }
    @objc func searchAction() {
        let vc = GlobalSearchTableViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func menuAction() {
        let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
        let removeAction = UIAlertAction(title: "edit_profile".localized(), style: .default, handler: { _ in
            let vc = EditProfileController.fromStoryboard()
            vc.viewModel = self.viewModelProfile
            self.editingProfile = true
            self.show(vc, sender: nil)
        })
        let changeAction = UIAlertAction(title: "settings".localized(), style: .default, handler: { _ in
            let vc = SettingsController.fromStoryboard()
            self.show(vc, sender: nil)
            
            })
        
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(removeAction)
        alert.addAction(changeAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.present(alert, animated: true, completion: nil)
    }
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
