//
//  NewsController + TableView.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/18/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SafariServices
import AVFoundation
import Alamofire
import RealmSwift
import AVKit
import SKPhotoBrowser
import Kingfisher
extension NewsController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsSequance.count + headerOffset
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (author_id == 0){
            // let head)erView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "newsHeaderCell")
            let header = Bundle.main.loadNibNamed("ContentInputTableViewCell", owner: self, options: nil)?.last as! ContentInputTableViewCell
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            header.imageLb.kf.setImage(with: URL(string: avatarUrl.encodeUrl()!), placeholder: gradientImage)
            header.imageLb.layer.cornerRadius =  header.imageLb.frame.width/2
            header.nameLb.text = userName
            header.backgroundColor = UIColor.white
            let tap = UITapGestureRecognizer(target: self, action:#selector(createNews(_:)))
            tap.delegate = self
            header.addGestureRecognizer(tap)
            return header
        }
        else {
            return nil
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (author_id != 0){
            return 0
        }
        else {
            return 135
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (author_id != 0){
             if (indexPath.row == 0){
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell", for: indexPath) as! HeaderTableViewCell
                cell.desckriptionLB.text = viewModelAlien.status.value
                cell.nameLB.text = viewModelAlien.name.value
                let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
                cell.avatarView.kf.setImage(with: URL(string: viewModelAlien.avatar.value.encodeUrl()!), placeholder: gradientImage)
                cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(headerAvatarImageView))
                cell.avatarView.isUserInteractionEnabled = true
                cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
                
                let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(headerBackgroundImageView))
                cell.headerImage.isUserInteractionEnabled = true
                cell.headerImage.addGestureRecognizer(tapGestureRecognizer1)
                
                
                cell.headerImage.kf.setImage(with: URL(string: viewModelAlien.background.value.encodeUrl()!), placeholder: gradientImage)
                cell.postCount.text = "\(viewModelAlien.publication_count.value)"
                cell.followersCount.text = "\(viewModelAlien.follower_count.value)"
                cell.friendsCount.text = "\(viewModelAlien.friend_count.value)"
                if (viewModelAlien.is_friend.value != 0){
                    cell.addFriendButton.setTitle("delete_from_friends".localized(), for: .normal)
                }
                else {
                    cell.addFriendButton.setTitle("add_to_friends".localized(), for: .normal)
                }
                if (viewModelAlien.is_i_subscribe.value != 0){
                        cell.subscribeButton.setTitle("subscribe".localized(), for: .normal)
                }
                else {
                    self.viewModelAlien.is_i_subscribe.value = 0
                    cell.subscribeButton.setTitle("unsubscribe".localized(), for: .normal)
                    }
                if (viewModelProfile.user_id.value == author_id){
                    cell.chatHeight.constant = 0
                    cell.chatButton.isHidden = true
                    cell.subscribeHeight.constant = 0
                    cell.addFriendHeight.constant = 0
                    cell.layoutIfNeeded()
                }
                cell.addFriendButton.addTarget(self, action:#selector(addFriendAction), for: .touchUpInside)
                cell.subscribeButton.addTarget(self, action:#selector(subscribeAction), for: .touchUpInside)
                cell.chatButton.addTarget(self, action:#selector(startChatAction), for: .touchUpInside)
                return cell
            }
             if (indexPath.row == 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryTableViewCell", for: indexPath) as! GalleryTableViewCell
                if (viewModelProfile.user_id.value == author_id){
                    if (self.navigationController != nil){
                        cell.setupDialog(gallery:viewModel.galleryData,vc: self,own_author:true,author_id: author_id,profile:self.viewModelProfile,navig:self.navigationController!)
                    }
                }
                else {
                    cell.setupDialog(gallery:viewModel.galleryData,vc: self,own_author:false,author_id: author_id,profile:self.viewModelProfile,navig:self.navigationController!)
                }
                return cell
            }
             else if (indexPath.row < userInfo.count + 2){
                let cell = tableView.dequeueReusableCell(withIdentifier: "InfoTableViewCell", for: indexPath) as! InfoTableViewCell
                let cellShow = tableView.dequeueReusableCell(withIdentifier: "ShowAllTableViewCell", for: indexPath) as! ShowAllTableViewCell
                if indexPath.row - 2 != 0 {
                    cell.infoHeight.constant = 0
                }
                switch userInfo[indexPath.row - 2] {
                case "city":
                    cell.titleLb.text = viewModelAlien.city.value
                    cell.subTitleLB.text = "city".localized()
                    cell.img.image =  #imageLiteral(resourceName: "icon_mark-1")
                    return cell
                case "work":
                    cell.titleLb.text = viewModelAlien.work.value
                    cell.subTitleLB.text = "work".localized()
                    cell.img.image =  #imageLiteral(resourceName: "icon_job")
                    return cell
                case "university":
                    cell.titleLb.text = viewModelAlien.university.value
                    cell.subTitleLB.text = "university".localized()
                    cell.img.image =  #imageLiteral(resourceName: "icon_univer")
                    return cell
                case "family":
                    cell.titleLb.text = viewModelAlien.family.value
                    cell.subTitleLB.text = "family".localized()
                    cell.img.image =  #imageLiteral(resourceName: "icon_univer")
                    return cell
                case "language":
                    cell.titleLb.text = viewModelAlien.language.value
                    cell.subTitleLB.text = "language".localized()
                    cell.img.image =  #imageLiteral(resourceName: "icon_univer")
                    return cell
                case "show":
                    return cellShow
                default:
                    return cell
                }
                return cell
            }
                else if (indexPath.row < userInfo.count + 3){
                let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCollectionViewTableViewCell", for: indexPath) as! FriendsCollectionViewTableViewCell
               if (viewModelProfile.user_id.value != author_id){
                cell.setupFriends(friends: self.viewModel.friensData, myfriends: self.viewModel.myFriendsData, navig: self.navigationController!)
                }
               else {
                  cell.setupOnlyFriends(friends: self.viewModel.friensData ,navig: self.navigationController!)
                }
                return cell
            }
        }
         if ((viewModelProfile.user_id.value == author_id) && indexPath.row == headerOffset - 1){
             let cell = tableView.dequeueReusableCell(withIdentifier: "ContentInputTableViewCell", for: indexPath) as! ContentInputTableViewCell
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.imageLb.kf.setImage(with: URL(string: avatarUrl.encodeUrl()!), placeholder: gradientImage)
            cell.imageLb.layer.cornerRadius =  cell.imageLb.frame.width/2
            cell.nameLb.text = userName
            cell.backgroundColor = UIColor.white
            let tap = UITapGestureRecognizer(target: self, action:#selector(createNews(_:)))
            tap.delegate = self
            cell.addGestureRecognizer(tap)
            return cell
        }
            
        else if (newsSequance[indexPath.row - headerOffset].newsType == .header){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell", for: indexPath) as! NameCell
            if (item.access_id == 1){
                    cell.public_width.constant = 19
                cell.publicIV.isHidden = false
            }
            else {
                cell.public_width.constant = 0
               cell.publicIV.isHidden = true
            }
               cell.layoutIfNeeded()
            cell.actionButton.removeTarget(self,  action:#selector(pickerAction), for: .touchUpInside)
            cell.actionButton.removeTarget(self,  action:#selector(pickerAlienAction), for: .touchUpInside)
            if (viewModelProfile.user_id.value == item.user_id){
                cell.actionButton.addTarget(self, action:#selector(pickerAction), for: .touchUpInside)
                cell.actionButton.tag = indexPath.row - headerOffset
                cell.actionButton.isHidden = false
            }
            else {
                cell.actionButton.addTarget(self, action:#selector(pickerAlienAction), for: .touchUpInside)
                cell.actionButton.tag = indexPath.row - headerOffset
                cell.actionButton.isHidden = false
            }
            cell.Name.text = item.nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
                cell.avatarView.kf.setImage(with: URL(string: item.avatar.encodeUrl()!), placeholder: gradientImage)
                cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClicked))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row - headerOffset
            
            // Configure the cell...
            
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .text){
            let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell", for: indexPath) as! DescriptionCell
            if (self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].is_has_link == 1){
               cell.descriptionLB.attributedText = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].publication_desc.attributedHtmlString
                cell.descriptionLB.numberOfLines = 5
                
                
            }
            else {
                let descStr = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].publication_desc
                
                if (descStr.count > 150){
                    let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                
                    let range = (first150 as NSString).range(of: "next".localized())
                    
                    let attribute = NSMutableAttributedString.init(string: first150)
                    attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)

                    cell.descriptionLB.attributedText = attribute
                }
                else {
                    cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].publication_desc
                }
            }
            
            // Configure the cell...
            
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .image){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageContentCell", for: indexPath) as! ImageContentCell
            cell.ImageContent.kf.setImage(with: URL(string: item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_url.encodeUrl()!), placeholder: nil)
                let heightInPixels =  newsSequance[indexPath.row - headerOffset].imageHeight
                
                let widthInPixels = newsSequance[indexPath.row - headerOffset].imageWidth
                let myViewWidth = self.tableView.frame.size.width
            
                let ratio = Double(myViewWidth)/Double(widthInPixels)
                let scaledHeight = Double(heightInPixels) * ratio
            
                cell.imageHeight.constant = CGFloat(scaledHeight)
                cell.layoutIfNeeded()
            

            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .footer){
            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterMenuCell", for: indexPath) as! FooterMenuCell
            if (self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].view_count != 0) {
             cell.visible_btn.setTitle("\(self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].view_count)", for: .normal)
             }
            else {
                 cell.visible_btn.setTitle("", for: .normal)
            }
            if (self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].comment_count != 0){
              cell.comments_btn.setTitle("\(self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].comment_count)", for: .normal)
                   cell.view_height.constant = 0
            }
            else {
              cell.view_height.constant = 8
              cell.comments_btn.setTitle("", for: .normal)
            }
            cell.comments_btn.addTarget(self, action:#selector(commentAction), for: .touchUpInside)
            cell.comments_btn.tag = indexPath.row
            if (self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_count != 0){
                cell.share_btn.setTitle("\(self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_count)", for: .normal)
                
            }
            else {
                cell.share_btn.setTitle("", for: .normal)
            }
            
            if (self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].like_count != 0){
                cell.like_btn.setTitle("\(self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].like_count)", for: .normal)
                
            }
            else {
                cell.like_btn.setTitle("", for: .normal)
            }
            if (self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].is_i_liked == 1){
                cell.like_btn.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.like_btn.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
                cell.like_btn.addTarget(self, action:#selector(likeAction), for: .touchUpInside)
                cell.like_btn.tag = indexPath.row
            
            cell.share_btn.addTarget(self, action:#selector(shareAction), for: .touchUpInside)
            cell.share_btn.tag = indexPath.row
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .audio){
            var item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let cell = tableView.dequeueReusableCell(withIdentifier: "AudioContentCell", for: indexPath) as! AudioContentCell
            
            cell.titleLb.text = item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_name
            let temp:Int = Int(item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_time)!
            let mins:Int = temp / 60
            let secs:Int = temp % 60
            if (mins >= 10){
                if (secs >= 10){
                    cell.dateLb.text = "\(mins):\(secs)"
                }
                else {
                   cell.dateLb.text = "\(mins):0\(secs)"
                }
            }
            else {
                if (secs >= 10){
                     cell.dateLb.text = "0\(mins):\(secs)"
                }
                else {
                     cell.dateLb.text = "0\(mins):0\(secs)"
                }
            }
            if let i = self.viewModel.realmDialog?.index(where: { $0.publication_id == item.publication_id }) {
                item = self.viewModel.realmDialog![i]
            }
        if item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_url.contains("https"){
            let origImage = UIImage(named: "icon_mini_download_green")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.playButton.setImage(tintedImage, for: .normal)
            cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
        else {
            if (cell.audioPlayer != nil){
                if (cell.audioPlayer.isPlaying){
            let origImage = UIImage(named: "icon_mini_pause")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.playButton.setImage(tintedImage, for: .normal)
            cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
                else {
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
            else {
                let origImage = UIImage(named: "icon_mini_play")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
            }
          cell.playButton.tag = indexPath.row - headerOffset
          cell.playButton.addTarget(self, action:#selector(playAction), for: .touchUpInside)
            // Configure the cell...
            
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .file){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let origImage = UIImage(named: "icon_file_default")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            let cell = tableView.dequeueReusableCell(withIdentifier: "document_cell", for: indexPath) as! DocumentsCell
            cell.file_image.image = tintedImage
            cell.file_image.tintColor = UIColor.random
            cell.file_extension_lb.text =  item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_name.fileExtension()
            cell.title_lb.text = item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_name
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .video){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
             let cell = tableView.dequeueReusableCell(withIdentifier: "VideoContentCell", for: indexPath) as! VideoContentCell
            cell.playButton.tag = indexPath.row - headerOffset
            cell.playButton.addTarget(self, action:#selector(playVideo), for: .touchUpInside)
                let asset = AVAsset(url:URL(string: item.file_list[self.newsSequance[indexPath.row - headerOffset].fileIndex].file_url.encodeUrl()!)!)
                    let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
                    let time = CMTimeMake(1, 60)
                    let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
                    if img != nil {
                        let frameImg  = UIImage(cgImage: img!)
                        DispatchQueue.main.async(execute: {
                            cell.previewImage.image = frameImg
                        })
                
            }
              return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .link){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let cell = tableView.dequeueReusableCell(withIdentifier: "LinkContentCell", for: indexPath) as! LinkContentCell
            cell.titleLb.text = item.meta_tagsTitle
            cell.descriptionLb.text = item.meta_tagsDescription
            let removeSymbols =  item.meta_tagsImage.replacingOccurrences(of: "\"", with: "")
            if (!item.meta_tagsImage.isEmpty){
       
                cell.linkImage.kf.setImage(with: URL(string: removeSymbols.encodeUrl()!), placeholder: nil)
                
                let heightInPixels =  newsSequance[indexPath.row - headerOffset].imageHeight
                
                let widthInPixels = newsSequance[indexPath.row - headerOffset].imageWidth
                let myViewWidth = self.tableView.frame.size.width
                
                let ratio = Double(myViewWidth)/Double(widthInPixels)
                let scaledHeight = Double(heightInPixels) * ratio
                
                
                if (heightInPixels != 0 && widthInPixels != 0){
                cell.imageHeight.constant = CGFloat(scaledHeight)
                cell.layoutIfNeeded()
                }
                
            }
            else {
                cell.imageHeight.constant = 0
                cell.layoutIfNeeded()
            }
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: item.publication_desc, options: [], range: NSRange(location: 0, length: item.publication_desc.utf16.count))
            
            for match in matches {
                guard let range = Range(match.range, in: item.publication_desc) else { continue }
                let url = item.publication_desc[range]
                if (url.contains("youtu")){
                    cell.playButton.isHidden = false
                    cell.youtubePlayer.isHidden  = true
                }
                else {
                    cell.playButton.isHidden = true
                    cell.youtubePlayer.isHidden  = true
                }
            }
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .multiple){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let image1 = newsSequance[indexPath.row - headerOffset].multipleIndexs[0]
            let image2 = newsSequance[indexPath.row - headerOffset].multipleIndexs[1]
            let cell = tableView.dequeueReusableCell(withIdentifier: "MultipleImageContentCell", for: indexPath) as! MultipleImageContentCell
            cell.image1.kf.setImage(with: URL(string: item.file_list[image1].file_url.encodeUrl()!), placeholder: nil)
            cell.image2.kf.setImage(with: URL(string: item.file_list[image2].file_url.encodeUrl()!), placeholder: nil)
            if (newsSequance[indexPath.row - headerOffset].multipleIndexs.count > 2){
                cell.moreLabel.text = "+\(newsSequance[indexPath.row - headerOffset].multipleIndexs.count - 2)"
                
            }
            else {
                cell.moreBackground.isHidden = true
            }
          
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .commentText){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTextCell", for: indexPath) as! CommentTextCell
            let comment = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            cell.Name.text = comment.comment_nickname
            cell.date.text = comment.comment_date
            cell.descriptionLB.text = ("  " + comment.comment_text)
            cell.descriptionLB.layer.cornerRadius = 8
            cell.descriptionLB.layer.masksToBounds = true
            if comment.comment_avatar.isEmpty {
                if let firstLetter = comment.comment_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.comment_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            if  (comment.comment_is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.comment_like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.isHidden = false
            cell.answerButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(goToComment), for: .touchUpInside)
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .commentImage){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentImageCell", for: indexPath) as! CommentImageCell
            let comment = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            cell.Name.text = comment.comment_nickname
            cell.date.text = comment.comment_date
            if comment.comment_avatar.isEmpty {
                if let firstLetter = comment.comment_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.comment_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            if  (comment.comment_is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.comment_like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.isHidden = false
            cell.answerButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(goToComment), for: .touchUpInside)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CommentImageAction))
            cell.imageContent.isUserInteractionEnabled = true
            cell.imageContent.addGestureRecognizer(tapGestureRecognizer)
            cell.imageContent.tag = indexPath.row
            //image

            cell.imageContent.kf.setImage(with: URL(string: comment.comment_file_list[0].file_url.encodeUrl()!), placeholder: nil)
            
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .commentVideo){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsVideoCell", for: indexPath) as! CommentsVideoCell
            let comment = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            cell.date.text = comment.comment_date
            if comment.comment_avatar.isEmpty {
                if let firstLetter = comment.comment_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.comment_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            if  (comment.comment_is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.comment_like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.isHidden = false
            cell.answerButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(goToComment), for: .touchUpInside)
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(CommentVideoAction), for: .touchUpInside)
            let asset = AVAsset(url:URL(string: comment.comment_file_list[0].file_url.encodeUrl()!)!)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            let time = CMTimeMake(1, 60)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {
                    cell.imageContent.image = frameImg
                })
                
            }
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .commentFile){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsFileCell", for: indexPath) as! CommentsFileCell
            let comment = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            cell.file_extension_lb.text = comment.comment_file_list[0].file_name.fileExtension()
            cell.title_lb.text = comment.comment_file_list[0].file_name
            cell.Name.text = comment.comment_nickname
            cell.date.text = comment.comment_date
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            if comment.comment_avatar.isEmpty {
                if let firstLetter = comment.comment_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.comment_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            if  (comment.comment_is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.comment_like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.isHidden = false
            cell.answerButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(goToComment), for: .touchUpInside)
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .commentAudio){
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentAudioCell", for: indexPath) as! CommentAudioCell
            let comment = self.viewModel.dialogs![newsSequance[indexPath.row].index]
            cell.titleLB.text = comment.comment_file_list[0].file_name
            let temp:Int = Int(comment.comment_file_list[0].file_time)!
            let mins:Int = temp / 60
            let secs:Int = temp % 60
            if (mins >= 10){
                if (secs >= 10){
                    cell.durationLB.text = "\(mins):\(secs)"
                }
                else {
                    cell.durationLB.text = "\(mins):0\(secs)"
                }
            }
            else {
                if (secs >= 10){
                    cell.durationLB.text = "0\(mins):\(secs)"
                }
                else {
                    cell.durationLB.text = "0\(mins):0\(secs)"
                }
            }
            cell.reportButton.addTarget(self, action:#selector(reportAction), for: .touchUpInside)
            cell.reportButton.tag = indexPath.row
            if comment.comment_file_list[0].file_url.contains("https"){
                let origImage = UIImage(named: "icon_mini_download_green")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
            else {
                if (cell.audioPlayer != nil){
                    if (cell.audioPlayer.isPlaying){
                        let origImage = UIImage(named: "icon_mini_pause")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                    else {
                        let origImage = UIImage(named: "icon_mini_play")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                }
                else {
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(playAudioCommentAction), for: .touchUpInside)
            cell.Name.text = comment.comment_nickname
            cell.date.text = comment.comment_date
            if comment.comment_avatar.isEmpty {
                if let firstLetter = comment.comment_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: comment.comment_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            if  (comment.comment_is_liked == 1){
                cell.likeButton.setImage(UIImage(named:"icon_heart_liked"), for: .normal)
                
            }
            else {
                cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
            }
            cell.likeButton.setTitle("\(comment.comment_like_count)", for: .normal)
            cell.likeButton.addTarget(self, action:#selector(likeComment), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            cell.answerButton.isHidden = false
            cell.answerButton.tag = indexPath.row
            cell.answerButton.addTarget(self, action:#selector(goToComment), for: .touchUpInside)
            return cell
        }
        else if (newsSequance[indexPath.row - headerOffset].newsType == .showComments){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShowAllCommentsTableViewCell", for: indexPath) as! ShowAllCommentsTableViewCell
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            cell.title_btn.setTitle(("show_all_comments".localized() + ": " + " \(item.comment_count)"), for: .normal)
            return cell
        }
         else if (newsSequance[indexPath.row - headerOffset].newsType == .shareDesk){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedDescriptionCell", for: indexPath) as! SharedDescriptionCell
            if (self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_is_has_link == 1){
                cell.descriptionLB.attributedText = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc.attributedHtmlString
                cell.descriptionLB.numberOfLines = 5
                
                
            }
            else {
                let descStr = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc
                
                if (descStr.count > 150){
                        let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                    
                        let range = (first150 as NSString).range(of: "next".localized())
                        
                    let attribute = NSMutableAttributedString.init(string: first150)
                    attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)
                    
                    cell.descriptionLB.attributedText = attribute
                }
                else {
                    cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc
                }
            }
            cell.date.text = item.share_publication_date
            cell.Name.text = item.share_nickname
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row - headerOffset
            // Configure the cell...
            
            return cell
         }
         else if (newsSequance[indexPath.row - headerOffset].newsType == .shareImage){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedImageContentCell", for: indexPath) as! SharedImageContentCell
            
            let descStr = item.share_publication_desc
            
            if (descStr.count > 150){
                    let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                
                    let range = (first150 as NSString).range(of: "next".localized())
                    
                let attribute = NSMutableAttributedString.init(string: first150)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)
                
                cell.descriptionLB.attributedText = attribute
            }
            else {
                cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc
            }
            cell.date.text = item.share_publication_date
            cell.ImageContent.kf.setImage(with: URL(string: item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_url.encodeUrl()!), placeholder: nil)
            
            let heightInPixels =  newsSequance[indexPath.row - headerOffset].imageHeight
            
            let widthInPixels = newsSequance[indexPath.row - headerOffset].imageWidth
            let myViewWidth = self.tableView.frame.size.width
            
            let ratio = Double(myViewWidth)/Double(widthInPixels)
            let scaledHeight = Double(heightInPixels) * ratio
            
            cell.imageHeight.constant = CGFloat(scaledHeight)
            cell.layoutIfNeeded()
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row - headerOffset
            return cell
         }
         else if (newsSequance[indexPath.row - headerOffset].newsType == .shareAudio){
            var item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedAudioContentCell", for: indexPath) as! SharedAudioContentCell
            
            cell.titleLb.text = item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_name
            let descStr = item.share_publication_desc
            
            if (descStr.count > 150){
                    let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                
                    let range = (first150 as NSString).range(of: "next".localized())
                    
                let attribute = NSMutableAttributedString.init(string: first150)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)
                
                cell.descriptionLB.attributedText = attribute
            }
            else {
                cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc
            }
            cell.date.text = item.share_publication_date
            let temp:Int = Int(item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_time)!
            let mins:Int = temp / 60
            let secs:Int = temp % 60
            if (mins >= 10){
                if (secs >= 10){
                    cell.dateLb.text = "\(mins):\(secs)"
                }
                else {
                    cell.dateLb.text = "\(mins):0\(secs)"
                }
            }
            else {
                if (secs >= 10){
                    cell.dateLb.text = "0\(mins):\(secs)"
                }
                else {
                    cell.dateLb.text = "0\(mins):0\(secs)"
                }
            }
            if let i = self.viewModel.realmDialog?.index(where: { $0.publication_id == item.publication_id }) {
                item = self.viewModel.realmDialog![i]
            }
            if item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_url.contains("https"){
                let origImage = UIImage(named: "icon_mini_download_green")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
            }
            else {
                if (cell.audioPlayer != nil){
                    if (cell.audioPlayer.isPlaying){
                        let origImage = UIImage(named: "icon_mini_pause")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                    else {
                        let origImage = UIImage(named: "icon_mini_play")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                }
                else {
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
            cell.playButton.tag = indexPath.row - headerOffset
            cell.playButton.addTarget(self, action:#selector(playAction), for: .touchUpInside)
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row - headerOffset
            // Configure the cell...
            
            return cell
         }
         else if (newsSequance[indexPath.row - headerOffset].newsType == .shareFile){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let origImage = UIImage(named: "icon_file_default")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedDocumentContentCell", for: indexPath) as! SharedDocumentContentCell
            let descStr = item.share_publication_desc
            
            if (descStr.count > 150){
                    let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                
                    let range = (first150 as NSString).range(of: "next".localized())
                    
                let attribute = NSMutableAttributedString.init(string: first150)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)
                
                cell.descriptionLB.attributedText = attribute
            }
            else {
                cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc
            }
            cell.date.text = item.share_publication_date
            cell.file_image.image = tintedImage
            cell.file_image.tintColor = UIColor.random
            cell.file_extension_lb.text =  item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_name.fileExtension()
            cell.title_lb.text = item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_name
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row - headerOffset
            return cell
         }
         else if (newsSequance[indexPath.row - headerOffset].newsType == .shareVideo){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedVideoContentCell", for: indexPath) as! SharedVideoContentCell
            let descStr = item.share_publication_desc
            
            if (descStr.count > 150){
                    let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                
                    let range = (first150 as NSString).range(of: "next".localized())
                    
                let attribute = NSMutableAttributedString.init(string: first150)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)
                
                cell.descriptionLB.attributedText = attribute
            }
            else {
                cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc
            }
            cell.date.text = item.share_publication_date
            cell.playButton.tag = indexPath.row - headerOffset
            cell.playButton.addTarget(self, action:#selector(playVideo), for: .touchUpInside)
            let asset = AVAsset(url:URL(string: item.file_list[self.newsSequance[indexPath.row - headerOffset].fileIndex].file_url.encodeUrl()!)!)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            let time = CMTimeMake(1, 60)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {
                    cell.previewImage.image = frameImg
                })
                
            }
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row - headerOffset
            return cell
         }
         else if (newsSequance[indexPath.row - headerOffset].newsType == .shareLink){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedLinkContentCell", for: indexPath) as! SharedLinkContentCell
            let descStr = item.share_publication_desc
            
            if (descStr.count > 150){
                    let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                
                    let range = (first150 as NSString).range(of: "next".localized())
                    
                let attribute = NSMutableAttributedString.init(string: first150)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)
                
                cell.descriptionLB.attributedText = attribute
            }
            else {
                cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc
            }
            cell.date.text = item.share_publication_date
            cell.titleLb.text = item.share_meta_tagsTitle
            cell.descriptionLb.text = item.share_meta_tagsDescription
            let removeSymbols =  item.share_meta_tagsImage.replacingOccurrences(of: "\"", with: "")
            if (!item.meta_tagsImage.isEmpty){
                cell.linkImage.kf.setImage(with: URL(string: removeSymbols.encodeUrl()!), placeholder: nil)

                let heightInPixels =  newsSequance[indexPath.row - headerOffset].imageHeight
                
                let widthInPixels = newsSequance[indexPath.row - headerOffset].imageWidth
                let myViewWidth = self.tableView.frame.size.width
                
                let ratio = Double(myViewWidth)/Double(widthInPixels)
                let scaledHeight = Double(heightInPixels) * ratio
                  if (heightInPixels != 0 && widthInPixels != 0){
                cell.imageHeight.constant = CGFloat(scaledHeight)
                cell.layoutIfNeeded()
                }
            }
            else {
                cell.imageHeight.constant = 0
                cell.layoutIfNeeded()
            }
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row - headerOffset
            
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: item.share_publication_desc, options: [], range: NSRange(location: 0, length: item.publication_desc.utf16.count))
            
            for match in matches {
                guard let range = Range(match.range, in: item.share_publication_desc) else { continue }
                let url = item.share_publication_desc[range]
                if (url.contains("youtu")){
                    print("ararraar ",url)
                    cell.playButton.isHidden = false
                    cell.youtubePlayer.isHidden  = true
                }
                else {
                    cell.playButton.isHidden = true
                    cell.youtubePlayer.isHidden  = true
                }
            }
            return cell
         }
         else if (newsSequance[indexPath.row - headerOffset].newsType == .shareMultipleImage){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let image1 = newsSequance[indexPath.row - headerOffset].multipleIndexs[0]
            let image2 = newsSequance[indexPath.row - headerOffset].multipleIndexs[1]
            let cell = tableView.dequeueReusableCell(withIdentifier: "SharedMultipleImageContentCell", for: indexPath) as! SharedMultipleImageContentCell
            let descStr = item.share_publication_desc
            
            if (descStr.count > 150){
                    let first150 = String(descStr.prefix(150)) + " " + "next".localized()
                
                    let range = (first150 as NSString).range(of: "next".localized())
                    
                let attribute = NSMutableAttributedString.init(string: first150)
                attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 57/255, green: 59/255, blue: 61/255, alpha: 1.0) , range: range)
                
                cell.descriptionLB.attributedText = attribute
            }
            else {
                cell.descriptionLB.text = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index].share_publication_desc
            }
            cell.date.text = item.share_publication_date
            cell.image1.kf.setImage(with: URL(string: item.file_list[image1].file_url.encodeUrl()!), placeholder: nil)
            cell.image2.kf.setImage(with: URL(string: item.file_list[image2].file_url.encodeUrl()!), placeholder: nil)
            if (newsSequance[indexPath.row - headerOffset].multipleIndexs.count > 2){
                cell.moreLabel.text = "+\(newsSequance[indexPath.row - headerOffset].multipleIndexs.count - 2)"
                
            }
            else {
                cell.moreBackground.isHidden = true
            }
            
            cell.Name.text = item.share_nickname
            cell.date.text = item.publication_date
            if item.avatar.isEmpty {
                if let firstLetter = item.share_nickname.first {
                    let prefix = firstLetter.description.capitalized
                    cell.Name.text = prefix
                }
            }
            let gradientImage = #imageLiteral(resourceName: "bg_gradient_3")
            cell.avatarView.kf.setImage(with: URL(string: item.share_avatar.encodeUrl()!), placeholder: gradientImage)
            cell.avatarView.layer.cornerRadius = cell.avatarView.frame.width/2
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedShare))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row - headerOffset
            return cell
         }
         else if (newsSequance[indexPath.row - headerOffset].newsType == .banner){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerContentCell", for: indexPath) as! BannerContentCell
            let item = newsSequance[indexPath.row - headerOffset].index
            cell.imageContent.kf.setImage(with: URL(string: self.viewModel.bannerData[item].banner_image.encodeUrl()!), placeholder: nil)
            cell.bannerTitleLB.text = self.viewModel.bannerData[item].banner_name
              return cell
         }
        else {

            let cell = tableView.dequeueReusableCell(withIdentifier: "FooterMenuCell", for: indexPath)
            
            // Configure the cell...
            
            return cell
        }
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("afafafaf",self.author_id , "   " ,self.viewModelProfile.user_id.value)
        self.tableView.deselectRow(at: indexPath, animated: true)
        if userInfo.count != 0  && indexPath.row - 2 <= userInfo.count - 1 && (indexPath.row - 2)>=0 && userInfo[indexPath.row - 2] == "show"{
                headerOffset -= self.userInfo.count
                self.userInfo.removeAll()
            
                if (!self.viewModelAlien.city.value.isEmpty){
                    self.userInfo.append("city")
                    self.headerOffset += 1
                }
                if (!self.viewModelAlien.work.value.isEmpty){
                    self.userInfo.append("work")
                    self.headerOffset += 1
                }
                if (!self.viewModelAlien.university.value.isEmpty){
                    self.userInfo.append("university")
                    self.headerOffset += 1
                }
                if (!self.viewModelAlien.family.value.isEmpty){
                    self.userInfo.append("family")
                    self.headerOffset += 1
                }
                if (!self.viewModelAlien.language.value.isEmpty){
                    self.userInfo.append("language")
                    self.headerOffset += 1
                }
                    print("qfqfqfqqqf" , self.userInfo.count)
            
                print("xcxxccx" , self.userInfo)
            
                   print("vcxxc" , self.headerOffset)
                    self.tableView.reloadData()
                DispatchQueue.main.async {
                    let index = IndexPath(row: 3, section: 0)
                    self.tableView.scrollToRow(at: index,at: .middle, animated: true)
                }
            }
        
         else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .header){
           let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = NewsControllerDetail.instantiate()
          //  self.isCreateOpened = true
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = item
            vc.viewModelProfile = self.viewModelProfile
            self.navigationController?.pushViewController(vc, animated: true)
          
        }
            else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .text){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            if (item.is_has_link == 1){
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                let matches = detector.matches(in: item.publication_desc, options: [], range: NSRange(location: 0, length: item.publication_desc.utf16.count))

                for match in matches {
                    guard let range = Range(match.range, in: item.publication_desc) else { continue }
                    let url = item.publication_desc[range]
                    guard let urlOpen = URL(string: String(url)) else { return }
                    UIApplication.shared.open(urlOpen)
                }
            }
            else {
                let vc = NewsControllerDetail.instantiate()
             //   self.isCreateOpened = true
                vc.hidesBottomBarWhenPushed = true
                vc.newsHistory = item
                vc.viewModelProfile = self.viewModelProfile
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
            else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .image){
                let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
                let photo = SKPhoto.photoWithImageURL(item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_url.encodeUrl()!)
                let browser = SKPhotoBrowser(photos: [photo])
                browser.initializePageIndex(0)
                SKPhotoBrowserOptions.displayGallery = false
                self.present(browser, animated: true, completion: nil)
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .footer){
           
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .audio){
          
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .file){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = SFSafariViewController(url: URL(string:item.file_list[newsSequance[indexPath.row - headerOffset].fileIndex].file_url.encodeUrl()!)!)
            self.present(vc, animated: true, completion: nil)
           
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .banner){
            let item = newsSequance[indexPath.row - headerOffset].index
            let vc = SFSafariViewController(url: URL(string:self.viewModel.bannerData[item].website.encodeUrl()!)!)
            self.present(vc, animated: true, completion: nil)
            
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .link){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: item.publication_desc, options: [], range: NSRange(location: 0, length: item.publication_desc.utf16.count))
            for match in matches {
                guard let range = Range(match.range, in: item.publication_desc) else { continue }
                let url = item.publication_desc[range]
                if (url.contains("youtu")){
                    let vc = NewsControllerDetail.instantiate()
                    //  self.isCreateOpened = true
                    vc.hidesBottomBarWhenPushed = true
                    vc.newsHistory = item
                    vc.viewModelProfile = self.viewModelProfile
                    self.navigationController?.pushViewController(vc, animated: true)
                    break
                }
                else {
                    let removeSymbols =  item.meta_tagsImage.replacingOccurrences(of: "\"", with: "")
                    let photo = SKPhoto.photoWithImageURL(removeSymbols.encodeUrl()!)
                    let browser = SKPhotoBrowser(photos: [photo])
                    browser.initializePageIndex(0)
                    SKPhotoBrowserOptions.displayGallery = false
                    self.present(browser, animated: true, completion: nil)
                    break
                }
            }
            
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .video){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = NewsControllerDetail.instantiate()
            self.isCreateOpened = true
          //  vc.newsHistory = item
            vc.viewModelProfile = self.viewModelProfile
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .showComments){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = NewsControllerDetail.instantiate()
         //   self.isCreateOpened = true
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = item
            vc.viewModelProfile = self.viewModelProfile
            vc.showComments = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else  if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .multiple){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
          //  let browser = SKPhotoBrowser(photos: images)
          //  for item.file_list.count {}
            var images = [SKPhoto]()
            
            for index1 in 0..<item.file_list.count {
                let photo = SKPhoto.photoWithImageURL(item.file_list[index1].file_url.encodeUrl()!)
                photo.shouldCachePhotoURLImage = true
                images.append(photo)
            }
            
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(0)
            SKPhotoBrowserOptions.displayGallery = false
            self.present(browser, animated: true, completion: nil)
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .shareDesk){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = NewsControllerDetail.instantiate()
         //   self.isCreateOpened = true
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = item
            vc.getShareData = true
            vc.viewModelProfile = self.viewModelProfile
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .shareImage){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = NewsControllerDetail.instantiate()
         //   self.isCreateOpened = true
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = item
            vc.getShareData = true
            vc.viewModelProfile = self.viewModelProfile
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .shareFile){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = NewsControllerDetail.instantiate()
         //   self.isCreateOpened = true
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = item
            vc.getShareData = true
            vc.viewModelProfile = self.viewModelProfile
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .shareLink){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: item.share_publication_desc, options: [], range: NSRange(location: 0, length: item.share_publication_desc.utf16.count))
            for match in matches {
                guard let range = Range(match.range, in: item.share_publication_desc) else { continue }
                let url = item.share_publication_desc[range]
                if (url.contains("youtu")){
                    let vc = NewsControllerDetail.instantiate()
                    //  self.isCreateOpened = true
                    vc.hidesBottomBarWhenPushed = true
                    vc.newsHistory = item
                    vc.viewModelProfile = self.viewModelProfile
                    self.navigationController?.pushViewController(vc, animated: true)
                    break
                }
                else {
                    let vc = NewsControllerDetail.instantiate()
                    //      self.isCreateOpened = true
                    vc.hidesBottomBarWhenPushed = true
                    vc.newsHistory = item
                    vc.getShareData = true
                    vc.viewModelProfile = self.viewModelProfile
                    self.navigationController?.pushViewController(vc, animated: true)
                    break
                }
            }
        }
        else if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .shareVideo){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = NewsControllerDetail.instantiate()
        //    self.isCreateOpened = true
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = item
            vc.getShareData = true
            vc.viewModelProfile = self.viewModelProfile
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else  if ((indexPath.row - headerOffset) >= 0) && (newsSequance[indexPath.row - headerOffset].newsType == .shareMultipleImage){
            let item = self.viewModel.dialogs![newsSequance[indexPath.row - headerOffset].index]
            let vc = NewsControllerDetail.instantiate()
       //     self.isCreateOpened = true
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = item
            vc.getShareData = true
            vc.viewModelProfile = self.viewModelProfile
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @objc func avatarClicked(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
          let item = self.viewModel.dialogs![newsSequance[myIndexPath.row].index]
        let vc = NewsController()
        vc.author_id = item.user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func avatarClickedShare(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row].index]
        let vc = NewsController()
        vc.author_id = item.share_user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func addFriendAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: 0, section: 0)
        let cell = self.tableView.cellForRow(at: myIndexPath as IndexPath) as! HeaderTableViewCell
       self.viewModel.sendFriendRequest(partner_id: viewModelAlien.user_id.value,success: {
        if (self.viewModelAlien.is_friend.value != 0){
            self.viewModelAlien.is_i_subscribe.value = 1
            cell.addFriendButton.setTitle("add_to_friends".localized(), for: .normal)
        }
        else {
            self.viewModelAlien.is_i_subscribe.value = 0
            cell.addFriendButton.setTitle("delete_from_friends".localized(), for: .normal)
        }
        })
    }
    @objc func subscribeAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: 0, section: 0)
        let cell = self.tableView.cellForRow(at: myIndexPath as IndexPath) as! HeaderTableViewCell
        self.viewModel.sendFriendRequest(partner_id: viewModelAlien.user_id.value,success: {
            if (self.viewModelAlien.is_i_subscribe.value != 0){
                self.viewModelAlien.is_i_subscribe.value = 1
                cell.subscribeButton.setTitle("subscribe".localized(), for: .normal)
            }
            else {
                self.viewModelAlien.is_i_subscribe.value = 0
                cell.subscribeButton.setTitle("unsubscribe".localized(), for: .normal)
            }
        })
    }
    @objc func pickerAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row].index]
        let removeAction = UIAlertAction(title: "editing".localized(), style: .default, handler: { _ in
            let vc = CreateNewsController.instantiate()
            vc.avatarUrl = self.viewModelProfile.avatar.value
            vc.userName = self.viewModelProfile.name.value
            vc.publication_id = item.publication_id
            vc.newsHistory = item
            vc.profile = self.viewModelProfile
            vc.hidesBottomBarWhenPushed = true
         //   self.isCreateOpened = true
            self.navigationController?.pushViewController(vc, animated: true)
        })
        let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
        
        UIPasteboard.general.string =  item.publication_desc
            
            
        })
        let changeAction = UIAlertAction(title: "delete_post".localized(), style: .default, handler: { _ in
            
            self.viewModel.removeNews(publication_id: item.publication_id) { [weak self] in
                let realm = try! Realm()
                if let existingItem = NewsController.getCurrentObject(publication_id: item.publication_id) {
                    try! realm.write {
                       realm.delete(existingItem)
                    }
                }
                self!.viewModel.dialogs?.removeAll()
                self!.bathCount = 1
                self!.currentOffset = 0
                self!.headerOffset = 0
                self!.bannerDataIndex = 0
                self!.bannerIndex = 0
                if (self!.author_id != 0){
                    self!.headerOffset = 3
                }
                if (self!.viewModelProfile.user_id.value == self!.author_id){
                    self!.headerOffset = 4
                     self!.getAlienInfo()
                }
                self!.newsSequance.removeAll()
                self!.loadData()
            }
            
            
        })
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(changeAction)
        alert.addAction(copyAction)
        alert.addAction(removeAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.present(alert, animated: true, completion: nil)
    }
    @objc func goToComment(_ sender: UIButton) {
         let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        let vc = NewsControllerDetail.instantiate()
     //   self.isCreateOpened = true
        vc.hidesBottomBarWhenPushed = true
        vc.newsHistory = item
        vc.viewModelProfile = self.viewModelProfile
        vc.showComments = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func reportAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myAlert = storyboard.instantiateViewController(withIdentifier: "REPORTSCONTROLLER") as! ReportViewController
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            myAlert.publication_id = item.publication_id
            self.present(myAlert, animated: true, completion: nil)
    }
    @objc func shareAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "SHARECONTROLLER") as! ShareViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.publication_id = item.publication_id
        myAlert.parentController = self
        self.present(myAlert, animated: true, completion: nil)
    }
    @objc func pickerAlienAction(_ sender: UIButton) {
        let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row].index]
        let copyAction = UIAlertAction(title: "copy".localized(), style: .default, handler: { _ in
            
            UIPasteboard.general.string =  item.publication_desc
            
            
        })
        let reportAction = UIAlertAction(title: "report".localized(), style: .default, handler: { _ in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let myAlert = storyboard.instantiateViewController(withIdentifier: "REPORTSCONTROLLER") as! ReportViewController
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            myAlert.publication_id = item.publication_id
            self.present(myAlert, animated: true, completion: nil)
            
        })
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(copyAction)
        alert.addAction(reportAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.present(alert, animated: true, completion: nil)
    }
    @objc func startChatAction(_ sender: UIButton) {
        let item = DialogItem()
        item.dialog_id   = "\(viewModelAlien.user_id.value)"
        item.chat_id     = viewModelAlien.user_id.value
        item.user_id     = viewModelAlien.user_id.value
        item.chat_name   = viewModelAlien.nickname.value
        item.avatar      = viewModelAlien.avatar.value
        item.phone       = viewModelAlien.phone.value
        item.contact_user_name = viewModelAlien.nickname.value
        item.chat_date   = viewModelAlien.last_visit.value.replacingOccurrences(of: "был(а) ", with: "")
        let chatInfo = DialogInfo(dialog: item)
        let vc = ChatController(type: .single, info: chatInfo ,dialogId: "\(self.viewModelAlien.user_id.value)" + "U")
        vc.hidesBottomBarWhenPushed = true
        self.show(vc, sender: nil)
    }
    @objc func commentAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        let vc = NewsControllerDetail.instantiate()
      //  self.isCreateOpened = true
        vc.hidesBottomBarWhenPushed = true
        vc.newsHistory = item
        vc.viewModelProfile = self.viewModelProfile
        vc.showComments = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func likeAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
         let cell = self.tableView.cellForRow(at: myIndexPath as IndexPath) as! FooterMenuCell
        viewModel.likeNews(publication_id:item.publication_id) { [weak self] in
            if (item.is_i_liked == 0){
            guard let vc = self else { return }
            cell.like_btn.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                cell.like_btn.setTitle("\(self!.viewModel.dialogs![self!.newsSequance[myIndexPath.row - self!.headerOffset].index].like_count + 1)", for: .normal)
            }
            else {
                cell.like_btn.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                cell.like_btn.setTitle("\(self!.viewModel.dialogs![self!.newsSequance[myIndexPath.row - self!.headerOffset].index].like_count - 1)", for: .normal)
            }
            let realm = try! Realm()
            if let existingItem = NewsController.getCurrentObject(publication_id: item.publication_id) {

                    try! realm.write {
                        if (existingItem.is_i_liked == 0 ){
                          existingItem.is_i_liked = 1
                         existingItem.like_count += 1
                        }
                        else {
                            existingItem.is_i_liked = 0
                            existingItem.like_count -= 1
                        }
                    }
            }
        }
    }
    @objc func playVideo(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let item = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        let videoURL = URL(string: item.file_list[self.newsSequance[myIndexPath.row - headerOffset].fileIndex].file_url.encodeUrl()!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
    @objc func playAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = self.tableView.cellForRow(at: myIndexPath as IndexPath) as! AudioContentCell
         var item = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        if let i = self.viewModel.realmDialog?.index(where: { $0.publication_id == item.publication_id }) {
            item = self.viewModel.realmDialog![i]
            }
            if item.file_list[newsSequance[myIndexPath.row - headerOffset].fileIndex].file_url.contains("https"){
                cell.loader.startAnimating()
                cell.playButton.isHidden = true
                downloadFileFromURL(audioUrl: item.file_list[newsSequance[myIndexPath.row - headerOffset].fileIndex].file_url, publication_id: item.publication_id,myIndexPath:myIndexPath,requestSuccess: { [weak self] () in
                    guard let vc = self else { return }
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    cell.loader.stopAnimating()
                    cell.playButton.isHidden = false
                    })
                    
        }
            else {
      if (cell.audioPlayer != nil){
            if (cell.audioPlayer.isPlaying){
                let origImage = UIImage(named: "icon_mini_play")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
               cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                cell.audioPlayer.pause()
            }
            else {
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(item.file_list[newsSequance[myIndexPath.row - headerOffset].fileIndex].file_url).csv")
                cell.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                cell.audioPlayer.prepareToPlay()
                cell.audioPlayer.delegate = cell
                cell.audioPlayer.play()
                let origImage = UIImage(named: "icon_mini_pause")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
        }
        else {

        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(item.file_list[newsSequance[myIndexPath.row - headerOffset].fileIndex].file_url).csv")
            cell.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
            cell.audioPlayer.prepareToPlay()
            cell.audioPlayer.delegate = cell
            cell.audioPlayer.play()
            let origImage = UIImage(named: "icon_mini_pause")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.playButton.setImage(tintedImage, for: .normal)
            cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
        }
                }
        }
    }
    func downloadFileFromURL(audioUrl:String,publication_id:Int,myIndexPath:NSIndexPath, requestSuccess: @escaping() -> Void) {

        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]

            documentsURL.appendPathComponent("p_\(publication_id).csv")
            return (documentsURL, [.removePreviousFile])
        }
        AF.download(audioUrl.encodeUrl()!, to: destination).response { response in
            let realm = try! Realm()
            if let existingItem = NewsController.getCurrentObject(publication_id: publication_id) {
                
                if (existingItem.file_list[
                    self.newsSequance[myIndexPath.row - self.headerOffset].fileIndex].file_url.contains("https")){
                    try! realm.write {
                        let file = ChatFile()
                        file.file_url = "p_\(publication_id)"
                        file.file_data = existingItem.file_list[self.newsSequance[myIndexPath.row - self.headerOffset].fileIndex].file_data
                        file.file_name = existingItem.file_list[self.newsSequance[myIndexPath.row - self.headerOffset].fileIndex].file_name
                        file.file_format = existingItem.file_list[self.newsSequance[myIndexPath.row - self.headerOffset].fileIndex].file_format
                        file.file_time = existingItem.file_list[self.newsSequance[myIndexPath.row - self.headerOffset].fileIndex].file_time
                        existingItem.file_list[self.newsSequance[myIndexPath.row - self.headerOffset].fileIndex] = file
                        
                    }
                }
                
            }
            requestSuccess()
       //     self.activityIndicator.stopAnimating()
      //      self.playIconNode.isHidden = false
      //      self.playIconNode.image = UIImage(named: "icon_mini_play")
        }
    }
    @objc func playAudioCommentAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = self.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentAudioCell
        let comment = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        if comment.comment_file_list[0].file_url.contains("https"){
            cell.loader.startAnimating()
            cell.playButton.isHidden = true
            downloadAudio(url: comment.comment_file_list[0].file_url, comment_id: comment.comment_id,myIndexPath:myIndexPath,requestSuccess: { [weak self] () in
                guard let vc = self else { return }
                let origImage = UIImage(named: "icon_mini_play")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.playButton.setImage(tintedImage, for: .normal)
                cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                cell.loader.stopAnimating()
                cell.playButton.isHidden = false
            })
            
        }
        else {
            if (cell.audioPlayer != nil){
                if (cell.audioPlayer.isPlaying){
                    let origImage = UIImage(named: "icon_mini_play")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    cell.audioPlayer.pause()
                }
                else {
                    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                        let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(comment.comment_file_list[0].file_url).csv")
                        cell.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                        cell.audioPlayer.prepareToPlay()
                        cell.audioPlayer.delegate = cell
                        cell.audioPlayer.play()
                        let origImage = UIImage(named: "icon_mini_pause")
                        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        cell.playButton.setImage(tintedImage, for: .normal)
                        cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                    }
                }
            }
            else {
                
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let audioURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(comment.comment_file_list[0].file_url).csv")
                    cell.audioPlayer = try! AVAudioPlayer(contentsOf:audioURL.absoluteURL)
                    cell.audioPlayer.prepareToPlay()
                    cell.audioPlayer.delegate = cell
                    cell.audioPlayer.play()
                    let origImage = UIImage(named: "icon_mini_pause")
                    let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                    cell.playButton.setImage(tintedImage, for: .normal)
                    cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
                }
            }
        }
        
    }
    @objc func likeComment(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let comment = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        let type = newsSequance[myIndexPath.row - headerOffset].newsType
        viewModel.likeComment(comment_id:comment.comment_id) { [weak self] in
            guard let vc = self else { return }
            var cell = self!.tableView.cellForRow(at: myIndexPath as IndexPath)
            
            switch type {
            case .commentText:
                let cell = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentTextCell
                
                if (comment.comment_is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count - 1)", for: .normal)
                }
            case .commentFile:
                let cell  = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentsFileCell
                if (comment.comment_is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count - 1)", for: .normal)
                }
            case .commentAudio:
                let cell  = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentAudioCell
                if (comment.comment_is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count - 1)", for: .normal)
                }
            case .commentImage:
                let cell  = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentImageCell
                if (comment.comment_is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count - 1)", for: .normal)
                }
            case .commentVideo:
                let cell  = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentsVideoCell
                if (comment.comment_is_liked == 0){
                    guard let vc = self else { return }
                    cell.likeButton.setImage(UIImage(named: "icon_heart_liked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count + 1)", for: .normal)
                }
                else {
                    cell.likeButton.setImage(UIImage(named:"icon_heart_unliked"), for: .normal)
                    cell.likeButton.setTitle("\(comment.comment_like_count - 1)", for: .normal)
                }
            default:
                cell = self!.tableView.cellForRow(at: myIndexPath as IndexPath) as! CommentsVideoCell
                
            }
            let realm = try! Realm()
            if let existingItem = NewsController.getCurrentObject(publication_id: comment.publication_id) {
                
                try! realm.write {
                    if (existingItem.comment_is_liked == 0 ){
                        existingItem.comment_is_liked = 1
                        existingItem.comment_like_count += 1
                    }
                    else {
                        existingItem.comment_is_liked = 0
                        existingItem.comment_like_count -= 1
                    }
                }
            }
         
            
        }
    }
    @objc func CommentVideoAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let comment = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        let videoURL = URL(string: comment.comment_file_list[0].file_url.encodeUrl()!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    @objc func CommentImageAction(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        let comment = self.viewModel.dialogs![newsSequance[myIndexPath.row - headerOffset].index]
        let photo = SKPhoto.photoWithImageURL(comment.comment_file_list[0].file_url.encodeUrl()!)
        let browser = SKPhotoBrowser(photos: [photo])
        browser.initializePageIndex(0)
        SKPhotoBrowserOptions.displayGallery = false
        self.present(browser, animated: true, completion: nil)
    }
    func downloadAudio(url: String,comment_id: Int,myIndexPath:NSIndexPath, requestSuccess: @escaping() -> Void) {
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("audioFilesC\(comment_id).csv")
            return (documentsURL, [.removePreviousFile])
        }
        AF.download(url.encodeUrl()!, to: destination).response { response in
    self.viewModel.newsCommentsData[self.newsSequance[myIndexPath.row].index].file_list[0].file_url = "audioFilesC\(comment_id)"
            requestSuccess()
            
        }
    }
    @objc func headerAvatarImageView(_ sender: UITapGestureRecognizer) {
        let photo = SKPhoto.photoWithImageURL(viewModelAlien.avatar.value.encodeUrl()!)
        let browser = SKPhotoBrowser(photos: [photo])
        browser.initializePageIndex(0)
        SKPhotoBrowserOptions.displayGallery = false
        self.present(browser, animated: true, completion: nil)
    }
    @objc func headerBackgroundImageView(_ sender: UITapGestureRecognizer) {
        let photo = SKPhoto.photoWithImageURL(viewModelAlien.background.value.encodeUrl()!)
        let browser = SKPhotoBrowser(photos: [photo])
        browser.initializePageIndex(0)
        SKPhotoBrowserOptions.displayGallery = false
        self.present(browser, animated: true, completion: nil)
    }
       override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = self.newsSequance.count - 1 + headerOffset
        if indexPath.row  == lastElement {
            if (!isLoading) {
                bathCount += 1
                self.loadData()
            }
        }
    }
    static func getCurrentObject(publication_id: Int) -> NewsHistory? {
        let realm = try! Realm()
        let obj = realm.objects(NewsHistory.self).filter("publication_id = \(publication_id)")
        return obj.first
    }
    @objc func createNews(_ sender: UITapGestureRecognizer) {
        let vc = CreateNewsController.instantiate()
        vc.avatarUrl = self.viewModelProfile.avatar.value
        vc.userName = self.viewModelProfile.name.value
        vc.hidesBottomBarWhenPushed = true
        isCreateOpened = true
        vc.profile = self.viewModelProfile
        self.navigationController?.pushViewController(vc, animated: true)
     //   vc.newsHistory = item
    }
}
