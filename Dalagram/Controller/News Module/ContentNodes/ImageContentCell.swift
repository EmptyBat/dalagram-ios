//
//  ImageCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/28/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class ImageContentCell: UITableViewCell {
    @IBOutlet weak var ImageContent: UIImageView!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
