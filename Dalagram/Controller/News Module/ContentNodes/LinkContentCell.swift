//
//  LinkContentCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/3/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import YouTubePlayer
class LinkContentCell: UITableViewCell {
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var youtubePlayer: YouTubePlayerView!
    @IBOutlet weak var linkImage: UIImageView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var descriptionLb: UILabel!

    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
