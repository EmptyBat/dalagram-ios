//
//  MultipleImageContentCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/4/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class MultipleImageContentCell: UITableViewCell {

    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var moreBackground: UIView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var imageHeight2: NSLayoutConstraint!
    @IBOutlet weak var imageHeight1: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
