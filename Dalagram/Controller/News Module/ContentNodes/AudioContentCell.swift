
//
//  AudioContentCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/2/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import AVFoundation
class AudioContentCell: UITableViewCell ,AVAudioPlayerDelegate {
    var audioPlayer : AVAudioPlayer!
    var audioUrl: URL? {
        didSet {
        }
    }
    var fileUrl = ""
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var playButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        let origImage = UIImage(named: "icon_mini_play")
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        playButton.setImage(tintedImage, for: .normal)
        playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
    }
}
