//
//  FriendsCollectionViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 11/2/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class FriendsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var avatarIV: UIImageView!

    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarIV.layer.cornerRadius = avatarIV.frame.width/2
    }
}
