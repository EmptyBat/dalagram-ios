//
//  FooterMenuCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/28/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class FooterMenuCell: UITableViewCell {
    @IBOutlet weak var view_height: NSLayoutConstraint!
    @IBOutlet weak var like_btn: UIButton!
    @IBOutlet weak var comments_btn: UIButton!
    @IBOutlet weak var share_btn: UIButton!
    @IBOutlet weak var visible_btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        like_btn.layer.borderWidth = 1
        like_btn.layer.borderColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0).cgColor
        
        comments_btn.layer.borderWidth = 1
        comments_btn.layer.borderColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0).cgColor
        
        share_btn.layer.borderWidth = 1
        share_btn.layer.borderColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0).cgColor
        
        visible_btn.layer.borderWidth = 1
        visible_btn.layer.borderColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1.0).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
