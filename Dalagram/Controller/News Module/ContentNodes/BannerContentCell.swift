//
//  BannerContentCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/30/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit

class BannerContentCell: UITableViewCell {
    @IBOutlet weak var bannerTitleLB: UILabel!
    
    @IBOutlet weak var imageContent: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
