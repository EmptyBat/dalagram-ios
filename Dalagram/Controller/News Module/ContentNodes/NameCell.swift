//
//  NameCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/28/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class NameCell: UITableViewCell {
    @IBOutlet weak var public_width: NSLayoutConstraint!
    @IBOutlet weak var publicIV: UIImageView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var avatarView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
