//
//  VideoContentCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/3/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class VideoContentCell: UITableViewCell {
    @IBOutlet weak var previewImage: UIImageView!
    
    @IBOutlet weak var playButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
