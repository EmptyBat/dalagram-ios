//
//  SharedLinkContentCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/13/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit
import YouTubePlayer
class SharedLinkContentCell: UITableViewCell {
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var youtubePlayer: YouTubePlayerView!
    @IBOutlet weak var publication_desk: UILabel!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var avatarView: UIImageView!

    @IBOutlet weak var linkImage: UIImageView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var descriptionLb: UILabel!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
