//
//  SharedMultipleImageContentCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/13/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit

class SharedMultipleImageContentCell: UITableViewCell {
    @IBOutlet weak var publication_desk: UILabel!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var avatarView: UIImageView!

    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var moreBackground: UIView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var imageHeight2: NSLayoutConstraint!
    @IBOutlet weak var imageHeight1: NSLayoutConstraint!
    @IBOutlet weak var descriptionLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
