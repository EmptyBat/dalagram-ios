//
//  NewsController.swift
//  Dalagram
//
//  Created by Toremurat on 11.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift
import SwipeCellKit
import RxSwift
import YouTubePlayer
class NewsController: UITableViewController,UIGestureRecognizerDelegate,YouTubePlayerDelegate {
    var newsSequance = [NewsSequence]()
    var userInfo = [String]()
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var viewModelAlien = ProfileViewModel()
    var viewModel = NewsControllerViewModel(user_id: 0)
    var notificationToken: NotificationToken? = nil
    var bathCount = 1
    var avatarUrl = ""
    var userName = ""
    var isLoading = false
    var isCreateOpened = false
    var editingProfile = false
    var currentOffset = 0
    var author_id = 0
    var headerOffset = 0
    var bannerIndex = 0
    var bannerDataIndex = 0
    //audio player
    lazy var titleView: DialogNavigationTitleView = {
        let titleView = DialogNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    
    
    lazy var navigationTitle: UILabel = {
        let label = UILabel()
        label.text = "News"
        label.font = UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.init(0.01))
        label.textColor = UIColor.white
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = NewsControllerViewModel(user_id: author_id)
        setBlueNavBar()
        configureUI()
        configureNavBar()
        if (author_id != 0){
            headerOffset = 3
            getAlienInfo()
            getGallery()
            getFriends()
        }
        loadBanners()
        setupData()
        configureEvents()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        // this is the replacement of implementing: "collectionView.addSubview(refreshControl)"
        self.tableView.refreshControl = refreshControl
        
        
    }
    private func loadBanners(){
        SVProgressHUD.show()
        isLoading = true
        viewModel.getBanners(city_id: 0) { [weak self] in
            guard let vc = self else { return }
            //     vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            self!.loadData()
        }
    }
    @objc func refresh(refreshControl: UIRefreshControl) {
      //  SVProgressHUD.show()
        isLoading = true
        viewModel.refreshNews(page: bathCount, author_id: author_id) { [weak self] in
            guard let vc = self else { return }
            //     vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            self!.bathCount = 1
            self!.currentOffset = 0
            self!.headerOffset = 0
            if (self!.author_id != 0){
                self!.headerOffset = 3
            }
            self!.bannerDataIndex = 0
            self!.bannerIndex = 0
            if (self!.viewModelProfile.user_id.value == self!.author_id){
                self!.headerOffset = 4
                 self!.getAlienInfo()
            }
            self!.newsSequance.removeAll()
            vc.createSequance()
            refreshControl.endRefreshing()
        }
    }
    
    public func refreshData() {
        //  SVProgressHUD.show()
        isLoading = true
        self.viewModel.dialogs?.removeAll()
        self.bathCount = 1
        self.currentOffset = 0
        self.headerOffset = 0
        self.bannerDataIndex = 0
        self.bannerIndex = 0
        if (author_id != 0){
            headerOffset = 3
        }
        if (viewModelProfile.user_id.value == author_id){
            self.headerOffset = 4
             getAlienInfo()
        }
        self.newsSequance.removeAll()
        viewModel.getUserNews(page: bathCount, author_id: author_id) { [weak self] in
            guard let vc = self else { return }
            //     vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            vc.createSequance()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listenMessageUpdates()
        if (self.newsSequance.count != 0) && isCreateOpened{
            self.bathCount = 1
            self.currentOffset = 0
            self.headerOffset = 0
            self.bannerDataIndex = 0
            self.bannerIndex = 0
            if (author_id != 0){
                headerOffset = 3
            }
            if (viewModelProfile.user_id.value == author_id){
            self.headerOffset = 4
                getAlienInfo()
            }
            self.viewModel.dialogs?.removeAll()
            self.newsSequance.removeAll()
            isCreateOpened = false
            loadData()
            
        }
    }
    deinit {
        notificationToken?.invalidate()
    }
    
    func listenMessageUpdates() {
        // Updating No Content View
        if viewModel.dialogs?.count != 0 {
         //   showNoContentView(dataCount: viewModel.dialogs?.count ?? 0)
        }
        
//        notificationToken = viewModel.dialogs?.observe({ [weak self] (changes: RealmCollectionChange) in
//            guard let tableView = self?.tableView else { return }
//            switch changes {
//            case .initial:
//            //    self!.createSequance()
//                print("initial")
//            case .update(_):
//                print("update")
//               // self?.tableView.reloadData()
//                
//            case .error(let error):
//                fatalError("\(error)")
//            }
//        })
    }
    @objc func getAlienInfo() {
        viewModelAlien.getAlienProfile(user_id: author_id, onCompletion: { [weak self] in
            if (self!.author_id != 0){
                self!.userInfo.removeAll()
                 print("dvsdsdds", self!.headerOffset)
                if (!self!.viewModelAlien.city.value.isEmpty){
                    self!.userInfo.append("city")
                    self!.headerOffset += 1
                }
                if (!self!.viewModelAlien.work.value.isEmpty){
                    self!.userInfo.append("work")
                    self!.headerOffset += 1
                }
                if (!self!.viewModelAlien.university.value.isEmpty){
                    self!.userInfo.append("university")
                    self!.headerOffset += 1
                }
                if (!self!.viewModelAlien.family.value.isEmpty){
                        self!.userInfo.append("family")
                        self!.headerOffset += 1
                }
                if (!self!.viewModelAlien.language.value.isEmpty){
                       self!.userInfo.append("language")
                       self!.headerOffset += 1
                }
                            print("nbnbnb", self!.headerOffset)
                if (self!.headerOffset > 7){
                    if (!self!.viewModelAlien.family.value.isEmpty) || (!self!.viewModelAlien.language.value.isEmpty){
                        self!.userInfo.removeLast()
                        self!.userInfo.removeLast()
                        self!.userInfo.append("show")
                        self!.headerOffset -= 1
                    }
                }
                else if (self!.headerOffset > 6) {
                      if (!self!.viewModelAlien.family.value.isEmpty) || (!self!.viewModelAlien.language.value.isEmpty){
                        self!.userInfo.removeLast()
                        self!.userInfo.append("show")
                    }
                }
            }
            print("affasfasaasasfa", self!.headerOffset)
            print("xxxdds", self!.userInfo.count)
            self?.tableView.reloadData()
        })
    }
    @objc func getGallery() {
        viewModel.getGallery(page: 1, author_id: author_id){ [weak self] in
            self?.tableView.reloadData()
        }
    }
    @objc func getFriends() {
        viewModel.getFriends(author_id: author_id){ [weak self] in
            self?.tableView.reloadData()
        }
        viewModel.getMyFriends(){ [weak self] in
            self?.tableView.reloadData()
        }
    }
    @objc func loadData() {
        SVProgressHUD.show()
        isLoading = true
        viewModel.getUserNews(page: bathCount, author_id: author_id) { [weak self] in
            guard let vc = self else { return }
       //     vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            vc.createSequance()
        }
    }
   
            
    func createSequance(){
        let currentBatchCounter = (bathCount * 10)
        guard currentBatchCounter <= self.viewModel.dialogs!.count || currentBatchCounter == 10  else {
            return
        }
        var newsArray = Array(self.viewModel.dialogs!)
        if (self.viewModel.dialogs!.count - currentBatchCounter >= 10) {
            newsArray = Array(newsArray[currentBatchCounter - 10..<currentBatchCounter  ])
        }
        else {
            newsArray = Array(newsArray[currentBatchCounter - 10..<newsArray.count])
            
        }
        for index1 in 0..<newsArray.count {
            if (self.viewModel.bannerData.count > 0){
            if (bannerIndex == 5){
                let news = NewsSequence()
                news.index = bannerDataIndex
                news.newsType = .banner
                self.newsSequance.append(news)
                if (bannerDataIndex < self.viewModel.bannerData.count - 1){
                    bannerDataIndex += 1
                }
                else {
                    bannerDataIndex = 0
                }
                bannerIndex = 0
            }
        }
            bannerIndex += 1
            
            let news = NewsSequence()
            news.index = index1 + currentOffset
            news.newsType = .header
            self.newsSequance.append(news)
            if (!newsArray[index1].publication_desc.isEmpty){
                let news = NewsSequence()
                news.index = index1 + currentOffset
                news.newsType = .text
                self.newsSequance.append(news)
            }
            if (newsArray[index1].is_has_link == 1){

                let news = NewsSequence()
                let removeSymbols = newsArray[index1].meta_tagsImage.replacingOccurrences(of: "\"", with: "")
                print("qffqfqfqq" ,removeSymbols)
               if (!removeSymbols.isEmpty){
                if let imageSource = CGImageSourceCreateWithURL(URL(string: removeSymbols.encodeUrl()!)! as CFURL, nil) {
                    if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                        let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                        let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                        news.imageWidth = myImageWidth
                        news.imageHeight = myImageHeight
                    }
                }
                }
                news.index = index1 + currentOffset
                news.newsType = .link
                self.newsSequance.append(news)
            }
            if (newsArray[index1].is_share == 1){
                var isMultiple = 0
                var first = true
                if (newsArray[index1].file_list.count == 0){
                    if (!newsArray[index1].share_publication_desc.isEmpty){
                        let news = NewsSequence()
                        news.index = index1 + currentOffset
                        news.newsType = .shareDesk
                        self.newsSequance.append(news)
                    }
                }
                if (newsArray[index1].share_is_has_link == 1){
                    
                    let news = NewsSequence()
                    let removeSymbols = newsArray[index1].meta_tagsImage.replacingOccurrences(of: "\"", with: "")
                    if (!removeSymbols.isEmpty){
                        if let imageSource = CGImageSourceCreateWithURL(URL(string: removeSymbols.encodeUrl()!)! as CFURL, nil) {
                            if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                                let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                                let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                                news.imageWidth = myImageWidth
                                news.imageHeight = myImageHeight
                            }
                        }
                    }
                    news.index = index1 + currentOffset
                    news.newsType = .shareLink
                    self.newsSequance.append(news)
                }
                for index3 in 0..<newsArray[index1].file_list.count {
                    let file_format = newsArray[index1].file_list[index3].file_format
                    if (file_format == "image") {
                        isMultiple += 1
                    }
                }
                var multiplesIndex = [Int]()
                for index2 in 0..<newsArray[index1].file_list.count {
                    let file_format = newsArray[index1].file_list[index2].file_format
                    
                    
                    if (file_format == "audio"){
                        let news = NewsSequence()
                        news.index = index1 + currentOffset
                        if (first == true){
                            news.newsType = .shareAudio
                            first = false
                        }
                        else {
                            news.newsType = .audio
                        }
                        news.fileIndex = index2
                        self.newsSequance.append(news)
                    }
                    else if (file_format == "image") {
                        if (isMultiple > 1){
                            print("asfsafsfafs",isMultiple , index2,index1)
                            if ((isMultiple - 1) == index2){
                                multiplesIndex.append(index2)
                                let news = NewsSequence()
                                news.index = index1 + currentOffset
                                if (first == true){
                                    news.newsType = .shareMultipleImage
                                    first = false
                                }
                                else {
                                    news.newsType = .multiple
                                }
                                news.fileIndex = index2
                                news.multipleIndexs = multiplesIndex
                                self.newsSequance.append(news)
                                print("afsfsafsafa",multiplesIndex)
                            }
                            else {
                                multiplesIndex.append(index2)
                                
                            }
                        }
                        else {
                            let news = NewsSequence()
                            news.index = index1 + currentOffset
                            if (first == true){
                                news.newsType = .shareImage
                                first = false
                            }
                            else {
                                news.newsType = .image
                            }
                            print("afafafafaf", news.newsType)
                                     print("xvvxvx",newsArray[index1].file_list[index2].file_url)
                            if let imageSource = CGImageSourceCreateWithURL(URL(string: newsArray[index1].file_list[index2].file_url.encodeUrl()!)! as CFURL, nil) {
                                if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                                    let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                                    let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                                     news.imageWidth = myImageWidth
                                    news.imageHeight = myImageHeight
                                }
                            }
                            news.fileIndex = index2
                            self.newsSequance.append(news)
                        }
                    }
                    else if (file_format == "video") {
                        let news = NewsSequence()
                        news.index = index1 + currentOffset
                        if (first == true){
                            news.newsType = .shareVideo
                            first = false
                        }
                        else {
                            news.newsType = .video
                        }
                        news.fileIndex = index2
                        self.newsSequance.append(news)
                    }
                    else if (file_format == "file") {
                        let news = NewsSequence()
                        news.index = index1 + currentOffset
                        if (first == true){
                            news.newsType = .shareFile
                            first = false
                        }
                        else {
                            news.newsType = .file
                        }
                        news.fileIndex = index2
                        self.newsSequance.append(news)
                    }
                }
                
            }
            else {
                var isMultiple = 0
            for index3 in 0..<newsArray[index1].file_list.count {
                
                let file_format = newsArray[index1].file_list[index3].file_format
                if (file_format == "image") {
                    isMultiple += 1
                }
            }
            var multiplesIndex = [Int]()
            for index2 in 0..<newsArray[index1].file_list.count {
                let file_format = newsArray[index1].file_list[index2].file_format
                
                if (file_format == "audio"){
                    let news = NewsSequence()
                    news.index = index1 + currentOffset
                    news.newsType = .audio
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
                else if (file_format == "image") {
                    if (isMultiple > 1){
                        if ((isMultiple - 1) == index2){
                            multiplesIndex.append(index2)
                            let news = NewsSequence()
                            news.index = index1 + currentOffset
                            news.newsType = .multiple
                            news.fileIndex = index2
                            news.multipleIndexs = multiplesIndex
                            self.newsSequance.append(news)
                            print("afsfsafsafa",multiplesIndex)
                        }
                        else {
                        multiplesIndex.append(index2)
                          
                        }
                    }
                    else {
                    let news = NewsSequence()
                        if let imageSource = CGImageSourceCreateWithURL(URL(string: newsArray[index1].file_list[index2].file_url.encodeUrl()!)! as CFURL, nil) {
                            if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                                let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                                let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                                news.imageWidth = myImageWidth
                                news.imageHeight = myImageHeight
                            }
                        }
                    news.index = index1 + currentOffset
                    news.newsType = .image
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                    }
                }
                else if (file_format == "video") {
                    let news = NewsSequence()
                    news.index = index1 + currentOffset
                    news.newsType = .video
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
                else if (file_format == "file") {
                    let news = NewsSequence()
                    news.index = index1 + currentOffset
                    news.newsType = .file
                    news.fileIndex = index2
                    self.newsSequance.append(news)
                }
            }
            }
            //shared publication
            
            let newsFooter = NewsSequence()
            newsFooter.index = index1 + currentOffset
            newsFooter.newsType = .footer
            self.newsSequance.append(newsFooter)
            
            if (newsArray[index1].comment_is_has_file == 1){
                if (newsArray[index1].comment_file_list[0].file_format == "image"){
                    let news = NewsSequence()
                    news.index = index1 + currentOffset
                    news.newsType = .commentImage
                    news.fileIndex = 0
                    self.newsSequance.append(news)
                }
                else if (newsArray[index1].comment_file_list[0].file_format == "video"){
                    let news = NewsSequence()
                    news.index = index1 + currentOffset
                    news.newsType = .commentVideo
                    news.fileIndex = 0
                    self.newsSequance.append(news)
                }
                else if (newsArray[index1].comment_file_list[0].file_format == "file"){
                    let news = NewsSequence()
                    news.index = index1 + currentOffset
                    news.newsType = .commentFile
                    news.fileIndex = 0
                    self.newsSequance.append(news)
                }
                else if (newsArray[index1].comment_file_list[0].file_format == "audio"){
                    let news = NewsSequence()
                    news.index = index1 + currentOffset
                    news.newsType = .commentAudio
                    news.fileIndex = 0
                    self.newsSequance.append(news)
                }
                let showComment = NewsSequence()
                showComment.index = index1 + currentOffset
                showComment.newsType = .showComments
                showComment.fileIndex = 0
                self.newsSequance.append(showComment)
                
            }
            else {
                print("afsfasfsa",newsArray[index1].comment_count)
                if (newsArray[index1].comment_count > 0){
                let news = NewsSequence()
                news.index = index1 + currentOffset
                news.newsType = .commentText
                self.newsSequance.append(news)
                    
                let showComment = NewsSequence()
                showComment.index = index1 + currentOffset
                showComment.newsType = .showComments
                showComment.fileIndex = 0
                self.newsSequance.append(showComment)
                    
                }
            }
            
        }
        SVProgressHUD.dismiss()
        self.tableView.reloadData()
        self.currentOffset += 10
        isLoading = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension NewsController {
    
    // MARK: - Configuring NavBar TitleView
    
    func configureNavBar() {
        
        let titleItem = UIBarButtonItem(customView: titleView)
        self.navigationItem.leftBarButtonItem = titleItem
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationTitle.text = ""
        
        let imagePlaceholder = #imageLiteral(resourceName: "placeholder")
        titleView.userAvatarView.image = imagePlaceholder
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        titleView.userAvatarView.isUserInteractionEnabled = true
        titleView.userAvatarView.addGestureRecognizer(tapGestureRecognizer)
    }
    // MARK: - NavBar TitleView Action
    
    @objc func userTitleViewPressed() {
        
        let transition = CATransition()
        transition.duration = 0.35
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: avatarUrl.encodeUrl()!), placeholder: nil)
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let user_id = viewModelProfile.user_id.value
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.show(vc, sender: nil)
    }
    /*
     var birth_date   = Variable<String>("")
     var work  = Variable<String>("")
     var language   = Variable<String>("")
     var speciality_id   = Variable<Int>(0)
     var university  = Variable<String>("")
     var family  = Variable<String>("")
     */
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
            self!.userName = self!.viewModelProfile.name.value
            self!.avatarUrl = self!.viewModelProfile.avatar.value
            if (self!.viewModelProfile.user_id.value == self!.author_id){
                if (self!.author_id != 0 && self!.editingProfile == false){
                    self!.headerOffset += 1
                    print("faaafafaf", self!.headerOffset)
                }
                else {
                    self!.editingProfile = false
                }
                print("afafafaf work")
            }

            if (self!.viewModelProfile.user_id.value == self!.author_id){
                let createMenu = UIBarButtonItem(image: UIImage(named: "icon_menu"), style: .plain, target: self, action: #selector(self!.menuAction))
                createMenu.tintColor = UIColor.white
                self!.navigationItem.rightBarButtonItems?.removeAll()
                self!.navigationItem.rightBarButtonItem = createMenu
            }
            else {
                let createSearch = UIBarButtonItem(image: UIImage(named: "icon_search"), style: .plain, target: self, action: #selector(self!.searchAction))
                createSearch.tintColor = UIColor.white
                
                let createShare = UIBarButtonItem(image: UIImage(named: "icon_mbriShare"), style: .plain, target: self, action: #selector(self!.CompanyProductAction))
                createShare.tintColor = UIColor.white
                
                self!.navigationItem.rightBarButtonItems = [createSearch, createShare]
            }
            self!.tableView.reloadData()
            
        })
    }
    func refreshTableView() {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
}
