//
//  CreateNewsController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/8/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import Gallery
import AVFoundation
import Photos
import GrowingTextView
import MobileCoreServices
import ISEmojiView
import Alamofire
import AVKit
import SafariServices
import SKPhotoBrowser
import SVProgressHUD
import RealmSwift
import SwiftyJSON
class CreateNewsController: UITableViewController,GalleryControllerDelegate,EmojiViewDelegate {
    @IBOutlet weak var newsTypeButton: UIButton!
    var keyboardType = false
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var avatarImg: UIImageView!
    var newsSequance = [NewsCreater]()
    var viewModel = NewsControllerViewModel(user_id: 0)
    var avatarUrl = ""
    var userName = ""
    var currentIndex = 0
    var accesId = 1
    var isLoading = false
    var publication_id = 0
    var images = [SKPhoto]()
    var newsHistory :NewsHistory? = nil
    var profile = ProfileViewModel()
    var files: Array<[String: String]> = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "create_new_post".localized()
        self.textView.text = "whats_new".localized()
        self.textView.delegate = self
        self.newsTypeButton.setTitle("access_for_all".localized(), for: .normal)
        // growing textview
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        configureUI()
        createToolbar()
        GalleryConfig()
        if (publication_id != 0 ){
            editPost()
        }
        // *** Hide keyboard when tapping outside ***
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func editPost(){
        self.textView.text = newsHistory?.publication_desc
        for index2 in 0..<newsHistory!.file_list.count {
            let file_format = self.newsHistory!.file_list[index2].file_format
            let file_name = self.newsHistory!.file_list[index2].file_name
            let file_url = self.newsHistory!.file_list[index2].file_url
            let file_time = self.newsHistory!.file_list[index2].file_time
            
            let file = ["file_url": file_url,
                        "file_format": file_format,
                        "file_name": file_name,
                        "file_time": file_time]
            if (file_format == "audio"){
                let news = NewsCreater()
                news.index = index2
                news.flle_url = file_url
                news.newsType = .audio
                news.files.append(file)
                self.files.append(file)
                self.newsSequance.append(news)
            }
            else if (file_format == "image") {
                
                let news = NewsCreater()
                news.index = index2
                news.flle_url = file_url
                news.newsType = .image
                news.files.append(file)
                self.files.append(file)
                self.newsSequance.append(news)
            }
            else if (file_format == "video") {
                let news = NewsCreater()
                news.index = index2
                news.flle_url = file_url
                news.newsType = .video
                news.files.append(file)
                self.files.append(file)
                self.newsSequance.append(news)
            }
            else if (file_format == "file") {
                let news = NewsCreater()
                news.index = index2
                news.flle_url = file_url
                news.newsType = .file
                news.files.append(file)
                self.files.append(file)
                self.newsSequance.append(news)
            }
            
        }
        self.tableView.reloadData()
    }
    func configureUI(){
        self.nameField.text = userName
        avatarImg.kf.setImage(with: URL(string: avatarUrl.encodeUrl()!), placeholder: nil)
        avatarImg.layer.cornerRadius =  avatarImg.frame.width/2
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
        self.navigationItem.rightBarButtonItem = doneButton
            setEmptyBackTitle()
            setBlueNavBar()
            //MARK: - TableView Configurations
        self.tableView.tableHeaderView = headerView
            tableView.registerNib(ContentInputTableViewCell.self)
            tableView.registerNib(NameCell.self)
            tableView.registerNib(DescriptionCell.self)
            tableView.registerNib(ImageContentCell.self)
            tableView.registerNib(FooterMenuCell.self)
            tableView.registerNib(AudioContentCell.self)
            tableView.registerNib(VideoContentCell.self)
            tableView.registerNib(LinkContentCell.self)
            tableView.registerNib(MultipleImageContentCell.self)
            tableView.register(UINib(nibName: "DocumentsCell", bundle: nil), forCellReuseIdentifier: "document_cell")
            tableView.separatorColor = UIColor.init(white: 0.85, alpha: 1.0)
            tableView.allowsSelection = true
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = 44
            tableView.separatorColor = UIColor.clear
            self.tableView.reloadData()
            
        }
    
    func GalleryConfig(){
        // Gallery Picker Configs
        Config.Grid.FrameView.borderColor = UIColor.navBlueColor
        Config.Camera.imageLimit = 6
        Config.initialTab = Config.GalleryTab.imageTab
        Config.Camera.recordLocation = false
        Config.VideoEditor.savesEditedVideoToLibrary = false
    }
    @objc func doneAction(){
        if (!isLoading){
            SVProgressHUD.show()
            if (publication_id != 0){
        self.viewModel.editNews(publication_id:publication_id,publication_desc:self.textView.text,access_id:accesId,files:files, success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                let publication_id = subJson["publication_id"].intValue
                NewsHistory.initWith(json: subJson, publication_id: publication_id)
            }
            self.goBack()
            SVProgressHUD.dismiss()
                })
            }
            else {
        self.viewModel.sendMultipleNewsFile(publication_desc:self.textView.text,access_id:accesId,files:files,success: {
            self.goBack()
            SVProgressHUD.dismiss()
                })
            }
        }
            view.endEditing(true)
        
    }

    func createToolbar(){
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolbarItems = [createEmoj(),createFile(),createImage(),createMusic(),createVideo(),spacer,spacer]
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setToolbarHidden(false, animated: false)
    }
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setToolbarHidden(true, animated: true)
    }
    func createEmoj()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_emoj"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(keyboarAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    func createFile()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_file"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(fileAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    func createImage()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_image"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(imageAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    func createMusic()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_music"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(fileAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    func createVideo()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_video"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(videoAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    @objc func imageAction(){
        let gallery = GalleryController()
        gallery.delegate = self
        self.present(gallery, animated: true, completion: nil)
    }
    @objc func fileAction(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    @objc func videoAction(){
        Config.initialTab = Config.GalleryTab.videoTab
        let gallery = GalleryController()
        gallery.delegate = self
        self.present(gallery, animated: true, completion: nil)
    }
    @objc func keyboarAction(){
        if (keyboardType == false) {
            let keyboardSettings = KeyboardSettings(bottomType: .categories)
            let emojiView = EmojiView(keyboardSettings: keyboardSettings)
            emojiView.translatesAutoresizingMaskIntoConstraints = false
            emojiView.delegate = self
            self.textView.inputView = emojiView
            keyboardType = true
            self.textView.reloadInputViews()
            self.textView.becomeFirstResponder()
        }
        else {
            keyboardType = false
            self.textView.inputView = nil
            self.textView.keyboardType = .default
            self.textView.reloadInputViews()
            self.textView.becomeFirstResponder()
        }
    }
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            
            view.layoutIfNeeded()
        }
    }
    
    // MARK: - Table view data source


    static func instantiate() -> CreateNewsController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateNewsController") as! CreateNewsController
    }
    // callback when tap a emoji on keyboard
    public func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        self.textView.insertText(emoji)
    }
    
    // callback when tap change keyboard button on keyboard
    public func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
        self.textView.inputView = nil
        self.textView.keyboardType = .default
        self.textView.reloadInputViews()
    }
    
    // callback when tap delete button on keyboard
    public func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        self.textView.deleteBackward()
    }
    
    // callback when tap dismiss button on keyboard
    public func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        self.textView.resignFirstResponder()
    }
    
    // MARK: - Gallery DidSelectImages
    
    public func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        print("Selected Gallery Image", images.count)
        if images.count == 1 {
            // Single Image
            if let selectedImage = images.first {
                selectedImage.resolve(completion: { (resolvedImage) in
                    if let image = resolvedImage {
                        if let data = UIImageJPEGRepresentation(image, 0.5) {
                            self.isLoading = true
                            self.viewModel.uploadMultipleNewsFile(with: data, format: "image"){ [weak self] (file) in
                                let news = NewsCreater()
                                news.index = self!.currentIndex
                                news.flle_url = file["file_url"]!
                                news.newsType = .image
                                self!.files.append(file)
                                self!.newsSequance.append(news)
                                self!.tableView.reloadData()
                                let photo = SKPhoto.photoWithImageURL(file["file_url"]!.encodeUrl()!)
                                photo.shouldCachePhotoURLImage = true
                                self!.images.append(photo)
                                self!.isLoading = false
                            }
                    }
                    }
                })
                    self.dismiss(animated: true, completion: nil)
            }
        } else {
            // Mutiple Images
            Image.resolve(images: images) { (resolvedImages) in
                if let images = resolvedImages as? [UIImage] {
                       SVProgressHUD.show()
                        for index in 0..<resolvedImages.count {
                            if let data = UIImageJPEGRepresentation(resolvedImages[index]!, 0.5) {
                                    self.isLoading = true
                                    self.viewModel.uploadMultipleNewsFile(with: data, format: "image"){ [weak self] (file) in
                                        let news = NewsCreater()
                                        news.index = self!.currentIndex
                                        news.newsType = .image
                                        news.flle_url = file["file_url"]!
                                        self!.files.append(file)
                                        self!.newsSequance.append(news)
                                        self!.tableView.reloadData()
                                        let photo = SKPhoto.photoWithImageURL(file["file_url"]!.encodeUrl()!)
                                        photo.shouldCachePhotoURLImage = true
                                        self!.images.append(photo)
                                    }
                                }
                            }
                   //     let imageContentNode = ImageContentNode(image: selectedImage, date: Date().getStringTime(), currentVC: self.controller)
                 //       return imageContentNode
             //       }
               //     _ = self.controller.sendCollectionViewWithNodes(imageNodes, numberOfRows: CGFloat(imageNodes.count), isIncomingMessage: false)
                    self.dismiss(animated: true, completion: nil)
                    self.isLoading = false
                    SVProgressHUD.dismiss()
                    
    }
            }
        }
    }
    @IBAction func newsTypeButton(_ sender: Any) {
        let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
        let allAction = UIAlertAction(title: "access_for_all".localized(), style: .default, handler: { _ in
            self.accesId = 1
            self.newsTypeButton.setTitle("access_for_all".localized(), for: .normal)
        })
        let forFriendsAction = UIAlertAction(title: "access_for_friends".localized(), style: .default, handler: { _ in
            
            self.accesId = 2
            self.newsTypeButton.setTitle("access_for_friends".localized(), for: .normal)
        })
        let forMeAction = UIAlertAction(title: "access_only_for_me".localized(), style: .default, handler: { _ in
            
            self.accesId = 3
            self.newsTypeButton.setTitle("access_only_for_me".localized(), for: .normal)
        })
    
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        if (self.profile.speciality_id.value == 1 || self.profile.speciality_id.value  == 2){
        alert.addAction(allAction)
    }
        alert.addAction(forFriendsAction)
        alert.addAction(forMeAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Gallery DidSelectVideo
    
    public func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        print("Select video delegate")
        var thumbVideoImage: UIImage?
        
        video.fetchThumbnail(size: CGSize(width: 300, height: 300), completion: { (image) in
            if let thumbImage = image {
                thumbVideoImage = thumbImage
            }
        })
        
        video.fetchAVAsset { (asset) in
            if let urlAsset = asset as? AVURLAsset {
                print("url video fetched")
                do {
                    let data = try Data(contentsOf: urlAsset.url)
                    print("url thumg fetched", urlAsset.url)
                    self.isLoading = true
                    SVProgressHUD.show()
                    if let image = thumbVideoImage {
                    //   _ = self.controller.sendVideo(image, videoUrl: urlAsset.url, videoData: data, isIncomingMessage: false)
                        var videoExtension = urlAsset.url.lastPathComponent
                        videoExtension = NSString(string: videoExtension).pathExtension
                        self.viewModel.uploadMultipleNewsFile(with: data, format: "video",fileExtension:videoExtension ){ [weak self] (file) in
                            let news = NewsCreater()
                            news.index = self!.currentIndex
                            news.flle_url = file["file_url"]!
                            news.newsType = .video
                            self!.files.append(file)
                            self!.newsSequance.append(news)
                            self!.tableView.reloadData()
                            self!.isLoading = false
                            SVProgressHUD.dismiss()
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                } catch {
                    WhisperHelper.showErrorMurmur(title: "Ошибка загрузки видео")
                }
            }
        }
    }
    
    public func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        print("requestLightbox delegate")
    }
    
    public func galleryControllerDidCancel(_ controller: GalleryController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - UIDocumentMenuDelegate

extension CreateNewsController: UIDocumentMenuDelegate, UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let fileName = url.lastPathComponent
        let fileExtension = NSString(string: fileName).pathExtension
        
        do {
            let data = try Data(contentsOf: url)
          //  _ = self.sendDocument(fileName, fileExtension: fileExtension, fileData: data, isIncomingMessage: false)
            self.isLoading = true
            SVProgressHUD.show()
            self.viewModel.uploadMultipleNewsFile(with: data, format: "document",fileExtension:fileExtension,fileName:fileName){ [weak self] (file) in
                let news = NewsCreater()
                news.index = self!.currentIndex
                news.flle_url = file["file_url"]!
                news.newsType = .video
                news.files = [file]
                self!.files.append(file)
                self!.newsSequance.append(news)
                self!.tableView.reloadData()
                self!.isLoading = false
                SVProgressHUD.dismiss()
            }
        } catch {
            WhisperHelper.showErrorMurmur(title: "Ошибка загрузки документа")
            print("error converting from file url to data")
        }
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)
    }
    
    public func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
        print("view was cancelled")
        self.dismiss(animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsSequance.count
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var file_url = "https://api.dalagram.com\(newsSequance[indexPath.row].flle_url)".encodeUrl()!
        if (newsSequance[indexPath.row].flle_url.contains("https")){
            file_url = newsSequance[indexPath.row].flle_url.encodeUrl()!
        }
        if (newsSequance[indexPath.row].newsType == .image){
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageContentCell", for: indexPath) as! ImageContentCell
            cell.ImageContent.kf.setImage(with: URL(string: file_url), placeholder: nil)
            let deleteIcon = UIImageView(image: #imageLiteral(resourceName: "icon_delete"))
            cell.ImageContent.addSubview(deleteIcon)
            deleteIcon.snp.makeConstraints { (make) in
                make.trailing.equalTo(cell.ImageContent).offset(3)
                make.top.equalTo(cell.ImageContent.snp.top).offset(-3)
            }
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.deleteAction))
            deleteIcon.isUserInteractionEnabled = true
            deleteIcon.addGestureRecognizer(tapGestureRecognizer)
            deleteIcon.tag = indexPath.row
            if let imageSource = CGImageSourceCreateWithURL(URL(string: file_url)! as CFURL, nil) {
                if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                    let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                    let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                    
                    let myViewWidth = self.tableView.frame.size.width
                    
                    let ratio = Double(myViewWidth)/Double(myImageWidth)
                    let scaledHeight = Double(myImageHeight) * ratio
                    
                    cell.imageHeight.constant = CGFloat(scaledHeight)
                    cell.layoutIfNeeded()
                    
                    
                }
            }
            return cell
        }
        else if (newsSequance[indexPath.row].newsType == .file){
            let origImage = UIImage(named: "icon_file_default")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            let cell = tableView.dequeueReusableCell(withIdentifier: "document_cell", for: indexPath) as! DocumentsCell
            cell.file_image.image = tintedImage
            cell.file_image.tintColor = UIColor.random
            cell.file_extension_lb.text = newsSequance[indexPath.row].files[0]["file_name"]?.fileExtension()
            cell.title_lb.text = newsSequance[indexPath.row].files[0]["file_name"]
            let deleteIcon = UIImageView(image: #imageLiteral(resourceName: "icon_delete"))
            cell.addSubview(deleteIcon)
            deleteIcon.snp.makeConstraints { (make) in
                make.trailing.equalTo(cell).offset(3)
                make.top.equalTo(cell.snp.top).offset(-3)
            }
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.deleteAction))
            deleteIcon.isUserInteractionEnabled = true
            deleteIcon.addGestureRecognizer(tapGestureRecognizer)
            deleteIcon.tag = indexPath.row
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoContentCell", for: indexPath) as! VideoContentCell
            cell.playButton.tag = indexPath.row
            cell.playButton.addTarget(self, action:#selector(playVideo), for: .touchUpInside)
            let asset = AVAsset(url:URL(string: file_url)!)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            let time = CMTimeMake(1, 60)
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {
                    cell.previewImage.image = frameImg
                    let deleteIcon = UIImageView(image: #imageLiteral(resourceName: "icon_delete"))
                      cell.previewImage.addSubview(deleteIcon)
                    deleteIcon.snp.makeConstraints { (make) in
                    make.trailing.equalTo(  cell.previewImage).offset(3)
                  make.top.equalTo(  cell.previewImage.snp.top).offset(-3)
                    }
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.deleteAction))
                    deleteIcon.isUserInteractionEnabled = true
                    deleteIcon.addGestureRecognizer(tapGestureRecognizer)
                    deleteIcon.tag = indexPath.row
                })
                
            }
            return cell
        }
    }
    @objc func deleteAction(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        self.files.remove(at: myIndexPath.row)
        self.newsSequance.remove(at: myIndexPath.row)
        self.tableView.reloadData()
    }
    @objc func playVideo(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let file_url = "https://api.dalagram.com\(newsSequance[myIndexPath.row].flle_url)".encodeUrl()!
        let videoURL = URL(string: file_url)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file_url = "https://api.dalagram.com\(newsSequance[indexPath.row].flle_url)".encodeUrl()!
        if (newsSequance[indexPath.row].newsType == .image){
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(newsSequance[indexPath.row].index)
            self.present(browser, animated: true, completion: nil)
        }
        else if (newsSequance[indexPath.row].newsType == .file){
            let vc = SFSafariViewController(url: URL(string:file_url)!)
            self.present(vc, animated: true, completion: nil)
            
        }
    }
}
    extension CreateNewsController: GrowingTextViewDelegate {
        
        // *** Call layoutIfNeeded on superview for animation when changing height ***
        
        func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
            UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
        func textViewDidChange(_ textView: UITextView) {
            print("12121121")
        }
      func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        }
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
                textView.text = "whats_new".localized()
                textView.textColor = UIColor.lightGray
            }
        }
    }

    


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


