//
//  InsertPhotoGalleryController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 5/24/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import Gallery
import AVFoundation
import Photos
import GrowingTextView
import MobileCoreServices
import ISEmojiView
import Alamofire
import AVKit
import SafariServices
import SKPhotoBrowser
import SVProgressHUD
class InsertPhotoGalleryController: UITableViewController,GalleryControllerDelegate,EmojiViewDelegate {
    @IBOutlet weak var newsTypeButton: UIButton!
    var keyboardType = false
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var textView: GrowingTextView!
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var avatarImg: UIImageView!
    var newsSequance = [NewsCreater]()
    var viewModel = NewsControllerViewModel(user_id: 0)
    var avatarUrl = ""
    var userName = ""
    var currentIndex = 0
    var accesId = 1
    var isLoading = false
    var publication_id = 0
    var images = [SKPhoto]()
    var newsHistory :NewsHistory? = nil
    var files: Array<[String: String]> = []
    var galleryCell :UITableViewCell? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "create_new_post".localized()
        self.newsTypeButton.setTitle("access_for_all".localized(), for: .normal)
        // growing textview
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        configureUI()
        createToolbar()
        GalleryConfig()
        if (publication_id != 0 ){
            editPost()
        }
        // *** Hide keyboard when tapping outside ***
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func editPost(){
        self.textView.text = newsHistory?.publication_desc
        for index2 in 0..<newsHistory!.file_list.count {
            let file_format = self.newsHistory!.file_list[index2].file_format
            let file_name = self.newsHistory!.file_list[index2].file_name
            let file_url = self.newsHistory!.file_list[index2].file_url
            let file_time = self.newsHistory!.file_list[index2].file_time
            
            let file = ["file_url": file_url,
                        "file_format": file_format,
                        "file_name": file_name,
                        "file_time": file_time]
            if (file_format == "audio"){
                let news = NewsCreater()
                news.index = index2
                news.flle_url = file_url
                news.newsType = .audio
                self.files.append(file)
                self.newsSequance.append(news)
            }
            else if (file_format == "image") {
                
                let news = NewsCreater()
                news.index = index2
                news.flle_url = file_url
                news.newsType = .image
                
                self.files.append(file)
                self.newsSequance.append(news)
            }
            else if (file_format == "video") {
                let news = NewsCreater()
                news.index = index2
                news.flle_url = file_url
                news.newsType = .video
                self.files.append(file)
                self.newsSequance.append(news)
            }
            else if (file_format == "file") {
                let news = NewsCreater()
                news.index = index2
                news.flle_url = file_url
                news.newsType = .file
                self.files.append(file)
                self.newsSequance.append(news)
            }
            
        }
        self.tableView.reloadData()
    }
    func configureUI(){
        self.nameField.text = userName
        avatarImg.kf.setImage(with: URL(string: avatarUrl.encodeUrl()!), placeholder: nil)
        avatarImg.layer.cornerRadius =  avatarImg.frame.width/2
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
        self.navigationItem.rightBarButtonItem = doneButton
        setEmptyBackTitle()
        setBlueNavBar()
        //MARK: - TableView Configurations
        self.tableView.tableHeaderView = headerView
        tableView.registerNib(ContentInputTableViewCell.self)
        tableView.registerNib(NameCell.self)
        tableView.registerNib(DescriptionCell.self)
        tableView.registerNib(ImageContentCell.self)
        tableView.registerNib(FooterMenuCell.self)
        tableView.registerNib(AudioContentCell.self)
        tableView.registerNib(VideoContentCell.self)
        tableView.registerNib(LinkContentCell.self)
        tableView.registerNib(MultipleImageContentCell.self)
        tableView.register(UINib(nibName: "DocumentsCell", bundle: nil), forCellReuseIdentifier: "document_cell")
        tableView.separatorColor = UIColor.init(white: 0.85, alpha: 1.0)
        tableView.allowsSelection = true
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.separatorColor = UIColor.clear
        self.tableView.reloadData()
        
    }
    
    func GalleryConfig(){
        // Gallery Picker Configs
        Config.Grid.FrameView.borderColor = UIColor.navBlueColor
        Config.Camera.imageLimit = 1
        Config.initialTab = Config.GalleryTab.imageTab
        Config.Camera.recordLocation = false
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.VideoEditor.savesEditedVideoToLibrary = false
    }
    @objc func doneAction(){
        guard files.count > 0 else {
              WhisperHelper.showErrorMurmur(title: "Загрузите фото")
              return
        }
        if (!isLoading){
            SVProgressHUD.show()
            if (publication_id != 0){ self.viewModel.editNews(publication_id:publication_id,publication_desc:self.textView.text,access_id:accesId,files:files,success: { (json)  in
                    self.goBack()
                    SVProgressHUD.dismiss()
                })
            }
            else { sendImageGalleryApi(publication_desc:self.textView.text,access_id:accesId,success: {
                    self.goBack()
                    let cell = self.galleryCell as! GalleryTableViewCell
                    let controller = cell.currentViewController as! NewsController
                    controller.getGallery()
                
                    SVProgressHUD.dismiss()
                })
            }
        }
        view.endEditing(true)
        
    }
    func sendImageGalleryApi(publication_desc: String = "",access_id:Int, success: @escaping ()->Void = {}) {
        
        var parameters = ["publication_desc": publication_desc,"access_id":access_id] as [String: Any]
        parameters["is_gallery"] = 1
        
        NetworkManager.makeRequest(.addFileNews(params: parameters, files: files), success: { (json) in
            
            print("makeRequest(.sendFileMessage",parameters)
            
            success()
        })
    }
    func createToolbar(){
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        toolbarItems = [createEmoj(),createImage(),spacer,spacer]
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setToolbarHidden(false, animated: false)
    }
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setToolbarHidden(true, animated: true)
    }
    func createEmoj()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_emoj"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(keyboarAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    func createFile()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_file"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(fileAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    func createImage()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_image"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(imageAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    func createMusic()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_music"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(fileAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    func createVideo()->UIBarButtonItem{
        let cancelButton = UIButton(type: .custom)
        cancelButton.setImage(UIImage(named: "attach_video"), for: .normal)
        cancelButton.setTitleColor(UIColor.red, for: .normal)
        cancelButton.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        cancelButton.addTarget(self, action: #selector(videoAction), for: .touchUpInside)
        let item = UIBarButtonItem(customView: cancelButton)
        return item
    }
    @objc func imageAction(){
        let gallery = GalleryController()
        gallery.delegate = self
        self.present(gallery, animated: true, completion: nil)
    }
    @objc func fileAction(){
    }
    @objc func videoAction(){
        Config.initialTab = Config.GalleryTab.videoTab
        let gallery = GalleryController()
        gallery.delegate = self
        self.present(gallery, animated: true, completion: nil)
    }
    @objc func keyboarAction(){
        if (keyboardType == false) {
            let keyboardSettings = KeyboardSettings(bottomType: .categories)
            let emojiView = EmojiView(keyboardSettings: keyboardSettings)
            emojiView.translatesAutoresizingMaskIntoConstraints = false
            emojiView.delegate = self
            self.textView.inputView = emojiView
            keyboardType = true
            self.textView.reloadInputViews()
            self.textView.becomeFirstResponder()
        }
        else {
            keyboardType = false
            self.textView.inputView = nil
            self.textView.keyboardType = .default
            self.textView.reloadInputViews()
            self.textView.becomeFirstResponder()
        }
    }
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = UIScreen.main.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                }
            }
            
            view.layoutIfNeeded()
        }
    }
    
    // MARK: - Table view data source
    
    
    static func instantiate() -> InsertPhotoGalleryController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InsertPhotoGalleryController") as! InsertPhotoGalleryController
    }
    // callback when tap a emoji on keyboard
    public func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        self.textView.insertText(emoji)
    }
    
    // callback when tap change keyboard button on keyboard
    public func emojiViewDidPressChangeKeyboardButton(_ emojiView: EmojiView) {
        self.textView.inputView = nil
        self.textView.keyboardType = .default
        self.textView.reloadInputViews()
    }
    
    // callback when tap delete button on keyboard
    public func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        self.textView.deleteBackward()
    }
    
    // callback when tap dismiss button on keyboard
    public func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        self.textView.resignFirstResponder()
    }
    
    // MARK: - Gallery DidSelectImages
    
    func uploadNewsApi(publication_desc: String = "", success: @escaping ()->Void = {}) {
        
        var parameters = ["publication_desc": publication_desc] as [String: Any]
        parameters["is_gallery"] = 1
        
        NetworkManager.makeRequest(.addNews(parameters), success: { (json) in
            success()
        })
    }
    public func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        print("Selected Gallery Image", images.count)
        if images.count == 1 {
            // Single Image
            if let selectedImage = images.first {
                selectedImage.resolve(completion: { (resolvedImage) in
                    if let image = resolvedImage {
                        if let data = UIImageJPEGRepresentation(image, 0.5) {
                            self.isLoading = true
                            self.viewModel.uploadMultipleNewsFile(with: data, format: "image"){ [weak self] (file) in
                                let news = NewsCreater()
                                news.index = self!.currentIndex
                                news.flle_url = file["file_url"]!
                                news.newsType = .image
                                self!.files.insert(file, at: 0)
                                self!.newsSequance.insert(news, at: 0)
                                if (self!.files.count == 2){
                                    self!.files.removeLast()
                                    self!.images.removeLast()
                                    self!.newsSequance.removeLast()
                                }
                                self!.tableView.reloadData()
                                let photo = SKPhoto.photoWithImageURL(file["file_url"]!.encodeUrl()!)
                                photo.shouldCachePhotoURLImage = true
                                self!.images.insert(photo, at: 0)
                                self!.isLoading = false
                            }
                        }
                    }
                })
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            // Mutiple Images
            Image.resolve(images: images) { (resolvedImages) in
                if let images = resolvedImages as? [UIImage] {
                    SVProgressHUD.show()
                    for index in 0..<resolvedImages.count {
                        if let data = UIImageJPEGRepresentation(resolvedImages[index]!, 0.5) {
                            self.isLoading = true
                            self.viewModel.uploadMultipleNewsFile(with: data, format: "image"){ [weak self] (file) in
                                let news = NewsCreater()
                                news.index = self!.currentIndex
                                news.newsType = .image
                                news.flle_url = file["file_url"]!
                                self!.files.append(file)
                                self!.newsSequance.append(news)
                                self!.tableView.reloadData()
                                let photo = SKPhoto.photoWithImageURL(file["file_url"]!.encodeUrl()!)
                                photo.shouldCachePhotoURLImage = true
                                self!.images.append(photo)
                            }
                        }
                    }
                    //     let imageContentNode = ImageContentNode(image: selectedImage, date: Date().getStringTime(), currentVC: self.controller)
                    //       return imageContentNode
                    //       }
                    //     _ = self.controller.sendCollectionViewWithNodes(imageNodes, numberOfRows: CGFloat(imageNodes.count), isIncomingMessage: false)
                    self.dismiss(animated: true, completion: nil)
                    self.isLoading = false
                    SVProgressHUD.dismiss()
                    
                }
            }
        }
    }
    @IBAction func newsTypeButton(_ sender: Any) {
            let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
            let allAction = UIAlertAction(title: "access_for_all".localized(), style: .default, handler: { _ in
                self.accesId = 1
                self.newsTypeButton.setTitle("access_for_all".localized(), for: .normal)
            })
            let forFriendsAction = UIAlertAction(title: "access_for_friends".localized(), style: .default, handler: { _ in
                
                self.accesId = 2
                self.newsTypeButton.setTitle("access_for_friends".localized(), for: .normal)
            })
            let forMeAction = UIAlertAction(title: "access_only_for_me".localized(), style: .default, handler: { _ in
                
                self.accesId = 3
                self.newsTypeButton.setTitle("access_only_for_me".localized(), for: .normal)
            })
        
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(allAction)
        alert.addAction(forFriendsAction)
        alert.addAction(forMeAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Gallery DidSelectVideo
    
    public func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        print("Select video delegate")
        var thumbVideoImage: UIImage?
        
        video.fetchThumbnail(size: CGSize(width: 300, height: 300), completion: { (image) in
            if let thumbImage = image {
                thumbVideoImage = thumbImage
            }
        })
        
        video.fetchAVAsset { (asset) in
            if let urlAsset = asset as? AVURLAsset {
                print("url video fetched")
                do {
                    let data = try Data(contentsOf: urlAsset.url)
                    print("url thumg fetched", urlAsset.url)
                    self.isLoading = true
                    SVProgressHUD.show()
                    if let image = thumbVideoImage {
                        //   _ = self.controller.sendVideo(image, videoUrl: urlAsset.url, videoData: data, isIncomingMessage: false)
                        var videoExtension = urlAsset.url.lastPathComponent
                        videoExtension = NSString(string: videoExtension).pathExtension
                        self.viewModel.uploadMultipleNewsFile(with: data, format: "video",fileExtension:videoExtension ){ [weak self] (file) in
                            let news = NewsCreater()
                            news.index = self!.currentIndex
                            news.flle_url = file["file_url"]!
                            news.newsType = .video
                            self!.files.append(file)
                            self!.newsSequance.append(news)
                            self!.tableView.reloadData()
                            self!.isLoading = false
                            SVProgressHUD.dismiss()
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                } catch {
                    WhisperHelper.showErrorMurmur(title: "Ошибка загрузки видео")
                }
            }
        }
    }
    
    public func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        print("requestLightbox delegate")
    }
    
    public func galleryControllerDidCancel(_ controller: GalleryController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - UIDocumentMenuDelegate

extension InsertPhotoGalleryController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return newsSequance.count
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var file_url = "https://api.dalagram.com\(newsSequance[indexPath.row].flle_url)".encodeUrl()!
        if (newsSequance[indexPath.row].flle_url.contains("https")){
            file_url = newsSequance[indexPath.row].flle_url.encodeUrl()!
        }
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageContentCell", for: indexPath) as! ImageContentCell
            cell.ImageContent.kf.setImage(with: URL(string: file_url), placeholder: nil)
            let deleteIcon = UIImageView(image: #imageLiteral(resourceName: "icon_delete"))
            cell.ImageContent.addSubview(deleteIcon)
            deleteIcon.snp.makeConstraints { (make) in
                make.trailing.equalTo(cell.ImageContent).offset(3)
                make.top.equalTo(cell.ImageContent.snp.top).offset(-3)
            }
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.deleteAction))
            deleteIcon.isUserInteractionEnabled = true
            deleteIcon.addGestureRecognizer(tapGestureRecognizer)
            deleteIcon.tag = indexPath.row
            if let imageSource = CGImageSourceCreateWithURL(URL(string: file_url)! as CFURL, nil) {
                if let imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, nil) as Dictionary? {
                    let myImageWidth = imageProperties[kCGImagePropertyPixelWidth] as! Int
                    let myImageHeight = imageProperties[kCGImagePropertyPixelHeight] as! Int
                    
                    let myViewWidth = self.tableView.frame.size.width
                    
                    let ratio = Double(myViewWidth)/Double(myImageWidth)
                    let scaledHeight = Double(myImageHeight) * ratio
                    
                    cell.imageHeight.constant = CGFloat(scaledHeight)
                    cell.layoutIfNeeded()
                    
                    
                }
            }
            return cell
    }
    @objc func deleteAction(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        self.files.remove(at: myIndexPath.row)
        self.newsSequance.remove(at: myIndexPath.row)
        self.tableView.reloadData()
    }
    @objc func playVideo(_ sender: UIButton) {
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let file_url = "https://api.dalagram.com\(newsSequance[indexPath.row].flle_url)".encodeUrl()!
        if (newsSequance[indexPath.row].newsType == .image){
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(newsSequance[indexPath.row].index)
            self.present(browser, animated: true, completion: nil)
        }
    }
}
extension InsertPhotoGalleryController: GrowingTextViewDelegate {
    
    // *** Call layoutIfNeeded on superview for animation when changing height ***
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    func textViewDidChange(_ textView: UITextView) {
        print("12121121")
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
    }
}

