//
//  NewsControllerViewModel.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/18/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import Alamofire
class NewsControllerViewModel {
    
      var dialogs: [NewsHistory]? = []
    var newsCommentsData: [NewsComments] = []
     var realmDialog: Results<NewsHistory>?
    var galleryData: [Gallery] = []
    var friensData: [JSONContact] = []
    var bannerData: [Banner] = []
    var myFriendsData: [JSONContact] = []
    let realm = try! Realm()
    private let refreshControl = UIRefreshControl()
    init(user_id:Int) {
        if (user_id == 0){
            realmDialog = realm.objects(NewsHistory.self).sorted(byKeyPath: "publication_id",ascending: false)
            }
           else {
            realmDialog = realm.objects(NewsHistory.self).sorted(byKeyPath: "publication_id",ascending: false).filter("user_id = \(user_id)")
            }
        
    }
    // MARK: - Dialogs GET Request
    
    func getUserNews(page: Int = 1, per_page: Int = 10,author_id:Int, success: @escaping (() -> Void) = {}) {
        let parameters = ["token": User.getToken(), "page": page, "per_page": per_page,"author_id":author_id] as [String : Any]
        NetworkManager.makeRequest(.getNews(parameters), success: { (json)  in
            print("afafafaf",json )
            for (_, subJson):(String, JSON) in json["data"] {
                let publication_id = subJson["publication_id"].intValue
                NewsHistory.initWith(json: subJson, publication_id: publication_id)
                let item = NewsHistory()
                item.publication_id     = subJson["publication_id"].intValue
                item.publication_desc   = subJson["publication_desc"].stringValue
                item.publication_date   = subJson["publication_date"].stringValue
                item.view_count         = subJson["view_count"].intValue
                item.comment_count      = subJson["comment_count"].intValue
                item.share_count        = subJson["share_count"].intValue
                item.like_count         = subJson["like_count"].intValue
                item.is_has_file        = subJson["is_has_file"].intValue
                item.is_share           = subJson["is_share"].intValue
                item.is_has_link        = subJson["is_has_link"].intValue
                item.is_i_liked         = subJson["is_i_liked"].intValue
                item.access_id          = subJson["access_id"].intValue
                let authorJson = subJson["author"]
                item.user_id = authorJson["user_id"].intValue
                item.avatar = authorJson["avatar"].stringValue
                item.user_name = authorJson["user_name"].stringValue
                item.phone = authorJson["phone"].stringValue
                item.last_visit = authorJson["last_visit"].stringValue
                item.contact_user_name = authorJson["contact_user_name"].stringValue
                item.universal_name = authorJson["universal_name"].stringValue
                item.nickname = authorJson["nickname"].stringValue
                
                //share publication
                
                if (subJson["is_has_link"].intValue == 1){
                    let meta_tagsJson = subJson["meta_tags"]
                    item.meta_tagsDescription = meta_tagsJson["description"].stringValue
                    item.meta_tagsTitle = meta_tagsJson["title"].stringValue
                    item.meta_tagsImage = meta_tagsJson["image"].stringValue
                }
                 var share_publicationJson = subJson
                if ( item.is_share == 1){
                    share_publicationJson = subJson["share_publication"]
                    let authorJson = share_publicationJson["author"]
                    item.share_user_id = authorJson["user_id"].intValue
                    item.share_avatar = authorJson["avatar"].stringValue
                    item.share_user_name = authorJson["user_name"].stringValue
                    item.share_phone = authorJson["phone"].stringValue
                    item.share_last_visit = authorJson["last_visit"].stringValue
                    item.share_contact_user_name = authorJson["contact_user_name"].stringValue
                    item.share_universal_name = authorJson["universal_name"].stringValue
                    item.share_nickname = authorJson["nickname"].stringValue
                    
                    item.share_publication_desc   = share_publicationJson["publication_desc"].stringValue
                    item.share_publication_date   = share_publicationJson["publication_date"].stringValue
                    item.share_publication_id        =
                        share_publicationJson["publication_id"].intValue
                    item.share_is_has_link        = share_publicationJson["is_has_link"].intValue
                    if (share_publicationJson["is_has_link"].intValue == 1){
                        let meta_tagsJson = share_publicationJson["meta_tags"]
                        item.meta_tagsDescription = meta_tagsJson["description"].stringValue
                        item.meta_tagsTitle = meta_tagsJson["title"].stringValue
                        item.meta_tagsImage = meta_tagsJson["image"].stringValue
                    }
                }
                for (_, subJson):(String, JSON) in share_publicationJson["file_list"] {
                    let file = ChatFile()
                    file.file_format = subJson["file_format"].stringValue
                    file.file_url = subJson["file_url"].stringValue
                    file.file_name = subJson["file_name"].stringValue
                    file.file_time = subJson["file_time"].stringValue
                    item.file_list.append(file)
                }
                let commentJson = subJson["comment"]
                item.comment_id         = commentJson["comment_id"].intValue
                item.comment_text      = commentJson["comment_text"].stringValue
                item.comment_date        = commentJson["comment_date"].stringValue
                item.comment_like_count         = commentJson["like_count"].intValue
                item.comment_is_liked        = commentJson["is_liked"].intValue
                item.comment_is_has_file        = commentJson["is_has_file"].intValue
                // comment author
                let commentAuthorJson = commentJson["author"]
                item.comment_user_id = commentAuthorJson["user_id"].intValue
                item.comment_avatar = commentAuthorJson["avatar"].stringValue
                item.comment_nickname = commentAuthorJson["nickname"].stringValue
                for (_, subJson):(String, JSON) in commentJson["file_list"] {
                    let file = ChatFile()
                    file.file_format = subJson["file_format"].stringValue
                    file.file_url = subJson["file_url"].stringValue
                    file.file_name = subJson["file_name"].stringValue
                    file.file_time = subJson["file_time"].stringValue
                    item.comment_file_list.append(file)
                }
                
                self.dialogs?.append(item)
            }
            
            success()
        })
    }
    func refreshNews(page: Int = 1, per_page: Int = 10,author_id:Int, success: @escaping (() -> Void) = {}) {
        let parameters = ["token": User.getToken(), "page": page, "per_page": per_page,"author_id":author_id] as [String : Any]
        NetworkManager.makeRequest(.getNews(parameters), success: { (json)  in
            self.dialogs?.removeAll()
            for (_, subJson):(String, JSON) in json["data"] {
                let publication_id = subJson["publication_id"].intValue
                NewsHistory.initWith(json: subJson, publication_id: publication_id)
                let item = NewsHistory()
                item.publication_id     = subJson["publication_id"].intValue
                item.publication_desc   = subJson["publication_desc"].stringValue
                item.publication_date   = subJson["publication_date"].stringValue
                item.view_count         = subJson["view_count"].intValue
                item.comment_count      = subJson["comment_count"].intValue
                item.share_count        = subJson["share_count"].intValue
                item.like_count         = subJson["like_count"].intValue
                item.is_has_file        = subJson["is_has_file"].intValue
                item.is_share           = subJson["is_share"].intValue
                item.is_has_link        = subJson["is_has_link"].intValue
                item.is_i_liked         = subJson["is_i_liked"].intValue
                item.access_id          = subJson["access_id"].intValue
                
                let authorJson = subJson["author"]
                item.user_id = authorJson["user_id"].intValue
                item.avatar = authorJson["avatar"].stringValue
                item.user_name = authorJson["user_name"].stringValue
                item.phone = authorJson["phone"].stringValue
                item.last_visit = authorJson["last_visit"].stringValue
                item.contact_user_name = authorJson["contact_user_name"].stringValue
                item.universal_name = authorJson["universal_name"].stringValue
                item.nickname = authorJson["nickname"].stringValue
                
                if (subJson["is_has_link"].intValue == 1){
                    let meta_tagsJson = subJson["meta_tags"]
                    item.meta_tagsDescription = meta_tagsJson["description"].stringValue
                    item.meta_tagsTitle = meta_tagsJson["title"].stringValue
                    item.meta_tagsImage = meta_tagsJson["image"].stringValue
                }
                var share_publicationJson = subJson
                if ( item.is_share == 1){
                    share_publicationJson = subJson["share_publication"]
                    let authorJson = share_publicationJson["author"]
                    item.share_user_id = authorJson["user_id"].intValue
                    item.share_avatar = authorJson["avatar"].stringValue
                    item.share_user_name = authorJson["user_name"].stringValue
                    item.share_phone = authorJson["phone"].stringValue
                    item.share_last_visit = authorJson["last_visit"].stringValue
                    item.share_contact_user_name = authorJson["contact_user_name"].stringValue
                    item.share_universal_name = authorJson["universal_name"].stringValue
                    item.share_nickname = authorJson["nickname"].stringValue
                    
                    item.share_publication_desc   = share_publicationJson["publication_desc"].stringValue
                    item.share_publication_date   = share_publicationJson["publication_date"].stringValue
                    item.is_has_link        = share_publicationJson["is_has_link"].intValue
                    if (share_publicationJson["is_has_link"].intValue == 1){
                        let meta_tagsJson = share_publicationJson["meta_tags"]
                        item.meta_tagsDescription = meta_tagsJson["description"].stringValue
                        item.meta_tagsTitle = meta_tagsJson["title"].stringValue
                        item.meta_tagsImage = meta_tagsJson["image"].stringValue
                    }
                }
                for (_, subJson):(String, JSON) in share_publicationJson["file_list"] {
                    let file = ChatFile()
                    file.file_format = subJson["file_format"].stringValue
                    file.file_url = subJson["file_url"].stringValue
                    file.file_name = subJson["file_name"].stringValue
                    file.file_time = subJson["file_time"].stringValue
                    item.file_list.append(file)
                }
                let commentJson = subJson["comment"]
                item.comment_id         = commentJson["comment_id"].intValue
                item.comment_text      = commentJson["comment_text"].stringValue
                item.comment_date        = commentJson["comment_date"].stringValue
                item.comment_like_count         = commentJson["like_count"].intValue
                item.comment_is_liked        = commentJson["is_liked"].intValue
                item.comment_is_has_file        = commentJson["is_has_file"].intValue
                // comment author
                let commentAuthorJson = commentJson["author"]
                item.comment_user_id = commentAuthorJson["user_id"].intValue
                item.comment_avatar = commentAuthorJson["avatar"].stringValue
                item.comment_nickname = commentAuthorJson["nickname"].stringValue
                for (_, subJson):(String, JSON) in commentJson["file_list"] {
                    let file = ChatFile()
                    
                    file.file_format = subJson["file_format"].stringValue
                    file.file_url = subJson["file_url"].stringValue
                    file.file_name = subJson["file_name"].stringValue
                    file.file_time = subJson["file_time"].stringValue
                    item.comment_file_list.append(file)
                }
                
                self.dialogs?.append(item)
            }
            
            success()
        })
    }
    func getlastNews(page: Int = 1, per_page: Int = 1,publication_id:Int, success: @escaping ((NewsHistory) -> Void) = {_ in }) {
        let firstTodoEndpoint: String = "https://api.dalagram.com/api/publication/\(publication_id)?token=\(User.getToken())"
        AF.request(firstTodoEndpoint, method: .get)
            .responseJSON { response in
                do {
                    guard let data = response.data else {
                        print ("data was nil?")
                        return
                    }
            var items = NewsHistory()
            let json = try JSON(data: data)
            for (_, subJson):(String, JSON) in json["data"] {
                let publication_id = subJson["publication_id"].intValue
                NewsHistory.initWith(json: subJson, publication_id: publication_id)
                let item = NewsHistory()
                item.publication_id     = subJson["publication_id"].intValue
                item.publication_desc   = subJson["publication_desc"].stringValue
                item.publication_date   = subJson["publication_date"].stringValue
                item.view_count         = subJson["view_count"].intValue
                item.comment_count      = subJson["comment_count"].intValue
                item.share_count        = subJson["share_count"].intValue
                item.like_count         = subJson["like_count"].intValue
                item.is_has_file        = subJson["is_has_file"].intValue
                item.is_share           = subJson["is_share"].intValue
                item.is_has_link        = subJson["is_has_link"].intValue
                item.is_i_liked         = subJson["is_i_liked"].intValue
                item.access_id          = subJson["access_id"].intValue
                
                let authorJson = subJson["author"]
                item.user_id = authorJson["user_id"].intValue
                item.avatar = authorJson["avatar"].stringValue
                item.user_name = authorJson["user_name"].stringValue
                item.phone = authorJson["phone"].stringValue
                item.last_visit = authorJson["last_visit"].stringValue
                item.contact_user_name = authorJson["contact_user_name"].stringValue
                item.universal_name = authorJson["universal_name"].stringValue
                item.nickname = authorJson["nickname"].stringValue
                
                if (subJson["is_has_link"].intValue == 1){
                    let meta_tagsJson = subJson["meta_tags"]
                    item.meta_tagsDescription = meta_tagsJson["description"].stringValue
                    item.meta_tagsTitle = meta_tagsJson["title"].stringValue
                    item.meta_tagsImage = meta_tagsJson["image"].stringValue
                }
                var share_publicationJson = subJson
                if ( item.is_share == 1){
                    share_publicationJson = subJson["share_publication"]
                    let authorJson = share_publicationJson["author"]
                    item.share_user_id = authorJson["user_id"].intValue
                    item.share_avatar = authorJson["avatar"].stringValue
                    item.share_user_name = authorJson["user_name"].stringValue
                    item.share_phone = authorJson["phone"].stringValue
                    item.share_last_visit = authorJson["last_visit"].stringValue
                    item.share_contact_user_name = authorJson["contact_user_name"].stringValue
                    item.share_universal_name = authorJson["universal_name"].stringValue
                    item.share_nickname = authorJson["nickname"].stringValue
                    
                    item.share_publication_desc   = share_publicationJson["publication_desc"].stringValue
                    item.share_publication_date   = share_publicationJson["publication_date"].stringValue
                    item.share_is_has_link        =
                        share_publicationJson["publication_id"].intValue
                    item.is_has_link        = share_publicationJson["is_has_link"].intValue
                    if (share_publicationJson["is_has_link"].intValue == 1){
                        let meta_tagsJson = share_publicationJson["meta_tags"]
                        item.meta_tagsDescription = meta_tagsJson["description"].stringValue
                        item.meta_tagsTitle = meta_tagsJson["title"].stringValue
                        item.meta_tagsImage = meta_tagsJson["image"].stringValue
                    }
                }
                for (_, subJson):(String, JSON) in share_publicationJson["file_list"] {
                    let file = ChatFile()
                    file.file_format = subJson["file_format"].stringValue
                    file.file_url = subJson["file_url"].stringValue
                    file.file_name = subJson["file_name"].stringValue
                    file.file_time = subJson["file_time"].stringValue
                    item.file_list.append(file)
                }
                let commentJson = subJson["comment"]
                item.comment_id         = commentJson["comment_id"].intValue
                item.comment_text      = commentJson["comment_text"].stringValue
                item.comment_date        = commentJson["comment_date"].stringValue
                item.comment_like_count         = commentJson["like_count"].intValue
                item.comment_is_liked        = commentJson["is_liked"].intValue
                item.comment_is_has_file        = commentJson["is_has_file"].intValue
                // comment author
                let commentAuthorJson = commentJson["author"]
                item.comment_user_id = commentAuthorJson["user_id"].intValue
                item.comment_avatar = commentAuthorJson["avatar"].stringValue
                item.comment_nickname = commentAuthorJson["nickname"].stringValue
                for (_, subJson):(String, JSON) in commentJson["file_list"] {
                    let file = ChatFile()
                    file.file_format = subJson["file_format"].stringValue
                    file.file_url = subJson["file_url"].stringValue
                    file.file_name = subJson["file_name"].stringValue
                    file.file_time = subJson["file_time"].stringValue
                    item.comment_file_list.append(file)
                }
                
                items = item
            }
            
            success(items)
        } catch let error as NSError {
            
        }
        }
    }
    func getGallery(page: Int = 1, per_page: Int = 10,author_id:Int, success: @escaping (() -> Void) = {}) {
        self.galleryData.removeAll()
        let parameters = ["token": User.getToken(), "page": page, "per_page": per_page,"author_id":author_id] as [String : Any]
        NetworkManager.makeRequest(.getGallery(parameters), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = Gallery(json: subJson)
                self.galleryData.append(new)
            }
            success()
        })
    }
    func getComments(publication_id:Int, success: @escaping (() -> Void) = {}) {
        let parameters = ["token": User.getToken(), "publication_id": publication_id, "page": 1, "per_page": 100] as [String : Any]
        NetworkManager.makeRequest(.getComments(parameters), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = NewsComments(json: subJson)
                self.newsCommentsData.append(new)
            }
            success()
        })
    }
    func likeNews(publication_id: Int,completion:@escaping ()->()) {
         let parameters = ["publication_id":publication_id] as [String : Any]
        NetworkManager.makeRequest(.likeNews(parameters), success: { (json)  in
                    completion()
                })
    }
    func likeComment(comment_id: Int,completion:@escaping ()->()) {
        let parameters = ["comment_id":comment_id] as [String : Any]
        NetworkManager.makeRequest(.likeNews(parameters), success: { (json)  in
            completion()
        })
    }
    func sendMessageApi(comment_text: String = "",publication_id:Int = 0,answer_comment_id:Int = 0, success: @escaping ()->Void = {}) {
        
        var parameters = ["comment_text": comment_text,"publication_id":publication_id] as [String: Any]   
        if (answer_comment_id != 0){
            parameters["answer_comment_id"] = answer_comment_id
        }
        NetworkManager.makeRequest(.addComment(parameters), success: { (json) in
            success()
        })
    }
    func uploadNewsApi(publication_desc: String = "", success: @escaping ()->Void = {}) {
        
        var parameters = ["publication_desc": publication_desc] as [String: Any]
            parameters["is_gallery"] = 1
        
        NetworkManager.makeRequest(.addNews(parameters), success: { (json) in
            success()
        })
    }
    
    func uploadMultipleNewsFile(with imageData: Data, format: String, fileExtension: String = "",fileTime: String = "", fileName: String = "", success: @escaping ([String: String]) -> Void) {
        NetworkManager.makeRequest(.uploadChatFile(imageData, format: format, fileExtension: fileExtension, fileName: fileName), success: { (json) in
            let jsonDictionary = ["file_url": json["file_url"].stringValue,
                                  "file_format": json["file_format"].stringValue,
                                  "file_name": json["file_name"].stringValue,
                                  "file_time": fileTime]
            success(jsonDictionary)
            
        })
    }
    func removeNews(publication_id:Int, success: @escaping (() -> Void) = {}) {
            let firstTodoEndpoint: String = "https://api.dalagram.com/api/publication/\(publication_id)?token=\(User.getToken())"
            AF.request(firstTodoEndpoint, method: .delete)
                .responseJSON { response in
                    guard response.error == nil else {
                        // got an error in getting the data, need to handle it
                        if let error = response.error {
                            print("Error: \(error)")
                        }
                        return
                    }
                    
                    WhisperHelper.showSuccessMurmur(title: "Успешно удалено")
                    print("DELETE ok",response)
                    success()
            }
        
    }
    func removeComment(comment_id:Int, success: @escaping (() -> Void) = {}) {
        let firstTodoEndpoint: String = "https://api.dalagram.com/api/comment/\(comment_id)?token=\(User.getToken())"
        AF.request(firstTodoEndpoint, method: .delete)
            .responseJSON { response in
                guard response.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling DELETE on /todos/1")
                    if let error = response.error {
                        print("Error: \(error)")
                    }
                    return
                }
                
                WhisperHelper.showSuccessMurmur(title: "Успешно удалено")
                print("DELETE ok",response)
                success()
        }
        
    }
    func editNews(publication_id:Int,publication_desc: String = "",access_id:Int,files: Array<[String: String]>, success: @escaping (JSON) -> Void) {
        let parameters = ["publication_desc": publication_desc,"access_id":access_id,"file_list":files] as [String: Any]
        let editUrl: String = "https://api.dalagram.com/api/publication/\(publication_id)?token=\(User.getToken())"
        AF.request(editUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                do {
                    guard let data = response.data else {
                        print ("data was nil?")
                        return
                    }
                    let json = try JSON(data: data)
                success(json)
                } catch let error as NSError {
                
                }
        }
    }
    func sendMultipleNewsFile(publication_desc: String = "",access_id:Int,files: Array<[String: String]>, success: @escaping () -> Void) {
        var parameters = ["publication_desc": publication_desc,"access_id":access_id] as [String: Any]
        parameters["is_gallery"] = 0

        NetworkManager.makeRequest(.addFileNews(params: parameters, files: files), success: { (json) in
        
                print("makeRequest(.sendFileMessage",parameters)
            
            success()
        })
        
    }
    func sendFriendRequest(partner_id : Int = 0, success: @escaping () -> Void) {
        let parameters = ["partner_id": partner_id] as [String : Any]
        NetworkManager.makeRequest(.sendFriendRequest(parameters), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            print("makeRequest(.sendFriendRequest",parameters)
            success()
        })
        
    }
    func getFriends(author_id:Int,onSuccess: @escaping () -> Void) {
        self.friensData.removeAll()
        let parameters = ["token": User.getToken(),"user_id":author_id] as [String : Any]
        NetworkManager.makeRequest(.getFriends(parameters), success: { (json) in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = JSONContact(json: subJson)
               self.friensData.append(new)
            }
            onSuccess()
        })
    }
    func getBanners(city_id:Int,onSuccess: @escaping () -> Void) {
        self.bannerData.removeAll()
        let parameters = ["token": User.getToken()] as [String : Any]
        NetworkManager.makeRequest(.getBanner(parameters), success: { (json) in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = Banner(json: subJson)
                self.bannerData.append(new)
            }
            onSuccess()
        })
    }
    func getMyFriends(onSuccess: @escaping () -> Void) {
        self.myFriendsData.removeAll()
        let parameters = ["token": User.getToken()] as [String : Any]
        NetworkManager.makeRequest(.getFriends(parameters), success: { (json) in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = JSONContact(json: subJson)
                self.myFriendsData.append(new)
            }
            onSuccess()
        })
    }
    func uploadCommentFile(with imageData: Data, format: String, fileExtension: String = "",fileTime: String = "", fileName: String = "",publication_id:Int = 0,answer_comment_id:Int = 0, success: @escaping () -> Void) {
        print("12121asas",fileName)
        NetworkManager.makeRequest(.uploadChatFile(imageData, format: format, fileExtension: fileExtension, fileName: fileName), success: { (json) in
            let jsonDictionary = ["file_url": json["file_url"].stringValue,
                                  "file_format": json["file_format"].stringValue,
                                  "file_name": json["file_name"].stringValue,
                                  "file_time": fileTime]
            var parameters: [String: Any] = [:]
            switch format {
            case "image":
                parameters["comment_text"] = "Фото"
            case "document":
                parameters["comment_text"] = "Файл"
            case "video":
                parameters["comment_text"] = "Видео"
            case "audio":
                parameters["comment_text"] = "Аудио"
            default: break
            }
                parameters["answer_comment_id"] = answer_comment_id
            
            parameters["publication_id"] = publication_id
            NetworkManager.makeRequest(.sendFileComment(params: parameters, files: [jsonDictionary]), success: { (json) in
                if let user = User.currentUser() {
               
                    print("makeRequest(.sendFileMessage",parameters)
                }
                success()
            })
        })
    }
    
}
