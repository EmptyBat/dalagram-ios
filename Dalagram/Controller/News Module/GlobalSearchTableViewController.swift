//
//  GlobalSearchTableViewController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/17/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift
import SwipeCellKit
import RxSwift
class GlobalSearchTableViewController: UITableViewController,UISearchBarDelegate {

    // MARK: - UIElements

    
    lazy var navigationTitle: UILabel = {
        let label = UILabel()
        label.text = "search".localized()
        label.font = UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.init(0.01))
        label.textColor = UIColor.white
        return label
    }()
    
    lazy var navigationLoader: UIActivityIndicatorView = {
        let loader = UIActivityIndicatorView(activityIndicatorStyle: .white)
        loader.hidesWhenStopped = true
        return loader
    }()
    
    // MARK: - Variables
    
    var viewModel = DialogsViewModel()
    var notificationToken: NotificationToken? = nil
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var searchController :UISearchController?
    var avatarUrl = ""
    var searchState = false
    var searchChannels = false
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    deinit {
        notificationToken?.invalidate()
        NotificationCenter.default.removeObserver(self, name: AppManager.loadDialogsNotification, object: nil)
    }

}
extension GlobalSearchTableViewController {
    // MARK: - NavBar TitleView Action

    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let user_id = viewModelProfile.user_id.value
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.show(vc, sender: nil)
    }
    func configureUI() {
        
        // MARK: - Navigation Bar
        searchAction()
        setEmptyBackTitle()
        setBlueNavBar()
        
        //MARK: - TableView Configurations
        tableView.registerNib(ChatCell.self)
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.init(white: 0.85, alpha: 1.0)
        // MARK: - UIBarButtonItem Configurations
        let createSearch = UIBarButtonItem(image: UIImage(named: "icon_search"), style: .plain, target: self, action: #selector(searchAction))
        createSearch.tintColor = UIColor.white
        
        let createShare = UIBarButtonItem(image: UIImage(named: "icon_mbriShare"), style: .plain, target: self, action: #selector(CompanyProductAction))
        createShare.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItems = [createSearch, createShare]
        
        // MARK: - UINavigationBar TitleView Configurations
        let titleView = UIView()
        titleView.addSubview(navigationTitle)
        navigationTitle.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        titleView.addSubview(navigationLoader)
        navigationLoader.snp.makeConstraints { (make) in
            make.left.equalTo(navigationTitle.snp.right).offset(8.0)
            make.centerY.equalTo(navigationTitle)
        }
        self.navigationItem.titleView = titleView
        view.backgroundColor = UIColor.init(white: 1.0, alpha: 0.98)
        //creating New Chat button On TableVew
        
    }
    
    // MARK: - Create Chat UIBarButton Action
    @objc func searchAction() {
        searchChannels = true
        self.searchController = UISearchController(searchResultsController: nil)
        // Set any properties (in this case, don't hide the nav bar and don't show the emoji keyboard option)
        self.searchController!.hidesNavigationBarDuringPresentation = false
        self.searchController!.dimsBackgroundDuringPresentation = false
        self.searchController!.searchBar.keyboardType = UIKeyboardType.default
        //  self.searchController!.searchBar.searchBarStyle =  .minimal
        self.searchController!.searchBar.placeholder = "search".localized()
        self.searchController!.searchBar.sizeToFit()
        // Make this class the delegate and present the search
        self.searchController!.searchBar.delegate = self
        self.present(self.searchController!, animated: true, completion: nil)
    }
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }
    // MARK: - SwipeCellKit Action
    
    @objc func showDeleteActionSheet(id: String, dialogItem: DialogItem) {
        let alert = UIAlertController(title: "Выберите", message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        let deleteAction = UIAlertAction(title: "Удалить", style: .default) { (act) in
            self.viewModel.removeDialog(by: id, dialogItem: dialogItem)
        }
        let clearAction = UIAlertAction(title: "Очистить", style: .default) { (act) in
            self.viewModel.clearDialog(by: id, dialogItem: dialogItem)
        }
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(deleteAction)
        alert.addAction(clearAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return viewModel.searchDialog.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell: ChatCell = tableView.dequeReusableCell(for: indexPath)
            cell.setupDialog(viewModel.searchDialog[indexPath.row])
            return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // FIXME:  OPTIONALS FORCE UNWRAPPING
        let currentDialog = viewModel.searchDialog[indexPath.row]
        let chatInfo = DialogInfo(dialog: currentDialog.dialogItem!)
        print("afsfasfa",chatInfo)
        if chatInfo.user_id != 0 { // Single Chat
            let vc = ChatController(type: .single, info: chatInfo, dialogId: currentDialog.id,avatarUrl:self.avatarUrl)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else if chatInfo.group_id != 0 { // Group Chat
            let vc = ChatController(type: .group, info: chatInfo, dialogId: currentDialog.id,avatarUrl:self.avatarUrl)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        } else if chatInfo.channel_id != 0 { // Channel
            let vc = ChatController(type: .channel, info: chatInfo, dialogId: currentDialog.id,avatarUrl:self.avatarUrl)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        viewModel.resetMessagesCounter(for: currentDialog)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

// MARK: - SwipeCellKit Delegates

extension GlobalSearchTableViewController {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        viewModel.searchDialog(by: searchText,searchChannel:searchChannels, onReload: { [weak self] in
            self?.tableView.reloadData()
        })
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchChannels = false
        
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        viewModel.getUserDialogs { [weak self] in
            guard let vc = self else { return }
            vc.showNoContentView(dataCount: vc.viewModel.dialogs?.count ?? 0)
            self?.tableView.reloadData()
        }
        searchChannels = false
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != ""{
        viewModel.searchDialog(by: searchText,searchChannel:searchChannels, onReload: { [weak self] in
            //     print("affasfas",self?.viewModel.searchDialog.count)
            self?.tableView.reloadData()
            
            
        })
    }
    }
}

