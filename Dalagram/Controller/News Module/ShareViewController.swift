//
//  ShareViewController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/10/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController,UITextViewDelegate {
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var reportLb: UILabel!
    @IBOutlet weak var contentTV: UITextView!
    @IBOutlet weak var privateBtn: UIButton!
    var accesId = 1
    var publication_id = 0
    var parentController : UIViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.shareBtn.layer.cornerRadius = 16
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        self.backgroundView.addGestureRecognizer(tap)
        
        self.backgroundView.isUserInteractionEnabled = true
        self.contentTV.text = "whats_new".localized()
        self.contentTV.textColor = UIColor.lightGray
        self.contentTV.delegate = self
        reportLb.text = "post".localized()
        self.privateBtn.setTitle("access_for_all".localized(), for: .normal)
        self.shareBtn.setTitle("share_send".localized(), for: .normal)
        // Do any additional setup after loading the view.
    }
    @objc private func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func privateBtnAction(_ sender: Any) {
            let alert = UIAlertController(title: "choise_action".localized(), message: nil, preferredStyle: .actionSheet)
            let allAction = UIAlertAction(title: "access_for_all".localized(), style: .default, handler: { _ in
                self.accesId = 1
                self.privateBtn.setTitle("access_for_all".localized(), for: .normal)
            })
            let forFriendsAction = UIAlertAction(title: "access_for_friends".localized(), style: .default, handler: { _ in
                
                self.accesId = 2
                self.privateBtn.setTitle("access_for_friends".localized(), for: .normal)
            })
            let forMeAction = UIAlertAction(title: "access_only_for_me".localized(), style: .default, handler: { _ in
                
                self.accesId = 3
                self.privateBtn.setTitle("access_only_for_me".localized(), for: .normal)
            })
        
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(allAction)
        alert.addAction(forFriendsAction)
        alert.addAction(forMeAction)
        alert.addAction(cancelAction)
        alert.view.tintColor = UIColor.navBlueColor
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func sendBtn(_ sender: Any) {
        var text = self.contentTV.text!
        if (text == "whats_new".localized()){
            text = ""
        }
        var parameters = ["publication_desc": text,"access_id":accesId,"share_publication_id":publication_id] as [String: Any]
        parameters["is_gallery"] = 0
       NetworkManager.makeRequest(.addNews(parameters), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            if (self.parentController is NewsController){
                let parentController = self.parentController as! NewsController
                parentController.refreshData()
            }
            self.dismiss(animated: true, completion: nil)
            
        })
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "whats_new".localized()
            textView.textColor = UIColor.lightGray
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
