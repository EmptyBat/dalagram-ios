//
//  ReportViewController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/3/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController {
    
    @IBOutlet weak var reportLb: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var spam_btn: UIButton!
    @IBOutlet weak var violince_btn: UIButton!
    @IBOutlet weak var mans_btn: UIButton!
    @IBOutlet weak var drugsBtn: UIButton!
    
    @IBOutlet weak var suicideBtn: UIButton!
    @IBOutlet weak var figthBtn: UIButton!
    @IBOutlet weak var kidsBtn: UIButton!
    @IBOutlet weak var sendReportButton: UIButton!
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var materialSwitch: UISwitch!
    @IBOutlet weak var callToViolenceSwitch: UISwitch!
    @IBOutlet weak var callToViolenceBtn: UIButton!
    @IBOutlet weak var violenceSwitch: UISwitch!
    @IBOutlet weak var violenceBtn: UIButton!
    @IBOutlet weak var kidsMaterialsSwitch: UISwitch!
    @IBOutlet weak var kidMaterialsBtn: UIButton!
    @IBOutlet weak var propogandaSwitch: UISwitch!
    @IBOutlet weak var insultSwitch: UISwitch!
    @IBOutlet weak var spamSwitch: UISwitch!
    var currentIndex = 0
    var publication_id = 0
    @IBAction func spambtn(_ sender: Any) {
    }
    @IBAction func insultBtn(_ sender: Any) {
    }
    @IBAction func materialsBtn(_ sender: Any) {
    }
    @IBAction func propogandaBtn(_ sender: Any) {
    }
    @IBAction func callToViolence(_ sender: Any) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sendReportButton.layer.cornerRadius = 16
        unSwitchAll()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        self.backgroundView.addGestureRecognizer(tap)
        
        self.backgroundView.isUserInteractionEnabled = true
        configLang()
        // Do any additional setup after loading the view.
    }
    private func configLang(){
        reportLb.text = "report".localized()
        sendBtn.setTitle("send_report".localized(), for: .normal)
        spam_btn.setTitle("spam".localized(), for: .normal)
        violince_btn.setTitle("insult".localized(), for: .normal)
        mans_btn.setTitle("adults".localized(), for: .normal)
        drugsBtn.setTitle("propaganda".localized(), for: .normal)
        suicideBtn.setTitle("suicide".localized(), for: .normal)
        figthBtn.setTitle("extremism".localized(), for: .normal)
        kidsBtn.setTitle("child".localized(), for: .normal)
    }
    @objc private func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func sendReportButton(_ sender: Any) {
        if (currentIndex != 0){
            let parameters = ["complaint_kind_id": currentIndex,"publication_id":publication_id] as [String : Any]
        NetworkManager.makeRequest(.sendReport(parameters), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
           self.dismiss(animated: true, completion: nil)
        
        })
        }
        else {
                 WhisperHelper.showErrorMurmur(title: "Выберите категорию!")
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.dismiss(animated: true, completion: nil)
    }
    private func unSwitchAll(){
       self.spamSwitch.isOn = false
       self.insultSwitch.isOn = false
       self.materialSwitch.isOn = false
       self.propogandaSwitch.isOn = false
       self.kidsMaterialsSwitch.isOn = false
       self.violenceSwitch.isOn = false
       self.callToViolenceSwitch.isOn = false
        
    }
    @IBAction func spamSwitch(_ sender: Any) {
        currentIndex = 1
        unSwitchAll()
        self.spamSwitch.isOn = true
    }
    @IBAction func insultSwitch(_ sender: Any) {
        currentIndex = 2
        unSwitchAll()
        self.insultSwitch.isOn = true
    }
    @IBAction func materialSwitch(_ sender: Any) {
        currentIndex = 3
        unSwitchAll()
        self.materialSwitch.isOn = true
    }
    @IBAction func propogandaSwitch(_ sender: Any) {
        currentIndex = 4
        unSwitchAll()
        self.propogandaSwitch.isOn = true
    }
    @IBAction func kidsMaterialSwitch(_ sender: Any) {
        currentIndex = 5
        unSwitchAll()
        self.kidsMaterialsSwitch.isOn = true
    }
    @IBAction func violenceSwitc(_ sender: Any) {
        currentIndex = 6
        unSwitchAll()
        self.violenceSwitch.isOn = true
    }
    @IBAction func callToViolenceSwitch(_ sender: Any) {
        currentIndex = 7
        unSwitchAll()
        self.callToViolenceSwitch.isOn = true
    }
}
