//
//  CommentAudioCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/5/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import AVFoundation
class CommentAudioCell: UITableViewCell,AVAudioPlayerDelegate {
    var audioPlayer : AVAudioPlayer!
    var audioUrl: URL? {
        didSet {
        }
    }
    @IBOutlet weak var avatarLeadingSpace: NSLayoutConstraint!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var durationLB: UILabel!
    @IBOutlet weak var titleLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        answerButton.setTitle("reply".localized(), for: .normal)
        reportButton.setTitle("report".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        let origImage = UIImage(named: "icon_mini_play")
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        playButton.setImage(tintedImage, for: .normal)
        playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
    }
}
