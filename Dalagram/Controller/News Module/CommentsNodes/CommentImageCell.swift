//
//  CommentImageCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/5/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class CommentImageCell: UITableViewCell {
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var avatarLeadingSpace: NSLayoutConstraint!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var imageContent: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
           answerButton.setTitle("reply".localized(), for: .normal)
        reportButton.setTitle("report".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
