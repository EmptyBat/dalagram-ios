//
//  CommentsFileCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/5/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class CommentsFileCell: UITableViewCell {
    @IBOutlet weak var avatarLeadingSpace: NSLayoutConstraint!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    
    @IBOutlet weak var file_extension_lb: UILabel!
    @IBOutlet weak var file_image: UIImageView!
    @IBOutlet weak var title_lb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        answerButton.setTitle("reply".localized(), for: .normal)
        reportButton.setTitle("report".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
