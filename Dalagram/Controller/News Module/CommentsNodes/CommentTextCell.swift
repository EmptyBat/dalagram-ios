//
//  CommentTextCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/4/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class CommentTextCell: UITableViewCell {
    
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var avatarLeadingSpace: NSLayoutConstraint!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var answerButton: UIButton!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    
    @IBOutlet weak var descriptionLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answerButton.setTitle("reply".localized(), for: .normal)
        reportButton.setTitle("report".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
