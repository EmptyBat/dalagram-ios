//
//  ShowAllTableViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 5/16/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class ShowAllTableViewCell: UITableViewCell {

    @IBOutlet weak var showAll_btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        showAll_btn.setTitle("show_all".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
