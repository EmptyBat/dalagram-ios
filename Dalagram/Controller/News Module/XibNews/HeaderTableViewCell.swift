//
//  HeaderTableViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/9/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var headerImage: UIImageView!
    
    @IBOutlet weak var post_lb: UIButton!
    @IBOutlet weak var subscribeHeight: NSLayoutConstraint!
    @IBOutlet weak var friends_lb: UIButton!
    @IBOutlet weak var addFriendHeight: NSLayoutConstraint!
    @IBOutlet weak var photos_lb: UIButton!
    @IBOutlet weak var followers_lb: UIButton!
    @IBOutlet weak var chatHeight: NSLayoutConstraint!
    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var addFriendButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var desckriptionLB: UILabel!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var photosCount: UILabel!
    @IBOutlet weak var friendsCount: UILabel!
    @IBOutlet weak var postCount: UILabel!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var avatarView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        post_lb.setTitle("posts".localized(), for: .normal)
        photos_lb.setTitle("photos".localized(), for: .normal)
        friends_lb.setTitle("friends".localized(), for: .normal)
        followers_lb.setTitle("followers".localized(), for: .normal)
        subscribeButton.layer.cornerRadius = 16
        subscribeButton.layer.borderWidth = 1.0
        subscribeButton.layer.borderColor = UIColor.darkBlueNavColor.cgColor
        
        addFriendButton.layer.cornerRadius = 16
        addFriendButton.layer.borderWidth = 1.0
        addFriendButton.layer.borderColor = UIColor.darkBlueNavColor.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
