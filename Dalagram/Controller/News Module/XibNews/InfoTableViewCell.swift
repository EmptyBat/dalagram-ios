//
//  InfoTableViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/9/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {
    @IBOutlet weak var title_lb: UILabel!
    @IBOutlet weak var infoHeight: NSLayoutConstraint!
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var subTitleLB: UILabel!
    @IBOutlet weak var titleLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.title_lb.text = "Information".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
