//
//  FriendsCollectionViewTableViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 11/2/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class FriendsCollectionViewTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var onlyFriendsBtn: UIButton!
    @IBOutlet weak var onlyFriends: UIView!
    
    @IBOutlet weak var onlyFriendsHeight: NSLayoutConstraint!
    @IBOutlet weak var friends: UIView!
    
    @IBOutlet weak var friendsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var footerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var friendsBtn: UIButton!
    @IBOutlet weak var myfridnsView: UIView!
    private var friensData: [JSONContact] = []
    private var myFriendsData: [JSONContact] = []
    @IBOutlet weak var friendsLine: UIView!
    @IBOutlet weak var myfriendsBtn: UIButton!
    @IBOutlet weak var friendsCollection: UICollectionView!
    var showIndex = 1
    var navig:UINavigationController?
    override func awakeFromNib() {
        super.awakeFromNib()
        friendsBtn.setTitle("friends_big".localized(), for: .normal)
        myfriendsBtn.setTitle("friends_all".localized(), for: .normal)
    }
    func setupOnlyFriends(friends:[JSONContact],navig:UINavigationController) {
        self.friensData = friends
        self.navig = navig
        if (self.friensData.count == 0){
            self.onlyFriendsHeight.constant = 0
            self.friendsHeight.constant = 0
            self.footerHeight.constant = 0
            self.collectionViewHeight.constant = 0
        }
        else {
            self.onlyFriendsHeight.constant = 33
            self.friendsHeight.constant = 33
            self.footerHeight.constant = 20
            self.collectionViewHeight.constant = 305
        }
        self.friends.isHidden = true
        self.onlyFriends.isHidden = false
          self.onlyFriendsBtn.setTitle("(\(self.friensData.count))" + " " + "friends".localized(), for: .normal)
        self.friendsCollection.delegate = self
        self.friendsCollection.dataSource = self
        let nibCell = UINib(nibName: "FriendsCollectionViewCell", bundle: nil)
        self.friendsCollection.register(nibCell, forCellWithReuseIdentifier: "cell")
        self.friendsCollection.reloadData()
    }
    func setupFriends(friends:[JSONContact],myfriends:[JSONContact],navig:UINavigationController) {
        self.navig = navig
        self.friends.isHidden = false
        self.onlyFriends.isHidden = true
        self.friensData = friends
        self.myFriendsData = myfriends
        if (self.friensData.count == 0){
            self.onlyFriendsHeight.constant = 0
            self.friendsHeight.constant = 0
            self.footerHeight.constant = 0
            self.collectionViewHeight.constant = 0
        }
        else {
            self.onlyFriendsHeight.constant = 33
            self.friendsHeight.constant = 33
            self.footerHeight.constant = 20
            self.collectionViewHeight.constant = 305
        }
        print("afafaa",friends,myfriends)
        self.friendsBtn.setTitle("(\(self.friensData.count))" + " " + "friends".localized(), for: .normal)
        self.myfriendsBtn.setTitle("(\(self.myFriendsData.count))" + " " + "friends_all".localized(), for: .normal)
        self.friendsCollection.delegate = self
        self.friendsCollection.dataSource = self
        let nibCell = UINib(nibName: "FriendsCollectionViewCell", bundle: nil)
        self.friendsCollection.register(nibCell, forCellWithReuseIdentifier: "cell")
        self.friendsCollection.reloadData()
        
        if (self.showIndex == 1){
            self.friendsLine.isHidden = true
            self.myfridnsView.isHidden = false
        }
        else {
            self.myfridnsView.isHidden = true
            self.friendsLine.isHidden = false
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FriendsCollectionViewCell
        if (showIndex == 1){
            cell.avatarIV.kf.setImage(with: URL(string:self.friensData[indexPath.row].avatar), placeholder: nil)
            cell.nameLabel.text = self.friensData[indexPath.row].nickname
        }
        else {
            cell.avatarIV.kf.setImage(with: URL(string:self.myFriendsData[indexPath.row].avatar), placeholder: nil)
            
            cell.nameLabel.text = self.myFriendsData[indexPath.row].nickname
        }
        return cell
}
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 15, left: 16, bottom: 0, right: 16)
    }
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(3 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(3))
        return CGSize(width: size, height: 140)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (showIndex == 1){
            return friensData.count
        }
        else {
            return myFriendsData.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (showIndex == 1){
               let item = friensData[indexPath.row]
                let vc = NewsController()
                vc.author_id = item.user_id
                vc.hidesBottomBarWhenPushed = true
                self.navig?.pushViewController(vc, animated: true)
        }
        else {
                let item = myFriendsData[indexPath.row]
                let vc = NewsController()
                vc.author_id = item.user_id
                vc.hidesBottomBarWhenPushed = true
                self.navig?.pushViewController(vc, animated: true)
        }

    }
    @IBAction func myFriendsBtn(_ sender: Any) {
        self.showIndex = 2
        self.friendsLine.isHidden = false
        self.myfridnsView.isHidden = true
        self.friendsCollection.reloadData()
    }
    @IBAction func friendsBtn(_ sender: Any) {
        self.showIndex = 1
        self.friendsLine.isHidden = true
        self.myfridnsView.isHidden = false
        self.friendsCollection.reloadData()
    }
}
