//
//  GalleryTableViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/9/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import Gallery
import SVProgressHUD
class GalleryTableViewCell: UITableViewCell , UICollectionViewDataSource, UICollectionViewDelegate ,SKPhotoBrowserDelegate,GalleryControllerDelegate {
    @IBOutlet weak var show_btn: UIButton!
    @IBOutlet weak var gallery_lb: UILabel!
    @IBOutlet weak var photoCountLB: UILabel!
    @IBOutlet weak var heightConstrain: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    var galleryData: [Gallery] = []
    var browser = SKPhotoBrowser()
    var currentIndex = 0
    var own_author = false
    var currentViewController:UIViewController?
    var navig:UINavigationController?
    var offset = 0
    var author_id = 0
    var profileValue = ProfileViewModel()
    var viewModel = NewsControllerViewModel(user_id: 0)
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        show_btn.setTitle("show_all".localized(), for: .normal)
    }
    
    func setupDialog(gallery:[Gallery] = [],vc:UIViewController,own_author:Bool,author_id:Int,profile:ProfileViewModel,navig:UINavigationController) {
        //set controller
        show_btn.setTitle("show_all".localized(), for: .normal)
        self.own_author = own_author
        if (own_author == true){
            offset = 1
        }
        self.profileValue = profile
        self.navig = navig
        self.author_id = author_id
        self.currentViewController = vc
        //SKPhoto
        SKPhotoBrowserOptions.displayAction = false
        SKPhotoBrowserOptions.displayStatusbar = true
        SKPhotoBrowserOptions.displayCounterLabel = false
        SKPhotoBrowserOptions.displayBackAndForwardButton = false
        SKPhotoBrowserOptions.displayGallery = true
        collectionView.delegate = self
        collectionView.dataSource = self
        // register cell
        let nibCell = UINib(nibName: "PhotoCell", bundle: nil)
        let nibAddCell = UINib(nibName: "AddPhotoCell", bundle: nil)
        collectionView.register(nibCell, forCellWithReuseIdentifier: "PhotoCell")
        collectionView.register(nibAddCell, forCellWithReuseIdentifier: "AddPhotoCell")
        collectionView.backgroundColor = UIColor.clear
        self.galleryData = gallery
        self.photoCountLB.text = "\(gallery.count)"
        self.collectionView.reloadData()
        if (self.galleryData.count != 0) || own_author{
            heightConstrain.constant = 121
            self.layoutIfNeeded()
        }
        else {
            heightConstrain.constant = 0
            self.layoutIfNeeded()
        }
            GalleryConfig()
    }
    func GalleryConfig(){
        // Gallery Picker Configs
        Config.Grid.FrameView.borderColor = UIColor.navBlueColor
        Config.Camera.imageLimit = 6
        Config.initialTab = Config.GalleryTab.imageTab
        Config.Camera.recordLocation = false
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.VideoEditor.savesEditedVideoToLibrary = false
    }
    func likePhoto(publication_id: Int,completion:@escaping ()->()) {
        let parameters = ["token": User.getToken(),"publication_id":publication_id] as [String : Any]
        NetworkManager.makeRequest(.likeNews(parameters), success: { (json)  in
            completion()
        })
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    // MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 9, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (own_author == true){
        return galleryData.count + 1
        }
        else {
        return galleryData.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (indexPath.row == 0) && own_author{
            let vc = InsertPhotoGalleryController.instantiate()
            
            vc.avatarUrl = profileValue.avatar.value
            vc.userName = profileValue.nickname.value
            vc.galleryCell = self
            vc.hidesBottomBarWhenPushed = true
            currentViewController?.navigationController?.pushViewController(vc, animated: true)
            
       //     let gallery = GalleryController()
        //    gallery.delegate = self
         //   currentViewController!.present(gallery, animated: true, completion: nil)
        }
        else {
            print("asfsafsaffsa",galleryData[indexPath.row - offset])
        currentIndex = indexPath.row - offset
        SKToolbarOptions.like_count = galleryData[indexPath.row - offset].like_count
        SKToolbarOptions.is_i_liked = galleryData[indexPath.row - offset].is_i_liked
        SKToolbarOptions.resend_count = galleryData[indexPath.row - offset].share_count
        SKToolbarOptions.view_count = galleryData[indexPath.row - offset].view_count
        SKToolbarOptions.comment_count = galleryData[indexPath.row -  offset].comment_count
        browser = SKPhotoBrowser(photos: createWebPhotos())
        browser.initializePageIndex(indexPath.row - offset)
        browser.delegate = self
        self.currentViewController!.present(browser, animated: true, completion: nil)
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (indexPath.row == 0)&&(own_author){
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPhotoCell", for: indexPath) as! AddPhotoCell
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.photo.kf.setImage(with: URL(string: galleryData[indexPath.row - offset].file_list[0].file_url.encodeUrl()!), placeholder: nil)
        
        return cell
    }
}
extension GalleryTableViewCell {
    func didShowPhotoAtIndex(_ index: Int) {
      //  print("test did show but i know")
        collectionView.visibleCells.forEach({$0.isHidden = false})
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
    }
    func didScrollToIndex(_ browser: SKPhotoBrowser, index: Int){
        currentIndex = index
        SKToolbarOptions.like_count = galleryData[index].like_count
        SKToolbarOptions.is_i_liked = galleryData[index].is_i_liked
        SKToolbarOptions.resend_count = galleryData[index].share_count
        SKToolbarOptions.view_count = galleryData[index].view_count
        SKToolbarOptions.comment_count = galleryData[index].comment_count
        browser.updateToolbarAction()
    }
    func ActionButtonClicked(_ index: Int) {
        if (index == 0){
            browser.backLikeAction(i_liked:galleryData[currentIndex].is_i_liked)
            likePhoto(publication_id:galleryData[index].publication_id) { [weak self] in
            }
            if (galleryData[currentIndex].is_i_liked == 0){
                galleryData[currentIndex].is_i_liked = 1
                galleryData[currentIndex].like_count += 1
            }
            else {
                galleryData[currentIndex].is_i_liked = 0
                galleryData[currentIndex].like_count -= 1
            }
            }
        else if (index == 1){
            let item = galleryData[currentIndex]
            let newsController = NewsHistory()
            newsController.publication_id = item.publication_id
            newsController.file_list.append(item.file_list[0])
            newsController.avatar = item.avatar
            newsController.is_i_liked = item.is_i_liked
            newsController.nickname = item.nickname
            
            newsController.publication_desc = item.publication_desc
            newsController.publication_date = item.publication_date
            newsController.view_count = item.view_count
            newsController.share_count = item.share_count
            newsController.comment_count = item.comment_count
            newsController.like_count = item.like_count
            
            currentViewController!.dismiss(animated: true, completion: nil)
            
            let vc = NewsControllerDetail.instantiate()
            vc.hidesBottomBarWhenPushed = true
            vc.newsHistory = newsController
            vc.showComments = true
            vc.galleryCell = self
            vc.isGallery = true
            currentViewController?.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    func willDismissAtPageIndex(_ index: Int) {
        collectionView.visibleCells.forEach({$0.isHidden = false})
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
    }
    
    func willShowActionSheet(_ photoIndex: Int) {
    }
    
    func didDismissAtPageIndex(_ index: Int) {
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = false
    }
    
    func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
        // handle dismissing custom actions
    }
    
    func removePhoto(_ browser: SKPhotoBrowser, index: Int, reload: @escaping (() -> Void)) {
        reload()
    }
    
    func viewForPhoto(_ browser: SKPhotoBrowser, index: Int) -> UIView? {
        return collectionView.cellForItem(at: IndexPath(item: index, section: 0))
    }
    
    func captionViewForPhotoAtIndex(index: Int) -> SKCaptionView? {
        return nil
    }
    public func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        print("Selected Gallery Image", images.count)
        if images.count == 1 {
            // Single Image
            if let selectedImage = images.first {
                selectedImage.resolve(completion: { (resolvedImage) in
                    if let image = resolvedImage {
                        if let data = UIImageJPEGRepresentation(image, 0.5) {
                            self.viewModel.uploadMultipleNewsFile(with: data, format: "image"){ [weak self] (file) in
                                let news = NewsCreater()
                                news.index = self!.currentIndex
                                news.flle_url = file["file_url"]!
                                news.newsType = .image
                                self!.collectionView.reloadData()
                                let photo = SKPhoto.photoWithImageURL(file["file_url"]!.encodeUrl()!)
                                photo.shouldCachePhotoURLImage = true
                            }
                        }
                    }
                })
                currentViewController!.dismiss(animated: true, completion: nil)
            }
        } else {
            // Mutiple Images
            Image.resolve(images: images) { (resolvedImages) in
                if let images = resolvedImages as? [UIImage] {
                    SVProgressHUD.show()
                    for index in 0..<resolvedImages.count {
                        if let data = UIImageJPEGRepresentation(resolvedImages[index]!, 0.5) {
                            self.viewModel.uploadMultipleNewsFile(with: data, format: "image"){ [weak self] (file) in
                                let news = NewsCreater()
                                news.index = self!.currentIndex
                                news.newsType = .image
                                news.flle_url = file["file_url"]!
                                self!.collectionView.reloadData()
                                let photo = SKPhoto.photoWithImageURL(file["file_url"]!.encodeUrl()!)
                                photo.shouldCachePhotoURLImage = true
                            }
                        }
                    }
                    //     let imageContentNode = ImageContentNode(image: selectedImage, date: Date().getStringTime(), currentVC: self.controller)
                    //       return imageContentNode
                    //       }
                    //     _ = self.controller.sendCollectionViewWithNodes(imageNodes, numberOfRows: CGFloat(imageNodes.count), isIncomingMessage: false)
                    self.currentViewController!.dismiss(animated: true, completion: nil)
                    SVProgressHUD.dismiss()
                    
                }
            }
        }
    }
    public func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        print("Select video delegate")
    }
    
    public func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        print("requestLightbox delegate")
    }
    
    public func galleryControllerDidCancel(_ controller: GalleryController) {
        currentViewController!.dismiss(animated: true, completion: nil)
    }
    @IBAction func showALLButton(_ sender: Any) {
        let gallery = UserGalleryController()
        gallery.author_id = author_id
        navig?.pushViewController(gallery, animated: true)
    }
}
private extension GalleryTableViewCell {

    func createWebPhotos() -> [SKPhotoProtocol] {
        return (0..<galleryData.count).map { (i: Int) -> SKPhotoProtocol in
            let photo = SKPhoto.photoWithImageURL(galleryData[i].file_list[0].file_url.encodeUrl()!)
            photo.caption = galleryData[i].publication_desc
            photo.shouldCachePhotoURLImage = true
            return photo
        }
    }
}
