//
//  PhotoCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/11/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var photo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         layer.masksToBounds = true
         photo.layer.cornerRadius =  12
    }

}
