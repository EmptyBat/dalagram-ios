//
//  ShowAllCommentsTableViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 5/24/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class ShowAllCommentsTableViewCell: UITableViewCell {
    @IBOutlet weak var title_btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
