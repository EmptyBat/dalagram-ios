//
//  ContentInputTableViewCell.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/18/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

class ContentInputTableViewCell: UITableViewCell {
    @IBOutlet weak var userCreds: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    
    @IBOutlet weak var clickTextView: UITextView!
    @IBOutlet weak var clickButton: UIButton!
    @IBOutlet weak var imageLb: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clickTextView.text = "enter_mesage".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
