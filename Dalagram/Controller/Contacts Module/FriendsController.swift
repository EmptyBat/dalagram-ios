//
//  FriendsController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/9/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import Contacts
import RealmSwift
import RxSwift
class FriendsController: UITableViewController {
    
    // MARK: - IBOutlets
    var contacts: Results<Object>?
    var currentViewController: UIViewController?
    fileprivate var searchBar: UISearchBar = UISearchBar()
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var parentNavigationController : UINavigationController?
    lazy var titleView: DialogNavigationTitleView = {
        let titleView = DialogNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    
    lazy var inviteFriendsView: InviteFriendsView = {
        let view = InviteFriendsView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 55))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(inviteFriendsPressed))
        view.addGestureRecognizer(tapGesture)
        return view
    }()
    
    // MARK: - Variables
    
    let viewModel = ContactsViewModel()
    
    // : isMainController resposible for reusing controller as contacts picker
    
    var isMainController: Bool = false
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setBlueNavBar()
        configureNavBar()
        configureEvents()
        setupData()
        getFriends()
        getInComingFriends()
        getBlocked()
        getOutComingFriends()
    }
    func getFriends(){
        viewModel.getFriends() { [weak self] in
            self?.tableView.reloadData()
        }
    }
    func getInComingFriends(){
        viewModel.getInComingFriends() { [weak self] in
            self?.tableView.reloadData()
        }
    }
    func getBlocked(){
        viewModel.getBlocked() { [weak self] in
            self?.tableView.reloadData()
        }
    }
    func getOutComingFriends(){
        viewModel.getOutComingFriends() { [weak self] in
            self?.tableView.reloadData()
        }
    }
    // MARK: - Configuring UI
    
    func configureUI() {
        setEmptyBackTitle()
        view.backgroundColor = UIColor.white
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.groupTableViewBackground
        tableView.registerNib(ContactCell.self)
        tableView.registerNib(FriendsRequestCell.self)
        tableView.registerNib(BlockedUserCell.self)
        
        
    }
    @objc func searchAction() {
        searchBar.becomeFirstResponder()
    }
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }
    // MARK: - Invite Friends Action
    
    @objc func inviteFriendsPressed() {
        let vc = InviteController()
        vc.viewModel = viewModel
        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    
}

extension FriendsController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            if (self.viewModel.inComingData.count != 0){
                return "income_friends".localized()
            }
            else {
                return nil
            }
        case 1:
            if (self.viewModel.outComingData.count != 0){
                return "outcome_friends".localized()
            }
            else {
                return nil
            }
        case 2:
            if (self.viewModel.friensData.count != 0){
                return "friends".localized()
            }
            else {
                return nil
            }
        case 3:
            if (self.viewModel.blockedData.count != 0){
                return "blocked_users".localized()
            }
            else {
                return nil
            }
        default:
            return ""
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return viewModel.inComingData.count
        case 1:
            return viewModel.outComingData.count
        case 2:
            return viewModel.friensData.count
        case 3:
            return viewModel.blockedData.count
            
        default:
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
        
        let cell: FriendsRequestCell = tableView.dequeReusableCell(for: indexPath)
        
        cell.setupRegisteredContact(self.viewModel.inComingData[indexPath.row] as! JSONContact)
        cell.acceptButton.addTarget(self, action:#selector(acceptAction), for: .touchUpInside)
        cell.rejectButton.addTarget(self, action:#selector(rejectInAction), for: .touchUpInside)
        cell.acceptButton.tag = indexPath.row
        cell.rejectButton.tag = indexPath.row
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedIncoming))
        cell.avatarView.isUserInteractionEnabled = true
        cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
        cell.avatarView.tag = indexPath.row
        return cell
        case 1:
        let cell: FriendsRequestCell = tableView.dequeReusableCell(for: indexPath)
            
        cell.setupRegisteredContact(self.viewModel.outComingData[indexPath.row] as! JSONContact)
        cell.rejectButton.addTarget(self, action:#selector(rejectOutAction), for: .touchUpInside)
        cell.acceptButton.isHidden = true
        cell.rejectButton.tag = indexPath.row
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedOutcoming))
        cell.avatarView.isUserInteractionEnabled = true
        cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
        cell.avatarView.tag = indexPath.row
        
        return cell
        
        case 2:
            let cell: ContactCell = tableView.dequeReusableCell(for: indexPath)
            
            cell.setupRegisteredContact(self.viewModel.friensData[indexPath.row] as! JSONContact)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(avatarClickedFriends))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizer)
            cell.avatarView.tag = indexPath.row
            
        return cell
        default:
        let cell: BlockedUserCell = tableView.dequeReusableCell(for: indexPath)
            
        cell.setupRegisteredContact(self.viewModel.blockedData[indexPath.row] as! JSONContact)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(unblockAction))
        cell.icon_blocked.isUserInteractionEnabled = true
        cell.icon_blocked.addGestureRecognizer(tapGestureRecognizer)
        cell.icon_blocked.tag = indexPath.row
        
        let tapGestureRecognizerAvatar = UITapGestureRecognizer(target: self, action: #selector(avatarClickedBlocked))
        cell.avatarView.isUserInteractionEnabled = true
        cell.avatarView.addGestureRecognizer(tapGestureRecognizerAvatar)
        cell.avatarView.tag = indexPath.row
            
        return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            /// Need to pass dialogInfo and dialogId which is user_id + prefix "U" as User
            if let contactItem = contacts![indexPath.row] as? Contact {
                let contactInfo = DialogInfo(contact: contactItem)
                let vc = ChatController(info: contactInfo, dialogId: String(contactItem.user_id) + "U")
                vc.hidesBottomBarWhenPushed = true
                self.tableView.tableHeaderView = inviteFriendsView
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 1:
            let user_id = self.viewModel.friensData[indexPath.row].user_id
            let vc = NewsController()
            vc.author_id = user_id
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case 2:
            let user_id = self.viewModel.friensData[indexPath.row].user_id
            let vc = NewsController()
            vc.author_id = user_id
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
            let user_id = self.viewModel.blockedData[indexPath.row].user_id
            let vc = NewsController()
            vc.author_id = user_id
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }
    
    @objc func acceptAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let user_id = self.viewModel.inComingData[myIndexPath.row].user_id
        self.viewModel.acceptRequest(partner_id: user_id,success: {
            self.getInComingFriends()
            self.getFriends()
            
        })
    }
    @objc func rejectInAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let user_id = self.viewModel.inComingData[myIndexPath.row].user_id
        self.viewModel.rejectRequest(partner_id: user_id,success: {
            self.getInComingFriends()
        })
    }
    @objc func rejectOutAction(_ sender: UIButton) {
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let user_id = self.viewModel.outComingData[myIndexPath.row].user_id
        self.viewModel.rejectRequest(partner_id: user_id,success: {
            self.getInComingFriends()
        })
    }
    @objc func unblockAction(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        let item = self.viewModel.blockedData[myIndexPath.row].user_id
        self.viewModel.blockUserRequest(partner_id: item,success: {
            self.getBlocked()
        })
    }
    @objc func avatarClickedIncoming(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        let user_id = self.viewModel.inComingData[myIndexPath.row].user_id
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func avatarClickedOutcoming(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        let user_id = self.viewModel.outComingData[myIndexPath.row].user_id
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func avatarClickedBlocked(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        let user_id = self.viewModel.blockedData[myIndexPath.row].user_id
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func avatarClickedFriends(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        let user_id = self.viewModel.friensData[myIndexPath.row].user_id
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension FriendsController {
    
    // MARK: - Configuring NavBar TitleView
    
    func configureNavBar() {
        
        let titleItem = UIBarButtonItem(customView: titleView)
        self.navigationItem.leftBarButtonItem = titleItem
        self.navigationItem.leftItemsSupplementBackButton = true
        
        
        let imagePlaceholder = #imageLiteral(resourceName: "placeholder")
        titleView.userAvatarView.image = imagePlaceholder
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        titleView.userAvatarView.isUserInteractionEnabled = true
        titleView.userAvatarView.addGestureRecognizer(tapGestureRecognizer)
    }
    // MARK: - NavBar TitleView Action
    
    @objc func userTitleViewPressed() {
        
        let transition = CATransition()
        transition.duration = 0.35
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: avatarUrl), placeholder: nil)
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let user_id = viewModelProfile.user_id.value
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.show(vc, sender: nil)
    }
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
        })
    }
    
}
