//
//  OwnContactsController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/13/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import Contacts
import RealmSwift


class OwnContactsController: UITableViewController {
    
    // MARK: - IBOutlets
    
    fileprivate var searchBar: UISearchBar = UISearchBar()
    
    lazy var closeButton: UIBarButtonItem = {
        let button = UIBarButtonItem(title: "Закрыть", style: .plain, target: self, action: #selector(closeButtonAction))
        button.tintColor = UIColor.white
        return button
    }()
    
    // MARK: - Variables
    
    let viewModel = ContactsViewModel()
    var completionBlock: AllContactsCompletion! = nil
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Выберите"
        
        configureUI()
        setBlueNavBar()
        
        viewModel.fetchContacts { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
            self?.viewModel.getContacts(onSuccess: {
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    // MARK: - Configuring UI
    
    func configureUI() {
        setEmptyBackTitle()
        view.backgroundColor = UIColor.white
        
        //MARK: SearchBar
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "search".localized()
        searchBar.sizeToFit()
        
        tableView.tableHeaderView = searchBar
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.groupTableViewBackground
        tableView.registerNib(ContactCell.self)
        
        self.navigationItem.leftBarButtonItem = closeButton
    }
    
    @objc func closeButtonAction() {
        self.dismissController()
    }
}

extension OwnContactsController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.phoneContacts.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return viewModel.phoneContacts[section].value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ContactCell = tableView.dequeReusableCell(for: indexPath)
            let contact = viewModel.phoneContacts[indexPath.section].value[indexPath.row]
            cell.setupSystemContact(contact, section: indexPath.section)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let contact = viewModel.phoneContacts[indexPath.section].value[indexPath.row]
            let phoneContact = ContactFacade(phoneContact: contact)
            completionBlock(phoneContact)
            dismissController()
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return viewModel.letters
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let sectioView = view as! UITableViewHeaderFooterView
        sectioView.textLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.init(0))
    }
    
    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return  viewModel.phoneContacts[section].key
    }
    
}
