//
//  FollowersController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/9/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import Contacts
import RealmSwift
import RxSwift
class FollowersController: UITableViewController {
    
    // MARK: - IBOutlets
    var contacts: Results<Object>?
    var currentViewController: UIViewController?
    fileprivate var searchBar: UISearchBar = UISearchBar()
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var parentNavigationController : UINavigationController?
    lazy var titleView: DialogNavigationTitleView = {
        let titleView = DialogNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    
    lazy var inviteFriendsView: InviteFriendsView = {
        let view = InviteFriendsView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 55))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(inviteFriendsPressed))
        view.addGestureRecognizer(tapGesture)
        return view
    }()
    
    // MARK: - Variables
    
    let viewModel = ContactsViewModel()
    
    // : isMainController resposible for reusing controller as contacts picker
    
    var isMainController: Bool = false
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setBlueNavBar()
        configureNavBar()
        configureEvents()
        setupData()
        viewModel.getFollowers() { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    // MARK: - Configuring UI
    
    func configureUI() {
        setEmptyBackTitle()
        view.backgroundColor = UIColor.white
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.groupTableViewBackground
        tableView.registerNib(ContactCell.self)
        
    }
    @objc func searchAction() {
        searchBar.becomeFirstResponder()
    }
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }
    // MARK: - Invite Friends Action
    
    @objc func inviteFriendsPressed() {
        let vc = InviteController()
        vc.viewModel = viewModel
        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    
}

extension FollowersController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return self.viewModel.followersData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ContactCell = tableView.dequeReusableCell(for: indexPath)
            
        cell.setupRegisteredContact(self.viewModel.followersData[indexPath.row] as! JSONContact)
        let tapGestureRecognizerAvatar = UITapGestureRecognizer(target: self, action: #selector(avatarClicked))
        cell.avatarView.isUserInteractionEnabled = true
        cell.avatarView.addGestureRecognizer(tapGestureRecognizerAvatar)
        cell.avatarView.tag = indexPath.row
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            /// Need to pass dialogInfo and dialogId which is user_id + prefix "U" as User
            if let contactItem = contacts![indexPath.row] as? Contact {
                let contactInfo = DialogInfo(contact: contactItem)
                let vc = ChatController(info: contactInfo, dialogId: String(contactItem.user_id) + "U")
                vc.hidesBottomBarWhenPushed = true
                self.tableView.tableHeaderView = inviteFriendsView
                self.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
    
   
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @objc func avatarClicked(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
        let user_id = self.viewModel.followersData[myIndexPath.row].user_id
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension FollowersController {
    
    // MARK: - Configuring NavBar TitleView
    
    func configureNavBar() {
        
        let titleItem = UIBarButtonItem(customView: titleView)
        self.navigationItem.leftBarButtonItem = titleItem
        self.navigationItem.leftItemsSupplementBackButton = true
        
        
        let imagePlaceholder = #imageLiteral(resourceName: "placeholder")
        titleView.userAvatarView.image = imagePlaceholder
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        titleView.userAvatarView.isUserInteractionEnabled = true
        titleView.userAvatarView.addGestureRecognizer(tapGestureRecognizer)
    }
    // MARK: - NavBar TitleView Action
    
    @objc func userTitleViewPressed() {
        
        let transition = CATransition()
        transition.duration = 0.35
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: avatarUrl), placeholder: nil)
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let vc = SettingsController.instantiate()
        self.show(vc, sender: nil)
    }
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
        })
    }
    
}
