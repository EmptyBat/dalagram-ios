//
//  ContactsController.swift
//  Dalagram
//
//  Created by Toremurat on 19.05.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import Contacts
import RealmSwift
import RxSwift

protocol ContactsSearchActionOutput {
    func SearchhideAction()
}
class ContactsController: UITableViewController {

    // MARK: - IBOutlets
    var contacts: Results<Object>?
    var ouput: ContactsSearchActionOutput?
    var currentViewController: UIViewController?
    fileprivate var searchBar: UISearchBar = UISearchBar()
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var parentNavigationController : UINavigationController?
    lazy var titleView: DialogNavigationTitleView = {
        let titleView = DialogNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    
    lazy var inviteFriendsView: InviteFriendsView = {
        let view = InviteFriendsView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 55))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(inviteFriendsPressed))
        view.addGestureRecognizer(tapGesture)
        return view
    }()
    
    // MARK: - Variables
    
    let viewModel = ContactsViewModel()
    
    // : isMainController resposible for reusing controller as contacts picker
    
    var isMainController: Bool = false
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setBlueNavBar()
        configureNavBar()
        configureEvents()
        setupData()
        viewModel.fetchContacts { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
            self?.viewModel.getContacts(onSuccess: {
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            })
        }
    }
    
    // MARK: - Configuring UI
    
    func configureUI() {
        if let vc = currentViewController as? ContactsSearchActionOutput {
            self.ouput = vc
        }
        contacts = RealmManager.shared.getObjects(type: Contact.self)
        setEmptyBackTitle()
        view.backgroundColor = UIColor.white
        tableView.tableHeaderView = inviteFriendsView
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.groupTableViewBackground
        tableView.registerNib(ContactCell.self)
        
    }
    @objc func searchAction() {
        searchBar.becomeFirstResponder()
    }
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }
    // MARK: - Invite Friends Action
    
    @objc func inviteFriendsPressed() {
        let vc = InviteController()
        vc.viewModel = viewModel
        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    

}

extension ContactsController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.phoneContacts.count + 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return (contacts?.count)!
        default:
            return viewModel.phoneContacts[section - 1].value.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ContactCell = tableView.dequeReusableCell(for: indexPath)
        if indexPath.section == 0 {
         
            cell.setupRegisteredContact(contacts![indexPath.row] as! Contact)
            
            let tapGestureRecognizerAvatar = UITapGestureRecognizer(target: self, action: #selector(avatarClicked))
            cell.avatarView.isUserInteractionEnabled = true
            cell.avatarView.addGestureRecognizer(tapGestureRecognizerAvatar)
            cell.avatarView.tag = indexPath.row
            
        } else {
            let contact = viewModel.phoneContacts[indexPath.section - 1].value[indexPath.row]
            cell.setupSystemContact(contact, section: indexPath.section)
            
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            /// Need to pass dialogInfo and dialogId which is user_id + prefix "U" as User
            if let contactItem = contacts![indexPath.row] as? Contact {
                    let contactInfo = DialogInfo(contact: contactItem)
                    let vc = ChatController(info: contactInfo, dialogId: String(contactItem.user_id) + "U")
                    vc.hidesBottomBarWhenPushed = true
                    self.ouput?.SearchhideAction()
                    self.tableView.tableHeaderView = inviteFriendsView
                    self.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return viewModel.letters
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let sectioView = view as! UITableViewHeaderFooterView
        sectioView.textLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.init(0))
    }

    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return index + 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0){
            return "dalagram_users".localized()
        }
        else {
            return section != 0 ? viewModel.phoneContacts[section - 1].key : " "
        }
    }
    @objc func avatarClicked(_ sender: UITapGestureRecognizer) {
        let myIndexPath = NSIndexPath(row: sender.view!.tag, section: 0)
         if let contactItem = contacts![myIndexPath.row] as? Contact {
        let vc = NewsController()
        vc.author_id = contactItem.user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    }
}
extension ContactsController {
    
    // MARK: - Configuring NavBar TitleView
    
    func configureNavBar() {
        
        let titleItem = UIBarButtonItem(customView: titleView)
        self.navigationItem.leftBarButtonItem = titleItem
        self.navigationItem.leftItemsSupplementBackButton = true

        
        let imagePlaceholder = #imageLiteral(resourceName: "placeholder")
        titleView.userAvatarView.image = imagePlaceholder
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        titleView.userAvatarView.isUserInteractionEnabled = true
        titleView.userAvatarView.addGestureRecognizer(tapGestureRecognizer)
    }
    // MARK: - NavBar TitleView Action
    
    @objc func userTitleViewPressed() {
        
        let transition = CATransition()
        transition.duration = 0.35
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: avatarUrl), placeholder: nil)
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let vc = SettingsController.instantiate()
        self.show(vc, sender: nil)
    }
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
        })
}
   
}
extension ContactsController: ContactSearchOutput {
    
    func SearchContasctsAction(text: String,id: Int,state : Bool) {
        print(title)
            self.tableView.tableHeaderView = nil
        if (state == false){
        let searchResults =  RealmManager.shared.getObjects(type: Contact.self)!.filter("user_name CONTAINS[c] '\(text)' OR contact_name CONTAINS[c] '\(text)'")
        print("afsfsafas",searchResults)
        if searchResults.count != 0 {
            self.contacts = searchResults
        }
        self.tableView.reloadData()
    }
        else {
            self.tableView.tableHeaderView = inviteFriendsView
        }
    }
}
