//
//  CompanyProductsController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/17/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import SafariServices

class CompanyProductsController: UITableViewController {
    var projectsData: [Projects] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Проекты Qaz Holding"
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 400
        self.getProjects()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    func getProjects() {
        self.projectsData.removeAll()
        let parameters = ["token": User.getToken()] as [String : Any]
        NetworkManager.makeRequest(.getProjects(parameters), success: { (json) in
            print("zxzxzx",json["data"])
            for (_, subJson):(String, JSON) in json["data"] {
                let new = Projects(json: subJson)
                print("121212dd",new)
                self.projectsData.append(new)
            }
            self.tableView.reloadData()
        })
    }
    // MARK: - Table view data source
    static func instantiate() -> CompanyProductsController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CompanyProductsController") as! CompanyProductsController
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.projectsData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
       let name = cell.viewWithTag(1) as? UILabel
       let text = cell.viewWithTag(2) as? UILabel
       let image = cell.viewWithTag(3) as? UIImageView
        name?.text = self.projectsData[indexPath.row].project_name
        text?.text = self.projectsData[indexPath.row].project_desc
        image?.kf.setImage(with: URL(string: self.projectsData[indexPath.row].project_image.encodeUrl()!), placeholder: nil)
        // Configure the cell...

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let vc = SFSafariViewController(url: URL(string:self.projectsData[indexPath.row].website.encodeUrl()!)!)
        self.present(vc, animated: true, completion: nil)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
