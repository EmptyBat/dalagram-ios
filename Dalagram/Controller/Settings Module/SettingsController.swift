//
//  SettingsController.swift
//  Dalagram
//
//  Created by Toremurat on 21.05.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import SKPhotoBrowser
import Localize_Swift
class SettingsController: UITableViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var exitLb: UILabel!
    @IBOutlet weak var changeLanguageLb: UILabel!
    @IBOutlet weak var blocked_lb: UILabel!
    @IBOutlet weak var alert_lb: UILabel!
    @IBOutlet weak var editProfile_lb: UILabel!
    // MARK: - Variables
    
    var viewModel = ProfileViewModel()
    let disposeBag = DisposeBag()
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureEvents()
        setBlueNavBar()
        configureUI()
        setupData()
        configureLang()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    // MARK: - Configuring UI
    private func configureLang() {
        self.exitLb.text = "exit".localized()
        self.blocked_lb.text = "blocked_users".localized()
        self.alert_lb.text = "notification_sound".localized()
        self.editProfile_lb.text = "edit_profile".localized()
     self.navigationItem.title = "profile_settings".localized()
        self.changeLanguageLb.text = "change_language".localized()
    }
    private func configureUI() {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.lightGrayColor
        tableView.tableFooterView = footerView
        setEmptyBackTitle()
    }
    
    private func configureEvents() {
        viewModel.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        
        viewModel.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.profileImage.kf.setImage(with: URL(string: avatarUrl), placeholder: #imageLiteral(resourceName: "bg_navbar_sky"))
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading 
    
    private func setupData() {
        loader.startAnimating()
        viewModel.getProfile(onCompletion: { [weak self] in
            self?.loader.stopAnimating()
            self?.loader.isHidden = true
            self?.phoneLabel.text = self?.viewModel.phone.value
            self?.nameLabel.text  = self?.viewModel.name.value
        })
    }
    static func instantiate() -> SettingsController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsController") as! SettingsController
    }
}

// MARK: TableView Delegate & DataSource

extension SettingsController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: // >> Avatar
            if let userImage = profileImage.image {
                let photo = SKPhoto.photoWithImage(userImage)
                let browser = SKPhotoBrowser(photos: [photo])
                browser.initializePageIndex(0)
                self.present(browser, animated: true, completion: nil)
            }
        case 1: // >> Edit
            let vc = EditProfileController.fromStoryboard()
            vc.viewModel = viewModel
            self.show(vc, sender: nil)
        case 2: // >> Notifications
            let vc = NotificationsController.fromStoryboard()
            self.show(vc, sender: nil)
        case 4: // >> Notifications
    let actionSheetController = UIAlertController(title: "title_setting".localized(), message: "", preferredStyle: .actionSheet)
    
    let cancelActionButton = UIAlertAction(title: "Cancel".localized(), style: .cancel) { action -> Void in
        print("Отмена")
    }
    actionSheetController.addAction(cancelActionButton)
    
    let deleteActionButton = UIAlertAction(title: "Қазақ тілі", style: .default) { action -> Void in
        print("Қазақ тілі")
        Localize.setCurrentLanguage("kk-KZ")
        self.configureLang()
    }
    actionSheetController.addAction(deleteActionButton)
    
    let saveActionButton = UIAlertAction(title: "Русский язык", style: .default) { action -> Void in
        print("Русский")
        Localize.setCurrentLanguage("ru")
        self.configureLang()
    }
    actionSheetController.addAction(saveActionButton)
    
    let endActionButton = UIAlertAction(title: "English", style: .default) { action -> Void in
        print("English")
        Localize.setCurrentLanguage("en")
        self.configureLang()
    }
    actionSheetController.addAction(endActionButton)
    self.present(actionSheetController, animated: true, completion: nil)
    
        case 5: // >> Logout
            User.removeUser()
            RealmManager.shared.deleteDatabase()
            AppDelegate.shared().configureRootController(isLogged: false)
        default:
            break
        }
    }
}
