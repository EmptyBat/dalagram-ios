//
//  ReplyController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/8/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift
typealias EditGroupCompletionBlockReply = () -> Void

class ReplyController: UITableViewController {
    var contacts: Results<Object>?
    // MARK: - IBOutlets
    
    fileprivate var searchBar: UISearchBar = UISearchBar()
    
    // MARK: - Variables
    
    let viewModel = ContactsViewModel()
    var chatType: DialogType = .single
    var dialogInfo: DialogInfo?
    var chatIds = [Int]()
    var groupCompletionReply: EditGroupCompletionBlock! = nil
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var avatarUrl = ""
    // MARK: - Initializer
    
    convenience init(chatType: DialogType,chatIds:[Int], dialogInfo: DialogInfo? = nil) {
        self.init()
        self.chatType = chatType
        self.dialogInfo = dialogInfo
        self.chatIds = chatIds
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setBlueNavBar()
        setupData()
        configureEvents()
        viewModel.fetchContacts { [weak self] in
            self?.viewModel.getContacts(onSuccess: {
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            })
        }
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self!.avatarUrl = avatarUrl
        }).disposed(by: disposeBag)
    }
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
        })
    }
    // MARK: - Configuring UI
    
    func configureUI() {
        setEmptyBackTitle()
        view.backgroundColor = UIColor.white
        contacts = RealmManager.shared.getObjects(type: Contact.self)
        //MARK: SearchBar
        searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        searchBar.placeholder = "search_contacts".localized()
        searchBar.sizeToFit()
        
        tableView.tableHeaderView = searchBar
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.groupTableViewBackground
        tableView.registerNib(ContactCell.self)
        
    }
    
}
extension ReplyController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (contacts?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ContactCell = tableView.dequeReusableCell(for: indexPath)
        cell.setupRegisteredContact(contacts![indexPath.row] as! Contact)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let contact = contacts![indexPath.row] as? Contact {
                switch chatType {
                case .single:
                    let contactInfo = DialogInfo(contact: contact)
                    let vc = ChatController(info: contactInfo, dialogId: String(contact.user_id) + "U",avatarUrl:avatarUrl, chatIds:chatIds)
                    vc.hidesBottomBarWhenPushed = true
                    self.show(vc, sender: nil)
                case .group:
                    if let info = dialogInfo {
                        NetworkManager.makeRequest(.addGroupMember(group_id: info.group_id, user_id: contact.user_id), success: { [weak self] (json) in
                            guard let vc = self else { return }
                            NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
                            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                            vc.navigationController?.popViewController(animated: true)
                            vc.groupCompletionReply()
                        })
                    }
                case .channel:
                    if let info = dialogInfo {
                        NetworkManager.makeRequest(.addChannelMember(channel_id: info.channel_id, user_id: contact.user_id), success: { [weak self] (json) in
                            guard let vc = self else { return }
                            NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
                            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                            vc.navigationController?.popViewController(animated: true)
                            vc.groupCompletionReply()
                        })
                }
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}
extension ReplyController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        let searchResults =  RealmManager.shared.getObjects(type: Contact.self)!.filter("user_name CONTAINS[c] '\(searchText)' OR contact_name CONTAINS[c] '\(searchText)'")
        print("afsfsafas",searchResults)
        if searchResults.count != 0 {
            self.contacts = searchResults
        }
        self.tableView.reloadData()

    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
          self.contacts = RealmManager.shared.getObjects(type: Contact.self)
            return
        }
        print(searchText)
        let searchResults =  RealmManager.shared.getObjects(type: Contact.self)!.filter("user_name CONTAINS[c] '\(searchText)' OR contact_name CONTAINS[c] '\(searchText)'")
        print("afsfsafas",searchResults)
        if searchResults.count != 0 {
            self.contacts = searchResults
        }
        self.tableView.reloadData()
    }
}

