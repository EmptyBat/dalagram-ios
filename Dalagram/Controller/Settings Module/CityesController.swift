//
//  CityesController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 2/2/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
class CityesController: UITableViewController {
    var cityData: [City] = []
    var viewModel: ProfileViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        getCities()
        self.navigationItem.title = "Выберите город"
    }
    func getCities() {
        setEmptyBackTitle()
        SVProgressHUD.show()
        self.cityData.removeAll()
        NetworkManager.makeRequest(.getCity(), success: { (json) in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = City(json: subJson)
                self.cityData.append(new)
            }
            SVProgressHUD.dismiss()
            self.tableView.reloadData()
        })
    }
    static func instantiate() -> CityesController {
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CityesController") as! CityesController
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.cityData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let name = cell.viewWithTag(1) as? UILabel
        name?.text = self.cityData[indexPath.row].city_name
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        print("didselect " ,  self.cityData[indexPath.row].city_id)
        print("afafafa" , self.viewModel.city_id.value)
        self.viewModel.city_id.value =  self.cityData[indexPath.row].city_id
        self.viewModel.city.value =  self.cityData[indexPath.row].city_name
        self.navigationController?.popViewController(animated: true)
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
