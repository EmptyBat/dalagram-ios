//
//  EditProfileController.swift
//  Dalagram
//
//  Created by Toremurat on 07.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class EditProfileController: UITableViewController,UITextFieldDelegate {
    @IBOutlet weak var new_password_lb: UILabel!
    @IBOutlet weak var return_password_lb: UILabel!
    @IBOutlet weak var old_password_lb: UILabel!
    @IBOutlet weak var lang_lb: UILabel!
    @IBOutlet weak var job_lb: UILabel!
    @IBOutlet weak var city_btn: UIButton!
    @IBOutlet weak var univer_lb: UILabel!
    @IBOutlet weak var birthday_btn: UIButton!
    @IBOutlet weak var phone_label: UILabel!
    @IBOutlet weak var email_lb: UILabel!
    
    @IBOutlet weak var birthday_lb: UILabel!
    @IBOutlet weak var city_lb: UILabel!
    // MARK: - IBOutlets
    @IBOutlet weak var changePassword: UIButton!
    @IBOutlet weak var newPasswordRepeat: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var oldPasswordField: UITextField!
    @IBOutlet weak var languageField: UITextField!
    @IBOutlet weak var workField: UITextField!
    @IBOutlet weak var universityField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var birhdayField: UITextField!
    @IBOutlet weak var backgroundButton: UIImageView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var statusField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var secondSaveButton: UIButton!
    var backgroundORAvatar = false
    lazy var saveButton: UIBarButtonItem = {
        let item = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(saveButtonPressed))
        item.tintColor = UIColor.white
        return item
    }()
    @objc func citiesController() {
        let vc = CityesController.instantiate()
        vc.viewModel = self.viewModel
        self.show(vc, sender: nil)
    }
    @IBAction func changePassword(_ sender: Any) {
        guard  newPassword.text != newPasswordRepeat.text else {
            WhisperHelper.showErrorMurmur(title: "Пароли не совпадают")
            return
        }
        guard  oldPasswordField.text != "" else {
            WhisperHelper.showErrorMurmur(title: "Введите старый пароль")
            return
        }
        viewModel.password          =  newPassword.text!
        viewModel.confirm_password  =  oldPasswordField.text ?? ""
        
        viewModel.editPassword {
            self.viewModel.isNeedToUpdate.value = true
            self.navigationController?.popViewController(animated: true)
        }
    }
    lazy var imagePicker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        return picker
    }()
    
    // MARK: - Variables
    
    var viewModel: ProfileViewModel!
    let disposeBag = DisposeBag()

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureEvents()
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
             switch(section)
        {
        case 1:
            return "Information".localized()
            case 2:
                return "change_password".localized()
        default:
            return ""
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cityField.text = viewModel.city.value
    }
    // MARK: - Configuring UI
    func configureUI() {
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        self.tableView.tableHeaderView = UIView(frame: frame)
        hideKeyboardWhenTappedAround()
        avatarView.layer.cornerRadius = avatarView.frame.width/2
        avatarView.layer.borderWidth = 1.0
        avatarView.layer.borderColor = UIColor.darkBlueNavColor.cgColor
        tableView.tableFooterView = UIView()
        self.navigationItem.rightBarButtonItem = saveButton
        secondSaveButton.layer.cornerRadius = 16
        changePassword.layer.cornerRadius = 16
        secondSaveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
        birhdayField.delegate = self
        birhdayField.addTarget(self, action: #selector(birthdayClicked), for: .touchDown)
        cityField.delegate = self
        cityField.addTarget(self, action: #selector(citiesController), for: .touchDown)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backgroundGesturePressed))
       // tapGestureRecognizer.cancelsTouchesInView = false
        backgroundButton.isUserInteractionEnabled = true
        backgroundButton.addGestureRecognizer(tapGestureRecognizer)
        //localization
        self.title = "edit".localized()
        self.phone_label.text = "phone_number".localized()
        self.univer_lb.text = "university".localized()
        self.job_lb.text = "work".localized()
        self.lang_lb.text = "language".localized()
        self.old_password_lb.text = "old_password".localized()
        self.return_password_lb.text = "return_password".localized()
        self.new_password_lb.text = "new_password".localized()

        self.languageField.placeholder = "language".localized()
        self.universityField.placeholder = "university".localized()
        self.cityField.placeholder = "city".localized()
        self.workField.placeholder = "work".localized()
        self.birhdayField.placeholder = "birthday".localized()
        self.nameField.placeholder = "nick_name".localized()
        self.statusField.placeholder = "status".localized()
        self.phoneField.placeholder = "phone_number".localized()
        birthday_lb.text = "birthday".localized()
        city_lb.text = "city".localized()
        changePassword.setTitle("change_password".localized(), for: .normal)
        saveButton.title = "save".localized()
        secondSaveButton.setTitle("save".localized(), for: .normal)
        
        self.oldPasswordField.placeholder = "old_password".localized()
        self.newPasswordRepeat.placeholder = "return_password".localized()
        self.newPassword.placeholder = "new_password".localized()

    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == birhdayField {
            return false;
        }
        else if textField == cityField{
            return false;
        }
        return true
    }
    // MARK: - Configuring Events
    func configureEvents() {
        nameField.text   = viewModel.name.value
        statusField.text = viewModel.status.value
        emailField.text  = viewModel.email.value
        phoneField.text  = viewModel.phone.value
        birhdayField.text = viewModel.birth_date.value
        cityField.text = viewModel.city.value
        universityField.text = viewModel.university.value
        workField.text = viewModel.work.value
        languageField.text = viewModel.language.value
        viewModel.avatar.asObservable().subscribe(onNext: {[unowned self] (avatarUrl) in
            self.avatarView.kf.setImage(with: URL(string: avatarUrl), placeholder: #imageLiteral(resourceName: "bg_navbar_sky"))

        }).disposed(by: disposeBag)
        
        
        viewModel.background.asObservable().subscribe(onNext: {[unowned self] (avatarUrl) in
           
            self.backgroundImage.kf.setImage(with: URL(string: self.viewModel.background.value.encodeUrl()!), placeholder: #imageLiteral(resourceName: "bg_navbar_sky"))
            
        }).disposed(by: disposeBag)
    }
    @IBAction func butonBirthday(_ sender: Any) {
        birthdayClicked()
    }
    @IBAction func buttonCity(_ sender: Any) {
        citiesController()
    }
    @objc func birthdayClicked() {
        let alert = UIAlertController(title: "Choose a birthday", message: "\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        alert.isModalInPopover = true
        
        let pickerFrame = UIDatePicker(frame: CGRect(x: 5, y: 25, width: 250, height: 240))
        pickerFrame.datePickerMode = .date
        pickerFrame.maximumDate = Date()
        alert.view.addSubview(pickerFrame)
        pickerFrame.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        alert.view.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
        // alert.view.backgroundColor = UIColor(red: 82/255, green: 185/255, blue: 98/255, alpha: 1.0)
        alert.addAction(UIAlertAction(title: "Pick", style: .default, handler: { (UIAlertAction) in
            
        }))
        self.present(alert,animated: true, completion: nil )
    }
    @objc func dateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = components.day, let month = components.month, let year = components.year {
            print("\(day) \(month) \(year)")
            let birthday = "\(day).\(month).\(year)"
            birhdayField.text = birthday
        }
    }
    // MARK: - Avatar View Action
    @objc func backgroundGesturePressed(_ sender: Any) {
        backgroundORAvatar = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func avatarGesturePressed(_ sender: Any) {
          backgroundORAvatar = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - Save BarButton Action
    @objc func saveButtonPressed() {
        viewModel.name.value    = nameField.text ?? ""
        viewModel.status.value  = statusField.text ?? ""
        viewModel.email.value   = emailField.text ?? ""
        
        viewModel.birth_date.value    = birhdayField.text ?? ""
        viewModel.city.value          = cityField.text ?? ""
        viewModel.university.value    = universityField.text ?? ""
        viewModel.work.value          = workField.text ?? ""
        viewModel.language.value      = languageField.text ?? ""
        
        viewModel.editProfile {
            self.viewModel.isNeedToUpdate.value = true
            self.navigationController?.popViewController(animated: true)
        }
    }

}

// MARK: - Image Picker Delegate

extension EditProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagePicker.dismiss(animated: true, completion: nil)
        if (backgroundORAvatar == false){
        viewModel.uploadAvatar(image: image)
        }
        else {
        viewModel.uploadBackground(image: image)
        }
    }
}
