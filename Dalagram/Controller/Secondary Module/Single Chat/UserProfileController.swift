//
//  UserProfileController.swift
//  Dalagram
//
//  Created by Toremurat on 10.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import RealmSwift
import SwiftyJSON
import RxSwift
class UserProfileController: UITableViewController,UITextViewDelegate {
    
    @IBOutlet weak var block_lb: UILabel!
    @IBOutlet weak var clean_chat_lb: UILabel!
    @IBOutlet weak var notification_lb: UILabel!
    @IBOutlet weak var shared_media_lb: UILabel!
    @IBOutlet weak var all_post_lb: UILabel!
    @IBOutlet weak var crypt_lb: UILabel!
    @IBOutlet weak var phone_lb: UILabel!
    @IBOutlet weak var status_lb: UILabel!
    @IBOutlet weak var userMailLabel: UITextView!
    @IBOutlet weak var fileCountLabel: UILabel!
    // MARK: - IBOutlets
    @IBOutlet weak var filesCountLabel: UILabel!
    @IBOutlet weak var mediaCollectionView: UICollectionView!
    @IBOutlet weak var userPhoneLabel: UITextView!
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet weak var userLastSeenLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userStatusLabel: UILabel!
    @IBOutlet weak var blockuserTitleLabel: UILabel!
    @IBOutlet weak var notificationLB: UILabel!
    @IBOutlet weak var notificationSW: UISwitch!
    // MARK: - Variables
    var contact: DialogInfo!
    var mediaFiles: [JSONChatFile] = []
    var dialogId: String = ""
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var user_id :Int = 0
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Профиль"
        configureUI()
        setupData()
        setEmptyBackTitle()
        
    }
    
    // MARK: - Configuring UI
    
    @IBAction func notificationSW(_ sender: Any) {
        let param = ["is_mute": "\(contact.is_mute)","partner_id":"\(contact.user_id)"]
        NetworkManager.makeRequest(.muteDialog(param), success: { (json)  in
            
        })
    }
    func configureUI() {
        self.userPhoneLabel.delegate = self
        self.userMailLabel.delegate = self
        self.userPhoneLabel.isEditable = false
        self.userPhoneLabel.dataDetectorTypes = .all
        self.userMailLabel.isEditable = false
        self.userMailLabel.dataDetectorTypes = .all
        mediaCollectionView.register(MediaCell.self)
        mediaCollectionView.delegate = self
        mediaCollectionView.dataSource = self
        tableView.tableFooterView = UIView()
        
        if (self.contact?.i_block_partner == 1){
            blockuserTitleLabel.text = "unblock".localized()
        }
        else {
             blockuserTitleLabel.text = "block".localized()
            
        }
        if (self.contact.is_mute == 0){
            notificationSW.isOn = true
        }
        else {
            notificationSW.isOn = false
        }
        //localization
        clean_chat_lb.text = "clear_chat".localized()
        notification_lb.text = "notification".localized()
        shared_media_lb.text = "shared_media".localized()
        all_post_lb.text = "all_posts".localized()
        crypt_lb.text = "crypt".localized()
        phone_lb.text = "phone".localized()
        status_lb.text = "status".localized()
        self.navigationItem.title = "profile".localized()
        
    }
    // MARK: - Setup Data
    
    func setupData() {
      //  userStatusLabel.text = contact.
        
        NetworkManager.makeRequest(.getMediaFiles(["page" : 1,"per_page": 10000, "partner_id": self.user_id]), success: { [weak self] (json) in
            guard let vc = self else { return }
            vc.mediaFiles.removeAll()
            for (_, subJson):(String, JSON) in json["data"] {
                let format = subJson["file_format"].stringValue
                    let file = JSONChatFile(json: subJson)
                    vc.mediaFiles.append(file)
                }
            print("afsfsaafs", json["data"],vc.mediaFiles.count)
            vc.filesCountLabel.text = "\(vc.mediaFiles.count)"
            vc.fileCountLabel.text = "\(vc.mediaFiles.count) files"
        })
        getProfile()
    }
    func getProfile(){
            NetworkManager.makeRequest(.getProfile(user_id: user_id), success: { (json) in
                let data = json["data"]
                self.contact.phone = data["phone"].stringValue
                self.userStatusLabel.text = data["user_status"].stringValue
                self.userMailLabel.text = data["email"].stringValue
                self.userLastSeenLabel.text = data["last_visit"].stringValue
                self.userPhoneLabel.text = data["phone"].stringValue
                self.userNameLabel.text = data["user_name"].stringValue
                self.userImageView.kf.setImage(with: URL(string:  data["avatar"].stringValue.encodeUrl()!))
            })
    }
    
    // MARK: - Clear Chat Request
    
    func clearChatRequest() {
        NetworkManager.makeRequest(.removeChat(["partner_id" : self.user_id, "is_delete_all": 1]), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            //NotificationCenter.default.post(name: AppManager.diloagHistoryNotification, object: nil)
            NotificationCenter.default.post(name: AppManager.loadDialogsNotification, object: nil)
        })
        removeCurrentDialogHistory()
    }
    
    // MARK: - Block User Request
    
    func blockUserRequest() {
        NetworkManager.makeRequest(.blockUser(["partner_id" : self.user_id]), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
//            NotificationCenter.default.post(name: AppManager.loadDialogsNotification, object: nil)
        })
    }
    
    func removeCurrentDialogHistory() {
        let realm = try! Realm()
        for object in realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialogId)) {
            try! realm.write {
                realm.delete(object)
            }
        }
    }
}

// MARK: - UITableViewDelegate, UITalbeViewDataSource

extension UserProfileController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: // Profile Photo
            
            if let userImage = userImageView.image {
                let photo = SKPhoto.photoWithImage(userImage)
                let browser = SKPhotoBrowser(photos: [photo])
                browser.initializePageIndex(0)
                self.present(browser, animated: true, completion: nil)
            }
        case 3: // call phone
            guard let number = URL(string: "tel://" + contact.phone) else { return }
            UIApplication.shared.open(number)
        case 4: // call email
            // guard let number = URL(string: "tel://" + contact.phone) else { return }
           // UIApplication.shared.open(number)
            print("")
        case 7: // Media files
            
            let vc = MediaFilesController(user_id: user_id, files: mediaFiles)
            self.show(vc, sender: nil)
            
        case 9: // Clear Chat
            
            if dialogId.isEmpty {
                return
            }
            let alert = UIAlertController(title: "are_u_sure".localized(), message: nil, preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.darkBlueNavColor
            let clearAction = UIAlertAction(title: "clear_chat".localized(), style: .default, handler: { [unowned self] (act) in
                self.clearChatRequest()
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            alert.addAction(clearAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            
        case 10: // Block contact
            
            let alert = UIAlertController(title: "choise".localized(), message: nil, preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.darkBlueNavColor
            var titleForBlock = ""
            
             if (self.contact?.i_block_partner == 1){
                titleForBlock = "unblock".localized()
            }
            else {
                 titleForBlock = "block".localized()
                
            }
            let clearAction = UIAlertAction(title: titleForBlock, style: .default, handler: { [unowned self] (act) in
                self.blockUserRequest()
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            alert.addAction(clearAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            
        default:
            break
        }
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource

extension UserProfileController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MediaCell = collectionView.dequeReusableCell(for: indexPath)
        cell.mediaImageView.kf.setImage(with: URL(string: mediaFiles[indexPath.row].file_url), placeholder: #imageLiteral(resourceName: "bg_gradient_0"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MediaCell {
            if let userImage = cell.mediaImageView.image {
                let photo = SKPhoto.photoWithImage(userImage)
                let browser = SKPhotoBrowser(photos: [photo])
                browser.initializePageIndex(0)
                self.present(browser, animated: true, completion: nil)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3.0
    }
}
