//
//  EditChannelController.swift
//  Dalagram
//
//  Created by Toremurat on 05.07.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import RealmSwift
class EditChannelController: UITableViewController {

    var channelMembers = ""
    var channelName = ""
    var channelLogin = ""
    var mediaFilesCount = 0
    @IBOutlet weak var channelImage: UIImageView!
    lazy var changeButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(title: "edit".localized(), style: .plain, target: self, action: #selector(changeBarButtonAction))
        item.tintColor = UIColor.white
        return item
    }()
    
    lazy var imagePicker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        return picker
    }()
    let realm = try! Realm()
    var changeButtonState: Bool = false
    var channelInfo: DialogInfo!
    var user_id: Int!
    var isChannelPublic: Int = 0 // false
    var channelContacts: [JSONContact] = []
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "channel".localized()
        configureUI()
        loadChannelDetails()
        getFiles()
    }
    
    // MARK: - Configuring UI
    
    func configureUI() {
        setEmptyBackTitle()
        
        if channelInfo.is_admin == 1 {
            self.navigationItem.rightBarButtonItem = changeButtonItem
        }
     //   self.hideKeyboardWhenTappedAroundTable()
        tableView.registerNib(ContactCell.self)
        tableView.tableFooterView = UIView()
        
        if let detail = channelInfo {
            channelImage.kf.setImage(with: URL(string: detail.avatar), placeholder: #imageLiteral(resourceName: "bg_gradient_3"))
            channelName = detail.user_name
        }
    }
    
    // MARK: Scroll View Dragging
    func hideKeyboardWhenTappedAroundTable() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        self.tableView.addGestureRecognizer(tap)
    }
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    // MARK: - Change Bar Button Action
    
    @objc func changeBarButtonAction() {
        let vc = EditChannelInfoController.fromStoryboard()
        vc.channelInfo = channelInfo
        vc.channel_login = channelLogin
        self.show(vc, sender: nil)
    }
    func getFiles() {
        //  userStatusLabel.text = contact.
        
        NetworkManager.makeRequest(.getMediaFiles(["page" : 1,"per_page": 10000, "channel_id": self.channelInfo.channel_id]), success: { [weak self] (json) in
            guard let vc = self else { return }
            var mediaFiles: [JSONChatFile] = []
            for (_, subJson):(String, JSON) in json["data"] {
                let format = subJson["file_format"].stringValue
                let file = JSONChatFile(json: subJson)
                mediaFiles.append(file)
            }
            vc.mediaFilesCount = mediaFiles.count
            vc.tableView.reloadData()
        })
    }

    
    // MARK: - Get Group Details
    
    func loadChannelDetails() {
        NetworkManager.makeRequest(.getChannelDetails(channel_id: channelInfo.channel_id), success: { [weak self] (json) in
            print(json)
            guard let vc = self else { return }
            vc.channelContacts.removeAll()
            
            let data = json["data"]
            vc.isChannelPublic = data["is_public"].intValue
            vc.channelName = data["channel_name"].stringValue
            vc.channelLogin = data["channel_login"].stringValue
            vc.channelImage.kf.setImage(with: URL(string: data["channel_avatar"].stringValue), placeholder: #imageLiteral(resourceName: "bg_gradient_3"))
            for (_, subJson):(String, JSON) in data["channel_users"] {
                let newContact = JSONContact(json: subJson)
                vc.channelContacts.append(newContact)
            }
            vc.tableView.reloadData()
        })
    }
    
    // MARK: - Edit Group Api
    
    func editChannelRequest() {
        if !channelName.isEmpty && !channelLogin.isEmpty {
            SVProgressHUD.show()
            NetworkManager.makeRequest(.editChannel(channel_id: channelInfo.channel_id, name: channelName, login: channelLogin), success: { (json) in
                SVProgressHUD.dismiss()
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
            })
        }
    }
    
    
    // MARK: - showMembersActionSheet. Action sheet for Members
    
    func showMembersActionSheet(data: JSONContact) {
        let alert = UIAlertController(title: "choise".localized(), message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        let profileAction = UIAlertAction(title: "show_profile".localized(), style: .default) { [unowned self] (act) in
            let vc = UserProfileController.fromStoryboard()
            vc.contact = DialogInfo(json: data)
            vc.user_id = data.user_id
            self.show(vc, sender: nil)
        }
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(profileAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - showAdminActionSheet
    
    func showAdminActionSheet(data: JSONContact) {
        let alert = UIAlertController(title: "choise".localized(), message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        
        let adminAction = UIAlertAction(title:  data.is_admin == 0 ? "make_admin".localized() : "cancel_admin".localized(), style: .default) { [unowned self] (act) in
            
            var channelAdminTarget = Dalagram.setChannelAdmin(channel_id: self.channelInfo.channel_id, user_id: data.user_id)
            if data.is_admin == 1 {
                channelAdminTarget = Dalagram.declineChannelAdmin(channel_id: data.user_id, user_id: data.user_id)
            }
            
            NetworkManager.makeRequest(channelAdminTarget, success: { (json) in
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
                self.loadChannelDetails()
            })
        }
        
        let deleteAction = UIAlertAction(title: "delete_from_channel".localized(), style: .default) { (act) in
            NetworkManager.makeRequest(.removeChannelMember(channel_id: self.channelInfo.channel_id, user_id: data.user_id), success: { (json) in
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
                self.loadChannelDetails()
            })
        }
        
        let profileAction = UIAlertAction(title: "show_profile".localized(), style: .default) { (act) in
            let vc = UserProfileController.fromStoryboard()
            vc.contact = DialogInfo(json: data)
            self.show(vc, sender: nil)
        }
        
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(adminAction)
        alert.addAction(deleteAction)
        alert.addAction(profileAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func removeDialog(by identifier: String) {
        if let item = realm.object(ofType: Dialog.self, forPrimaryKey: "\(identifier)") {
            try! realm.write {
                realm.delete(item)
            }
        }
        for item in realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", identifier)) {
            try! realm.write {
                realm.delete(item)
            }
        }

        NetworkManager.makeRequest(.removeChannelMember(channel_id: self.channelInfo.channel_id, user_id: 0), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
}


// MARK: - Table view data source

extension EditChannelController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1 // info Cell
        case 1: return 2 // Notifications & Media Cell
        case 2: return 1 // Add Members Cell
        case 3: return channelContacts.count
        case 4: return 1
        default: return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath)
            let channelNameTV = cell.viewWithTag(1) as? UILabel
            let channelMembersTV = cell.viewWithTag(2) as? UILabel
            let channelLoginTV = cell.viewWithTag(3) as? UILabel
            channelNameTV!.text = channelName
            channelLoginTV!.text = channelLogin
            if(channelContacts.count  == 1){
                channelMembersTV!.text = "\(channelContacts.count) " + "members_count_1".localized()
            }
            else if (channelContacts.count > 4) {
                channelMembersTV!.text = "\(channelContacts.count) " + "members_count_1".localized()
            }
            else {
                channelMembersTV!.text = "\(channelContacts.count) " + "members_count_1".localized()
            }
             return cell
        case 1 where indexPath.row == 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MediaCell", for: indexPath)
            let mediaFiles = cell.viewWithTag(1) as? UILabel
             let sharedMedia = cell.viewWithTag(2) as? UILabel
            sharedMedia?.text = "shared_media".localized()
            mediaFiles!.text = "\(mediaFilesCount) " + "files".localized()
            return cell
        case 1 where indexPath.row == 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath)
             let sharedMedia = cell.viewWithTag(2) as? UILabel
            sharedMedia?.text = "notification".localized()
            return cell
        case 2:
            if channelInfo.is_admin == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembersCell", for: indexPath)
                return cell
            } else {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "HeaderCell")
                cell.textLabel?.text = "members".localized()
                cell.textLabel?.textColor = UIColor.lightGray
                cell.isUserInteractionEnabled = false
                return cell
            }
        case 3:
            let cell: ContactCell = tableView.dequeReusableCell(for: indexPath)
            cell.setupRegisteredContact(channelContacts[indexPath.row])

            return cell
        case 4:
      let cell = tableView.dequeueReusableCell(withIdentifier: "exitCell", for: indexPath)
         let sharedMedia = cell.viewWithTag(2) as? UILabel
        sharedMedia?.text = "exit_channel".localized()
        return cell
        default:
            return UITableViewCell()
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0: // Change photo
            self.present(imagePicker, animated: true, completion: nil)
        case 1 where indexPath.row == 0: // Collection of media
            let vc = MediaFilesController(user_id: channelInfo.channel_id,type:.channel)
            self.show(vc, sender: nil)
        case 2: // Add a Group Member
            let vc = ChatContactsController(chatType: .channel, dialogInfo: channelInfo)
            vc.title = "choise_contacts".localized()
            vc.groupCompletion = { [unowned self] in
                self.loadChannelDetails()
            }
            self.show(vc, sender: nil)
        case 3: // List of Group Members
            if channelInfo.is_admin == 1 {
                showAdminActionSheet(data: channelContacts[indexPath.row])
            } else {
                showMembersActionSheet(data: channelContacts[indexPath.row])
            }
        case 4: //exit from channel
            let alert = UIAlertController(title: "are_u_sure_channel".localized(), message: nil, preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.darkBlueNavColor
            let clearAction = UIAlertAction(title: "exit_channel".localized(), style: .default, handler: { [unowned self] (act) in
                self.removeDialog(by: "\(self.channelInfo!.channel_id)")
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            alert.addAction(clearAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 3 ? 0.0 : 0.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0  &&  indexPath.row == 0 ? 108 : 50.0
    }
}

// MARK: - UIImagePickerControllerDelegate

extension EditChannelController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagePicker.dismiss(animated: true, completion: nil)
        channelImage.image = image
        if let data = channelInfo, let photo = UIImageJPEGRepresentation(image, 0.5) {
            SVProgressHUD.show()
            NetworkManager.makeRequest(.uploadGroupPhoto(group_id: data.group_id, image: photo), success: { (json) in
                print(json)
                SVProgressHUD.dismiss()
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
            })
        }
        
    }
}
