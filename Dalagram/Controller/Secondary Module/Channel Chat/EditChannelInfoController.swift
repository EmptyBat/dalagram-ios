//
//  EditChannelInfoController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/24/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SVProgressHUD
class EditChannelInfoController: UITableViewController {
    @IBOutlet weak var channelLoginField: UITextField!
    @IBOutlet weak var channelNameField: UITextField!
    @IBOutlet weak var changeImage: UIImageView!
    
    @IBOutlet weak var channelImage: UIImageView!
    var channelInfo: DialogInfo!
    var channel_login = ""
    lazy var imagePicker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        return picker
    }()
    
    lazy var changeButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(title: "edit".localized(), style: .done, target: self, action: #selector(changeBarButtonAction))
        item.tintColor = UIColor.white
        return item
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    func configureUI() {
        channelNameField.placeholder = "channel_name".localized()
        channelLoginField.placeholder = "login_channel".localized()
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageChangeAction))
        changeImage.addGestureRecognizer(tap)
        self.title = "edit_channel".localized()
        self.navigationItem.rightBarButtonItem = changeButtonItem
        
        if let detail = channelInfo {
            channelImage.kf.setImage(with: URL(string: detail.avatar), placeholder: #imageLiteral(resourceName: "bg_gradient_0"))
            channelNameField.text = detail.user_name
            channelLoginField.text = channel_login
            
        }
       hideKeyboardWhenTappedAround()
        
    }
    @objc func changeBarButtonAction() {
        if !channelNameField.text!.isEmpty && !channelLoginField.text!.isEmpty {
            SVProgressHUD.show()
            NetworkManager.makeRequest(.editChannel(channel_id: channelInfo.channel_id, name: channelNameField.text!, login: channelLoginField.text!), success: { (json) in
                SVProgressHUD.dismiss()
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
            })
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    @objc func imageChangeAction() {
        self.present(imagePicker, animated: true, completion: nil)
    }
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension EditChannelInfoController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagePicker.dismiss(animated: true, completion: nil)
        channelImage.image = image
        if let data = channelInfo, let photo = UIImageJPEGRepresentation(image, 0.5) {
            SVProgressHUD.show()
            NetworkManager.makeRequest(.uploadChannelPhoto(channel_id: data.channel_id, image: photo), success: { (json) in
                print(json)
                SVProgressHUD.dismiss()
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
            })
        }
        
    }
}
