//
//  EditGroupController.swift
//  Dalagram
//
//  Created by Toremurat on 29.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON
import RealmSwift
class EditGroupController: UITableViewController {
    @IBOutlet weak var groupImage: UIImageView!
    var groupName = ""
    var mediaFilesCount = 0
    lazy var changeButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(title: "edit".localized(), style: .plain, target: self, action: #selector(changeBarButtonAction))
        item.tintColor = UIColor.white
        return item
    }()
    
    lazy var imagePicker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = .photoLibrary
        return picker
    }()
    
    var changeButtonState: Bool = false
    var groupInfo: DialogInfo!
    var groupContacts: [JSONContact] = []
    
    let realm = try! Realm()
    var user_id: Int!
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "group".localized()
        configureUI()
        loadGroupDetails()
        getFiles()
    }
    
    // MARK: - Configuring UI
    
    func configureUI() {
        setEmptyBackTitle()
        
        if groupInfo.is_admin == 1 {
            self.navigationItem.rightBarButtonItem = changeButtonItem
        }
        tableView.registerNib(ContactCell.self)
        tableView.tableFooterView = UIView()
        if let detail = groupInfo {
            groupImage.kf.setImage(with: URL(string: detail.avatar), placeholder: #imageLiteral(resourceName: "bg_gradient_0"))
            groupName = detail.user_name
        }
    }
    
    // MARK: Scroll View Dragging
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    // MARK: - Change Bar Button Action
    
    @objc func changeBarButtonAction() {
        let vc = EditGroupInfoController.fromStoryboard()
        vc.groupInfo = groupInfo
        self.show(vc, sender: nil)
    }
    
    // MARK: - Get Group Details
    
    func loadGroupDetails() {
        NetworkManager.makeRequest(.getGroupDetails(group_id: groupInfo.group_id), success: { [weak self] (json) in
            guard let vc = self else { return }
            vc.groupContacts.removeAll()
            print("affsafsa",json)
            vc.groupName = json["data"]["group_name"].stringValue
            vc.groupInfo.user_name = json["data"]["group_name"].stringValue
            vc.groupImage.kf.setImage(with: URL(string: json["data"]["group_avatar"].stringValue), placeholder: #imageLiteral(resourceName: "bg_gradient_0"))
            for (_, subJson):(String, JSON) in json["data"]["group_users"] {
                let newContact = JSONContact(json: subJson)
                vc.groupContacts.append(newContact)
            }
            vc.tableView.reloadData()
        })
    }
    func getFiles() {
        //  userStatusLabel.text = contact.
        
        NetworkManager.makeRequest(.getMediaFiles(["page" : 1,"per_page": 10000, "group_id": self.groupInfo.group_id]), success: { [weak self] (json) in
            guard let vc = self else { return }
            var mediaFiles: [JSONChatFile] = []
            for (_, subJson):(String, JSON) in json["data"] {
                let format = subJson["file_format"].stringValue
                let file = JSONChatFile(json: subJson)
                mediaFiles.append(file)
            }
           vc.mediaFilesCount = mediaFiles.count
           vc.tableView.reloadData()
        })
    }
    // MARK: - Edit Group Api
    
    func editGroupRequest() {
     /*   if groupInfo.user_name != groupNameField.text && !groupNameField.text!.isEmpty {
            SVProgressHUD.show()
            NetworkManager.makeRequest(.editGroup(group_id: groupInfo.group_id, group_name: groupNameField.text!), success: { (json) in
                SVProgressHUD.dismiss()
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
            })
        }
 */
    }
    
    // MARK: Action sheet for Members
    
    func showMembersActionSheet(data: JSONContact) {
        let alert = UIAlertController(title: "choise".localized(), message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        let profileAction = UIAlertAction(title: "show_profile".localized(), style: .default) { [unowned self] (act) in
            let vc = UserProfileController.fromStoryboard()
            vc.contact = DialogInfo(json: data)
            vc.user_id = data.user_id
            self.show(vc, sender: nil)
        }
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(profileAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Action sheet for Admin
    
    func showAdminActionSheet(data: JSONContact) {
        let alert = UIAlertController(title: "choise".localized(), message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.darkBlueNavColor
        let adminAction = UIAlertAction(title: data.is_admin == 0 ? "make_admin".localized() : "cancel_admin".localized(), style: .default) { [unowned self] (act) in
            var groupAdminTarget = Dalagram.setGroupAdmin(group_id: self.groupInfo.group_id, user_id: data.user_id)
            if data.is_admin == 1 {
                groupAdminTarget = Dalagram.declineGroupAdmin(group_id: self.groupInfo.group_id, user_id: data.user_id)
            }
            NetworkManager.makeRequest(groupAdminTarget, success: { (json) in
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
                self.loadGroupDetails()
            })
        }
        
        let deleteAction = UIAlertAction(title: "delete_from_group".localized(), style: .default) { (act) in
            NetworkManager.makeRequest(.removeGroupMember(group_id: self.groupInfo.group_id, user_id: data.user_id), success: { (json) in
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
                self.loadGroupDetails()
            })
        }
        
        let profileAction = UIAlertAction(title: "show_profile".localized(), style: .default) { (act) in
            let vc = UserProfileController.fromStoryboard()
            vc.contact = DialogInfo(json: data)
            self.show(vc, sender: nil)
        }
        
        let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
        alert.addAction(adminAction)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        alert.addAction(profileAction)
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Table view data source

extension EditGroupController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return 1 // info cell
        case 1: return 2 // Notifications & Media Cell
        case 2: return 1 // Add Members Cell
        case 3: return groupContacts.count
        case 4: return 1 // exit from group
        default: return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath)
            let groupNameTV = cell.viewWithTag(1) as? UILabel
            let groupMembersTV = cell.viewWithTag(2) as? UILabel
            groupNameTV!.text = groupName
            if(groupContacts.count  == 1){
                groupMembersTV!.text = "\(groupContacts.count) " + "members_count_1".localized()
            }
            else if (groupContacts.count > 4) {
                groupMembersTV!.text = "\(groupContacts.count) " + "members_count_2".localized()
            }
            else {
                groupMembersTV!.text = "\(groupContacts.count)  " + "members_count_3".localized()
                        
            }
            return cell
        case 1 where indexPath.row == 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MediaCell", for: indexPath)
            let mediaFiles = cell.viewWithTag(1) as? UILabel
             let sharedMedia = cell.viewWithTag(2) as? UILabel
            sharedMedia?.text = "shared_media".localized()
            mediaFiles!.text = "\(mediaFilesCount) " + "files".localized()
            return cell
        case 1 where indexPath.row == 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath)
             let sharedMedia = cell.viewWithTag(2) as? UILabel
            sharedMedia?.text = "notification".localized()
            return cell
        case 2:
            if groupInfo.is_admin == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AddMembersCell", for: indexPath)
                 let sharedMedia = cell.viewWithTag(2) as? UILabel
                sharedMedia?.text = "add_members".localized()
                return cell
            } else {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "HeaderCell")
                cell.textLabel?.text = "members".localized()
                cell.textLabel?.textColor = UIColor.lightGray
                cell.isUserInteractionEnabled = false
                return cell
            }
        case 3:
            let cell: ContactCell = tableView.dequeReusableCell(for: indexPath)
            cell.setupRegisteredContact(groupContacts[indexPath.row])
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "exitCell", for: indexPath)
             let sharedMedia = cell.viewWithTag(2) as? UILabel
            sharedMedia?.text = "exit_group".localized()
            return cell
        default:
            return UITableViewCell()
        }
        
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
     //   case 0: // Change photo
     //       self.present(imagePicker, animated: true, completion: nil)
        case 1 where indexPath.row == 0: // Collection of media
            let vc = MediaFilesController(user_id: groupInfo.group_id,type:.group)
            self.show(vc, sender: nil)
        case 2: // Add a Group Member
            let vc = ChatContactsController(chatType: .group, dialogInfo: groupInfo)
            vc.title = "choise_contacts".localized()
            vc.groupCompletion = { [unowned self] in
                self.loadGroupDetails()
            }
            self.show(vc, sender: nil)
        case 3: // List of Group Members
            if groupInfo.is_admin == 1 {
                showAdminActionSheet(data: groupContacts[indexPath.row])
            } else {
                showMembersActionSheet(data: groupContacts[indexPath.row])
            }
        case 4 : // exit from group
            let alert = UIAlertController(title: "are_u_sure_group".localized(), message: nil, preferredStyle: .actionSheet)
            alert.view.tintColor = UIColor.darkBlueNavColor
            let clearAction = UIAlertAction(title: "exit_group".localized(), style: .default, handler: { [unowned self] (act) in
                self.removeDialog(by: "\(self.groupInfo!.group_id)")
            })
            let cancelAction = UIAlertAction(title: "cancel".localized(), style: .cancel, handler: nil)
            alert.addAction(clearAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 3 ? 0.0 : 0.0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
           return indexPath.section == 0  &&  indexPath.row == 0 ? 71 : 50.0
    }
}
// MARK: - UIImagePickerControllerDelegate

extension EditGroupController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imagePicker.dismiss(animated: true, completion: nil)
        groupImage.image = image
        if let data = groupInfo, let photo = UIImageJPEGRepresentation(image, 0.5) {
            SVProgressHUD.show()
            NetworkManager.makeRequest(.uploadGroupPhoto(group_id: data.group_id, image: photo), success: { (json) in
                print(json)
                SVProgressHUD.dismiss()
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                NotificationCenter.default.post(name: AppManager.dialogDetailsNotification, object: nil)
            })
        }
        
    }
    func removeDialog(by identifier: String) {
        if let item = realm.object(ofType: Dialog.self, forPrimaryKey: "\(identifier)") {
            try! realm.write {
                realm.delete(item)
            }
        }
        for item in realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", identifier)) {
            try! realm.write {
                realm.delete(item)
            }
        }
        
        NetworkManager.makeRequest(.removeGroupMember(group_id: self.groupInfo.group_id, user_id:0 ), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            self.navigationController?.popToRootViewController(animated: true)
        })
    }
}
