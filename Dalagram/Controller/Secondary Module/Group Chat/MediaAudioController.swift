//
//  MediaAudioController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/30/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
class MediaAudioController: UITableViewController  {
    
    // MARK: Private Variables
    var mediaFiles: [JSONChatFile] = []
    var audiofiles: [URL] = []
    var partnerId: Int = 0
    var type: DialogType = .single
    var parentNavigationController : UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        setupData()
    }
    func setupData() {
        var parametr  = ""
        if (self.type == .group){
            parametr = "group_id"
        }
        else  if (self.type == .channel){
            parametr = "channel_id"
        }
        else {
            parametr = "partner_id"
        }
        NetworkManager.makeRequest(.getMediaFiles(["page" : 1,"per_page": 10000,parametr : partnerId]), success: { [weak self] (json) in
            guard let vc = self else { return }
            vc.mediaFiles.removeAll()
            var index = 0
            for (_, subJson):(String, JSON) in json["data"] {
                let format = subJson["file_format"].stringValue
                if format == "audio" {
                    let file = JSONChatFile(json: subJson)
                    vc.mediaFiles.append(file)
                    vc.downloadAudio(url: file.file_url,index: index)
                    index += 1
                }
            }
            vc.tableView.reloadData()
        })
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   //     let notificationName = Notification.Name("detail_history")
      //  NotificationCenter.default.post(name: notificationName, object: history[indexPath.section].history_id)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "audio_cell", for: indexPath) as! AudioCell
        cell.title_lb.text = mediaFiles[indexPath.row].file_name
        if (audiofiles.count == mediaFiles.count){
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 2
            formatter.numberStyle = .decimal
            print(formatter.string(from: sizePerMB(url:audiofiles[indexPath.row]) as NSNumber) ?? "n/a")
        cell.size_lb.text = "\(formatter.string(from: sizePerMB(url:audiofiles[indexPath.row]) as NSNumber) ?? "n/a") MB"
        cell.audioUrl = audiofiles[indexPath.row]
            
            let origImage = UIImage(named: "icon_mini_play")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.playButton.setImage(tintedImage, for: .normal)
            cell.playButton.tintColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1.0)
        }
            return cell
    }
    func configUI() {
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.register(UINib(nibName: "AudioCell", bundle: nil), forCellReuseIdentifier: "audio_cell")
        
    }
    func downloadAudio(url: String,index: Int) {
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("audioFiles\(index)")
            return (documentsURL, [.removePreviousFile])
        }
        
        AF.download(url, to: destination).response { response in
            self.audiofiles.append(response.fileURL!)
              self.tableView.reloadData()

        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return mediaFiles.count
    }
    func sizePerMB(url: URL?) -> Double {
        guard let filePath = url?.path else {
            return 0.0
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                let formatter = NumberFormatter()
                return size.doubleValue / 100000
            }
        } catch {
            print("Error: \(error)")
        }
        return 0.0
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
