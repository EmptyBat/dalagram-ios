//
//  MediaColletionController.swift
//  Dalagram
//
//  Created by Toremurat on 04.07.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
import SKPhotoBrowser

class MediaColletionController: UIViewController {
    
    // MARK: - IBOutlets
var type: DialogType = .single
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MediaCell.self)
        collectionView.backgroundColor = UIColor.white
        //collectionView.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        return collectionView
    }()
    
    // MARK: - Variables
    
    var mediaFiles: [JSONChatFile] = []
    var images = [SKPhoto]()
    var partnerId: Int = 0
    var parentNavigationController : UINavigationController?
    // MARK: - Life cycle
    convenience init(user_id: Int, files: [JSONChatFile]) {
        self.init()
        self.mediaFiles = files
        self.partnerId = user_id
        setupData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setupData()
    }
    
    // MARK: - Configuring UI
    
    func configureUI() {
        collectionView.contentInset = UIEdgeInsets.zero
        self.automaticallyAdjustsScrollViewInsets = false
        view.backgroundColor = UIColor.white
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            if #available(iOS 11.0, *) {
                make.edges.equalTo(view.safeAreaLayoutGuide)
            } else {
                make.edges.equalToSuperview()
            }
        }
    }
    func setupData() {
        var parametr  = ""
        if (self.type == .group){
            parametr = "group_id"
        }
        else  if (self.type == .channel){
              parametr = "channel_id"
        }
        else {
             parametr = "partner_id"
        }
        NetworkManager.makeRequest(.getMediaFiles(["page" : 1,"per_page": 10000,parametr : partnerId]), success: { [weak self] (json) in
            guard let vc = self else { return }
            vc.mediaFiles.removeAll()
            for (_, subJson):(String, JSON) in json["data"] {
                let format = subJson["file_format"].stringValue
                if format == "image" || format == "video" {
                    let file = JSONChatFile(json: subJson)
                    vc.mediaFiles.append(file)
                    let photo = SKPhoto.photoWithImageURL(file.file_url.encodeUrl()!)
                    photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
                    self!.images.append(photo)
                }
            }
            vc.collectionView.reloadData()
        })
    }

}

extension MediaColletionController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mediaFiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MediaCell = collectionView.dequeReusableCell(for: indexPath)
        cell.mediaImageView.kf.setImage(with: URL(string: mediaFiles[indexPath.row].file_url.encodeUrl()!), placeholder: #imageLiteral(resourceName: "bg_gradient_2"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MediaCell {
                let browser = SKPhotoBrowser(photos: images)
                browser.initializePageIndex(indexPath.row)
                self.present(browser, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = collectionView.frame.width/4 - 8.0
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2.0
    }
    
}
