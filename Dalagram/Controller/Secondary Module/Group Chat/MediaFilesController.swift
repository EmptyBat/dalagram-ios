//
//  MediaFilesController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/29/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import PageMenu
class MediaFilesController: UIViewController {
    var pageMenu : CAPSPageMenu?
    private var mediaFiles: [JSONChatFile] = []
    private var partnerId: Int = 0
    private var type: DialogType = .single
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Материалы"
        // Do any additional setup after loading the view.
    }
    convenience init(user_id: Int, files: [JSONChatFile]? = nil,type: DialogType = .single) {
        self.init()
        if (files != nil){
        self.mediaFiles = files!
        }
        self.partnerId = user_id
        self.type = type
        setupPageMenu()
    }
    func setupPageMenu() {
        var controllerArray : [UIViewController] = []
        let controller_media: MediaColletionController = MediaColletionController()
        controller_media.title = "Медиа"
        controller_media.partnerId = partnerId
        controller_media.mediaFiles = mediaFiles
        controller_media.type = self.type
        controller_media.parentNavigationController = self.navigationController
        controllerArray.append(controller_media)
        let controller_documents: MediaDocumentController = MediaDocumentController()
        controller_documents.title = "Документы"
        controller_documents.partnerId = partnerId
        controller_documents.type = self.type
   //     controller_documents.mediaFiles = mediaFiles
        controller_documents.parentNavigationController = self.navigationController
        controllerArray.append(controller_documents)
        let controller_audio: MediaAudioController = MediaAudioController()
        controller_audio.title = "Аудио"
        controller_audio.partnerId = partnerId
        controller_audio.type = self.type
    //    controller_audio.mediaFiles = mediaFiles
        controller_audio.parentNavigationController = self.navigationController
        controllerArray.append(controller_audio)
        let transparentColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        let BlueColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1)
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(transparentColor),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(BlueColor),
            .selectionIndicatorHeight(3.0),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .selectedMenuItemLabelColor(BlueColor),
            .menuHeight(40.0),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .addBottomMenuHairline(false),
            .centerMenuItems(false)
        ]
        // Initialize page menu with controller array, frame, and optional parameters
        // fix for iphone x , i like the phone(no)
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0, width: view.frame.width, height: view.frame.height), pageMenuOptions: parameters)
        pageMenu!.didMove(toParentViewController: self)
        addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        // pageMenu!.didMove(toParentViewController: self)
    }
    func willMoveToPage(controller: UIViewController, index: Int){
        
        pageMenu!.moveToPage(index)
    }
    func didMoveToPage(controller: UIViewController, index: Int){
        pageMenu!.moveToPage(index)
    }
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
