//
//  MediaDocumentController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 12/30/18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
import SafariServices
class MediaDocumentController: UITableViewController {
    var mediaFiles: [JSONChatFile] = []
    var partnerId: Int = 0
    var type: DialogType = .single
    var parentNavigationController : UINavigationController?
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        setupData()
    }
    func setupData() {
        var parametr  = ""
        if (self.type == .group){
            parametr = "group_id"
        }
        else  if (self.type == .channel){
            parametr = "channel_id"
        }
        else {
            parametr = "partner_id"
        }
        NetworkManager.makeRequest(.getMediaFiles(["page" : 1,"per_page": 10000,parametr : partnerId]), success: { [weak self] (json) in
            guard let vc = self else { return }
            vc.mediaFiles.removeAll()
            for (_, subJson):(String, JSON) in json["data"] {
                let format = subJson["file_format"].stringValue
                if format == "file" {
                    let file = JSONChatFile(json: subJson)
                    vc.mediaFiles.append(file)
                }
            }
            vc.tableView.reloadData()
        })
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let vc = SFSafariViewController(url: URL(string:mediaFiles[indexPath.row].file_url.encodeUrl()!)!)
            self.present(vc, animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let origImage = UIImage(named: "icon_file_default")
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        let cell = tableView.dequeueReusableCell(withIdentifier: "document_cell", for: indexPath) as! DocumentsCell
        cell.file_image.image = tintedImage
        cell.file_image.tintColor = UIColor.random
        cell.file_extension_lb.text =  mediaFiles[indexPath.row].file_name.fileExtension()
        cell.title_lb.text = mediaFiles[indexPath.row].file_name
        return cell
    }
    func configUI() {
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        tableView.register(UINib(nibName: "DocumentsCell", bundle: nil), forCellReuseIdentifier: "document_cell")
        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return mediaFiles.count
    }

}
