//
//  NotificationController+SearchBar.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 5/24/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit

extension UserController {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

extension UserController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        loadSearchData(search:searchText)
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            loadData()
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        loadSearchData(search:searchText)
    }
}
