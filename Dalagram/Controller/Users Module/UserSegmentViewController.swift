//
//  UserSegmentViewController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 11/12/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import PageMenu
import RxSwift
import SwiftyJSON
class UserSegmentViewController: UIViewController,UISearchBarDelegate {
    var pageMenu : CAPSPageMenu?
    private var mediaFiles: [JSONChatFile] = []
    private var partnerId: Int = 0
    let disposeBag = DisposeBag()
    var viewModelProfile = ProfileViewModel()
    var parentNavigationController : UINavigationController?
    var searchController :UISearchController?
    var ouput: ContactSearchOutput?
    var UserData: [UserSpeciality] = []
    
    lazy var titleView: DialogNavigationTitleView = {
        let titleView = DialogNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadSegments()
        configureUI()
        setBlueNavBar()
        configureNavBar()
        configureEvents()
        setupData()
    }
    func setupPageMenu() {
        var controllerArray : [UIViewController] = []
          for (speciality):(UserSpeciality) in UserData {
            let userController = UserController()
            userController.title = speciality.speciality_name
            userController.currentViewController = self
            userController.speciality_id = speciality.speciality_id
            userController.parentNavigationController = self.navigationController
            controllerArray.append(userController)
        }
        let transparentColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        let BlueColor = UIColor(red: 99/255, green: 141/255, blue: 192/255, alpha: 1)
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.1),
            .scrollMenuBackgroundColor(transparentColor),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(BlueColor),
            .selectionIndicatorHeight(3.0),
            .unselectedMenuItemLabelColor(UIColor.lightGray),
            .selectedMenuItemLabelColor(BlueColor),
            .menuHeight(40.0),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .addBottomMenuHairline(false),
            .centerMenuItems(false)
        ]
        // Initialize page menu with controller array, frame, and optional parameters
        // fix for iphone x , i like the phone(no)
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: view.frame.width, height: view.frame.height), pageMenuOptions: parameters)
        pageMenu!.didMove(toParentViewController: self)
        addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        // pageMenu!.didMove(toParentViewController: self)
    }
    func configureUI() {
        setEmptyBackTitle()
        view.backgroundColor = UIColor.white
        
        //MARK: SearchBar
        /*    searchBar.searchBarStyle = .minimal
         searchBar.placeholder = " Поиск"
         searchBar.sizeToFit()
         
         tableView.tableHeaderView = inviteFriendsView
         tableView.tableFooterView = UIView()
         tableView.separatorColor = UIColor.groupTableViewBackground
         tableView.registerNib(ContactCell.self)
         */
        // MARK: - UIBarButtonItem Configurations
        let createSearch = UIBarButtonItem(image: UIImage(named: "icon_search"), style: .plain, target: self, action: #selector(searchAction))
        createSearch.tintColor = UIColor.white
        
        let createShare = UIBarButtonItem(image: UIImage(named: "icon_mbriShare"), style: .plain, target: self, action: #selector(CompanyProductAction))
        createShare.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItems = [createSearch, createShare]
        
    }
    @objc func searchAction() {
        let vc = GlobalSearchTableViewController()
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }
    func willMoveToPage(controller: UIViewController, index: Int){
        
        pageMenu!.moveToPage(index)
    }
    func didMoveToPage(controller: UIViewController, index: Int){
        pageMenu!.moveToPage(index)
    }
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension UserSegmentViewController {
    
    // MARK: - Configuring NavBar TitleView
    
    func configureNavBar() {
        
        let titleItem = UIBarButtonItem(customView: titleView)
        self.navigationItem.leftBarButtonItem = titleItem
        self.navigationItem.leftItemsSupplementBackButton = true
        
        
        let imagePlaceholder = #imageLiteral(resourceName: "placeholder")
        titleView.userAvatarView.image = imagePlaceholder
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        titleView.userAvatarView.isUserInteractionEnabled = true
        titleView.userAvatarView.addGestureRecognizer(tapGestureRecognizer)
    }
    // MARK: - NavBar TitleView Action
    
    @objc func userTitleViewPressed() {
        
        let transition = CATransition()
        transition.duration = 0.35
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: avatarUrl), placeholder: nil)
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let user_id = viewModelProfile.user_id.value
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.show(vc, sender: nil)
    }
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
        })
    }
    func loadSegments(){
        NetworkManager.makeRequest(.getSpeciality(), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = UserSpeciality(json: subJson)
                self.UserData.append(new)
            }
            self.setupPageMenu()
        })
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchText = searchBar.text else { return }
        self.ouput?.SearchContasctsAction(text: searchText,id :0,state:false)
        self.searchController?.isActive = false
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        self.ouput?.SearchContasctsAction(text: "",id :0,state:true)
        UIView.animate(withDuration: 0.3) {
            self.pageMenu?.view.frame = CGRect(x: 0.0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.view.layoutIfNeeded()
        }
        self.searchController?.isActive = false
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.ouput?.SearchContasctsAction(text: "",id :0,state:true)
        UIView.animate(withDuration: 0.3) {
            self.pageMenu?.view.frame = CGRect(x: 0.0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.view.layoutIfNeeded()
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            //        viewModel.getAllDialogs(onReload: { [weak self] in
            //         self?.tableView.reloadData()
            //     })
            return
        }
        self.ouput?.SearchContasctsAction(text: searchText,id :0,state:false)
        
    }
}

