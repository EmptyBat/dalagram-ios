//
//  UserController.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 5/7/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
import RxSwift
class UserController: UITableViewController {
    var isLoading = false
    var bathCount = 1
    var viewModelProfile = ProfileViewModel()
    let disposeBag = DisposeBag()
    var UserData: [Users] = []
    var speciality_id = 1
    var currentViewController: UIViewController?
    var parentNavigationController : UINavigationController?
    lazy var titleView: DialogNavigationTitleView = {
        let titleView = DialogNavigationTitleView()
        titleView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userTitleViewPressed))
        titleView.addGestureRecognizer(tapGesture)
        return titleView
    }()
    lazy var navigationTitle: UILabel = {
        let label = UILabel()
        label.text = "News"
        label.font = UIFont.systemFont(ofSize: 18.0, weight: UIFont.Weight.init(0.01))
        label.textColor = UIColor.white
        return label
    }()
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "search".localized()
        searchBar.delegate = self
        searchBar.sizeToFit()
        return searchBar
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        setBlueNavBar()
        configureUI()
        configureNavBar()
        setupData()
        configureEvents()
        setEmptyBackTitle()
    }
    func configureUI(){
        //MARK: - TableView Configurations
        tableView.tableHeaderView = searchBar
        tableView.registerNib(UsersTableViewCell.self)
        // MARK: - UIBarButtonItem Configurations
        /* let createSearch = UIBarButtonItem(image: UIImage(named: "icon_search"), style: .plain, target: self, action: #selector(searchAction))
        createSearch.tintColor = UIColor.white
        
        let createShare = UIBarButtonItem(image: UIImage(named: "icon_mbriShare"), style: .plain, target: self, action: #selector(CompanyProductAction))
        createShare.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItems = [createSearch, createShare]*/
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 63
        self.tableView.reloadData()
        
    }
    @objc func searchAction() {
        
    }
    @objc func CompanyProductAction() {
        let vc = CompanyProductsController.instantiate()
        self.show(vc, sender: nil)
    }
     func loadData(){
        self.UserData.removeAll()
        isLoading = true
        let parameters = ["token": User.getToken(), "page": bathCount, "per_page": 20,"speciality_id":speciality_id] as [String : Any]
        
        NetworkManager.makeRequest(.getUser(parameters), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = Users(json: subJson)
                self.UserData.append(new)
            }
            print("asffasf",json["data"])
            if (json["data"].count != 0){
                self.showNoContentView(dataCount: self.UserData.count ?? 0,contentText:true)
                self.tableView.reloadData()
                self.isLoading = false
            }
        })
    }
     func loadSearchData(search:String){
        self.UserData.removeAll()
        isLoading = true
        let parameters = ["token": User.getToken(), "page": bathCount, "per_page": 20,"speciality_id":speciality_id,"search":search] as [String : Any]
        
        NetworkManager.makeRequest(.getUser(parameters), success: { (json)  in
            for (_, subJson):(String, JSON) in json["data"] {
                let new = Users(json: subJson)
                self.UserData.append(new)
            }
            if (json["data"].count != 0){
                self.showNoContentView(dataCount: self.UserData.count ?? 0,contentText:true)
                self.tableView.reloadData()
                self.isLoading = false
            }
        })
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return UserData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersTableViewCell", for: indexPath) as! UsersTableViewCell
        cell.setupUsers(UserData[indexPath.row])
        return cell
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = (self.UserData.count - 1)
        if indexPath.row  == lastElement && self.UserData.count > 19{
            if (!isLoading) {
                bathCount += 1
                self.loadData()
            }
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NewsController()
        vc.author_id = self.UserData[indexPath.row].user_id
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UserController {
    
    // MARK: - Configuring NavBar TitleView
    
    func configureNavBar() {
        
        let titleItem = UIBarButtonItem(customView: titleView)
        self.navigationItem.leftBarButtonItem = titleItem
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationTitle.text = ""
        
        let imagePlaceholder = #imageLiteral(resourceName: "placeholder")
        titleView.userAvatarView.image = imagePlaceholder
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        titleView.userAvatarView.isUserInteractionEnabled = true
        titleView.userAvatarView.addGestureRecognizer(tapGestureRecognizer)
    }
    // MARK: - NavBar TitleView Action
    
    @objc func userTitleViewPressed() {
        
        let transition = CATransition()
        transition.duration = 0.35
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
    }
    func configureEvents() {
        viewModelProfile.isNeedToUpdate.asObservable().subscribe(onNext: { [weak self] (isUpdate) in
            isUpdate ? self?.setupData() : ()
        }).disposed(by: disposeBag)
        viewModelProfile.avatar.asObservable().subscribe(onNext: { [weak self]
            (avatarUrl) in
            self?.titleView.userAvatarView.kf.setImage(with: URL(string: avatarUrl.encodeUrl()!), placeholder: nil)
        }).disposed(by: disposeBag)
    }
    
    // MARK: FIXME - ActivityIndicator on failure image loading
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let user_id = viewModelProfile.user_id.value
        let vc = NewsController()
        vc.author_id = user_id
        vc.hidesBottomBarWhenPushed = true
        self.show(vc, sender: nil)
    }
    func setupData() {
        viewModelProfile.getProfile(onCompletion: { [weak self] in
            self!.tableView.reloadData()
            
        })
    }
}
