//
//  DialogHistory.swift
//  Dalagram
//
//  Created by Toremurat on 19.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class DialogHistory: Object {
    
    @objc dynamic var id: String            = ""
    @objc dynamic var chat_id: Int          = 0
    @objc dynamic var chat_kind: String     = ""
    @objc dynamic var answer_chat_id: Int   = 0
    @objc dynamic var chat_text: String     = ""
    @objc dynamic var is_read: Int          = 0
    @objc dynamic var chat_date: String     = ""
    @objc dynamic var chat_time: String     = ""
    @objc dynamic var answer_chatText: String     = ""
    @objc dynamic var answer_nickname: String     = ""
    @objc dynamic var answerFileFormat: String     = ""
    @objc dynamic var answer_is_contact: Int     = 0
    
    @objc dynamic var action_name: String   = ""
    @objc dynamic var view_count: Int       = 0
    @objc dynamic var sender_phone: String  = ""
    @objc dynamic var sender_avatar: String = ""
    @objc dynamic var sender_user_id: Int   = 0
    @objc dynamic var sender_name: String   = ""
    @objc dynamic var is_resend: Int        = 0
    @objc dynamic var is_has_link : Int = 0
    
    @objc dynamic var recipient_phone: String  = ""
    @objc dynamic var recipient_avatar: String = ""
    @objc dynamic var recipient_user_id: Int   = 0
    @objc dynamic var recipient_name: String   = ""
    //sticker
    @objc dynamic var is_sticker: Int       = 0
    @objc dynamic var sticker_id: Int       = 0
    @objc dynamic var sticker_name: String     = ""
    @objc dynamic var sticker_image: String    = ""
    //contacs
    @objc dynamic var contact_name: String      = ""
    @objc dynamic var contact_phone: String     = ""
    @objc dynamic var is_exist: Int          = 0
    @objc dynamic var contact_user_id: String   =   ""
    
    @objc dynamic var is_contact: Int           = 0
    @objc dynamic var is_has_file : Int         = 0
    @objc dynamic var answerFileUrl : String    = ""
    
    //lnk Content
    @objc dynamic var link_description : String    = ""
    @objc dynamic var link_title : String    = ""
    @objc dynamic var link_image : String    = ""
    
    
    var file_list = List<ChatFile>()
    
    // Create Text message
    
    static func initWith(dialog_id: String, chat_text: String, sender_user_id: Int, sender_name: String = "") {
        let realm = try! Realm()
        let item = DialogHistory()
        item.id = dialog_id
        item.chat_text = chat_text
        item.sender_user_id = sender_user_id
        item.sender_name = sender_name
        try! realm.write {
            realm.add(item)
        }
    }
    
    static func createFileMessage(dialog_id: String = "", chat_id: Int, files: List<ChatFile>, sender_user_id: Int) {
        let realm = try! Realm()
      
            let item = DialogHistory()
            item.id = dialog_id
            item.chat_id = chat_id
            item.sender_user_id = sender_user_id
            item.file_list = files
            try! realm.write {
                realm.add(item)
            }
        
    }
    
    // Synscronize and update from server
    
    static func initWith(json: JSON, dialog_id: String) {
        let realm = try! Realm()
        if let existingItem = getCurrentObject(chat_id: json["chat_id"].intValue) {
            // UPDATE
            try! realm.write {
                print("is updated",json)
                existingItem.action_name            = json["action_name"].stringValue
                existingItem.answer_chat_id         = json["answer_chat_id"].intValue
                existingItem.chat_text              = json["chat_text"].stringValue
                existingItem.answer_nickname    = json["answer_chat"]["nickname"].stringValue ?? json["answer_chat"]["user_name"].stringValue
                existingItem.answer_chatText        = json["answer_chat"]["chat_text"].stringValue
                existingItem.answer_is_contact        = json["answer_chat"]["is_contact"].intValue
                existingItem.chat_date              = json["chat_date"].stringValue
                existingItem.chat_time              = json["chat_time"].stringValue
                existingItem.is_read                =
                    json["is_read"].intValue
                existingItem.sender_avatar          = json["sender"]["avatar"].stringValue
                existingItem.sender_user_id         = json["sender"]["user_id"].intValue
                existingItem.view_count             = json["view_count"].intValue
                existingItem.recipient_phone        = json["recipient"]["phone"].stringValue
                existingItem.recipient_user_id      = json["recipient"]["user_id"].intValue
                existingItem.recipient_avatar       = json["recipient"]["avatar"].stringValue
                existingItem.is_contact             = json["is_contact"].intValue
                for (_, subJson):(String, JSON) in json["answer_chat"]["file_list"] {
                    existingItem.answerFileUrl      = subJson["file_url"].stringValue
                    existingItem.answerFileFormat   = subJson["file_format"].stringValue
                }
                existingItem.is_resend              = json["is_resend"].intValue
                existingItem.is_has_link            = json["is_has_link"].intValue
                existingItem.is_has_file            = json["answer_chat"]["is_has_file"].intValue
                existingItem.contact_name           = json["contact"]["contact_name"].stringValue
                existingItem.contact_phone          = json["contact"]["contact_phone"].stringValue
                existingItem.is_exist               = json["contact"]["is_exist"].intValue
                existingItem.contact_user_id        = json["contact"]["user_id"].stringValue
                
                existingItem.is_sticker           = json["is_sticker"].intValue
                existingItem.sticker_id           = json["sticker"]["sticker_id"].intValue
                existingItem.sticker_name           = json["sticker"]["sticker_name"].stringValue
                existingItem.sticker_image           = json["sticker"]["sticker_image"].stringValue
                
                existingItem.sender_name = json["sender"]["user_name"].string ?? json["sender"]["contact_user_name"].string ?? json["sender"]["phone"].stringValue
                existingItem.recipient_name = json["recipient"]["contact_user_name"].string ?? json["recipient"]["chat_name"].stringValue
                existingItem.link_title = json["meta_tags"]["title"].stringValue
                existingItem.link_description = json["meta_tags"]["description"].stringValue
                existingItem.link_image = json["meta_tags"]["image"].stringValue
            }
        } else {
            print("is created",json)
            // CREATE
            let item = DialogHistory()
            item.id             = dialog_id
            item.chat_id        = json["chat_id"].intValue
            item.chat_kind      = json["chat_kind"].stringValue
            item.chat_text      = json["chat_text"].stringValue
            item.answer_nickname    = json["answer_chat"]["nickname"].stringValue ?? json["answer_chat"]["user_name"].stringValue
            item.answer_chatText    = json["answer_chat"]["chat_text"].stringValue
            item.answer_is_contact        = json["answer_chat"]["is_contact"].intValue
            item.chat_date      = json["chat_date"].stringValue
            item.chat_time      = json["chat_time"].stringValue
            item.is_read        = json["is_read"].intValue
            item.action_name    = json["action_name"].stringValue
            item.answer_chat_id = json["answer_chat_id"].intValue
            item.view_count     = json["view_count"].intValue
            item.sender_phone   = json["sender"]["phone"].stringValue
            item.sender_avatar  = json["sender"]["avatar"].stringValue
            item.sender_user_id = json["sender"]["user_id"].intValue
            item.sender_name    = json["sender"]["user_name"].string ?? json["sender"]["contact_user_name"].string ?? json["sender"]["phone"].stringValue
            
            item.is_sticker           = json["is_sticker"].intValue
            item.sticker_id           = json["sticker"]["sticker_id"].intValue
            item.sticker_name           = json["sticker"]["sticker_name"].stringValue
            item.sticker_image           = json["sticker"]["sticker_image"].stringValue
            
            item.recipient_phone    = json["recipient"]["phone"].stringValue
            item.recipient_user_id  = json["recipient"]["user_id"].intValue
            item.recipient_avatar   = json["recipient"]["avatar"].stringValue
            item.recipient_name     = json["recipient"]["contact_user_name"].string ?? json["recipient"]["chat_name"].stringValue
            item.is_resend          = json["is_resend"].intValue
            item.is_has_link            = json["is_has_link"].intValue
            item.is_contact         = json["is_contact"].intValue
             for (_, subJson):(String, JSON) in json["answer_chat"]["file_list"] {
            item.answerFileUrl      = subJson["file_url"].stringValue
            item.answerFileFormat   = subJson["file_format"].stringValue
            }
            item.is_has_file        = json["answer_chat"]["is_has_file"].intValue
            item.contact_name       = json["contact"]["contact_name"].stringValue
            item.contact_phone      = json["contact"]["contact_phone"].stringValue
            item.contact_user_id    = json["contact"]["user_id"].stringValue
            item.is_exist           = json["contact"]["is_exist"].intValue
            item.link_title         = json["meta_tags"]["title"].stringValue
            item.link_description   = json["meta_tags"]["description"].stringValue
            item.link_image   = json["meta_tags"]["image"].stringValue
            for (_, subJson):(String, JSON) in json["file_list"] {
                let file = ChatFile()
                file.file_format = subJson["file_format"].stringValue
                file.file_url = subJson["file_url"].stringValue
                file.file_name = subJson["file_name"].stringValue
                file.file_time = subJson["file_time"].stringValue
                item.file_list.append(file)
            }
            try! realm.write {
                realm.add(item)
            }
        }
    }
    static func initWithSocket(json: JSON, dialog_id: String) {
        let realm = try! Realm()
            // CREATE
            let item = DialogHistory()
            item.id             = dialog_id
            item.chat_id        = json["chat_id"].intValue
            item.chat_kind      = json["chat_kind"].stringValue
            item.chat_text      = json["chat_text"].stringValue
            item.answer_nickname    = json["answer_chat"]["nickname"].stringValue ?? json["answer_chat"]["user_name"].stringValue
            item.answer_chatText    = json["answer_chat"]["chat_text"].stringValue
            item.answer_is_contact        = json["answer_chat"]["is_contact"].intValue
            item.chat_date      = json["chat_date"].stringValue
            item.chat_time      = json["chat_time"].stringValue
            item.is_read        = json["is_read"].intValue
            item.action_name    = json["action_name"].stringValue
            item.answer_chat_id = json["answer_chat_id"].intValue
            item.view_count     = json["view_count"].intValue
            item.sender_phone   = json["sender"]["phone"].stringValue
            item.sender_avatar  = json["sender"]["avatar"].stringValue
            item.sender_user_id = json["sender"]["user_id"].intValue
            item.sender_name    = json["sender"]["user_name"].string ?? json["sender"]["contact_user_name"].string ?? json["sender"]["phone"].stringValue
            item.is_sticker           = json["is_sticker"].intValue
            item.sticker_id           = json["sticker"]["sticker_id"].intValue
            item.sticker_name           = json["sticker"]["sticker_name"].stringValue
            item.sticker_image           = json["sticker"]["sticker_image"].stringValue
            
            item.recipient_phone    = json["recipient"]["phone"].stringValue
            item.recipient_user_id  = json["recipient"]["user_id"].intValue
            item.recipient_avatar   = json["recipient"]["avatar"].stringValue
            item.recipient_name     = json["recipient"]["contact_user_name"].string ?? json["recipient"]["chat_name"].stringValue
            item.is_resend          = json["is_resend"].intValue
            item.is_has_link            = json["is_has_link"].intValue
            item.is_contact         = json["is_contact"].intValue
            for (_, subJson):(String, JSON) in json["answer_chat"]["file_list"] {
            item.answerFileUrl      = subJson["file_url"].stringValue
            item.answerFileFormat   = subJson["file_format"].stringValue
            }
            item.is_has_file        = json["answer_chat"]["is_has_file"].intValue
            item.contact_name       = json["contact"]["contact_name"].stringValue
            item.contact_phone      = json["contact"]["contact_phone"].stringValue
            item.is_exist           = json["contact"]["is_exist"].intValue
            item.contact_user_id    = json["contact"]["user_id"].stringValue
            item.link_title = json["meta_tags"]["title"].stringValue
            item.link_description = json["meta_tags"]["description"].stringValue
            item.link_image = json["meta_tags"]["image"].stringValue
            for (_, subJson):(String, JSON) in json["file_list"] {
                let file = ChatFile()
                file.file_format = subJson["file_format"].stringValue
                file.file_url =  "https://api.dalagram.com" + subJson["file_url"].stringValue
                file.file_name = subJson["file_name"].stringValue
                file.file_time = subJson["file_time"].stringValue
                item.file_list.append(file)
            }
            try! realm.write {
                realm.add(item)
            }
    }
    static func initMakeIs_read(dialog_id: String) {
        let realm = try! Realm()
        let historyObjects = realm.objects(DialogHistory.self).filter(NSPredicate(format: "id = %@", dialog_id))
        for message in historyObjects {
            // UPDATE
            try! realm.write {
                message.is_read                =
                1
            }
        }
    }
    static func removeChatIds(dialog_id: String,messagesList:[Int] = []) {
        let realm = try! Realm()
            for chatId in messagesList {
                print("aaffssfa ",chatId)
                if  let existingItem = getCurrentObject(chat_id: chatId) {
            try! realm.write {
                realm.delete(existingItem)
                
            }
                }
        }
    }
    static func getCurrentObject(chat_id: Int) -> DialogHistory? {
        let realm = try! Realm()
        let obj = realm.objects(DialogHistory.self).filter("chat_id = \(chat_id)")
        return obj.first
    }
    
}


