//
//  Users.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 5/9/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import RealmSwift

struct Users {
    
    var action: String      = ""
    var user_id: Int        = 0
    //author
    var avatar: String          = ""
    var user_name: String       = ""
    var phone: String           = ""
    var last_visit: String      = ""
    var contact_user_name: String  = ""
    var universal_name: String  = ""
    var nickname: String        = ""
    var user_status: String     = ""
    var city: String            = ""
    //speciality
    var speciality_name: String      = ""
    var speciality_id: Int        = 0
    init(json: JSON) {
        self.action         = json["action"].stringValue
        self.user_id        = json["user_id"].intValue
        self.user_id = json["user_id"].intValue
        self.avatar = json["avatar"].stringValue
        self.user_name = json["user_name"].stringValue
        self.phone = json["phone"].stringValue
        self.last_visit = json["last_visit"].stringValue
        self.contact_user_name = json["contact_user_name"].stringValue
        self.universal_name = json["universal_name"].stringValue
        self.nickname = json["nickname"].stringValue
        self.user_status = json["user_status"].stringValue
        self.city = json["city"].stringValue
        let specialityJson = json["speciality"]
        self.speciality_id = specialityJson["speciality_id"].intValue
        self.speciality_name = specialityJson["speciality_name"].stringValue
        
    }
}

