//
//  UserSpeciality.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 11/16/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
struct UserSpeciality {
    //speciality
    var speciality_name: String      = ""
    var speciality_id: Int        = 0
    init(json: JSON) {
        self.speciality_id = json["speciality_id"].intValue
        self.speciality_name = json["speciality_name"].stringValue
        
    }
}

