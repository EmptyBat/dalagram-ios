//
//  Notification.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/11/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//
import Foundation
import UIKit
import SwiftyJSON
import RealmSwift

struct UserNotification {
    
    var action: String      = ""
    var action_id: Int      = 0
    var user_id: Int        = 0
    //author
    var avatar: String          = ""
    var user_name: String       = ""
    var phone: String           = ""
    var last_visit: String      = ""
    var contact_user_name: String  = ""
    var universal_name: String  = ""
    var nickname: String        = ""
    var publication_id: Int     = 0
    var comment_id: Int     = 0
    
    
    init(json: JSON) {
        self.action         = json["action"].stringValue
        self.user_id        = json["user_id"].intValue
        self.action_id        = json["action_id"].intValue
        let authorJson = json["author"]
        self.user_id = authorJson["user_id"].intValue
        self.avatar = authorJson["avatar"].stringValue
        self.user_name = authorJson["user_name"].stringValue
        self.phone = authorJson["phone"].stringValue
        self.last_visit = authorJson["last_visit"].stringValue
        self.contact_user_name = authorJson["contact_user_name"].stringValue
        self.universal_name = authorJson["universal_name"].stringValue
        self.nickname = authorJson["nickname"].stringValue
        let publicationJson = json["publication"]
        self.publication_id = publicationJson["publication_id"].intValue
        let commentJson = json["comment"]
        self.comment_id = commentJson["comment_id"].intValue
    }
}
