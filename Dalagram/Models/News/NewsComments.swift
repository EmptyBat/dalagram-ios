//
//  NewsComments.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/4/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import Foundation
import SwiftyJSON
struct NewsComments {
    
    var comment_id: Int = 0
    var is_liked: Int    = 0
    var parent_id: Int   = 0
    var parent_author_id: Int     = 0
    var comment_text: String   = ""
    var comment_date: String  = ""
    var like_count: Int = 0
    var is_has_file: Int = 0
    //author
    var user_id: Int            = 0
    var avatar: String          = ""
    var user_name: String       = ""
    var nickname: String        = ""
    
    var file_list = [ChatFile]()
    
    init(json: JSON) {
            self.comment_id         = json["comment_id"].intValue
            self.is_liked    = json["is_liked"].intValue
            self.parent_id       = json["parent_id"].intValue
            self.parent_author_id     = json["parent_author_id"].intValue
            self.comment_text     = json["comment_text"].stringValue
            self.comment_date = json["comment_date"].stringValue
            self.like_count     = json["like_count"].intValue
            self.is_has_file = json["is_has_file"].intValue
        
        let authorJson = json["author"]
        self.user_id = authorJson["user_id"].intValue
        self.avatar = authorJson["avatar"].stringValue
        self.user_name = authorJson["user_name"].stringValue
        self.nickname = authorJson["nickname"].stringValue
        
        for (_, subJson):(String, JSON) in json["file_list"] {
            let file = ChatFile()
            file.file_format = subJson["file_format"].stringValue
            file.file_url = subJson["file_url"].stringValue
            file.file_name = subJson["file_name"].stringValue
            file.file_time = subJson["file_time"].stringValue
            self.file_list.append(file)
        }
    }
}

