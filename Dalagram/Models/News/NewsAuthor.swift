//
//  NewsAuthor.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/28/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class JSONNewsAuthor {
    
    var user_id: Int            = 0
    var avatar: String          = ""
    var user_name: String       = ""
    var phone: String           = ""
    var last_visit: String      = ""
    var contact_user_name: String  = ""
    var universal_name: String  = ""
    var nickname: String        = ""
    init(json: JSON) {
        self.user_id = json["user_id"].intValue
        self.avatar = json["avatar"].stringValue
        self.user_name = json["user_name"].stringValue
        self.phone = json["phone"].stringValue
        self.last_visit = json["last_visit"].stringValue
        self.contact_user_name = json["contact_user_name"].stringValue
        self.universal_name = json["universal_name"].stringValue
        self.nickname = json["nickname"].stringValue
    }
    
}
class NewsAuthor: Object {
    
    @objc dynamic var user_id: Int            = 0
    @objc dynamic var avatar: String          = ""
    @objc dynamic var user_name: String       = ""
    @objc dynamic var phone: String           = ""
    @objc dynamic var last_visit: String      = ""
    @objc dynamic var contact_user_name: String  = ""
    @objc dynamic var universal_name: String  = ""
    @objc dynamic var nickname: String        = ""
}
