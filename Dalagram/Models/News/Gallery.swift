//
//  Gallery.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/13/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Gallery {
    var publication_id: Int = 0
    var access_id: Int    = 0
    var view_count: Int   = 0
    var comment_count: Int     = 0
    var publication_desc: String   = ""
    var publication_date: String  = ""
    var share_count: Int = 0
    var like_count: Int = 0
    
    var is_share: Int = 0
    var is_has_file: Int = 0
    
    var is_has_link: Int = 0
    var is_i_liked: Int = 0
    
    //author
    var user_id: Int            = 0
    var avatar: String          = ""
    var user_name: String       = ""
    var phone: String           = ""
    var last_visit: String      = ""
    var contact_user_name: String  = ""
    var universal_name: String  = ""
    var nickname: String        = ""
    
    var file_list = [ChatFile]()
    
    init(json: JSON) {
        self.publication_id         = json["publication_id"].intValue
        self.access_id    = json["access_id"].intValue
        self.view_count       = json["view_count"].intValue
        self.comment_count     = json["comment_count"].intValue
        self.publication_desc     = json["publication_desc"].stringValue
        self.publication_date = json["publication_date"].stringValue
        self.share_count     = json["share_count"].intValue
        self.like_count = json["like_count"].intValue
        self.is_share     = json["is_share"].intValue
        self.is_has_file = json["is_has_file"].intValue
        self.is_has_link     = json["is_has_link"].intValue
        self.is_i_liked = json["is_i_liked"].intValue
        
        let authorJson = json["author"]
        self.user_id = authorJson["user_id"].intValue
        self.avatar = authorJson["avatar"].stringValue
        self.user_name = authorJson["user_name"].stringValue
        self.phone = authorJson["phone"].stringValue
        self.last_visit = authorJson["last_visit"].stringValue
        self.contact_user_name = authorJson["contact_user_name"].stringValue
        self.universal_name = authorJson["universal_name"].stringValue
        self.nickname = authorJson["nickname"].stringValue
        
        for (_, subJson):(String, JSON) in json["file_list"] {
            let file = ChatFile()
            file.file_format = subJson["file_format"].stringValue
            file.file_url = subJson["file_url"].stringValue
            file.file_name = subJson["file_name"].stringValue
            file.file_time = subJson["file_time"].stringValue
            self.file_list.append(file)
        }
    }
}
