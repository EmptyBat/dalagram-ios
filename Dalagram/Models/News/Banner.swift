//
//  Banner.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 1/30/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
struct Banner {
    var banner_name: String = ""
    var banner_image: String = ""
    var website: String = ""
    
    init(json: JSON) {
        self.banner_name         = json["banner_name"].stringValue
        self.banner_image        = json["banner_image"].stringValue
        self.website             = json["website"].stringValue
        
    }
}
