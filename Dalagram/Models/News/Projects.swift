//
//  Projects.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 2/1/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
struct Projects {
    var project_name: String = ""
    var project_desc: String = ""
    var project_image: String = ""
    var website: String = ""
    
    init(json: JSON) {
        self.project_name         = json["project_name"].stringValue
        self.project_desc        = json["project_desc"].stringValue
        self.project_image        = json["project_image"].stringValue
        self.website             = json["website"].stringValue
        
    }
}
