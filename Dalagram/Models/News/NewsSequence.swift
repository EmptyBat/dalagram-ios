//
//  NewsSequence.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 4/2/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON
enum itemType {
    case contentHeaderSelf
    case headerAlien
    case gallery
    case info
    case header
    case text
    case image
    case audio
    case video
    case file
    case footer
    case link
    case multiple
    case commentText
    case commentImage
    case commentVideo
    case commentFile
    case commentAudio
    case showComments
    case shareDesk
    case shareImage
    case shareAudio
    case shareVideo
    case shareLink
    case shareMultipleImage
    case shareFile
    case banner
}

class NewsSequence: NSObject {
     var newsType: itemType = .header
     var index: Int = 0
     var imageHeight: Int = 0
     var imageWidth: Int = 0
     var fileIndex: Int = 0
     var multipleIndexs = [Int]()
}
