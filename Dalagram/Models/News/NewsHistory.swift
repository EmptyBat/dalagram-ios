//
//  NewsHistory.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 3/27/19.
//  Copyright © 2019 BuginGroup. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class NewsHistory: Object {
    
    @objc dynamic var publication_id: Int            = 0
    @objc dynamic var publication_desc: String       = ""
    @objc dynamic var publication_date: String       = ""
    @objc dynamic var view_count: Int                = 0
    @objc dynamic var comment_count: Int             = 0
    @objc dynamic var share_count: Int               = 0
    @objc dynamic var like_count: Int                = 0
    @objc dynamic var is_has_file: Int               = 0
    @objc dynamic var is_has_link: Int               = 0
    @objc dynamic var is_share: Int                  = 0
    @objc dynamic var is_i_liked: Int                = 0
    @objc dynamic var updatedDate: Date = Date()
    @objc dynamic var access_id: Int            = 0
    //author
    @objc dynamic var user_id: Int            = 0
    @objc dynamic var avatar: String          = ""
    @objc dynamic var user_name: String       = ""
    @objc dynamic var phone: String           = ""
    @objc dynamic var last_visit: String      = ""
    @objc dynamic var contact_user_name: String  = ""
    @objc dynamic var universal_name: String  = ""
    @objc dynamic var nickname: String        = ""
    
    //share_author
    @objc dynamic var share_user_id: Int            = 0
    @objc dynamic var share_avatar: String          = ""
    @objc dynamic var share_user_name: String       = ""
    @objc dynamic var share_phone: String           = ""
    @objc dynamic var share_last_visit: String      = ""
    @objc dynamic var share_contact_user_name: String  = ""
    @objc dynamic var share_universal_name: String  = ""
    @objc dynamic var share_nickname: String        = ""
    @objc dynamic var share_is_has_link: Int        = 0
    @objc dynamic var share_publication_id: Int     = 0
    //share_author meta tags
    @objc dynamic var share_meta_tagsDescription: String  = ""
    @objc dynamic var share_meta_tagsTitle: String  = ""
    @objc dynamic var share_meta_tagsImage: String        = ""
    
    
    //share publicat
    @objc dynamic var share_publication_desc: String       = ""
    @objc dynamic var share_publication_date: String       = ""
    
    //meta tags
    @objc dynamic var meta_tagsDescription: String  = ""
    @objc dynamic var meta_tagsTitle: String  = ""
    @objc dynamic var meta_tagsImage: String        = ""
    
    //comment
    @objc dynamic var comment_id: Int       = 0
    @objc dynamic var comment_text: String  = ""
    @objc dynamic var comment_date: String  = ""
    @objc dynamic var comment_like_count: Int     = 0
    @objc dynamic var comment_is_liked: Int       = 0
    @objc dynamic var comment_is_has_file: Int    = 0
    
    @objc dynamic var comment_user_id: Int      = 0
    @objc dynamic var comment_avatar: String    = ""
    @objc dynamic var comment_nickname: String  = ""
    
    var file_list = List<ChatFile>()
    
    var comment_file_list = List<ChatFile>()
    
    static func initWith(json: JSON, publication_id: Int) {
        let realm = try! Realm()
        if let existingItem = getCurrentObject(publication_id: json["publication_id"].intValue) {
            // UPDATE
            try! realm.write {
                print("is updated",json)
                existingItem.updatedDate = Date()
                existingItem.publication_id     = json["publication_id"].intValue
                existingItem.publication_desc   = json["publication_desc"].stringValue
                existingItem.publication_date   = json["publication_date"].stringValue
                existingItem.view_count         = json["view_count"].intValue
                existingItem.comment_count      = json["comment_count"].intValue
                existingItem.share_count        = json["share_count"].intValue
                existingItem.like_count         = json["like_count"].intValue
                existingItem.is_has_file        = json["is_has_file"].intValue
                existingItem.is_has_link        = json["is_has_link"].intValue
                existingItem.is_share           = json["is_share"].intValue
                existingItem.is_i_liked         = json["is_i_liked"].intValue
                existingItem.access_id         = json["access_id"].intValue
                
                let authorJson = json["author"]
                existingItem.user_id = authorJson["user_id"].intValue
                existingItem.avatar = authorJson["avatar"].stringValue
                existingItem.user_name = authorJson["user_name"].stringValue
                existingItem.phone = authorJson["phone"].stringValue
                existingItem.last_visit = authorJson["last_visit"].stringValue
                existingItem.contact_user_name = authorJson["contact_user_name"].stringValue
                existingItem.universal_name = authorJson["universal_name"].stringValue
                existingItem.nickname = authorJson["nickname"].stringValue
                
                
                
                if (json["is_has_link"].intValue == 1){
                let meta_tagsJson = json["meta_tags"]
                existingItem.meta_tagsDescription = meta_tagsJson["description"].stringValue
                existingItem.meta_tagsTitle = meta_tagsJson["title"].stringValue
                existingItem.meta_tagsImage = meta_tagsJson["image"].stringValue
                }
                var share_publicationJson = json
                if ( existingItem.is_share == 1){
                    share_publicationJson = json["share_publication"]
                    let authorJson = share_publicationJson["author"]
                    existingItem.share_user_id = authorJson["user_id"].intValue
                    existingItem.share_avatar = authorJson["avatar"].stringValue
                    existingItem.share_user_name = authorJson["user_name"].stringValue
                    existingItem.share_phone = authorJson["phone"].stringValue
                    existingItem.share_last_visit = authorJson["last_visit"].stringValue
                    existingItem.share_contact_user_name = authorJson["contact_user_name"].stringValue
                    existingItem.share_universal_name = authorJson["universal_name"].stringValue
                    existingItem.share_nickname = authorJson["nickname"].stringValue
                    
                    existingItem.share_publication_desc   = share_publicationJson["publication_desc"].stringValue
                    existingItem.share_publication_date   = share_publicationJson["publication_date"].stringValue
                    existingItem.share_is_has_link        =
                        share_publicationJson["is_has_link"].intValue
                    
                    existingItem.share_publication_id        =
                        share_publicationJson["publication_id"].intValue
                    
                    if (share_publicationJson["is_has_link"].intValue == 1){
                        let meta_tagsJson = share_publicationJson["meta_tags"]
                        existingItem.meta_tagsDescription = meta_tagsJson["description"].stringValue
                        existingItem.meta_tagsTitle = meta_tagsJson["title"].stringValue
                        existingItem.meta_tagsImage = meta_tagsJson["image"].stringValue
                    }
                }
                for (_, subJson):(String, JSON) in share_publicationJson["file_list"] {
                    if let i = existingItem.file_list.index(where: { $0.file_name == subJson["file_name"].stringValue}) {
                    }
                    else{
                    let file = ChatFile()
                    file.file_format = subJson["file_format"].stringValue
                    file.file_url = subJson["file_url"].stringValue
                    file.file_name = subJson["file_name"].stringValue
                    file.file_time = subJson["file_time"].stringValue
                    existingItem.file_list.append(file)
                    }
                }
                let commentJson = json["comment"]
                existingItem.comment_id         = commentJson["comment_id"].intValue
                existingItem.comment_text      = commentJson["comment_text"].stringValue
                existingItem.comment_date        = commentJson["comment_date"].stringValue
                existingItem.comment_like_count         = commentJson["like_count"].intValue
                existingItem.comment_is_liked        = commentJson["is_liked"].intValue
                existingItem.comment_is_has_file        = commentJson["is_has_file"].intValue
               // comment author
                let commentAuthorJson = commentJson["author"]
                existingItem.comment_user_id = commentAuthorJson["user_id"].intValue
                existingItem.comment_avatar = commentAuthorJson["avatar"].stringValue
                existingItem.comment_nickname = commentAuthorJson["nickname"].stringValue
            }
        } else {
            print("is created",json)
            // CREATE
            let item = NewsHistory()
            item.publication_id     = json["publication_id"].intValue
            item.publication_desc   = json["publication_desc"].stringValue
            item.publication_date   = json["publication_date"].stringValue
            item.view_count         = json["view_count"].intValue
            item.comment_count      = json["comment_count"].intValue
            item.share_count        = json["share_count"].intValue
            item.like_count         = json["like_count"].intValue
            item.is_has_file        = json["is_has_file"].intValue
            item.is_has_link        = json["is_has_link"].intValue
            item.is_i_liked         = json["is_i_liked"].intValue
            item.access_id          = json["access_id"].intValue

            let authorJson = json["author"]
            item.share_user_id = authorJson["user_id"].intValue
            item.share_avatar = authorJson["avatar"].stringValue
            item.share_user_name = authorJson["user_name"].stringValue
            item.share_phone = authorJson["phone"].stringValue
            item.share_last_visit = authorJson["last_visit"].stringValue
            item.share_contact_user_name = authorJson["contact_user_name"].stringValue
            item.share_universal_name = authorJson["universal_name"].stringValue
            item.share_nickname = authorJson["nickname"].stringValue

            if (json["is_has_link"].intValue == 1){
                let meta_tagsJson = json["meta_tags"]
            item.meta_tagsDescription = meta_tagsJson["description"].stringValue
            item.meta_tagsTitle = meta_tagsJson["title"].stringValue
            item.meta_tagsImage = meta_tagsJson["image"].stringValue
            }
            var share_publicationJson = json
            if ( item.is_share == 1){
                share_publicationJson = json["share_publication"]
                let authorJson = share_publicationJson["author"]
                item.user_id = authorJson["user_id"].intValue
                item.avatar = authorJson["avatar"].stringValue
                item.user_name = authorJson["user_name"].stringValue
                item.phone = authorJson["phone"].stringValue
                item.last_visit = authorJson["last_visit"].stringValue
                item.contact_user_name = authorJson["contact_user_name"].stringValue
                item.universal_name = authorJson["universal_name"].stringValue
                item.nickname = authorJson["nickname"].stringValue
                
                item.share_publication_desc   = share_publicationJson["publication_desc"].stringValue
                item.share_publication_date   = share_publicationJson["publication_date"].stringValue
                
                item.share_publication_id        =
                    share_publicationJson["publication_id"].intValue
                item.share_is_has_link        = share_publicationJson["is_has_link"].intValue
                if (share_publicationJson["is_has_link"].intValue == 1){
                    let meta_tagsJson = share_publicationJson["meta_tags"]
                    item.meta_tagsDescription = meta_tagsJson["description"].stringValue
                    item.meta_tagsTitle = meta_tagsJson["title"].stringValue
                    item.meta_tagsImage = meta_tagsJson["image"].stringValue
                }
            }
            for (_, subJson):(String, JSON) in share_publicationJson["file_list"] {
                let file = ChatFile()
                file.file_format = subJson["file_format"].stringValue
                file.file_url = subJson["file_url"].stringValue
                file.file_name = subJson["file_name"].stringValue
                file.file_time = subJson["file_time"].stringValue
                item.file_list.append(file)
            }
            let commentJson = json["comment"]
            item.comment_id         = commentJson["comment_id"].intValue
            item.comment_text      = commentJson["comment_text"].stringValue
            item.comment_date        = commentJson["comment_date"].stringValue
            item.comment_like_count         = commentJson["like_count"].intValue
            item.comment_is_liked        = commentJson["is_liked"].intValue
            item.comment_is_has_file        = commentJson["is_has_file"].intValue
            // comment author
            let commentAuthorJson = commentJson["author"]
            item.comment_user_id = commentAuthorJson["user_id"].intValue
            item.comment_avatar = commentAuthorJson["avatar"].stringValue
            item.comment_nickname = commentAuthorJson["nickname"].stringValue
            for (_, subJson):(String, JSON) in commentJson["file_list"] {
                let file = ChatFile()
                file.file_format = subJson["file_format"].stringValue
                file.file_url = subJson["file_url"].stringValue
                file.file_name = subJson["file_name"].stringValue
                file.file_time = subJson["file_time"].stringValue
                item.comment_file_list.append(file)
            }
            try! realm.write {
                realm.add(item)
            }
        }
    }
    static func getCurrentObject(publication_id: Int) -> NewsHistory? {
        let realm = try! Realm()
        let obj = realm.objects(NewsHistory.self).filter("publication_id = \(publication_id)")
        return obj.first
    }
}
