//
//  City.swift
//  Dalagram
//
//  Created by Almas Abdrasilov on 2/2/20.
//  Copyright © 2020 BuginGroup. All rights reserved.
//

import UIKit
import SwiftyJSON
struct City {
    var city_name: String = ""
    var city_id: Int = 0
    
    init(json: JSON) {
        self.city_id         = json["city_id"].intValue
        self.city_name        = json["city_name"].stringValue
        
    }
}
