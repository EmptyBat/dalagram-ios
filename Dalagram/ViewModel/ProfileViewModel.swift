//
//  ProfileViewModel.swift
//  Dalagram
//
//  Created by Toremurat on 07.06.18.
//  Copyright © 2018 BuginGroup. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxSwift

struct ProfileViewModel {
    var user_id    = Variable<Int>(0)
    var name    = Variable<String>("")
    var background   = Variable<String>("")
    var phone   = Variable<String>("")
    var avatar  = Variable<String>("")
    var status  = Variable<String>("")
    var email   = Variable<String>("")
    
    var birth_date   = Variable<String>("")
    var city  = Variable<String>("")
    var city_id  = Variable<Int>(0)
    var work  = Variable<String>("")
    var language   = Variable<String>("")
    var speciality_id   = Variable<Int>(0)
    var university  = Variable<String>("")
    var family  = Variable<String>("")
    
    var last_visit   = Variable<String>("")
    var nickname   = Variable<String>("")
    var photo_count   = Variable<Int>(0)
    var publication_count  = Variable<Int>(0)
    var friend_count  = Variable<Int>(0)
    var follower_count   = Variable<Int>(0)
    var is_friend  = Variable<Int>(0)
    var is_i_subscribe   = Variable<Int>(0)
    var is_he_subscribe   = Variable<Int>(0)
    
    var confirm_password: String = ""
    var password: String = ""
    
    var isNeedToUpdate = Variable<Bool>(false)
    
    func getProfile(onCompletion: @escaping () -> Void) {
        NetworkManager.makeRequest(.getProfile(user_id: nil), success: { (json) in
            let data = json["data"]
            print("afsfsfsaa",data)
            print(json)
            User.initWith(json: data)
            self.user_id.value     = data["user_id"].intValue
            self.background.value     = data["background"].stringValue
            self.name.value     = data["user_name"].stringValue
            self.avatar.value   = data["avatar"].stringValue
            self.phone.value    = data["phone"].stringValue
            self.last_visit.value    = data["last_visit"].stringValue
            self.status.value   = data["user_status"].stringValue
            self.email.value    = data["email"].stringValue
            self.nickname.value    = data["nickname"].stringValue
            self.photo_count.value     = data["photo_count"].intValue
            self.publication_count.value   = data["publication_count"].intValue
            self.friend_count.value    = data["friend_count"].intValue
            self.follower_count.value   = data["follower_count"].intValue
            self.is_friend.value    = data["is_friend"].intValue
            self.is_i_subscribe.value    = data["is_i_subscribe"].intValue
            self.is_he_subscribe.value    = data["is_he_subscribe"].intValue
            
            self.birth_date.value   = data["birth_date"].stringValue
            let cityInfo = data["city_info"]
            self.city.value    = cityInfo["city_name"].stringValue
            self.city_id.value    = cityInfo["city_id"].intValue
            self.work.value    = data["work"].stringValue
            self.language.value    = data["language"].stringValue
            self.city.value    = cityInfo["city_name"].stringValue
            self.city_id.value    = cityInfo["city_id"].intValue
            let specialityInfo = data["speciality"]
            self.speciality_id.value    = specialityInfo["speciality_id"].intValue
            self.university.value    = data["university"].stringValue
            self.family.value    = data["family"].stringValue
            
            onCompletion()
        })
    }
    func getAlienProfile(user_id:Int,onCompletion: @escaping () -> Void) {
        NetworkManager.makeRequest(.getProfile(user_id: user_id), success: { (json) in
            let data = json["data"]
            print("assafsfaas",json)
            User.initWith(json: data)
            self.user_id.value     = data["user_id"].intValue
            self.background.value     = data["background"].stringValue
            self.name.value     = data["user_name"].stringValue
            self.avatar.value   = data["avatar"].stringValue
            self.phone.value    = data["phone"].stringValue
            self.last_visit.value    = data["last_visit"].stringValue
            self.status.value   = data["user_status"].stringValue
            self.email.value    = data["email"].stringValue
            self.nickname.value    = data["nickname"].stringValue
            self.photo_count.value     = data["photo_count"].intValue
            self.publication_count.value   = data["publication_count"].intValue
            self.friend_count.value    = data["friend_count"].intValue
            self.follower_count.value   = data["follower_count"].intValue
            self.is_friend.value    = data["is_friend"].intValue
            self.is_i_subscribe.value    = data["is_i_subscribe"].intValue
            self.is_he_subscribe.value    = data["is_he_subscribe"].intValue
            
            self.birth_date.value   = data["birth_date"].stringValue
            let cityInfo = data["city_info"]
            self.city.value    = cityInfo["city_name"].stringValue
            self.city_id.value    = cityInfo["city_id"].intValue
            self.work.value    = data["work"].stringValue
            self.language.value    = data["language"].stringValue
            let specialityInfo = data["speciality"]
            self.speciality_id.value    = specialityInfo["speciality_id"].intValue
            self.university.value    = data["university"].stringValue
            self.family.value    = data["family"].stringValue
            //self.password       = data["password"].stringValue
            onCompletion()
        })
    }
    func editProfile(_ onCompletion: @escaping () -> Void) {
        let params = ["user_status" : status.value,
                      "user_name"   : name.value,
                      "email"       : email.value,
                      "phone"       : phone.value,
                      "birth_date"  : birth_date.value,
                      "city_id"     : "\(city_id.value)",
                      "work"        : work.value,
                      "language"    : language.value,
                      "university"  : university.value,
                      "family"      : family.value]
        print("params ", params)
        NetworkManager.makeRequest(.editProfile(params), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            onCompletion()
        })
    }
    func editPassword(_ onCompletion: @escaping () -> Void) {
        let params = ["password" : password,
                      "confirm_password"   : confirm_password]
        
        NetworkManager.makeRequest(.editProfile(params), success: { (json) in
            WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
            onCompletion()
        })
    }
    func uploadAvatar(image: UIImage) {
        if let data = UIImageJPEGRepresentation(image, 0.5) {
            NetworkManager.makeRequest(.uploadAvatar(data), success: { (json) in
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                self.avatar.value = json["image_url"].stringValue
            })
        }
    }
    func uploadBackground(image: UIImage) {
        if let data = UIImageJPEGRepresentation(image, 0.5) {
            NetworkManager.makeRequest(.uploadBackground(data), success: { (json) in
                WhisperHelper.showSuccessMurmur(title: json["message"].stringValue)
                self.background.value = json["image_url"].stringValue
            })
        }
    }
}
